<?php

return [


    // Static global variables
    'testimonials' => [
    [
            "name"          => 'Melina',
            "text"          => 'Bonjour nous sommes ravis de votre accueil chaleureux séjour très bien organisé par Morena je suis venue une première fois
                    pour implants mammaires et j\'ai été très contente du travail en 2018 et je suis retournée en fin 2018 pour
                    mettre des implants mammaires plus gros taille 5 \'95 plus une liposuccion et le résultat et véritablement
                    incroyable merci encore.',
            "operation"     => 'Implants mammaires et la liposuccion',
            "pays"          => '',
            "date"          => 'Février'
        ],
        [
            "name"          => 'Myriam',
            "text"          => 'Voilà maintenant 2 ans que je prospectais pour 3 interventions de chirurgie esthétique. Habitant en France, j’ai consulté
                    de nombreux sites français et pour autant je n’avais pas encore arrêté mon choix. Une amie tunisienne m’a
                    fait part d’une clinique en Tunisie et d’un médecin : le docteur Djemal (elle venait de subir une augmentation
                    mammaire). J’ai alors approfondie cette voie tunisienne. <br> J’ai découvert le docteur Djemal
                    sur de nombreux sites mais quoi de plus probant que de voir le résultat sur ma copine qui avait subi une
                    augmentation mammaire très réussie : c’est ce qui m’a décidé à arrêter mon choix vers la clinique Myron et
                    le docteur Djemal. <br> Le contact et la prise de rendez-vous avec la clinique : tout est parfait. L’arrivée
                    en Tunisie et ma prise en charge dès l’aéroport : encore parfait. Consultation avec le chirurgien le Dr Djemal
                    : il vous écoute, il vous conseille, pas d’arrière-pensée financière, on se sent de suite en confiance et
                    rassurée d’être avec un professionnel. 3 opérations en une seule fois (augmentation mammaire, liposuccion,
                    lifting mannequin) avec un résultat qui a dépassé toutes mes espérances : BRAVO. <br> Je tenais à faire ce
                    témoignage car vraiment je suis revenue en France RAVIE. Merci à toute l’équipe de la clinique qui sont toujours
                    à votre écoute et qui sont tous très chaleureux, un merci particulier au docteur Djemal et un grand merci
                    à Inès son assistante pour sa disponibilité son écoute et son réconfort. Encore Merci à tous. Pour ceux qui
                    comme moi avaient des appréhensions, Je vous conseille cette clinique et le docteur Djemal.',
            "operation"     => 'Augmentation mammaire, Liposuccion, Lifting mannequin',
            "pays"          => 'France',
            "date"          => ''
        ],
        [
            "name"          => 'Samia',
            "text"          => 'Bonjour Personnellement ma sœur et moi sommes très satisfaite de notre opération . Je tiens à remercier l’infirmière qui
                    c’est occupé de moi, Inès pour son accueil, et son suivi et notre magicien le docteur Djemal . Cet intervention
                    a changé mon quotidien dans ma vie de femme et je ne regrette rien . À bientôt',
            "operation"     => '',
            "pays"          => '',
            "date"          => 'Février'
        ],
        [
            "name"          => 'Yamina',
            "text"          => 'Résultat au-delà de mes attentes. Le DR Djemal a des mains en or. Merci de m\'avoir redonné goût à la mode. Je lui reconfierais
                    mon corps les yeux fermés. Merci à Morena et à Ines pour le suivi, l\'organisation et pour le soutien moral
                    qui est plus que nécessaire lorsque nous effectuons cette opération. Cette liposuccion complété a changer
                    ma vie d\'épouse, de femme, le regard des autres sur moi. Mais le plus important mon regarde sur moi Cette
                    lipo ma redonné confiance en moi, ma redonne l\'envie de m\'aimer. Sans hésiter je referai la liposuccion complété.
                    Merci à toute l\'équipe.',
            "operation"     => 'Liposuccion',
            "pays"          => '',
            "date"          => ''
        ],
        [
            "name"          => 'Laetitia',
            "text"          => 'Le 19 décembre 2017, j ai vécu une aventure extraordinaire, j\'ai fait une augmentation mammaire, cela a complètement changé
                    ma vie, je suis tellement heureuse d\'avoir franchi le pas. Bien sûr ça reste une opération mais on est vraiment
                    entre de très bonnes mains. Il n y a rien a dire sur la prise en charge que ce soit avant l opération on
                    est très bien conseillée, on répond à toutes nos interrogations... une fois sur place là aussi rien à dire
                    Nabil notre chauffeur nous attend à notre arrivée à l aéroport et le jour de l opération toute l équipe est
                    super Ines est vraiment adorable et notre Morena est une vraie maman avec nous. Pendant le séjour à la clinique
                    toute l équipe est juste formidable toujours au petit soin avec nous.... et bien sûr le docteur Djemal le
                    meilleur des chirurgiens, il a fait un travail remarquable... je le recommande à 100%, je ne pourrais jamais
                    lui dire assez merci....séjour à l hôtel au top.
                    <br/> Et même après l opération on a toujours un suivi.
                    <br/> Alors à toutes celles qui hésitent à franchir le pas, je n ai qu un mot à vous dire " Foncez " vraiment
                    vous ne serez pas déçue, au bout d\'un mois d opération plus aucune douleur, je n ai eu aucun ecchymose, et
                    je me sens tellement mieux dans mon corps, vraiment je revis car pour moi c\'était un réel complexe.
                    <br/> Vraiment encore un immense merci au Dr Djemal et à toute son équipe.',
            "operation"     => '',
            "pays"          => 'France',
            "date"          => ''
        ],
        [
            "name"          => 'ANICK',
            "text"          => 'Bonjour, moi et ma copine avons décidé un jour de partir en Tunisie pour subir une augmentation mammaire avec le Dr Djemal,
                    pour ma part on m\'a conseillé un lifting avec augmentation avec prothèses. Par la suite j\'ai décidé d\'ajouter
                    une lipo complète a ca....
                    <br/> J\'ai eu des discussions régulières avec Morena dès que j\'avais des questions et des appréhensions.
                    La clinique est superbe, le personnel est super, je m\'en suis même faites des amies ,nous avons été prises
                    en charge dès notre arrivée par Nabil (le chauffeur) qui est très gentil et par Morena dès notre arrivée
                    à la clinique. Nous avons toujours réussi à avoir un service en francais, tout était parfait!!! Même à mon
                    retour au Québec le personnel de l\'hôpital qui a retiré mes points ici n\'en revenait pas comment c\'était
                    bien fait!!
                    <br/> Un gros merci à tout l\'équipe xx
                    <br/> Ps : même de retour au Qc le dr Djemal a continué de répondre à mes questions lorsque j\'en avais donc
                    suivi après excellent!',
            "operation"     => 'Augmentation mammaire',
            "pays"          => 'Canada',
            "date"          => ''
        ],
        [
            "name"          => 'Karine',
            "text"          => 'Bonjour je m\'appelle Karine j\'ai 35 ans Je suis allée à la clinique myron en mai 2017 pour une augmentation mammaire, et
                    je peux vous dire que le personnel, la clinique, le chirurgien et Morena, la personne qui m\'a prise en charge,
                    sont adorables et professionnels. Ils sont à nos petits soins. Et ainsi qu\'Inès qui était là à notre écoute...
                    <br/> Je recommande vivement la clinique Myron.',
            "operation"     => 'Augmentation mammaire',
            "pays"          => 'France',
            "date"          => ''
        ],
        [
            "name"          => 'Monique',
            "text"          => 'Je souhaitais vous envoyer un petit message afin de vous remercier de ce que vous avez fait pendant mon séjour, je sais vous
                allez me dire que c\'est votre travail, que c\'est normal, mais non, ce n\'est pas le cas partout, on est pas
                toujours aussi bien traité dans d\'autres cliniques, encore merci pour tout, comme dit à Inès, je vais faire
                bonne presse car vous le méritez, je sais que tout n\'est pas facile au quotidien, qu\'il n\'y a pas que des
                clients ou patients faciles et sympathiques !;-)
                <br/> En tant que partient, j\'aurai une requête à vous faire;-) , montrez le message suivant à toutes les
                filles du 5e étage qui ont travaillé durant mon court passage en clinique : (3au5 janvier 2018)
                <br/> J\'aimerai également dire un GRAND MERCI à toutes les personnes à qui on ne dit que trop rarement des
                compliments je parle des femmes de ménage aux infirmières, aux assistantes, à toutes ces filles qui font
                un travail difficile, elles méritent qu\'on leur fasse des compliments, car elles ont été très gentilles,
                très sympathiques et très compétentes, je n\'ai que des compliments à leur faire ! MERCI pour TOUT je sais
                que vous faites votre job, mais je sais ô combien celui-ci est difficile !
                <br/> CHOKRAN !!!!!
                <br/> Un petit clin d\'oeil également à Nabil, comme taxi, c\'est la classe ! Merci pour ta gentillesse, ta
                disponibilité et ton sourire ! Longue vie à toi et ta famille ;-)
                <br/> Merci à toutes et à tous, merci aussi au Dr Djemal bien sûr pour son travail !',
            "operation"     => '',
            "pays"          => 'France',
            "date"          => ''
        ],
        [
            "name"          => 'Samia L.',
            "text"          => 'Je pesais 84kg avant ma lipo et aujourd\'hui j\'en suis à 73kg ça m\'a changé la vie et je suis super motivée, je fais des séances
                de drainage lymphatique de la marche et de l\'aquabike. Je ne vous remercierai jamais assez vous et le faiseur
                de miracles le merveilleux Dr. DJEMAL TAHER.',
            "operation"     => '',
            "pays"          => 'Suisse',
            "date"          => ''
        ],
        [
            "name"          => 'Annie',
            "text"          => 'J\'ai été opérée en octobre dernier pour une rhinoplastie. Dr Djemal m\'a été recommandé par une amie qui avait fait une liposuccion
                et injecter la graisse aux fesses. Elle était tellement heureuse de sa nouvelle silhouette, que j\'ai décidé
                d\'envoyer un mail pour avoir des informations. Morena a répondu à toutes mes questions avec beaucoup de patience,
                et j\'en ai posé !!! Dr Djemal a su immédiatement me rassurer avec sa gentillesse et douceur. L\'intervention
                n\'était pas douloureuse du tout, mais beaucoup de bleus et gonflée au max. les infirmières et personnel de
                la clinique étaient toujours présents et aux petits soins. Ines et morena, souriantes, etaient toujours présentes
                pour nous rassurer et tout organiser … la clinique est moderne, propre et confortable. L\'hotel ok , sans
                plus. MERCI A TOUTE l\'EQUIPE, mon nez est super',
            "operation"     => 'Lipossuccion abdominoplastie',
            "pays"          => 'France',
            "date"          => ''
        ],
        [
            "name"          => 'Nesrine',
            "text"          => 'I am so happy I feel well after this Operation. I thank my Doctor Djmal so much and the whole Team. ça va super bien !!!
                Oui ça va me faire plaisir écrire un petit message car mon séjour à été au delà de mes attentes !!! Merci
                beaucoup pour votre accueil et votre support tout au long de notre séjour !!!',
            "operation"     => '',
            "pays"          => 'Dubai',
            "date"          => ''
        ],
//        [
//            "name"          => '',
//            "text"          => '',
//            "operation"     => '',
//            "pays"          => '',
//            "date"          => ''
//        ],

    ]

];
