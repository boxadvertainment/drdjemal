<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    // Title of the site
    'name' => 'Dr Djemal - Chirurgie esthétique en Tunisie',

    // This address is used for admin purposes, like notification from contact form
    'email' => 'contact@dr-djemal.com',

    'qualifications' => [
        'Actif',
        'Non Actif',
    ],

    'recaptcha' => [
        'sitekey' => env('RECAPTCHA_SITEKEY'),
        'secret' => env('RECAPTCHA_SECRET'),
    ],

    // Static global variables
    'interventions' => [
        'Chirurgie De La Silhouette' => [
            "Lifting Des Cuisses" => 'Lifting Des Cuisses',
            "Lipoaspiration Des Cuisses" => 'Lipoaspiration Des Cuisses',
            "Lipoaspiration Des Fesses" => 'Lipoaspiration Des Fesses',
//            "Prothèses Des Fesses" => 'Prothèses Des Fesses',
            "Lipofilling Des Fesses" => 'Lipofilling Des Fesses',
            "Implants Mollets" => 'Implants Mollets',
            "Plastie Abdominale" => 'Plastie Abdominale',
            "Liposuccion" => 'Liposuccion',
            "Chirurgie Des Bras" => 'Chirurgie Des Bras',
            "Bodylift" => 'Bodylift',
            "Chirurgie Intime" => 'Chirurgie Intime',
        ],
        'Medecine Esthetique' => [
            "Botox" => 'Botox',
//            "Acide Hyaluronique" => 'Acide Hyaluronique',
        ],
        'Chirurgie des Seins' =>  [
            "Lifting Des Seins" => 'Lifting Des Seins',
            "Augmentation Mammaire" => 'Augmentation Mammaire',
            "Réduction Mammaire" => 'Réduction Mammaire',
            "Gynécomastie" => 'Gynécomastie',
//            "Augmentation Pectoraux" => 'Augmentation Pectoraux',
        ],
        'Chirurgie du Visage' =>  [
            "Blepharoplastie" => 'Blepharoplastie',
            "Otoplastie" => 'Otoplastie',
            "Rhinoplastie" => 'Rhinoplastie',
            "Mentoplastie" => 'Mentoplastie',
            "Profiloplastie" => 'Profiloplastie',
//            "Chirurgie De La Calvitie" => 'Chirurgie De La Calvitie',
//            "Lifting Cervico-Facial" => 'Lifting Cervico-Facial',
            "Lifting Complet" => 'Lifting Complet',
            "Lipofilling" => 'Lipofilling',
            "Chirurgie Des Levres" => 'Chirurgie Des Levres',
            "Liposuccion Du Cou" => 'Liposuccion Du Cou',
        ],
        'Chirurgie Reparatrice et Reconstructrice' =>  [
            "Maladies Destructrices" => 'Maladies Destructrices',
            "Malformations Congénitales" => 'Malformations Congénitales',
            "Traumatismes Accidentels" => 'Traumatismes Accidentels',
            "Cancer Du Sein" => 'Cancer Du Sein',
        ]
    ],

    'SURGERIES' => [
        'BODY SURGERY' => [
            "THIGH LIFT" => 'THIGH LIFT',
            "THIGH LIPOSUCTION" => 'THIGH LIPOSUCTION',
            "BUTTOCKS LIPOSUCTION" => 'BUTTOCKS LIPOSUCTION',
            "BUTTOCKS IMPLANTS" => 'BUTTOCKS IMPLANTS',
            "BUTTOCKS LIPOFILLING" => 'BUTTOCKS LIPOFILLING',
            "CALF IMPLANTS" => 'CALF IMPLANTS',
            "TUMMY TUCK" => 'TUMMY TUCK',
            "LIPOSUCTION" => 'LIPOSUCTION',
            "UPPER ARM SURGERY" => 'UPPER ARM SURGERY',
            "BODYLIFT" => 'BODYLIFT',
            "INTIMATE SURGERY" => 'INTIMATE SURGERY',
        ],
        'AESTHETIC TREATMENTS' => [
            "BOTOX INJECTIONS" => 'BOTOX INJECTIONS',
            "HYALURONIC INJECTIONS" => 'HYALURONIC INJECTIONS',
        ],
        'BREAST SURGERY' =>  [
            "BREAST UPLIFT" => 'BREAST UPLIFT',
            "BREAST AUGMENTATION" => 'BREAST AUGMENTATION',
            "BREAST REDUCTION" => 'BREAST REDUCTION',
            "MALE BREAST REDUCTION/GYNAECOMASTIA" => 'MALE BREAST REDUCTION/GYNAECOMASTIA',
            "MALE BREAST IMPLANT" => 'MALE BREAST IMPLANT',
        ],
        'FACE SURGERY' =>  [
            "EYELID SURGERY" => 'EYELID SURGERY',
            "EAR SURGERY" => 'EAR SURGERY',
            "RHINOPLASTY" => 'RHINOPLASTY',
            "CHIN SURGERY" => 'CHIN SURGERY',
            "PROFILE SURGERY" => 'PROFILE SURGERY',
            "HAIR TRANSPLANT" => 'HAIR TRANSPLANT',
            "NECKLIFT/MINI LIFT" => 'NECKLIFT/MINI LIFT',
            "FACELIFT" => 'FACELIFT',
            "FAT INJECTIONS" => 'FAT INJECTIONS',
            "FAT INJECTIONS" => 'FAT INJECTIONS',
            "NECK LIPOSUCTION" => 'NECK LIPOSUCTION',
        ],
        'RECONSTRUCTIVE SURGERY' =>  [
            "RECONSTRUCTIVE SURGERY" => 'RECONSTRUCTIVE SURGERY'
        ]
    ],

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'fr',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'fr',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

//        PragmaRX\Firewall\Vendor\Laravel\ServiceProvider::class,
//        Torann\GeoIP\GeoIPServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

//        'Firewall' => PragmaRX\Firewall\Vendor\Laravel\Facade::class,
//        'GeoIP' => Torann\GeoIP\Facades\GeoIP::class,


    ],

];
