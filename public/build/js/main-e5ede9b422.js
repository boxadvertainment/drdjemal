'use strict';

(function ($) {
  "use strict";

  if (APP_ENV == 'production') {
    console.log = function () {};
  }

  $('.loader').addClass('hide');
  function showLoader() {
    $('.loader').removeClass('hide');
    $('body').css('overflow', 'hidden');
  }

  function hideLoader() {
    $('.loader').addClass('hide');
    $('body').css('overflow', 'visible');
  }

  function showPage(selector) {
    $(selector).removeClass('hide').siblings().addClass('hide');
    facebookUtils.resizeCanvas();
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    error: function error(jqXHR) {
      if (/<html>/i.test(jqXHR.responseText)) sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');else sweetAlert({
        title: "Oups!",
        text: jqXHR.responseText,
        type: "error",
        html: true
      });
    }
  });

  function hasHtml5Validation() {
    return typeof document.createElement('input').checkValidity === 'function';
  }

  if (hasHtml5Validation()) {
    $('#consultation-form, #consultation-form-modal').submit(function (e) {
      if (!this.checkValidity()) {
        e.preventDefault();
        $(this).addClass('invalid');
        $('#status').html('invalid');
      } else {
        $(this).removeClass('invalid');
        var $this = $(this);

        if ($this.hasClass('disabled')) return false;
        $this.addClass('disabled');
        //disable button

        $.ajax({
          url: $this.attr('action'),
          method: 'POST',
          data: $this.serialize()
        }).done(function (response) {
          $this.removeClass('disabled');
          if (response.success) {
            $this[0].reset();
            $('#consultationModal').modal('hide');
            return swal("Merci!", "On vous contactera dés que possible", "success");
          }
          return sweetAlert({ title: 'Oups!', text: response.message, type: 'error', html: true });
        });
        e.preventDefault();
      }
    });
  }

  $('.branch-btn').hover(function (event) {
    event.preventDefault();

    $('.branch-btn').parent('li').removeClass('active');
    $(this).parent('li').addClass('active');

    $('.treatments-list').stop().slideUp(100);
    $(this).parent().find('.treatments-list').stop().delay(100).slideDown(300);
  });
})(jQuery);
//# sourceMappingURL=main.js.map
