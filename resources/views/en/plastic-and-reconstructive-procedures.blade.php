@extends('en.innerLayout-en')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Plastic & Reconstructive</span>Procedures</h1>
      </div>
    </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2>Definition&nbsp;</h2>

        <p>Reconstructive surgery is performed on injuries or conditions such as cleft lip and palate, abnormal structures of the body caused by congenital defects, developmental abnormalities, trauma, infection, tumors or disease. </p>
        <p>It is generally performed to improve functions, but may also be done to approximate a normal appearance.</p>
        <h2>Breast reconstruction</h2>
        <p>Breast reconstruction is achieved through several plastic surgery techniques that attempt to restore a breast to near normal shape, appearance and size following mastectomy.</p>
        <p>Although breast reconstruction can rebuild your breast, the results are highly variable.</p>

        <strong>About symmetry:</strong>

        <p> If only one breast is affected, it alone may be reconstructed.</p>
        <p> In addition, a breast lift, breast reduction or breast augmentation may be recommended for the opposite breast to improve symmetry of the size and position of both breasts.</p>

        <h2>Child's cleft lip and palate repair</h2>

        <p>Cleft lip (cheiloschisis) and cleft palate (palatoschisis) are among the most common birth defects affecting children.</p>
        <p>The incomplete formation of the upper lip (cleft lip) or roof of the mouth (cleft palate) can occur individually, or both defects may occur together.</p>
        <p>A cleft, or separation of the upper lip and/or the roof of the mouth, occurs very early in the development of your unborn child.</p>
        <p>Cleft lip and cleft palate repair is a type of plastic surgery to correct this abnormal development both to restore function and to restore a more normal appearance.</p>
        <p>Most clefts can be repaired through specialized plastic surgery techniques, improving your child’s ability to eat, speak, hear and breathe, and to restore a more normal appearance and function.</p>

        <h2>SCAR REVISION</h2>

        <p>Scar revision surgery is meant to minimize the scar so that it is more consistent with your surrounding skin tone and texture.</p>
        <br>
        <p>Scars are visible signs that remain after a wound has healed.</p>
        <ul>
            <li>They are unavoidable results of injury or surgery, and their development can be unpredictable.</li>
            <li>Poor healing may contribute to scars that are obvious, unsightly or disfiguring.</li>
            <li>A wound that heals well can result in a scar that affects your appearance. </li>
            <li>Scars may be raised or recessed, different in color or texture from surrounding healthy tissue or particularly noticeable due to their size, shape or location.</li>
        </ul>
            <p>Your treatment options may vary based on the type and degree of scarring and can include:</p>
        <ul>
            <li>Simple topical treatments.</li>
            <li>Minimally invasive procedures.</li>
            <li>Surgical revision with advanced techniques in wound closure.</li>
            <li>Although scar revision can provide a more pleasing cosmetic result or improve a scar that has healed poorly, a scar cannot be completely erased.</li>
        </ul>


    </div>
@endsection

@section('title','Eyes intervention in Tunisia - Dr Djemal: Nose surgery in Tunisia ')
@section('description',"You are going to have eyes intervention or nose surgery in Tunisia? Choose Dr Djemal ")
