<div class="floating-ui">

        <div class="top-header">
          <div class="container">
            <ul class="list-inline pull-left">
              <li><a href="https://www.facebook.com/Dr-Djemal-228429484155370" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/dr_djemal" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://tn.linkedin.com/in/tahar-djemal-131071112" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <ul class="list-inline pull-right">
              <li><a href="https://api.whatsapp.com/send?phone=+21697222294"><i class="fa fa-phone"></i> (+216) 97 222 294</a></li>
              <li><a href="{{ url('contact-en') }}"><i class="fa fa-envelope"></i> Contact Us</a></li>
              <li>
                <div class="btn-group">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe"></i> English <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url('acceuil') }}">Français</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <!--/.top-header -->

        <nav class="navbar">
          <div class="container">

            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ url('home') }}"><img src="{{ asset('/img/logo.png') }}" alt="" class="img-responsive"></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('home') }}" class="{{ $page == 'home' ? 'active' : ''}}">Home</a></li>
                <li><a href="{{ url('cv-en') }}" class="{{ $page == 'cv-en' ? 'active' : ''}}">CV</a></li>
                <li><a href="{{ url('interventions-procedures') }}" class="{{ $page == 'interventions-procedures' ? 'active' : ''}}">SURGERIES</a></li>
                <li><a href="{{ url('the-clinic') }}" class="{{ $page == 'the-clinic' ? 'active' : ''}}">Clinic Myron</a></li>
                <li><a href="{{ url('before-after-testimonials') }}" class="{{ $page == 'before-after-testimonials' ? 'active' : ''}}">Before/After</a></li>
                <li><a href="{{ url('testimonials') }}" class="{{ $page == 'testimonials' ? 'active' : ''}}">Testimonials</a></li>
                <li class="btn-cta-wrapper"><a href="#" class="btn-cta" data-toggle="modal" data-target="#consultationModal">FREE consultation</a></li>
              </ul>
            </div><!--/.nav-collapse -->

          </div><!--/.container -->
        </nav>
      </div>
