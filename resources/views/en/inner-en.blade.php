@extends('en.innerLayout-en')

@section('class', 'home')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Chirurgien plastique</span> DR Taher Djemal</h1>
      </div>
    </header>
@endsection

@section('innerContent')
            <div role="tabpanel" class="tab-pane active" id="home">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos id quam ipsam magnam tempora cupiditate ratione enim adipisci natus perferendis dolorum repellat ad eos, laboriosam debitis veniam accusamus eum beatae quisquam, alias minima earum, rem est similique. Placeat dicta itaque provident similique exercitationem perferendis tenetur, voluptate, quibusdam optio laudantium eveniet.</div>
    <div class="content">
        <h2 class="content-title">Botox</h2>
        <p>Le Lorem Ipsum est simplement du <a href="#">faux texte employé</a> dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. <br>Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié.</p>
        <ul>
        <li>Austin ( <a href="#brewdrop">BrewDrop</a>, <a href="#drizly">Drizly</a>, <a href="#minibar">Minibar</a>, <a href="#thirstie">Thirstie</a>, <a href="#topshelf">Topshelf</a> )</li>
        <li>Baltimore ( <a href="#drizly">Drizly</a>, <a href="#thirstie">Thirstie</a> )</li>
        <li>Chicago ( <a href="#drizly">Drizly</a>, <a href="#minibar">Minibar</a>, <a href="#swill">Swill</a>, <a href="#thirstie">Thirstie</a> )</li>
        <li>Dallas ( <a href="#drizly">Drizly</a>, <a href="#minibar">Minibar</a>, <a href="#swill">Swill</a>, <a href="#thirstie">Thirstie</a>, <a href="#topshelf">Topshelf</a> )</li>
        <li>Denver ( <a href="#drizly">Drizly</a>, <a href="#liquorlimo">Liquor Limo</a>, <a href="#minibar">Minibar</a> )</li>
        </ul>
        <p><a href="http://topshelfapp.com/"><img src="http://blog.onfleet.com/wp-content/uploads/2015/09/topshelf-1024x514.jpg" alt="topshelf" width="1024" class="alignnone size-large wp-image-485"></a></p>
        <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié.</p>
    </div>
      <!-- /.content -->

      <div class="btn-cta-wrapper">
        <a href="#" class="btn-cta-content"> poster vos questions, un spécialiste vous répondra <i class="fa fa-arrow-circle-right"></i></a>
      </div>
      <!-- /.btn-cta-wrapper -->

      <div class="gallery-wrapper">
        <h3 class="title">Galerie photo</h3>
        <div class="gallery">

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/banner-innerpages.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/bloc1.png" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/banner-innerpages.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/bloc1.png" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/banner-innerpages.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/bloc1.png" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/banner-innerpages.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/bloc1.png" alt="">
            </a>
          </div>
        </div>
      </div>

      <div class="tabs-wrapper">
        <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos id quam ipsam magnam tempora cupiditate ratione enim adipisci natus perferendis dolorum repellat ad eos, laboriosam debitis veniam accusamus eum beatae quisquam, alias minima earum, rem est similique. Placeat dicta itaque provident similique exercitationem perferendis tenetur, voluptate, quibusdam optio laudantium eveniet.</div>
            <div role="tabpanel" class="tab-pane" id="profile">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil at ullam nam. Voluptatibus cum nesciunt totam, nemo. Ea minus laboriosam ratione, doloribus cum eius dolores animi provident magni rem quam harum ex magnam pariatur? Temporibus laborum, repellat quos mollitia nisi saepe quo necessitatibus praesentium beatae consequuntur commodi aperiam eius iste.</div>
            <div role="tabpanel" class="tab-pane" id="messages">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, sit nemo illum accusantium porro aliquid minus. Eius dolorum voluptate, eos explicabo porro alias, vel ducimus quasi dicta, voluptates fugiat ut sequi enim amet. Cumque illum consequuntur dolores nobis, mollitia necessitatibus possimus aspernatur a itaque sit in, eaque error ea delectus.</div>
          </div>
        </div>
@endsection
