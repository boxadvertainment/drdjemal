@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> Liposuction </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>Liposuction (Lipoplasty)</h2>

        <p>Liposuction is a procedure that can help sculpt the body by removing unwanted fat from specific areas, including the abdomen, hips, buttocks, thighs, knees, upper arms, chin. During the past decade, liposuction, which is also known as &quot;lipoplasty&quot; has benefited from several new refinements.</p>

        <p>Although no type of liposuction is a substitute for dieting and exercise, liposuction can remove stubborn areas of fat that don&#39;t respond to traditional weight-loss methods.</p>

        <h2>Before Surgery</h2>

        <p>Your surgeon will evaluate your health, determine where your fat deposits lie and assess the condition of your skin. Your surgeon will explain the body-contouring methods that may be most appropriate for you. For example, if you believe you want liposuction in the abdominal area, you may learn that an abdominoplasty or &quot;tummy tuck&quot; may more effectively meet your goals.</p>

        <p>Your Surgeon will recommend to do a complete blood test.</p>

        <p>Liposuction will be performe under general anesthesia and you will stay at least one night in the clinic.</p>

        <h2>Surgery</h2>

        <p>The time required to perform liposuction may vary considerably, depending on the size of the area, the amount of fat being removed.</p>

        <p>Liposuction is a procedure in which localized deposits of fat are removed to recontour one or more areas of the body. Through a tiny incision, a narrow tube or cannula is inserted and used to vacuum the fat layer that lies deep beneath the skin. The cannula is pushed then pulled through the fat layer, breaking up the fat cells and suctioning them out. The suction action is provided by a vacuum pump or a large syringe. If many sites are being treated, your surgeon will then move on to the next area, working to keep the incisions as inconspicuous as possible.</p>

        <p>Fluid is lost along with the fat, and it&#39;s crucial that this fluid be replaced during the procedure to prevent shock. For this reason, patients need to be carefully monitored and receive intravenous fluids during and immediately after surgery.</p>

        <h2>Technique</h2>

        <p>Fluid Injection, a technique in which a medicated solution is injected into fatty areas before the fat is removed. The fluid helps the fat be removed more easily, reduces blood loss and provides anesthesia during and after surgery. Fluid injection also helps to reduce the amount of bruising after surgery. The amount of fluid that is injected varies depending on</p>

        <p>The scars from liposuction are small and strategically placed to be hidden from view.</p>

        <h2>After Surgery</h2>

        <p>Don&#39;t expect to look or feel great right after surgery. Even though the newer techniques are believed to reduce some post-operative discomforts, you may still experience some pain, burning, swelling, and temporary numbness.</p>

        <p>Healing is a gradual process. You will start walking around as soon as possible to reduce swelling and to help prevent blood clots from forming in your legs. You will begin to feel better after about a week or two and you should be back at work within a few days. The stitches dissolve on their own within the first week to 10 days.</p>

        <p>Activity that is more strenuous should be avoided for about a month as your body continues to heal. Although most of the bruising and swelling usually disappears within three weeks, some swelling may remain for six months or more.</p>

        <p>You will see a noticeable difference in the shape of your body quite soon after surgery. However, improvement will become even more apparent after about four to six weeks, when most of the swelling has subsided. After about three months, any persistent mild swelling usually disappears and the final contour will be visible.</p>

    </div>
@endsection

@section('title','Aesthetic surgeon Tunisia-Dr Djemal: Liposuction in Tunisia')
@section('description',' Dr Djemal is an aesthetic surgeon who is practicing liposuction in Tunisia ')
