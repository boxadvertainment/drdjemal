@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> BUTTOCK IMPLANTS </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>BUTTOCK IMPLANTS</h2>

        <p><a href="http://en.wikipedia.org/wiki/Buttock_augmentation">Buttock augmentation</a>,&nbsp;enhancement of the&nbsp;<a href="http://en.wikipedia.org/wiki/Buttock">buttocks</a>&nbsp;using silicone implants or fat grafting and transfer from other areas of the body</p>

        <p>The buttocks are augmented and lifted with the use of silicone&nbsp;<strong>buttock implants</strong>&nbsp;(specifically designed for Buttock Augmentation) or with your own fat, also called&nbsp;fat grafting, fat injections, fat transfer.</p>

        <h2>SURGERY</h2>

        <p>A small incision of 2 inches (5 cm) between the buttocks, inside the midline or intergluteal fold, which can be very easily concealed</p>

        <p>Through this incision, the buttock implants are then positioned in a sub muscular or intramuscular plane depending on the quantity and quality of tissues to proper covering the implants, to make them non visible or touchable and obtaining natural results. The bigger the implants are, and less patient&rsquo;s tissue covering the buttock implants, the results will be more obvious.</p>

        <p>Round or oval shape buttock implants will be used according to the case.</p>

        <p>A&nbsp;<strong>buttock augmentation with fat grafting</strong>&nbsp;is indicated in men or women who desire to improve their body contour in addition to buttock augmentation</p>

        <p>We use round or oval shape buttock implants according to the case. Learn more at about buttock implants</p>

        <p>Fat injection has a short recovery period without limitations in regards to mobilization, and it has less discomfort compared to buttock augmentation with implants.</p>

        <h2>AFTER SURGERY</h2>

        <p>After having fat grafting surgery, you should expect to be doing normal activities after 3 to 5 days.</p>

        <p>After having buttock implants surgery you should normally take two weeks to progressively return to normal activities</p>

        <p>It takes 1 to 3 hours under generql anesthesia</p>

        <p>You should wear a special garnment from 4 to 6 weeks for most of the gluteoplasties</p>

        <p>A bed resting is required only the day of buttock surgery. Walking the next days helps to subsiding the swelling ant do prevent deep vein thrombosis</p>

        <p>No sunbathing for 3 to 4 weeks post op</p>

        <p>An elastic garment is worn for 6 to 8 weeks post op</p>

        <p>The stitches dissolve on their own within the first week to 10 days.</p>

    </div>
@endsection


@section('title','Surgeon Djemal-Dr Djemal: Buttock surgery in Tunisia')
@section('description','Dr Djemal is a skilled aesthetic surgeon who practices buttock surgery in Tunisia')
