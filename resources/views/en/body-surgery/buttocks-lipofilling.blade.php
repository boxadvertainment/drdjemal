@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> BUTTOCK LIPOFILLING </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>BUTTOCK LIPOFILLING/ BUTTOCK AUGMENTATION WITH FAT GRAFTING</h2>

        <p>Buttock lipofilling is a surgery to increase the volume of the buttocks by injecting fat from the patient himself.</p>

        <p>A liposuction will be done in order to suck maximum fat; it is usually taken from the stomach, hips or thighs. Liposuction is particularly soft and atraumatic to fat cells, which are preserved and remain alive.</p>

        <p>The fat is collected directly into a sterile jar. It is processed, filtered, purified and then it goes into a machine (centrifuge) to isolate the living fat cells from the rest of the fibrous elements. The rest of blood cells and also fat cells are broken being removed in the form of oil.</p>

        <p>The surgeon then distributes the fat between the two buttocks and between the three parts of each buttock in a harmonious way. These microinjections are carried out meticulously and especially evenly to allow a natural look and shaped buttocks.</p>

        <p>The grafted fat cells increase the volume and projection of the . About 50-60% of these fat cells will remain definitely. This means that 40-50% of the injected cells will disappear but the transplanted cells that remain will ensure the desired volume . The final result will be visible after 6 months.</p>

        <p>Buttocks lipofilling has very simple postoperative effects. It is not very painful.</p>

        <p>You should wear a special garnment from 4 to 6 weeks.</p>

        <p>Mild swelling and bruising will appear the day after surgery and will disappear after a few days.</p>

        <p>Some discomfort may be felt in the seated position the first four days.</p>

        <p>The postoperative care are swelling and hardness of the areas for a month.</p>

        <p>The result is immediately visible and is accentuated by the swelling for a month. You can feel a loss of volume after one month .</p>

    </div>
@endsection

@section('title','Buttock surgery in Tunisia-Dr Djemal: Aesthetic surgeon Tunisia')
@section('description','Dr Djemal is a skilled aesthetic surgeon who practices buttock surgery in Tunisia')