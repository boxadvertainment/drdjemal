@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> Calf Implants </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>Calf Implants (Calf Augmentation)</h2>

        <p>Calf implants are best suited for lower leg sculpting, and also can correct muscle imbalance as a result of both physical and congenital defects.</p>

        <p>Calf implants are usually only performed by a small group of plastic and cosmetic surgeons who have performed these procedures and have the extensive experience required in forming (sculpting) and placing the solid silicone implants that are used in the medical procedure&mdash;assuring that they don&#39;t shift from their original positions or develop infection, two areas where serious medical problems could occur after surgery.</p>

        <h2>SURGERY</h2>

        <p>During the calf implant procedure (one hour or two), an incision is made in the natural crease behind the knee.</p>

        <p>The surgeon then creates a pocket large enough to place the calf implants. The implants can be placed on both the upper inside or outside of the leg.</p>

        <p>This area, consisting of muscle and fat, will form scar tissue that will help keep the implant in place. The incision is closed with sutures, and a bandage is applied to reduce swelling and discomfort.</p>

        <p>Calf augmentation is often performed in conjunction with liposuction of the ankle and leg to create a thinner and more defined look.</p>

        <h2>AFTER SURGERY</h2>

        <p>Although normal activities can be resumed a few days after surgery, vigorous physical activities should be delayed for about six months.</p>

        <p>After calf augmentation, legs can appear fuller, more muscular, and better defined.</p>

    </div>
@endsection

@section('title','Plastic surgery in Tunisia-Dr Djemal: Surgeon in Tunisia')
@section('description','Dr Djemal is one of the best surgeons, he practices plastic surgery in Tunisia')