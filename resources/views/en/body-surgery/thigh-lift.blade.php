@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> THIGH LIFT </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

      <h2>THIGH LIFT</h2>

      <p>The natural ageing process causes the skin to lose its elasticity. Substantial weight loss can also leave flabby skin.</p>

      <p>With an Arm Lift (Brachioplasty) or Thigh Lift, it can help to restore and redrape these areas, providing a more youthful and firmer appearance.</p>

      <p>The surgery of arm and thigh lifts is made in a clinic under a general anaesthetic. These procedures usually take 2-3 hours.</p>

      <h2>SURGERY: Thigh Lifts</h2>

      <p>1. The incision is made along the inner thigh area, which may extend from the inner knee to the groin.</p>

      <p>2. In some cases, where additional lift is needed, the incision may carry on along either the femoral line or buttock fold.</p>

      <p>3. The excess skin is then carefully removed.</p>

      <p>Patients undergoing a thigh lift&nbsp;or rm lift can go home after one night.</p>

      <h2>Preparing For Your Surgery</h2>

      <p>If the amount of loose skin is minimal and the excess fat deposits are located below your navel, a short horizontal incision may be all that is necessary.</p>

      <p>In some cases your surgeon may recommend liposuction as well to remove fat.</p>

      <h2>After your surgery</h2>

      <p>The areas operated on will initially feel tight and swollen. The swelling is mild to moderate, and peaks at two to three days. The sutures are covered with adhesive strips and surgical gauze.</p>

      <p><strong>Bleeding: </strong>Small amounts of oozing and bleeding are very common but should be no more than a slow staining of the gauze dressing.</p>

      <p><strong>Mobility after thigh lifts: </strong>Because of the location of the incisions for a thigh lift, it is impossible to avoid lying on them. Change position at least every 30 minutes and move very carefully while putting as little stress on the incision lines as possible.</p>

      <p><strong>Stitches: </strong>The stitches will be re-absorbed by the body .You will be able to shower on the third day after surgery.</p>

      <p><strong>Pain: </strong>You can expect some moderate pain after this procedure. Your surgeon may prescribe pain medication for the first few days.</p>

      <p><strong>Bruising and Swelling: </strong>There may be bruising and swelling which usually subsides after two to three weeks. You will be given a garment to wear for up to six weeks. This will help to alleviate the swelling.</p>

      <h2>Getting Back to Normal</h2>

      <p>Recovery time varies from person to person. Your surgeon will advise you to take two to three weeks off work.</p>

    </div>
@endsection

@section('title','Plastic surgery in Tunisia-Dr Djemal: Lifting thighs in Tunisia')
@section('description','Lifting thighs is a plastic surgery practiced by Dr Djemal in Tunisia')
