@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> INTIMATE SURGERY </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2>INTIMATE SURGERY</h2>

        <p><strong>Vaginal rejuvenation</strong> is the surgery that aims to restore the pelvic muscles or a distended vagina. It is also performed to reduce the labia minora for aesthetic purposes.</p>

        <p>The purpose of this procedure is to restore the perennial muscles, strengthen tissue and shrink the vaginal canal by giving it an optimal diameter.</p>

        <p>This procedure is indicated for women after one or more deliveries or simply with the effect of natural aging process, it allows finding more reinforced and toned tissues.</p>

        <p>Vaginal rejuvenation is a little painful procedure. However, if mild pain is felt simple analgesics will eliminate the discomfort.</p>

        <p>The patient should wash daily with foamy Betadine and water to allow better healing. Bath can only be taken starting from the fourth day.</p>

        <p>The patient may get back to social and professional activity starting from the fifth day, after a few days of rest.</p>

        <p>The resumption of sexual activity is expected after at least a month after surgery, and pregnancy is still possible after the intervention.</p>

        <p>The result is significant once the resumption of sexual activity. As for scars, they are invisible through the proper healing of vaginal tissues.</p>

        <h2>Labiaplasty /LABIOPLASTY</h2>

        <p>Surgical procedure that reduces and reshapes the labia minora, the skin that surrounds the vaginal opening. It can be performed on one or both sides of the labia.</p>

        <p>Labial enlargement is usually congenital, but can also be caused by hormonal changes or childbirth. Whether you find them unattractive, uncomfortable, or they make it more difficult to fit into your clothing, it can be corrected to the proportions to give you a natural, comfortable appearance.</p>

        <p>Penile enlargement, or phalloplasty, can lengthen the penis.</p>

    </div>
@endsection

@section('title','Plastic surgery in Tunisia-Dr Djemal: Gynecomastia in Tunisia')
@section('description','Gynecomastia is a plastic surgery practiced by Dr Djemal in Tunisia for those who suffer from excessive mammary gland development')
