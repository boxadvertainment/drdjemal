@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> Tummy Tuck </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2>Tummy Tuck (Abdominoplasty)</h2>

        <p>Abdominoplasty, known as a &quot;tummy tuck,&quot; is a major surgical procedure to remove excess skin and fat from the middle and lower abdomen and to tighten the muscles of the abdominal wall. The procedure can dramatically reduce the appearance of a protruding abdomen. But, it does produce a permanent scar, which, depending on the extent of the original problem and the surgery required to correct it, can extend from hip to hip.</p>

        <p>Tummy tuck is recommended for men or women who are bothered by a large fat deposit or loose abdominal skin and to women who, through multiple pregnancies, have stretched their abdominal muscles and skin beyond the point where they can return to normal. Loss of skin elasticity in older patients, which frequently occurs with slight obesity, can also be improved.</p>

        <h2>Planning Surgery</h2>

        <p>If your fat deposits are limited to the area below the navel, you may require a less complex procedure called a partial abdominoplasty, also known as a mini-tummy tuck. A complete tummy tuck done in conjunction with liposuction to remove will help for a better body contour. Or maybe liposuction alone would create the best result.</p>

        <h2>Anesthesia</h2>

        <p>General anesthesia and your stay at the clinic will be for one night at least.</p>

        <h2>The Surgery</h2>

        <p>Complete tummy tuck usually takes two to three hours, depending on the extent of work required. Partial abdominoplasty may take an hour or two.</p>

        <p>The surgeon will make a long incision from hipbone to hipbone, just above the pubic area. A second incision is made to free the navel from surrounding tissue. With partial abdominoplasty, the incision is much shorter and the navel may not be moved, although it may be pulled into an unnatural shape as the skin is tightened and stitched.</p>

        <p>These muscles are tightened by pulling them close together and stitching them into their new position. This provides a firmer abdominal wall and narrows the waistline.</p>

        <p>The skin flap is then stretched down and the extra skin is removed. A new hole is cut for your navel, which is then stitched in place. Finally, the incisions will be stitched, dressings will be applied, and a temporary tube may be inserted to drain excess fluid from the surgical site.</p>

        <p>In partial abdominoplasty, the skin is separated only between the incision line and the navel. This skin flap is stretched down, the excess is removed, and the flap is stitched back into place.</p>

        <h2>After Surgery</h2>

        <p>For the first few days, your abdomen will probably be swollen and you&#39;re likely to feel some pain and discomfort which can be controlled by medication. Depending on the extent of the surgery, you may be you may have to remain hospitalized for one to two days.</p>

        <p>Your doctor will give you instructions for showering and changing your dressings. And though you may not be able to stand straight at first, you should start walking as soon as possible.</p>

        <p>The stitches dissolve on their own within the first week to 10 days.</p>

        <h2>BACK TO NORMAL</h2>

        <p>Exercise will help you heal better. Even people who have never exercised before should begin an exercise program to reduce swelling, lower the chance of blood clots, and tone muscles. Vigorous exercise, however, should be avoided until you can do it comfortably.</p>

        <p>Your scars may actually appear to worsen during the first three to six months as they heal, but this is normal. Expect it to take nine months to a year before your scars flatten out and lighten in color. While they&#39;ll never disappear completely, abdominal scars will not show under most clothing, even under bathing suits.</p>

        <p>Abdominoplasty, whether partial or complete, produces excellent results for patients with weakened abdominal muscles or excess skin. And in most cases, the results are long lasting, if you follow a balanced diet and exercise regularly.</p>
    </div>
@endsection
@section('title','Abdominoplasty in Tunisia-Dr Djemal: Surgeon Djemal')
@section('description','If you suffer from overweight and you want to have an abdominoplasty in Tunisia, Surgeon Djemal is an exellent one')