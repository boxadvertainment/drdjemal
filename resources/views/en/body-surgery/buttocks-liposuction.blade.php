@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> BUTTOCKS LIPOSUCTION </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2>BUTTOCKS LIPOSUCTION</h2>

        <p><strong>Procedure:</strong></p>

        <p>Improve body shape by removing exercise-resistant fat deposits with a tube and vacuum device.</p>

        <p>Performed using the tumescent technique, in which targeted fat cells are infused with saline containing solution with a local anaesthetic before liposuction to reduce post-operative bruising and swelling. Common locations for liposuction include chin, cheeks,<strong> </strong>neck, upper arms, above breasts, abdomen,<strong> buttocks</strong>, hips, thighs, knees, calves, ankles.</p>

        <p>&bull; Length: 1 to 2 hours</p>

        <p>&bull; Anesthesia: general.</p>

        <p>&bull; Inpatient: Extensive procedures may require short inpatient stay.</p>

        <p>&bull; Side Effects: Temporary bruising, swelling, numbness, soreness, burning sensation. Tumescent: Temporary fluid drainage from incision sites</p>

        <p>&bull; Risks: Asymmetry. Rippling or bagginess of skin.</p>

        <p>&bull; Recovery: Back to work: Few days. More strenuous activity: 1 to 2 weeks. Full recovery from swelling and bruising: 1 to 4 months or more</p>

        <p>&bull; Duration of Results: Permanent, with sensible diet and exercise.</p>
    </div>
@endsection

@section('title','Surgeon in Tunisia-Dr Djemal: Lifting Tunisia')
@section('description','Dr Djemal is a surgeon who is expert in lifting in Tunisia')
