@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> Thigh Liposculpture (Liposuction) </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2><a href="http://www.ienhance.com/procedures/thigh-liposculpture">Thigh Liposculpture (Liposuction)</a></h2>

        <p>Liposuction is a technique for reshaping the body by permanently removing localized fat deposits. This procedure is popular with both men and women with permanent results.</p>

        <p>Fatty deposits on the inner and outer thighs are especially resistant to diet and exercise. Liposuction can help to trim the excess fat in the thighs to achieve a more attractive balance and symmetry to the body. In fact, this is the most common area of the body for liposuction.</p>

        <p>During the procedure, a small incision is made under the skin and a small thin tube called a cannula is inserted. The canula will remove the fat between the skin and the thigh muscles.</p>

        <h2>SURGERY</h2>

        <p>Before surgery, the surgeon will mark the precise areas of the body where the fat is to be removed.</p>

        <p>During the procedure, the surgeon makes a tiny incision in the skin, typically in or near the buttock crease or at the site of a previous scar, and inserts a thin tube called a cannula into the fatty area. The cannula is used to break up the fat deposits and sculpt the area to the desired proportions. The unwanted fat is removed with a high pressure vacuum, leaving the skin, muscles, nerves, and blood vessels intact.</p>

        <p>In the tumescent technique, a saline solution containing a local anesthetic and adrenaline is injected into the area to be treated, which makes the fat deposits easier to break up and extract. This extra fluid also minimizes trauma to the surrounding tissue, reducing swelling and post-operative pain. The administration of adrenaline also decreases bleeding during surgery, further reducing risks.</p>

        <p>On average, the procedure takes one to two hours.</p>

        <p>If you&#39;re having a large amount of fat removed it is necessary to spend the night in the hospital so your recovery process can be monitored by a medical staff.</p>

        <h2>AFTER SURGERY</h2>

        <p>For a day or two, you can expect to feel tired. Your legs will be stiff and sore, and you may experience some pain, burning, swelling, bleeding, or temporary numbness.</p>

        <p>You will be fitted with a tight compression garment to be worn for up to 4 weeks after your surgery. This specially designed garment will help reduce pain and swelling, and help your body settle into its new shape.</p>

        <h2>Going back to normal</h2>

        <p>Most of the bruising and swelling should subside within three weeks.</p>

        <p>You should avoid strenuous activities for several weeks as your body heals, but within 6 weeks you should be able to resume all normal activities.</p>

        <p>It&#39;s important to continue wearing the compression garment, which should be easily hidden under your clothing.</p>

        <p>You should see the visible results of your surgery within two or three weeks, but the full effects may not be evident for six months to a year. The more fat that you had removed, the longer it will take to achieve optimal results, because the skin has to adapt to a more radical change in shape.</p>

        <p>Thigh liposuction is often performed in conjunction with buttock liposuction.</p>

    </div>
@endsection

@section('title','Plastic surgery in Tunisia-Dr Djemal: Lifting thighs in Tunisia')
@section('description','Lifting thighs is a plastic surgery practiced by Dr Djemal in Tunisia')
