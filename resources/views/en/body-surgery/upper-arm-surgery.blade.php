@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">BODY SURGERY</span> Upper Arm Lift </h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2>Upper Arm Lift (Brachioplasty)</h2>

        <p>With age, upper arm skin can become loose and flabby.</p>

        <p>An arm lift, also known as brachioplasty, is a surgical procedure to remove loose skin and excess fat deposits in the upper arm.</p>

        <p>With an Arm Lift (Brachioplasty) it can help to restore and redrape these areas, providing a more youthful and firmer appearance.</p>

        <p>The surgery of arm lifts is made in a clinic under a general anaesthetic. These procedures usually take 2 hours.</p>

        <p>The surgeon will discuss brachioplasty with patients who want to tighten this skin and look as good as they feel. In some cases your plastic surgeon may suggest that liposuction be used alone or in conjunction with an arm lift to remove excess fat in the upper arms.</p>

        <h2>SURGERY: Arm Lifts</h2>

        <p>1. Incisions will be made along the inner arm, which may extend from the armpit to the elbow.</p>

        <p>2. The skin is separated from the underlying fat and muscle and excess skin is carefully removed.</p>

        <h2>After your surgery</h2>

        <p>The areas operated on will initially feel tight and swollen. The swelling is mild to moderate, and peaks at two to three days. The sutures are covered with adhesive strips and surgical gauze.</p>

        <p><strong>Stitches: </strong>All the stitch You will be able to shows dissolve on the third day after surgery.</p>

        <p><strong>Pain: </strong>You can expect some moderate pain after this procedure. Your surgeon may prescribe pain medication for the first few days</p>

        <p><strong>Bruising and Swelling: </strong>There may be bruising and swelling which usually subsides after two to three weeks. You will be given a garment to wear for up to six weeks. This will help to alleviate the swelling.</p>

        <h2>Getting Back to Normal</h2>

        <p>Recovery time varies from person to person. Your surgeon will advise you to take two to three weeks off work.</p>
    </div>
@endsection

@section('title','Aesthetic clinic in Tunisia-Dr Djemal: Liposuction in Tunisia')
@section('description','Dr Djemal is an aesthetic surgeon who is practicing liposuction in a well knowen aesthetic clinic in Tunisia')
