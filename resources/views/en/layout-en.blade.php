<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    @include('en.partials.socialMetaTags')

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    @include('en.partials.favicon')

    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700,200' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset(elixir('css/main.css')) }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-formhelpers.css') }}">
    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Q3QEQSRD7C"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Q3QEQSRD7C');
</script>
</head>
<body class="@yield('class')">


    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Your browser is out of date !</h6>
        <p>To view this site correctly and delivering the best experience, we recommend you upgrade your browser.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Update Now </a>
        </p>
        <a href="#" class="close-btn" title="Close" data-dismiss="alert" aria-label="Close">&times;</a>
    </div>
    <![endif]-->

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    @yield('header')

    <main class="main" role="main">
        @yield('content')
    </main>

    <footer class="footer">
      <div class="container">
        <div class="row no-gutter">
          <div class="col-md-5 col-sm-6">
            <div class="row no-gutter">
              <div class="footer-col col-md-6 col-xs-6 footer-links">
                <ul>
                    <li><a href="{{ url('home') }}"><i class="fa fa-caret-right"></i>Home</a></li>
                    <li><a href="{{ url('cv-en') }}"><i class="fa fa-caret-right"></i>CV</a></li>
                    <li><a href="{{ url('interventions-procedures') }}"><i class="fa fa-caret-right"></i>interventions</a></li>
                    <li><a href="{{ url('the-clinic') }}"><i class="fa fa-caret-right"></i>The Clinic</a></li>
                </ul>
              </div><!--//foooter-col-->
              <div class="footer-col col-md-6 col-xs-6 footer-links">
                <ul>
                    <li><a href="{{ url('avant-apres-et-temoignages') }}"><i class="fa fa-caret-right"></i>Before/After</a></li>
                    <li><a href="{{ url('contact-en') }}"><i class="fa fa-caret-right"></i>Contact Us</a></li>
                    <!-- <li><a href="{{ url('cv-en') }}"><i class="fa fa-caret-right"></i>Plan du site</a></li> -->
                </ul>
              </div><!--//foooter-col-->
            </div>
          </div>
          <div class="footer-col col-md-5 col-sm-6 address">
            <p>Rue de la feuille d’érable, Résidence la brise du lac<br>
              1053  Les Berges du Lac 2 Tunis, Tunisie<br>
              Phone: (+216) 97 400 029
            </p>
          </div><!--//foooter-col-->
          <div class="clearfix visible-sm-block"></div>
          <div class="footer-col col-md-2 col-sm-12 logo-footer">
            <a href="{{ url('/') }}"><img src="/img/logo-footer.png" alt=""></a>
          </div>
        </div>
      </div>
    </footer>

    @yield('body')

    <!-- Modal -->
    <div class="modal fade modalEn" id="consultationModal" tabindex="-1" role="dialog" aria-labelledby="DevisGratuit">
      <form id="consultation-form-modal" action="{{ action('AppController@submitConsultation') }}" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
              <div class="title">
                <center><img src="/img/title-form-en.png" alt="" /></center>
                <br>
                <br>
              </div>
              <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Name & Surname"  required title="Field Required">
              </div>
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" required  title="Email incorrect">
              </div>
              <div class="form-group">
                <input type="tel" name="phone" class="form-control" placeholder="Phone" required title="Phone incorrect">
              </div>
              <!-- Country Select from bootstrap Helpers-->
              <div class="form-group">
                  <div id="country" class="bfh-selectbox bfh-countries" data-country="TN" data-flags="true" data-filter="true"></div>
              </div>
              <!-- End Country Select from bootstrap Helpers-->
              <div class="form-group">
                <div class="select-wrapper">
                  <select class="form-control" name="intervention" required>
                    <option value="">Surgeries</option>
                      @foreach(Config::get('app.SURGERIES') as $label => $group)
                          <optgroup label="{{ $label }}">
                              @foreach($group as $item => $intervention)
                                  <option value="{{ $item }}">{{ $intervention }}</option>
                              @endforeach
                          </optgroup>
                      @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <textarea name="message" class="form-control" rows="3" placeholder="Message" required></textarea>
              </div>
              <div class="form-group">
                
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default form-control" data-dismiss="modal"> Close </button>
            <button type="submit" name="submit" class="form-control submit"> <i class="fa fa-check"></i> Submit </button>
          </div>
        </div>
      </div>
      </form>
    </div>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/bootstrap-formhelpers.js') }}"></script>
    <script src="{{ asset(elixir('js/main.js')) }}"></script>
    @yield('scripts')
    <script>
      $(".galerie").colorbox({rel:'galerie',maxWidth:'90%', minWidth:'400px'});
    </script>
</body>
</html>
