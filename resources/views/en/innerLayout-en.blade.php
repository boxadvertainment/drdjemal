@extends('en.layout-en')
@section('content')
    <div class="container inside-page">
        <div class="row">
          <div class="col-md-9 col-sm-8 col-md-push-3 col-sm-push-4 content-wrapper">
              @yield('innerContent')
          </div>
          <!-- /.content-wrapper -->

          <!-- Sidebar -->
          <aside class="col-md-3 col-sm-4 col-xs-8 col-xs-offset-2 col-md-pull-9 col-sm-pull-8 col-sm-offset-0" id="sidebar">
            <ul class="list-unstyled branchs-list">
              <li class="{{ Request::segment(1) == 'body-surgery' ? 'active' : '' }}">

                <a href="#" class="branch-btn">
                  <h4 class="title">BODY SURGERY</h4>
                  <span class="read-more">Read More</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('body-surgery/thigh-lift') }}" class="treatment-btn">Thigh Lift</a></li>
                  <li><a href="{{ url('body-surgery/thigh-liposuction') }}" class="treatment-btn">THIGH LIPOSUCTION</a></li>
                  <li><a href="{{ url('body-surgery/buttocks-liposuction') }}" class="treatment-btn">BUTTOCKS LIPOSUCTION</a></li>
                  <li><a href="{{ url('body-surgery/buttocks-implants') }}" class="treatment-btn">BUTTOCKS IMPLANTS</a></li>
                  <li><a href="{{ url('body-surgery/buttocks-lipofilling') }}" class="treatment-btn">BUTTOCKS LIPOFILLING</a></li>
                  <li><a href="{{ url('body-surgery/calf-implants') }}" class="treatment-btn">CALF IMPLANTS</a></li>
                  <li><a href="{{ url('body-surgery/tummy-tuck') }}" class="treatment-btn">TUMMY TUCK</a></li>
                  <li><a href="{{ url('body-surgery/liposuction') }}" class="treatment-btn">LIPOSUCTION</a></li>
                  <li><a href="{{ url('body-surgery/upper-arm-surgery') }}" class="treatment-btn">UPPER ARM SURGERY</a></li>
                  <li><a href="{{ url('body-surgery/body-lift') }}" class="treatment-btn">BODYLIFT</a></li>
                  <li><a href="{{ url('body-surgery/intimate-surgery') }}" class="treatment-btn">INTIMATE SURGERY</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'aesthetic-treatments' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">AESTHETIC TREATMENTS</h4>
                  <span class="read-more">Read More</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('aesthetic-treatments/botox-injections') }}" class="treatment-btn">BOTOX Injections</a></li>
                  <li><a href="{{ url('aesthetic-treatments/hyaluronic-injections') }}" class="treatment-btn">HYALURONIC Injections</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'breast-surgery' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">BREAST SURGERY</h4>
                  <span class="read-more">Read More</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('breast-surgery/breast-uplift') }}" class="treatment-btn">BREAST UPLIFT</a></li>
                  <li><a href="{{ url('breast-surgery/breast-augmentation') }}" class="treatment-btn">BREAST AUGMENTATION</a></li>
                  <li><a href="{{ url('breast-surgery/breast-reduction') }}" class="treatment-btn">BREAST REDUCTION</a></li>
                  <li><a href="{{ url('breast-surgery/male-breast-reduction-gynaecomastia') }}" class="treatment-btn">MALE BREAST REDUCTION/Gynaecomastia</a></li>
                  <li><a href="{{ url('breast-surgery/male-breast-implant') }}" class="treatment-btn">MALE BREAST IMPLANT</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'face-surgery' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">FACE SURGERY</h4>
                  <span class="read-more">Read More</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('face-surgery/eyelid-surgery') }}" class="treatment-btn">EYELID SURGERY</a></li>
                  <li><a href="{{ url('face-surgery/ear-surgery') }}" class="treatment-btn">EAR SURGERY</a></li>
                  <li><a href="{{ url('face-surgery/rhinoplasty') }}" class="treatment-btn">RHINOPLASTY</a></li>
                  <li><a href="{{ url('face-surgery/chin-surgery') }}" class="treatment-btn">CHIN SURGERY</a></li>
                  <li><a href="{{ url('face-surgery/profile-surgery') }}" class="treatment-btn">PROFILE SURGERY</a></li>
                  <li><a href="{{ url('face-surgery/hair-transplant') }}" class="treatment-btn">HAIR TRANSPLANT</a></li>
                  <li><a href="{{ url('face-surgery/necklift-mini-lift') }}" class="treatment-btn">NECKLIFT/MINI LIFT</a></li>
                  <li><a href="{{ url('face-surgery/facelift') }}" class="treatment-btn">FACELIFT</a></li>
                  <li><a href="{{ url('face-surgery/fat-injections') }}" class="treatment-btn">FAT INJECTIONS</a></li>
                  <li><a href="{{ url('face-surgery/lips-surgery') }}" class="treatment-btn">LIPS SURGERY</a></li>
                  <li><a href="{{ url('face-surgery/neck-liposuction') }}" class="treatment-btn">NECK LIPOSUCTION</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'plastic-and-reconstructive-procedures' ? 'active' : '' }}">
                <a href="{{ url('plastic-and-reconstructive-procedures') }}" class="branch-btn">
                  <h4 class="title">RECONSTRUCTIVE SURGERY</h4>
                  <span class="read-more">Read More</span>
                </a>
              </li>
            </ul>

            <div class="contact-box">
              <h4>Contact Us</h4>
              <p>
                <big>Phone: (+216) 97 400 029</big> <br>
                Rue de la feuille d’érable,
                 Résidence la brise du lac
                1053  Les Berges du Lac 2 Tunis, <br>
                Tunisie</p>
            </div>

            <div class="myron-box">
              <img src="/img/myron-logo-sidebar.png" alt="Myron Clinic">
              <p>Myron Clinic is a state-of- the- art clinic equipped with the latest technology and modern facilities enabling high levels of quality and safety, including several perfectly-equipped operating theatres and an intensive care unit, for all types of surgeries.</p>
              <a href="http://myronclinic.com/" target="_blank"> More info</a>
            </div>

          </aside>
          <!-- /#sidebar md-3 -->

        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->

@endsection
