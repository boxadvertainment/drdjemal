@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>NECK LIPOSUCTION</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>NECK LIPOSUCTION</h2>

        <p>Neck liposuction produces consistently good results, and has a particularly high satisfaction rate.</p>

        <p>Certain areas of the body can accumulate fat that is hard to lose.</p>

        <p>The neck is one such area. Fatty deposits in the neck can detract from the appearance of the entire body, making a younger person seem older, or a fit person seems out of shape. Liposuction can give this area a new streamlined contour, enhancing the effect of facial features and improving the profile.</p>

        <p>A firm, trim neck gives the face a more youthful look.</p>

        <h2>SURGERY</h2>

        <p>The surgeon makes a tiny incision in the skin, typically in the crease just under the chin or behind the ear lobes. He inserts a thin tube called a cannula into the fatty area. The cannula is used to break up the fat deposits and sculpt the area to the desired proportions. The unwanted fat is removed with a high pressure vacuum, leaving the skin, muscles, nerves, and blood vessels intact.</p>

        <p>The procedure takes about one hour under general anesthesia</p>

        <h2>AFTER SURGERY</h2>

        <p>Your neck may be stiff and store for a few days, and you may experience some pain, burning, swelling, bleeding or temporary numbness.</p>

        <p>Your stitches will dissolve on their own in a week to 10 days. You will be fitted with a compression band to cut down on swelling and to hold your neck in its new shape until the tissues have adjusted.</p>

        <p>Most of the bruising and swelling should subside within three weeks. You should be able to return to work within a week or even within a few days if your work is fairly sedentary. Remember that you will be wearing the compression band for several weeks after your surgery to ensure that your neck stays firmly shaped.</p>

        <p>Most people are pleased with the new contour of their necks. Even though the neck size has been reduced, there may be extra skin once the fat has been removed. Although wearing the compression band will help to firm the area, this extra skin may sag, especially if your skin elasticity is poor.</p>

        <p>It may be necessary to perform a neck lift or face lift, either concurrently or after this procedure.</p>

        <p>Neck liposuction is often performed in conjunction with neck lift or face lift.</p>


    </div>
@endsection


@section('title','Liposuction in Tunisia-Dr Djemal: Face surgery in Tunisia')
@section('description','Dr Djemal practice liposuction and face surgery in Tunisia, you must choose him as your surgeon')
