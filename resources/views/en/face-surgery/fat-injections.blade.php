@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>FAT INJECTION</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>LIPOFILLING / FAT INJECTION</h2>

        <p>It involves extracting fat cells from the patient&#39;s abdomen, thighs, buttocks or elsewhere and reinjecting them beneath the facial skin.</p>

        <p>Fat is most often used to fill in &quot;sunken&quot; cheeks or laugh lines between the nose and mouth, to correct skin depressions or indentations, to minimize forehead wrinkles and to enhance the lips.</p>

        <p>Allergic reaction is not a factor for fat because it&#39;s harvested from a patient&#39;s own body.</p>

        <h2>PROCEDURE</h2>

        <p>The fat is withdrawn using a syringe with a large-bore needle or a cannula (the same instrument used in liposuction) attached to a suction device. The fat is then prepared and injected into the recipient site with a needle. Sometimes an adhesive bandage is applied over the injection site.</p>

        <p>As with collagen, &quot;overfilling&quot; is necessary to allow for fat absorption in the weeks following treatment. When fat is used to fill sunken cheeks or to correct areas on the face other than lines, this overcorrection of newly injected fat may temporarily make the face appear abnormally puffed out or swollen.</p>

        <h2>AFTER SURGERY</h2>

        <p>You can expect some swelling, bruising or redness in both the donor and recipient sites. The severity of these symptoms depends upon the size and location of the treated area. You should stay out of the sun until the redness and bruising subsides - usually about 48 hours. In the meantime, you may use makeup with sunblock protection to help conceal your condition.</p>

        <p>The swelling and puffiness in the recipient site may last several weeks, especially if a large area was filled.</p>

        <h2>Results</h2>

        <p>The duration of the fat injections varies significantly from patient to patient. Though some patients have reported results lasting a year or more, the majority of patients find that at least half of the injected fullness disappears within 3-6 months. Therefore, repeated injections may be necessary. Your doctor will advise you on how to maintain your results with repeat treatments.</p>

        <p>If you&#39;re like most patients, you&#39;ll be very satisfied with the results of your injectable treatments. You may be surprised at the pleasing results that can be gained from this procedure.</p>


    </div>
@endsection

@section('title','Face surgery in Tunisia-Dr Djemal: Facelift in Tunisia')
@section('description','Dr Djemal does face surgery and facelift in Tunisia you woud better choose him as surgeon')