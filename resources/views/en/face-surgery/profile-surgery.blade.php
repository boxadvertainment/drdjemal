@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>Profile Surgery</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>PROFILE SURGERY</h2>

        <p><br />
            Procedure to balance the nose and chin. Profile surgery can make a huge difference in your profile, reducing a prominent nose and bringing the chin and brow into balance.</p>

        <p>By analyzing the facial profile, the surgeon may determine that the nose&rsquo;s appearance is exaggerated because of an underdeveloped chin or a sloping brow.</p>

        <p>In this case, a combination of procedures that addresses all the aspects of facial balance can yield the best results.</p>

        <p>Chin lipofilling, chin augmentation moving the bone or placing a small implant at the point of the chin often is recommended in conjunction with a nose surgery.</p>

        <p>It also can be done alone, or in conjunction with a facelift, liposuction of the face or neck, or other procedures.</p>

        <p>Underdeveloped cheekbones can make your nose appear more prominent than it is. They also may contribute to premature aging.</p>

        <p>Several types of implants may be used to increase the midface region</p>

        <p><br />
            A weak chin or jaw not only disturbs facial harmony, it also can contribute to premature signs of aging. To correct the problem, your surgeon may advise chin augmentation combined with liposuction to remove the excess fat. These may be done in conjunction with other facial rejuvenation procedures in order to refine the angle of the chin and jaw.</p>

        <p><br />
            A number of other procedures are also available to improve the contours of the face:</p>

        <ul>
            <li>
                <p>Ear surgery may be recommended to reposition or reshape unattractive ears.</p>
            </li>
            <li>
                <p>Chin reduction can reduce the size of a chin that is too prominent.</p>
            </li>
            <li>
                <p>Liposuction is done to remove excess fatty deposits from the neck or jowl area.</p>
            </li>
            <li>
                <p>Neck lift surgery can correct a sunken appearance in the throat area and eliminate turkey neck.</p>
            </li>
        </ul>

    </div>
@endsection
@section('title','Surgeon Djemal-Dr Djemal: Facelift in Tunisia')
@section('description','Dr Djemal is a surgeon specialist in facelift in Tunisia')