@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">Face Surgery</span>Eyelid Surgery</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2>Eyelid Surgery (Blepharoplasty)</h2>

        <p>Eyelid surgery is a procedure to remove fat-usually along with excess skin and muscle from the upper and lower eyelids.</p>

        <p>Eyelid surgery can correct drooping upper lids and puffy bags below your eyes.</p>

        <p>However, it won&#39;t remove crow&#39;s feet or other wrinkles, eliminate dark circles under your eyes, or lift sagging eyebrows. It can be done alone, or in conjunction with other facial surgery procedures such as a facelift.</p>

        <h2>The Surgery</h2>

        <p>Blepharoplasty usually takes half an hour.</p>

        <p>The surgeon makes incisions following the natural lines of your eyelids; in the creases of your upper lids, and just below the lashes in the lower lids. The incisions may extend into the crow&#39;s feet or laugh lines at the outer corners of your eyes. Working through these incisions, the surgeon separates the skin from underlying fatty tissue and muscle, removes excess fat, and often trims sagging skin and muscle. The incisions are then closed with very fine sutures.</p>

        <p>If you have a pocket of fat beneath your lower eyelids but don&#39;t need to have any skin removed, your surgeon may perform a transconjunctival blepharoplasty. In this procedure the incision is made inside your lower eyelid, leaving no visible scar.</p>

        <h2>After Surgery</h2>

        <p>Keep your head elevated for several days, and use cold compresses to reduce swelling and bruising. (Bruising varies&nbsp;from person to person: it reaches its peak during the first week, and generally lasts anywhere from two weeks to a month.)</p>

        <p>You&#39;ll be shown how to clean your eyes, which may be gummy for a week or so. Many doctors recommend eyedrops, since your eyelids may feel dry at first and your eyes may burn or itch. For the first few weeks you may also experience excessive tearing, sensitivity to light.</p>

        <p>The stitches will be removed 5 days after surgery. Once they&#39;re out, the swelling and discoloration around your eyes will gradually subside, and you&#39;ll start to look and feel much better.</p>

        <h2>Getting Back to Normal</h2>

        <p>You should be able to read or watch television after two or three days. However, you won&#39;t be able to wear contact lenses for about two weeks, and even then they may feel uncomfortable for a while.</p>

        <p>Most people feel ready to go out in public (and back to work) in a week to 10 days. By then, you&#39;ll probably be able to wear makeup to hide the bruising that remains. You may be sensitive to sunlight, wind, and other irritants for several weeks.</p>

        <p>It&#39;s especially important to avoid activities that raise your blood pressure, including bending, lifting, and rigorous sports. You may also be told to avoid alcohol, since it causes fluid retention.</p>

        <h2>Your New Look</h2>

        <p>Healing is a gradual process, and your scars may remain slightly pink for six months or more after surgery. Eventually, though, they&#39;ll fade to a thin, nearly invisible white line.</p>

      </div>
@endsection
@section('title','Face surgery in Tunisia-Dr Djemal: Blepharoplasty in Tunisia')
@section('description','Dr Djemal is a specialist in face surgery in Tunisia and blepharoplasity if you want to correct correct drooping eyelids')