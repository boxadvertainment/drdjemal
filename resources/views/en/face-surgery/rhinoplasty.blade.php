@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>Rhinoplasty</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>Nose Surgery (Rhinoplasty)</h2>

        <p>Nose surgery is one of the most common of all plastic surgery procedures:</p>

        <ul>
            <li>
                <p>reduce or increase the size of your nose</p>
            </li>
            <li>
                <p>change the shape of the tip or the bridge</p>
            </li>
            <li>
                <p>narrow the span of the nostrils</p>
            </li>
            <li>
                <p>change the angle between your nose and your upper lip</p>
            </li>
            <li>
                <p>It may also correct a birth defect or injury</p>
            </li>
            <li>
                <p>help relieve some breathing problems</p>
            </li>
        </ul>

        <p>Rhinoplasty can be performed to meet aesthetic goals or for reconstructive purposes-to correct birth defects and breathing problems.</p>

        <h2>SCARRING</h2>

        <ul>
            <li>
                <p>when rhinoplasty is performed from inside the nose, there is no visible scarring at all</p>
            </li>
            <li>
                <p>when an &quot;open&quot; technique is used, or when the procedure calls for the narrowing of flared nostrils, the small scars on the base of the nose are usually not visible.</p>
            </li>
        </ul>

        <h2>SURGERY</h2>

        <p>Rhinoplasty is performed under general anesthesia and usually takes an hour .</p>

        <p>The skin of the nose is separated from its supporting framework of bone and cartilage, which is then sculpted to the desired shape. Finally, the skin is redraped over the new framework.</p>

        <p>Usually rhinoplasty is performed from within the nose, making the incisions inside the nostrils. But, in more complicated cases; they make a small incision across the columella, the vertical strip of tissue separating the nostrils.</p>

        <p>When the surgery is complete, a splint will be applied to help your nose maintain its new shape. Nasal packs may be placed in your nostrils to stabilize the septum, the dividing wall between the air passages.</p>

        <p>After surgery, during the first twenty-four hours-your face will feel puffy, your nose may ache, and you may have a dull headache.</p>

        <p>You&#39;ll notice that the swelling and bruising around your eyes will increase at first, reaching a peak after two or three days. Applying cold compresses will reduce this swelling and make you feel a bit better Most of the swelling and bruising should disappear within 10 days or so.</p>

        <p>A little bleeding is common during the first few days following surgery, and you may continue to feel some stuffiness for several weeks. It is better not to blow your nose for a week or so, while the tissues heal.</p>

        <p>Avoid strenuous activity (jogging, swimming, bending, any activity that increases your blood pressure) for two to three weeks.</p>

        <p>Avoid hitting or rubbing your nose, or getting it sunburned, for eight weeks. Be gentle when washing your face and hair or using cosmetics.</p>

        <p>You can wear contact lenses as soon as you feel like it, but glasses are another story. Once the splint is off, they&#39;ll have to be taped to your forehead or propped on your cheeks for another six to seven weeks, until your nose is completely healed.</p>

        <h2>Your New Nose:</h2>

        <p>In the days following surgery, when your face is bruised and swollen, it&#39;s easy to forget that you will be looking better. Day by day, your nose will begin to look better . Within a week or two, you&#39;ll no longer look as if you&#39;ve just had surgery.</p>

        <p>Healing is a slow and gradual process. Some subtle swelling may be present for months, especially in the tip. The final results of rhinoplasty may not be apparent for 6 months or more.</p>

    </div>
@endsection

@section('title','Face and neck lift in Tunisia-Dr Djemal: Rhinoplasty in Tunisia')
@section('description','Dr Djemal is practicing Rhinoplasty, face and neck lift in Tunisia')
