@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>Ear Surgery</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>Ear Surgery (Otoplasty)</h2>

        <p>Ear surgery, or otoplasty, is usually done to set prominent ears back closer to the head or to reduce the size of large ears.</p>

        <p>For the most part, the operation is done on children between the ages of seen and 14. Ears are almost fully grown by age four.Ear surgery on adults is also possible, and there are generally no additional risks associated with ear surgery on an older patient.</p>

        <h2>Anesthesia</h2>

        <p>If your child is young, your surgeon may recommend general anesthesia, so the child will sleep through the operation. For older children or adults, the surgeon may prefer to use local anesthesia, combined with a sedative, so you or your child will be awake but relaxed.</p>

        <h2>Surgery</h2>

        <p>Ear surgery usually takes about one hour, although complicated procedures may take longer. The technique will depend on the problem.</p>

        <p>With one of the more common techniques, the surgeon makes a small incision in the back of the ear to expose the ear cartilage. He or she will then sculpt the cartilage and bend it back toward the head. Non-removable stitches may be used to help maintain the new shape. Occasionally, the surgeon will remove a larger piece of cartilage to provide a more natural-looking fold when the surgery is complete.</p>

        <p>Another technique involves a similar incision in the back of the ear. Skin is removed and stitches are used to fold the cartilage back on itself to reshape the ear without removing cartilage.</p>

        <p>In most cases, ear surgery will leave a faint scar in the back of the ear that will fade with time. Even when only one ear appears to protrude, surgery is usually performed on both ears for a better balance.</p>

        <h2>Getting Back to Normal</h2>

        <p>Adults and children are usually up and around within a few hours of surgery.</p>

        <p>The patient&#39;s head will be wrapped in a bulky bandage immediately following surgery to promote the best molding and healing. The ears may throb or ache a little for a few days, but this can be relieved by medication.</p>

        <p>The next day, the bulky bandages will be replaced by a lighter head dressing similar to a headband. Be sure to follow your surgeon&#39;s directions for wearing this dressing, especially at night.</p>

        <p>The stitches dissolve on their own within the first week to 10 days.</p>

        <p>Any activity in which the ear might be bent should be avoided for a month or so. Most adults can go back to work about five days after surgery. Children can go back to school after seven days or so, if they&#39;re careful about playground activity. You may want to ask your child&#39;s teacher to keep an eye on the child for a few weeks.</p>

    </div>
@endsection
@section('title','Face surgery in Tunisia-Dr Djemal: Otoplasty in Tunisia')
@section('description','Dr Djemal practice face surgery and if you have protruding ears he also practice otoplasty in tunisia')
