@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>Hair Transplant</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>Hair Transplant/ micro-grafts</h2>

        <p>It is important to understand that you will never have the coverage you had prior to your hair loss, but surgery may camouflage the thin areas and give you more fullness.</p>

        <p>Healthy hair growth is necessary at the back and sides of the head to serve as donor areas. Donor areas are the places on the head from which grafts and flaps are taken. Some factors, such as hair color, texture and waviness or curliness may also affect the cosmetic result.</p>

        <p>In this procedure, there is a risk that some of the grafts won&#39;t &quot;take.&quot; Although it is normal for the hair contained within the plugs to fall out before establishing regrowth in its new location, sometimes the skin plug dies and surgery must be repeated. At times, patients with plug grafts will notice small bumps on the scalp that form at the transplant sites. These areas can usually be covered with surrounding hair.</p>

        <h2>PROCEDURE</h2>

        <p>Hair transplantation involves removing small pieces of hair-bearing scalp grafts from a donor site and relocating them to a bald or thinning area.</p>

        <p>Grafts differ by size and shape. Several surgical sessions may be needed to achieve satisfactory fullness. Eight months are usually recommended between each session. It may take up to two years before you see the final result with a full transplant series.</p>

        <p>The amount of coverage is partly dependent upon the color and texture of your hair. Coarse, gray or light-colored hair affords better coverage than fine, dark-colored hair. Just before surgery, the &quot;donor area&quot; will be trimmed short so that the grafts can be easily accessed and removed. The stitches are usually concealed with the surrounding hair.</p>

        <p>After the grafting session is complete, the scalp will be cleansed and covered with gauze. You will wear a pressure bandage for a day.</p>

        <p>As you heal, you&#39;ll notice that the scar will be obscured by relocated hair, which grows to the very edge of the incision.</p>

        <h2>After procedure</h2>

        <p>Bandages will be removed one day later. You may gently wash your hair within two days following surgery. Any stitches will dissolve in a week to 10 days.</p>

        <p>You should avoid vigorous exercise and contact sports for at least three weeks.</p>

        <p>Many patients who have had transplants are surprises to find that their &quot;new&quot; hair falls out within six weeks after surgery. This is normal and temporary. After hair falls out, it will take another five to six weeks before hair growth resumes. You can expect about a half-inch of growth per month.</p>

        <p>You may need a surgical &quot;touch-up&quot; procedure to create more natural-looking results after your incisions have healed.</p>

    </div>
@endsection

@section('title','Hair transplant in Tunisia-Dr Djemal: Face surgery in Tunisia')
@section('description','Dr Djemal is a specialist not only in face surgery but also in hair transplant in Tunisia')
