@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>Chin Surgery</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>Chin Surgery (Genioplasty)</h2>

        <p>Chin surgery, also known as mentoplasty or genioplasty is a surgical procedure to reshape the chin either by enhancement with an implant usually silicone or reduction surgery on the bone.</p>

        <p>Many times a plastic surgeon may recommend chin surgery to a patient having nose surgery in order to achieve facial proportion, as the size of the chin may magnify or minimize the perceived size of the nose.</p>

        <p>Chin surgery helps provide a harmonious balance to your facial features so that you feel better about the way you look.</p>


    </div>
@endsection
@section('title','Face surgery in Tunisia-Dr Djemal: Face and neck lift in Tunisia')
@section('description','Dr Djemal is an expert in face surgery, face and neck lift in Tunisia')