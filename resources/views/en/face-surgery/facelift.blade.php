@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">

        <h1 class="page-title"><span class="intervantion">Face Surgery</span>FaceLift</h1>

    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>FACELIFT</h2>

        <p>Ageing can cause the face to change dramatically during middle age and old age.</p>

        <p>Facelift surgery can make you look much younger. A cosmetic surgery facelift raises the facial skin and tissues, changes the layout of the underlying muscle and makes the face tighter and smoother.</p>

        <p>As men and women age the muscles of the face become weaker and slacker, and the skin becomes less elastic.</p>

        <p>This can be corrected by facelift surgery, with the best results gained from people whose face and neck have begun to show signs of ageing but whose skin is still fairly elastic. A good bone structure which defines the face shape well is also a great asset, increasing the success of a cosmetic surgery facelift.</p>

        <p>Many people decide on a facelift between the ages of forty and sixty. The effects can last about ten years.</p>

        <p>A facelift can also be performed on part of the face, such as the brow area, the lower face, or the neck area only. Alternatively, a full face lift is available, and further procedures such as eyelid surgery may be done at the same time to reduce droopy eyelids or eye &lsquo;bags&rsquo; underneath the eyes.</p>

        <h2>FACELIFT SURGERY</h2>

        <p>The facelift operation itself takes at least two to three hours.&nbsp;</p>

        <p>The incisions usually begin above the hairline at the temples, extend in a line in front of the ear and then continue behind the earlobe to the lower scalp</p>

        <p>Facelift surgery involves separating the skin from the fat and muscle beneath. Fat may be removed from around the neck and chin to give a tighter jaw line. The underlying muscle and membranes are tightened, the skin is pulled back with any excess skin removed. This smoothes out wrinkles and gives a much smoother appearance to the skin of the face. The surgeon uses fine sutures to keep the new facial structure in place and to close the skin. Metal clips are sometimes used on the scalp.</p>

        <p>Your face will be wrapped in bandages, which come off after one day. Stitches are removed between five and ten days after surgery.</p>

        <p>Face lift operation rarely cause much pain. You may be advised to use cold compresses to reduce swelling and to rest with your head up. You must also avoid bending, straining, saunas, and massage, and keep your face out of the sun for several weeks.</p>

        <h2>AFTER SURGERY</h2>

        <p>Facelift surgery has some side effects like the bruising during several days .</p>

        <p>Scars normally fade well and are disguised by the hairline. Smoking makes scars heal more slowly, so many surgeons recommend smoking cessation.</p>

    </div>
@endsection

@section('title','Lifting Tunisia-Dr Djemal: Facelift in Tunisia')
@section('description','Dr Djemal is practicing plastic surgery in a well knowen clinic in Tunisia')