@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('en.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Face Surgery</span>Necklift/Mini Lift</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">

        <h2>FACELIFT (RHYTIDECTOMY)</h2>

        <p>Time plays havoc with the skin, especially the skin on your face and neck.</p>

        <p>Deep creases form between the nose and mouth. The jawline grows slack and jowly. Folds and fat deposits appear around the neck.</p>

        <p>You can improve the most visible signs of ageing by removing excess fat, tightening underlying muscles, and redraping the skin of your face and neck.</p>

        <p>It&rsquo;s designed to smooth the loose skin on your face and neck, tighten underlying tissue, lift muscle and remove excess fat.</p>

        <p>The result: your face will look firmer, fresher, and younger.</p>

        <h2>SURGERY</h2>

        <p>A facelift can be done alone, or in conjunction with other procedures such as eyelid surgery, or nose reshaping.</p>

        <p>Facelift surgery requires a general anesthesia.</p>

        <h2>BEFORE SURGERY</h2>

        <p>During the consultation, you will be asked about the results you would like to achieve from a facelift.</p>

        <p>Your surgeon will assess the thickness, texture and elasticity of your skin, and the severity of the wrinkles and folds. Your hairline will be examined to determine where incisions can be discreetly placed. All of these factors, as well as your bone structure and underlying tissue, will be considered in developing a personal treatment plan.</p>

        <h2>SURGERY</h2>

        <ol>
            <li>
                <p>Generally, an incision is hidden in the natural contour of your ear, and then extends around the earlobe and back into the hairline.</p>
            </li>
            <li>
                <p>Through these discreet incisions, your surgeon will free the skin from the underlying tissue.</p>
            </li>
            <li>
                <p>After the skin has been pulled up and back, the excess is removed. In some instances, the deeper tissues may also need to be repositioned.</p>
            </li>
        </ol>

        <p>The procedure may also include the removal of fatty tissue from the area underneath the chin.</p>

        <h2>AFTER SURGERY</h2>

        <p>Generally, the greatest amount of swelling occurs 24 to 48 hours after surgery, but it may take several weeks before all the puffiness is resolved.</p>

        <p>Most bruising will disappear within two weeks. After a few days, you will be permitted to wear make-up, which will help to conceal any discoloration. You will also experience some numbness in the facial area, which may be present for several weeks. Stitches are usually removed in stages over a period of 5-10 days. But most stitches dissolve on their own within the first week to 10 days.</p>

        <h2>RESULTS</h2>

        <p>The results of your facelift will be subtle. Since the healing process is gradual, you should expect to wait several weeks for an accurate picture of your &quot;new look&quot;.</p>


    </div>
@endsection

@section('title','Face surgery in Tunisia-Dr Djemal: Face and neck lift in Tunisia')
@section('description','Dr Djemal is an expert in face surgery, face and neck lift in Tunisia')