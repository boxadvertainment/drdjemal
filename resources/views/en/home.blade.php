@extends('en.layout-en')

@section('class', 'home')

@section('header')
<header class="header">

      @include('en.partials.header')

      <div class="container">

        <div class="consultation-form col-md-3 col-sm-6 col-xs-8">
          <form id="consultation-form" action="{{ action('AppController@submitConsultation') }}" method="post">
            <div class="title">
              <img src="img/title-form-en.png" alt="" />
              <br>
              <br>
            </div>
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Name & Surname"  required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="Email" required  title="incorrect Email">
            </div>
            <div class="form-group">
              <input type="tel" name="phone" class="form-control" placeholder="Phone" required title="Incorrect Phone Number">
            </div>
            <!-- Country Select from bootstrap Helpers-->
            <div class="form-group">
              <div class="bfh-selectbox bfh-countries" data-country="TN" data-flags="true" data-filter="true"></div>
            </div>
            <!-- End Country Select from bootstrap Helpers-->
            <div class="form-group">
              <div class="select-wrapper">
                <select class="form-control" name="intervention" required>
                  <option value="">Surgeries</option>
                    @foreach(Config::get('app.SURGERIES') as $label => $group)
                        <optgroup label="{{ $label }}">
                            @foreach($group as $item => $intervention)
                                <option value="{{ $item }}">{{ $intervention }}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <textarea name="message" class="form-control" rows="3" placeholder="Message" required></textarea>
            </div>

            <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha.sitekey')}}" style="display: flex; justify-content: center; margin-bottom: .5rem"></div>
            
            <div class="form-group">
              <button type="submit" name="submit" class="form-control submit"> <i class="fa fa-check"></i> Submit </button>
            </div>
          </form>
        </div>
      </div>

      <div id="banner" class="carousel slide" data-ride="carousel" data-wrap="false">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="img/bg-banner-en.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/bg-banner2-en.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/bg-banner3-en.jpg" alt="...">
          </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#banner" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left fa fa-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#banner" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right fa fa-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </header>
@endsection

@section('content')
    <div class="presentation text-center">
      <div class="container">
        <h1 class="title">Dr Taher Djemal</h1>
        <div class="sub-title"><span>PRESENTATION</span></div>
          <p>For Aesthetic plastic  surgery in Tunisia,Dr. Djemal is a leading expertise in his practice advocating cosmetic surgery excellence, innovation and a high level of safety.</p>
          <p>President of the association of plastic surgeons in Tunisia and member of the international society of Plastic surgeons ISAPS. He regularly delivers lectures at national and international conferences.</p>
          <p>Dr. Djemal is very sensitive to his patients needs and takes the time to make sure they are well informed about their choices. He believes that he doesn't only help you look younger but also help you enhance your natural beauty.</p>
          <a href="{{ url('cv-en') }}" class="btn"> Read More</a>
      </div>
    </div>

    <div class="operation">
      <div class="container">
        <a href="{{ url('body-surgery') }}" class="operation-item">
            <img src="img/bloc1.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">BODY SURGERY</h2>
              <span class="read-more">Read More</span>
            </div>
            <div class="mask">
                <div>BODY SURGERY</div>
            </div>
        </a>
        <a href="{{ url('aesthetic-treatments') }}" class="operation-item">
            <img src="img/bloc2.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">AESTHETIC TREATMENTS</h2>
              <span class="read-more">Read More</span>
            </div>
            <div class="mask">
                <div>AESTHETIC TREATMENTS</div>
            </div>
        </a>
        <a href="{{ url('breast-surgery') }}" class="operation-item">
            <img src="img/bloc3.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">BREAST SURGERY</h2>
              <span class="read-more">Read More</span>
            </div>
            <div class="mask">
                <div>BREAST SURGERY</div>
            </div>
        </a>
        <a href="{{ url('face-surgery') }}" class="operation-item">
            <img src="img/bloc4.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">FACE SURGERY</h2>
              <span class="read-more">Read More</span>
            </div>
            <div class="mask">
                <div>FACE SURGERY</div>
            </div>
        </a>
        <a href="{{ url('plastic-and-reconstructive-procedures') }}" class="operation-item">
            <img src="img/bloc5.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">Plastic and Reconstructive Procedures</h2>
              <span class="read-more">Read More</span>
            </div>
            <div class="mask">
                <div>Plastic and Reconstructive Procedures</div>
            </div>
        </a>
      </div>
    </div>

    <div class="info-section">
      <div class="container">
        <a href="http://blog.dr-djemal.com/" target="_blank">
          <div class="blog-bloc bloc">
            <h2 class="title">BLOG</h2>
            <p>View articles and news about plastic surgery in Tunisia</p>
          </div>
        </a>
        <a href="{{ url('contact-en') }}">
          <div class="contact-bloc bloc">
            <h2 class="title">CONTACT US</h2>
            <p>Phone: (+216) 97 400 029
                contact@dr-djemal.com</p>
          </div>
        </a>
      </div>
    </div>
@endsection

@section('title','Chirurgie esthétique en Tunisie  - Dr Djemal : Intervention esthétique en Tunisie')
@section('description','Vous envisagez une chirurgie esthétique en Tunisie ? Dr Djemal, chirugien esthétique de renommée réaliser votre chirurgie esthétique en Tunisie')
