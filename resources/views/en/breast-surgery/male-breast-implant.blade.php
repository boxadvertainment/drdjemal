@extends('en.innerLayout-en')

@section('class', 'home')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
          <h1 class="page-title"><span class="intervantion">BREAST SURGERY : </span> PECTORAL BODY IMPLANTS</h1>
      </div>
    </header>
@endsection

@section('innerContent')

    <div class="content">

        <h2>PECTORAL BODY IMPLANTS</h2>

        <p>Pectoral implants may be an excellent option for men who, even though they exercise frequently, wish to further shape or enhance their chests for the defined, muscular appearance they desire.</p>

        <p>Pec implants shape, enlarge, and firm the chest muscles. Pectoral body implants help patients achieve the appearance they desire while still allowing for a natural feel and appearance.</p>

        <p>Your plastic surgeon will make an incision in your armpit along the edge of the muscle. A space will be created beneath the pectoral muscle down to its lower edge. The implant is placed in this pocket, and the incision is closed.</p>

        <p>Sometimes, liposuction may be performed in conjunction with your pectoral implant procedure in order to achieve better definition in the region. This is an ideal option if you have excess fat in the chest area which needs to be removed before your pectoral implants are inserted.</p>

        <p>An elastic garment is worn for 3 to 4 weeks post op.This will be your dressing garment . After that, it can be discarded.</p>

        <p>Bruising may persist for a week or so. Hardness or lumpiness within the tissues due to swelling can persist for a month or so.</p>

        <p>The stitches dissolve on their own within the first week to 10 days.</p>

        <p>You should resume physical activity as soon as you are able to, but it is important to limit vigorous arm movement and light lifting for a week following surgery. Afterwards, you may gradually increase your level of activity over the next three weeks. Full activity may be resumed after one month. Before this, daily physical activity should primarily consist of walking either outside or on a treadmill, riding an exercise bike, or bicycling out-of-doors.</p>

    </div>
      <!-- /.content -->
@endsection

@section('title','Breast prostheses in Tunisia-Dr Djemal: Breast surgery in Tunisia')
@section('description','You want to have breast protheses or a breast surgery, consult Dr Djemal the best in its fields')
