@extends('en.innerLayout-en')

@section('class', 'home')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
          <h1 class="page-title"><span class="intervantion">BREAST SURGERY : </span> GYNAECOMASTIA</h1>
      </div>
    </header>
@endsection

@section('innerContent')

    <div class="content">

        <h2><strong>Male Breast Reduction (GYNAECOMASTIA)</strong></h2>

        <p>Enlarged male breasts (Gynaecomastia) are a far more common problem than you may imagine.</p>

        <p>It can cause untold embarrassment and heartache, especially if it occurs during adolescence. Males with this condition will go to any lengths to avoid situations in which they may have to expose their chests, such as playing sports, swimming or sunbathing, sometimes withdrawing completely from social activity.</p>

        <h2><strong>What does male breast reduction involve?</strong></h2>

        <p>The procedure removes fat and or glandular tissue from the breasts and in extreme cases removes excess skin. The surgery is carried out under general anesthetic and you would be required to stay in hospital for one night.</p>

        <h2><strong>The Surgery</strong></h2>

        <p>Two methods are available for this procedure, and sometimes a combination is used.</p>

        <ol>
            <li>
                <p>By making a half moon incision around or below the areola and cutting away the unwanted breast tissue.</p>
            </li>
            <li>
                <p>the surgeon is able to break up and draw off excess fat through a fine cannula. The breast fat is approached from a tiny incision towards the armpit or under the nipple.<br />
                    This incision heals well and is difficult to see</p>
            </li>
        </ol>

        <p>Bruising may persist for a week or so. Hardness or lumpiness within the tissues due to swelling can persist for a month or so.</p>

        <p>The stitches dissolve on their own within the first week to 10 days.</p>

        <h2><strong>After Surgery</strong></h2>

        <p>Following treatment, the breast tissue does not grow back but the swelling can take up to six months to settle completely. The operation is very successful in that it is a permanent.</p>

        <p>Many patients return to work between one and two weeks following surgery. Lifting should be avoided for about 14 days. Sport, particularly contact sport, should be avoided for at least one month. This includes swimming. Gentle exercise such as walking is recommended.</p>

    </div>
      <!-- /.content -->
@endsection
@section('title','Breast surgery in Tunisia-Dr Djemal: Gynecomastia in Tunisia')
@section('description','Dr Djemal is practicing Breast surgery and gynecomastia in Tunisia')