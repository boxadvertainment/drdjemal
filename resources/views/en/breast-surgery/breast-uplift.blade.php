@extends('en.innerLayout-en')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
          <h1 class="page-title"><span class="intervantion">BREAST SURGERY : </span> Breast uplift (Mastopexy)</h1>
      </div>
    </header>
@endsection

@section('innerContent')

    <div class="content">

        <h2>Breast uplift (Mastopexy)</h2>
        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/uplift/uplift1.png') }}" alt="">
            </div>
            <p>Over the years, factors such as pregnancy, nursing, and the force of gravity take their toll on a woman's breasts. As the skin loses its elasticity, the breasts often lose their shape and firmness and begin to sag. </p>
            <p>Breast lift can also reduce the size of the areola. If your breasts are small or have lost volume, breast implants are inserted in conjunction with mastopexy.</p>

            </div>
        <h2>Planning Surgery</h2>

        <p>The surgeon will discuss the variables, such as your age, the size and shape of your breasts, and the condition of your skin, and whether an implant is advisable.</p>

        <p>Your breast uplift will be performed in a clinic .You can expect to stay one day..</p>

        <h2>Anesthesia</h2>
        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/uplift/uplift2.png') }}" alt="">
            </div>
        <p>Breast uplifts are performed under general anesthesia and it takes two hours.</p>

        <p>The incision outlines the area from which breast skin will be removed and defines the new location for the nipple. When the excess skin has been removed, the nipple and areola are moved to the higher position. The skin surrounding the areola is then brought down and together to reshape the breast. Stitches are located around the areola and in a vertical line extending downwards from the nipple area.</p>

        <p> Some patients with relatively small breasts and minimal sagging will require less extensive incisions. One such procedure is the "doughnut (or concentric) mastopexy," in which circular incisions are made around the areola.</p>
            <div class="col-xs-9">
        <p> If you&#39;re having an implant inserted along with your breast lift, it will be placed in a pocket directly under the breast tissue, or deeper, under the muscle of the chest wall.</p>
        </div>
        </div>
        <h2>After Surgery</h2>
        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/uplift/uplift2.png') }}" alt="">
            </div>
        <p>Your breasts will be bruised, swollen, and uncomfortable for a day or two.</p>

        <p>You&#39;ll need to wear a bra around the clock for three to four weeks. The stitches dissolve on their own within the first week to 10 days.</p>

        <p>You can expect some loss of feeling in your nipples and breast skin, caused by the swelling after surgery. This numbness usually fades as the swelling subsides over the next six weeks or so. In some patients, however, it may last a year .</p>
        </div>
        <h2>Getting Back to Normal</h2>

        <p>Avoid lifting anything over your head for three to four weeks. Your surgeon will give you detailed instructions for resuming your normal activities. You may be instructed to avoid strenuous sports for about a month</p>

        <p>It is important to remember that mastopexy scars are extensive and permanent. They often remain lumpy and red for months, then gradually become less obvious, sometimes eventually fading to thin white lines. Fortunately, the scars can usually be placed so that you can wear even low-cut tops.</p>

    </div>
      <!-- /.content -->
@endsection

@section('title','Mammal augmentation in Tunisia-Dr Djemal: Breast reconstruction Tunisia')
@section('description','You can have a mammal augmentation and a breast reconstruction you would better consult Dr Djemal')