@extends('en.innerLayout-en')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">BREAST SURGERY : </span>BREAST REDUCTION</h1>
      </div>
    </header>
@endsection

@section('innerContent')

    <div class="content">



        <h2><strong>BREAST REDUCTION</strong></h2>

        <p>Breast reduction is for every woman who wants her breasts to be proportional to the rest of her body and, therefore, more comfortable.</p>

        <ul>
            <li>
                <p>Breasts that are too large in proportion to your body frame</p>
            </li>
            <li>
                <p>Heavy pendulous breast with nipples that point downwards</p>
            </li>
            <li>
                <p>Skin irritation beneath your breasts</p>
            </li>
            <li>
                <p>One breast that is much larger than the other</p>
            </li>
            <li>
                <p>Back, neck or shoulder pain caused by the weight of your breasts</p>
            </li>
            <li>
                <p>Indentations in your shoulders from straining bra straps</p>
            </li>
            <li>
                <p>Restrictions of physical activity due to the size and weight of your breasts</p>
            </li>
        </ul>

        <p>The most common method involves two incisions:</p>

        <ul>
            <li>
                <p>Around the areola</p>
            </li>
            <li>
                <p>Vertically from the bottom edge of the areola to the crease underneath the breast</p>
            </li>
        </ul>

        <p>After your surgeon has removed excess breast tissue, fat and skin, the nipple and areola are moved to a higher position.</p>

        <h2><strong>Anesthesia</strong></h2>

        <p>Breast reduction requires a general anesthetic. You will spend one&nbsp;night in hospital.</p>

        <h2><strong>After Surgery</strong></h2>

        <ul>
            <li>
                <p>You will need to wear a support bra for up to six weeks, until the swelling and bruising subsides.</p>
            </li>
            <li>
                <p>You should avoid any straining, bending and lifting for at least four weeks following surgery.</p>
            </li>
            <li>
                <p>The incisions from the procedure should fade over time. Fortunately, the incisions are in locations easily concealed by clothing, even low-cut necklines.</p>
            </li>
            <li>
                <p>The stitches dissolve on their own within the first week to 10 days.</p>
            </li>
        </ul>

        <h2><br />
            &nbsp;<strong>Getting Back to Normal</strong></h2>

        <p>Depending on the extent of your breast reduction and your general physical condition, you may be able to return to non-strenuous work any where from two to three weeks after surgery. Indeed, many women are able to resume most of their normal activities, including some form of mild exercise, after just four weeks.</p>

        <h2><strong>THE RESULTS</strong></h2>

        <p>Breast reduction surgery is to make your breasts smaller and firmer. Without the excessive weight of large breasts, you are likely to find greater enjoyment than before in playing sports and engaging in other physical activity.</p>


        <!--
        Note: This demo uses ​​CKFinder to upload and manage files.

        CKEditor Enterprise
        Get a license with a full year of support and upgrades and have legal peace of mind. Learn more
        CKFinder
        Add our powerful and user friendly Ajax file manager to CKEditor.Learn more
        Enterprise Solutions
        CKSource custom solutions are designed for large projects requiring additional non-standard options. Receive end-to-end project care related to CKEditor Premium and CKFinder.
        Learn more

        Site Audit
        Development
        Assistance
        Trainings
        SLA
        SpellChecker
        114 173
        REGISTERED DEVS
        141 166
        LINES OF CODE
        16 087 313
        NUMBER OF DOWNLOADS
        824 748
        COFFEE CUPS CONSUMED
        OPEN SOURCE
        © 2003 - 2016 CKSource Sp. z o.o. sp.k. All rights reservedTerms of usePrivacy PolicyProud to be Open Source
        This website uses cookies. By using this site, you consent to their usage according to your browser’s current settings.
        -->


    </div>
      <!-- /.content -->
@endsection
@section('title','Breast reconstruction Tunisia-Dr Djemal: Breast surgery in Tunisia')
@section('description','Looking for a breast surgery or a breast reconstruction, consult Dr Djemal the best in its field')