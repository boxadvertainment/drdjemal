@extends('en.innerLayout-en')

@section('class', 'home')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
          <h1 class="page-title"><span class="intervantion">BREAST SURGERY : </span>BREAST AUGMENTATION</h1>
      </div>
    </header>
@endsection

@section('innerContent')

    <div class="content">
        <h2>BREAST AUGMENTATION</h2>

        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/augmentation/img1.jpg') }}" alt="">
            </div>

        <p>Breast enlargement is a surgical procedure to enhance the size and shape of a woman&#39;s breast for a number of reasons:</p>

        <ul>
            <li>
                <p>To enhance the body contour of a woman who, for personal reasons, feels her breast size is too small.</p>
            </li>
            <li>
                <p>To restore breast volume lost due to weight loss or following pregnancy</p>
            </li>

            <li>
                <p>To achieve better symmetry when breasts are moderately disproportionate in size and shape</p>
            </li>
            <li>
                <p>To improve the shape of breasts that are sagging or have lost firmness, often used with a breast lift procedure</p>
            </li>
            <li>
                <p>To provide the foundation of a breast contour when a breast has been removed or disfigured by surgery to treat breast cancer</p>
            </li>
            <li>
                <p>To improve breast appearance or create the appearance of a breast that is missing or disfigured due to trauma, heredity, or congenital abnormalities</p>
            </li>

        </ul>

        </div>

        <h2>BREAST PROSTHESES/IMPLANTS</h2>

        <p>The choice of implant size, shape and other features will be determined based on your breast anatomy, body type and your desired increase in size. Your lifestyle, goals and personal preferences, as well as your plastic surgeon&#39;s recommendations.</p>

        <p>Breast implants are filled with elastic silicone gel.</p>

        <p>You should be aware that breast implants are not guaranteed to last a lifetime and future surgery may be required to replace one or both implants. Regular examinations for breast health and to evaluate the condition of your implants are important.</p>

        <p>The most common problem, capsular contracture, occurs if the scar or capsule around the implant begins to tighten.</p>

        <p>Some women report that their nipples become oversensitive, under sensitive or even numb. These symptoms usually disappear within time, but may be permanent in some patients.</p>

        <p>Breast implants do not generally interfere with a woman&#39;s ability to breast feed, or present a health hazard during pregnancy to a woman or her baby.</p>

        <p>Following the placement of breast implants mammography is technically more difficult.</p>

        <h2>Anesthesia</h2>

        <p>Breast augmentation can be performed with a general anesthesia, so you&#39;ll sleep through the entire operation.</p>

        <h2>The Surgery</h2>

        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/augmentation/img3.jpg') }}" alt="">
            </div>

        <p>The method of inserting and positioning your implant will depend on your anatomy and your surgeon&#39;s recommendation. The incision can be made either in the crease where the breast meets the chest, around the areola (the dark skin surrounding the nipple), or in the armpit. Every effort will be made to assure that the incision is placed so resulting scars will be as inconspicuous as possible.</p>

        <p>The surgery usually takes one hour to complete.</p>
        </div>

        <h2>After Surgery</h2>
        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/augmentation/img4.jpg') }}" alt="">
            </div>
        <p>You should wear surgical bra during 4 weeks. You may also experience a burning sensation in your nipples for about two weeks, but this will subside as bruising fades.</p>

        <p>Your stitches will come out in a week to 10 days, but the swelling in your breasts may take three to five weeks to disappear.</p>
        </div>
        <h2>Getting Back to Normal</h2>

        <p>You should be able to return to work within a few days, depending on the level of activity required for your job.</p>

        <p>Your breasts will probably be sensitive to direct stimulation for two to three weeks, so you should avoid much physical contact. After that, breast contact is fine once your breasts are no longer sore, usually three to four weeks after surgery.</p>

        <p>Your scars will be firm and pink for at least six weeks. Then they may remain the same size for several months, or even appear to widen. After several months, your scars will begin to fade.</p>

      </div>
      <!-- /.content -->
@endsection

@section('title','Breast surgery in Tunisia-Dr Djemal: Mammal augmentation in Tunisia')
@section('description','You can have a mammal augmentation and a breast reconstruction you would better consult Dr Djemal')