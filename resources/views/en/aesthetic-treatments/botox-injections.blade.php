@extends('en.innerLayout-en')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Aesthetic Treatments : </span> BOTOX INJECTIONS</h1>
      </div>
    </header>
@endsection

@section('innerContent')

    <div class="content">
        <h2>BOTOX INJECTIONS</h2>

        <div class="row">
            <div class="col-xs-3">
                    <img src="{{ asset('/img/box_injection/image_botox1.jpg') }}" alt="">
            </div>
            <div class="col-xs-9">
                <p>
                    BOTOX,the cosmetic form of botulinum toxin, is a popular non-surgical injection that temporarily reduces or eliminates frown lines, forehead creases, crows feet near the eyes and thick bands in the neck.
                </p>
            </div>
        </div>

        <hr/>
        <div class="row">
            <div class="col-xs-3">
                <img src="{{ asset('/img/box_injection/image_botox2.jpg') }}" alt="">
            </div>
            <div class="col-xs-9">
                <p>
                    The toxin blocks the nerve impulses, temporarily paralyzing the muscles that cause wrinkles while giving the skin a smoother, more refreshed appearance.
                    Studies have also suggested that Botox is effective in relieving migraine headaches, excessive sweating and muscle spasms in the neck and eyes and even nasal muscles (to decrease nostril flaring).
                </p>
            </div>
        </div>

        <h2>Botulinum Toxin Injections</h2>

        <p>BOTOX injections have proven to be a very popular nonsurgical cosmetic procedure. Surgeons have found that the type of lines and wrinkles that respond to BOTOX injections are those caused by the muscles-specifically those muscles that contract during facial expressions such as frowning or squinting.</p>

        <h2>Technique:</h2>

        <p>The patient is asked to contract the muscles in the area being treated so the surgeon can determine the proper location for injection. It is injected directly into the muscle with a tiny needle. It takes a few days to realize the effect of the injections.</p>

        <h2>Benefits:</h2>

        <ul>
            <li>
                <p>Long experience has proven BOTOX to be safe.</p>
            </li>
            <li>
                <p>Treatment is reversible within several months.</p>
            </li>
            <li>
                <p>May be beneficial for treatment of migraine headaches.</p>
            </li>
            <li>
                <p>BOTOX injections are a temporary solution for the treatment of wrinkles. The effects last from 6 to 8 months and require repeat treatments.</p>
            </li>
        </ul>

        <p>Possible side effects include local numbness, swelling, bruising, or a burning sensation during injection. Some patients have reported temporary headache and nausea</p>

        <p>Patients, who show early signs of aging, as well as those who may not be suitable candidates for more extensive aesthetic facial surgery, may be good candidates for this procedure. Certain medications (some antibiotics, anti-inflammatories, or aspirin) and even some vitamins and herbs may increase the potency of BOTOX and may increase bleeding and bruising at the time of injection.</p>

    </div>
      <!-- /.content -->
@endsection

@section('title','Cosmetic surgery in Tunisia-Dr Djemal: Botox Tunisia')
@section('description','Dr Djemal is not only practicing cosmetic surgery but also botox in Tunisia')
