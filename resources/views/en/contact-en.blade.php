@extends('en.innerLayout-en')

@section('class', 'page contact-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
          <h1 class="page-title"><span class="intervantion">Do not hesitate to</span> contact us</h1>
      </div>
</header>
@endsection

@section('innerContent')

    <div class="content">
        <h2 class="content-title">Contact Us</h2>

        <p>If you have questions or for more information, please contact us.</p>
      </div>
            <ul class="phone-list list-unstyled list-inline">
              <li class="clearfix"><i class="fa fa-map-marker"></i> Rue de la feuille d’érable, Résidence la brise du lac 1053 Les Berges du Lac 2 Tunis, Tunisie</li>
              <li><i class="fa fa-phone-square"></i> <a href="#">(+216) 97 400 029</a></li>
              <li><i class="fa fa-envelope"></i> <a href="mailto:contact@dr-djemal.com">contact@dr-djemal.com</a> </li>
              </ul>
            <form id="" action="{{ url('contact') }}" class="contact-form" method="post">
                @if (Request::session()->has('success'))
                <div class="col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0">
                    <div class="alert alert-success" role="alert">Votre message a bien été envoyé</div>
                </div>
                @endif
                {!! csrf_field() !!}
              <div class="col-md-6 col-xs-8 col-xs-offset-2 col-md-offset-0">
                <div class="form-group">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Name & Surname" required title="Field Required">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required  title="Email incorrect">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject"  required title="Field Required">
                </div>
              </div>
              <div class="col-md-6 col-xs-8 col-xs-offset-2 col-md-offset-0">
                <div class="form-group">
                  <textarea name="message" class="form-control" rows="6" id="message" name="message" placeholder="Message" required title="Field Required"></textarea>
                </div>
              </div>
              <div class="col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0">
                <div class="form-group">
                  <button type="submit" class="form-control" id="submit"> <i class="fa fa-check"></i> Submit </button>
                </div>
              </div>
            </form>
        @foreach($errors->all() as $error)
        <p class="alert alert-danger">{{$error}}</p>
        @endforeach

      <!-- /.content -->
@endsection

@section('title','Clinic in aesthetic surgery in Tunisia-Dr Djemal: Cosmetic Surgery Prices Tunisia')
@section('description','If you want to know cosmetic surgery prices in Tunisia visit our clinic aesthetic surgery in Tunisia where Dr Djemal is practicing')
