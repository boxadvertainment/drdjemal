@extends('en.innerLayout-en')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2 class="content-title">BODY SURGERY</h2>
        <p>Many people have an area of their body that they are unhappy with.</p>
        <p>Body procedures can improve the shape of your body and tighten excess or loose skin .</p>
        <p>Our surgeons have been performing liposuction, tummy tuck, thigh and arm lifts, buttocks injections… and many other body procedures. There are many different reasons (ex: the normal effects of aging or childbearing) why people choose cosmetic surgery.</p>
    </div>
@endsection
@section('title','Chirurgie De La Silhouette - Dr Djemal : Chirurgie esthétique Tunisie')
@section('description','Les interventions de chirurgie de la silhouette permettent de redessiner et  remodeler certaines parties du corps telles que le ventre, l’abdomen, les hanches, les fesses, le dos...')