@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-dr.jpg);">

    @include('en.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">Plastic Surgeon</span> DR Taher Djemal</h1>
    </div>
  </header>
@endsection

@section('innerContent')
    <div class="content">
      <h2 class="content-title">Presentation</h2>
      <div class="picutre-wrapper pull-left">
        <img src="img/dr-image.png" alt="">
      </div>
      <div class="description">
        <p>Dr DJEMAL operates from the very best clinics in Tunis.</p>
        <p>He uses an experienced staff and works with a registered specialist anaesthesiologist.</p>
        <p>Dr DJEMAL maintains a high ethical standard and keeps himself well informed of the latest surgical techniques from around the world.</p>
        <p>His skills as a plastic and reconstructive surgeon are internationally acknowledged.</p>
      </div>
    </div>
    <!-- /.content -->

      <div class="tabs-wrapper">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#parcours" aria-controls="parcours" role="tab" data-toggle="tab">Career</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Diplomas</a></li>
          <li role="presentation"><a href="#medias" aria-controls="medias" role="tab" data-toggle="tab">Magazines</a></li>
          <li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Media</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="parcours">

            <h3>Educations</h3>
            <ul>
              <li><b>1983   : </b>Bachelor of General Medicine (Tunis)</li>
              <li><b>1988   : </b>Bachelor of Cosmetic, Reconstructive and Facial Surgery (Tunis)</li>
              <li><b>1990   : </b>Bachelor of the French College of Cosmetic Surgery (Paris) </li>
              <li><b>1990   : </b>Bachelor of Cosmetic and Facial Surgery - University of Medicine - Nancy, France.</li>
            </ul>

            <h3>Training Abroad</h3>
            <ul>
              <li><b>From 1990 to 1992:</b> Cosmetic surgeon at the “Foch” hospital in Paris and at the Nancy hospital (France).</li>
              <li><b>1994   : </b>Plastic surgeon in a private clinic in Baltimore, USA for three months.</li>
            </ul>

            <h3>Professional experience</h3>
            <ul>
              <li><b>1992 : </b>Opening of a successful private practice in Tunis..
                <p>Dr has wide experience and his skills cover all areas of cosmetic Surgery, (facial surgery, liposuction, breast surgery, hair graft…).</p>

                <p>As well as performing operations on his private Tunisian and foreign patients, Dr DJEMAL operates on difficult cases of face and body reconstruction in the hospitals of Tunis.</p>

                <p>Thanks to his skills and to his generosity, hundreds of children and adults affected by congenital malformations, accidents or disease, have regained access to a more normal life...</p>
              </li>
            </ul>

            <h3>Professional Associations</h3>
            <ul>
              <li><b>From 1998 to 2004 : </b>General Secretary of the Tunisian Society of Cosmetic Surgeons.</li>
              <li><b>Since 2004   : </b>President of the Tunisian Society of Cosmetic Surgeons.
              <li><b>2010  : </b>Member of the ISAPS.
              <li>Member of the Mediterranean Society of Cosmetic Surgeons.</li>
              <li>Organisation of seminars and conferences during which Dr DJEMAL performs live surgery in Germany, Italy, Spain, Portugal, Martinique and the Middle East...</li>
              <li>For Aesthetic plastic surgery in Tunisia, Dr. Djemal is a leading expertise in his practice advocating cosmetic surgery excellence, innovation and a high level of safety.</li>
              <li>He regularly delivers lectures at national and international conferences.</li>
              <li>He is been in practice for over twenty five years and with his new clinic The Myron International clinic and a team of highly trained medical expertise, he became a prestigious name in the field of cosmetic surgery and skin rejuvenation.</li>
              <li>His practice ranges from Face lift, eyelid surgery, liposuction, tummy tuck, breast surgery, hair graft and so on.</li>
              <li>His skills as a plastic and reconstructive surgeon are internationally acknowledged.</li>
            </ul>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="profile">
            <div class="col-md-6"><a href="{{ asset('img/about/certif1.jpg') }}" class="galerie"><img src="{{ asset('img/about/certif1.jpg') }}" width="100%"></a></div>
            <div class="col-md-6"><a href="{{ asset('img/about/certif2.jpg') }}" class="galerie"><img src="{{ asset('img/about/certif2.jpg') }}" width="100%"></a></div>
            <div class="col-md-6"><a href="{{ asset('img/about/certif3.jpg') }}" class="galerie"><img src="{{ asset('img/about/certif3.jpg') }}" width="100%"></a></div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="medias">
            <div class="row">
              <div class="col-md-4"><a href="{{ asset('img/about/mag1.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag1.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag2.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag2.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag3.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag3.jpg') }}" width="100%"></a></div>
             </div>
            <div class="row">
              <div class="col-md-4"><a href="{{ asset('img/about/mag4.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag4.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag5.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag5.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag6.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag6.jpg') }}" width="100%"></a></div>
            </div>
            <div class="row">
              <div class="col-md-4"><a href="{{ asset('img/about/mag7.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag7.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag8.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag8.jpg') }}" width="100%"></a></div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="videos">
            <div class="row">
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/5G9WvcAc3A8?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/3Vr8WrqA65U?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/vjiB78k8_D0?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/rsGHUQLXQs?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/HRi8BzILxk4?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>


        </div>
      </div>

@endsection

@section('title','Aesthetic intervention in Tunisia-Dr Djemal: Plastic surgery in Tunisia')
@section('description',' Dr Djemal is practicing plastic surgery in a well knowen clinic in Tunisia  ')
