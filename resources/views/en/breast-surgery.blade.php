@extends('en.innerLayout-en')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('innerContent')
    <div class="content">
              <h2 class="content-title">BREAST SURGERY</h2>
<p>There are three general categories of cosmetic surgery performed on the breasts: breast augmentation, breast reduction, and breast reconstruction. To increase the size; breast-implant or fat-grafting can be the solution.
    Male breast reduction: this procedure removes fat and or glandular tissue from the breasts and in extreme cases removes excess skin.</p>
    </div>
@endsection
@section('title','Chirurgie Des Seins - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','La chirurgie des seins reste une solution pour réaliser rapidement une poitrine ferme, épanouie et vaincre ses complexes. Pour augmenter le volume, l’augmentation mammaire...')