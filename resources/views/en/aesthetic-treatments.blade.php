@extends('en.innerLayout-en')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">
    @include('en.partials.header')
    <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span>Interventions</h1>
    </div>
</header>
@endsection

@section('innerContent')
    <div class="content">
        <h2 class="content-title">Non-surgical treatments: aesthetic treatments</h2>
        <p>They are less invasive, do not require the extensive recovery period and have a much lower risk of side effects. The results tend to last for a shorter period of time than surgical results and are not able to change the features and skin of the face as a traditional procedure. Considered as an excellent cosmetic option for men and women who seek aesthetic enhancement of minor facial imperfections.</p>
    </div>
@endsection
@section('title','Medecine Esthetique - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','Les soins esthétiques permettent de remédier de manière douce et précoce, lorsque surviennent les premiers signes de vieillissement. Ils interviennent sur les rides en les comblant au moyen de produits...')