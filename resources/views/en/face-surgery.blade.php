@extends('en.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('innerContent')
    <div class="content">
              <h2 class="content-title">FACE SURGERY</h2>
<p>Various facial plastic surgery procedures performed by our surgeons. Face surgery specializes in treating many diseases, injuries and defects in the head, neck, face, jaws and the hard and soft tissues of the oral (mouth) and Maxillofacial (jaws and face) region.</p>
    </div>
@endsection
@section('title','Chirurgie Du Visage - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','Plusieurs interventions permettent de rafraichir et de rajeunir le visage : le lifting cervico-facial ou complet retend la peau relâchée. La rhinoplastie donne un nez plus harmonieux et la blépharoplastie...')