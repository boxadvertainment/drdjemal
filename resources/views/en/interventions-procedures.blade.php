@extends('en.innerLayout-en')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title">PROCEDURES / SURGERIES</h1>
      </div>
    </header>
@endsection

@section('innerContent')
    <div class="content">
        <h2 class="content-title">PROCEDURES / SURGERIES</h2>
        <h3>Breast Surgeries</h3>
        <ul>
            <li>
                <p>BREAST AUGMENTATION to enhance the size and shape</p>
            </li>
            <li>
                <p>Breast reduction</p>
            </li>
            <li>
                <p>Breastlift, or mastopexy: procedure to raise and reshape sagging breasts</p>
            </li>
            <li>
                <p>Protracting inverted nipples</p>
            </li>
        </ul>

        <p>GYNAECOMASTIA: MALE BREAST REDUCTION<br />
            &nbsp;</p>

        <h2>FACE SURGERIES</h2>

        <ul>
            <li>
                <p><a href="https://proxy-nl.hide.me/go.php?u=WCVCqi%2BA7mmoWuu4lxdglA90sDX38IeDe4XPWnsx2eNDYLWjhmXmXtOLhmIkN%2Bjy7WR%2F34E%3D&amp;b=5">RHINOPLASTY: </a> to reduce or increase the size of your nose or to change the shape</p>
            </li>
            <li>
                <p><a href="https://proxy-nl.hide.me/go.php?u=WCVCqi%2BA7mmoWuu4lxdglA90sDX38IeDe4XPWnsx2eNDYLWjhmXmXtObgm46MPns42dn15eM1So%3D&amp;b=5">BLEPHAROPLASTY </a> : to correct drooping upper lids and puffy bags below your eyes.</p>
            </li>
            <li>
                <p>Otoplasty: to set prominent ears back closer to the head or to reduce the size of large ears.</p>
            </li>
            <li>
                <p>Chin surgery ,mentoplasty or genioplasty : surgical procedure to reshape the chin</p>
            </li>
            <li>
                <p><strong>PROFILE SURGERY :</strong> procedure to achieve facial proportion ( nose and chin)</p>
            </li>
            <li>
                <p><a href="https://proxy-nl.hide.me/go.php?u=WCVCqi%2BA7mmoWuu4lxdglA90sDX38IeDe4XPWnsx2eNDYLWjhmXmXtOVh20%2BMfb5oXRuxJKR3yBF0qqoTZeF&amp;b=5">Necklift </a> &nbsp;: to smooth the loose skin on face and neck</p>
            </li>
            <li>
                <p>facelift /rhytidectomy: to improve the most visible signs of aging on face and neck and eyes</p>
            </li>
            <li>
                <p>Lips surgery</p>
            </li>
        </ul>

        <h3>Tummy and HIPS</h3>

        <ul>
            <li>
                <p>Liposuction&nbsp;to sculpt the body by removing unwanted fat</p>
            </li>
            <li>
                <p>Abdominoplasty ( &quot;tummy tuck,&quot;) :to remove excess skin and fat from the middle and lower abdomen</p>
            </li>
        </ul>

        <h3>LEGS</h3>

        <ul>
            <li>
                <p><a href="https://proxy-nl.hide.me/go.php?u=WCVCqi%2BA7mmoWuu4lxdglA90sDX38IeDe4XPWnsx2eNDYLWjhmXmXtOVh3slK%2B39735k2MmcyWIL274%3D&amp;b=5">LIPOSUCTION</a> to remove unwanted fat on thighs, knees , ankles and calves</p>
            </li>
            <li>
                <p>THIGH LIFT&nbsp;: to remove loose skin and excess fat deposits on thighs.</p>
            </li>
        </ul>

        <h3>BUTTOCKS</h3>

        <ul>
            <li>
                <p>Liposuction</p>
            </li>
            <li>
                <p>BUTTOCK IMPLANTS&nbsp;: enhancement of the&nbsp;<a href="http://en.wikipedia.org/wiki/Buttock">buttocks</a>&nbsp;using silicone implants</p>
            </li>
        </ul>

        <p>Buttocks lift surgery or &quot;Brazilian butt lift surgery</p>

        <h3>ARMS</h3>

        <ul>
            <li>
                <p>arm lift or brachioplasty :surgical procedure to remove loose skin and excess fat deposits in the upper arm.</p>
            </li>
            <li>
                <p>Arm liposuction</p>
            </li>
        </ul>


        <p><strong>Hair Transplant/ micro-grafts : </strong>Hair transplantation involves removing small pieces of hair-bearing scalp grafts from a donor site and relocating them to a bald or thinning area.</p>

    </div>
@endsection
@section('title','Aesthetic intervention in Tunisia-Dr Djemal: Plastic surgery in Tunisia ')
@section('description',' Dr Djemal is doing aesthetic interventions and plastic surgery in Tunisia, he is one of the best surgeon ')