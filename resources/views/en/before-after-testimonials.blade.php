@extends('en.innerLayout-en')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-dr.jpg);">

    @include('en.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">Result</span> Before/After & TESTIMONIALS</h1>
    </div>

  </header>
@endsection

@section('innerContent')
    <div class="content">
      <h2 class="content-title">Before/After</h2>
    </div>

    <div class="tabs-wrapper">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#visage" aria-controls="visage" role="tab" data-toggle="tab">face</a></li>
        <li role="presentation"><a href="#silhouette" aria-controls="silhouette" role="tab" data-toggle="tab">Body</a></li>
        <li role="presentation"><a href="#seins" aria-controls="seins" role="tab" data-toggle="tab">breast</a></li>
        <li role="presentation"><a href="#hommes" aria-controls="hommes" role="tab" data-toggle="tab">men</a></li>
        <li role="presentation"><a href="#chirurgiereparatrice" aria-controls="chirurgiereparatrice" role="tab" data-toggle="tab">Reconstructive surgery</a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="visage">

          <div class="before-after gallery-wrapper clearfix">

            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/1.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/1.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/2.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/2.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/3.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/3.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/5.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/5.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/6.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/6.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/7.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/7.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/8.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/8.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/08.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/08.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/9.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/9.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/09.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/09.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/10.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/10.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/11.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/11.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/12.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/12.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/13.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/13.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/14.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/14.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/16.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/16.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/17.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/17.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/18.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/18.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/19.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/19.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/20.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/20.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/21.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/21.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/22.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/22.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/23.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/23.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/24.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/24.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/25.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/25.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/26.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/26.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/27.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/27.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/28.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/28.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/29.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/29.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/30.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/30.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/31.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/31.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/37.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/37.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/38.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/38.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/39.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/39.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/1.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/1.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/2.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/2.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/3.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/3.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/5.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/5.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/7.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/7.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/9.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/9.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/11.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/11.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/16.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/16.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/18.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/18.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/19.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/19.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/20.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/20.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/22.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/22.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/24.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/24.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/32.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/32.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/34.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/34.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/35.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/35.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/visage/36.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/visage/36.png') }}" alt="">
              </a>
            </div>





          </div><!-- gallery-wrapper End -->

        </div><!-- Tab visage -->

        <div role="tabpanel" class="tab-pane fade" id="silhouette">

          <div class="before-after gallery-wrapper clearfix">

            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/1.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/1.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/01.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/01.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/2.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/2.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/02.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/02.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/3.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/3.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/03.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/03.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/4.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/4.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/5.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/5.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/6.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/6.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/7.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/7.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/8.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/8.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/9.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/9.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/10.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/10.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/11.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/11.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/12.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/12.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/13.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/13.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/14.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/14.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/15.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/15.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/16.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/16.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/17.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/17.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/18.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/18.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/silhouette/19.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/silhouette/19.jpg') }}" alt="">
              </a>
            </div>



          </div><!-- gallery-wrapper End -->

        </div><!-- Tab silhouette End -->
        <div role="tabpanel" class="tab-pane fade" id="seins">

          <div class="before-after gallery-wrapper clearfix">

            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/1.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/1.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/2.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/2.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/3.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/3.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/4.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/4.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/5.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/5.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/6.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/6.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/7.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/7.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/8.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/8.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/9.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/9.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/10.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/10.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/11.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/11.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/12.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/12.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/13.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/13.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/14.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/14.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/15.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/15.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/16.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/16.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/seins/17.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/seins/17.jpg') }}" alt="">
              </a>
            </div>


          </div><!-- gallery-wrapper End -->

        </div><!-- Tab seins End -->

        <div role="tabpanel" class="tab-pane fade" id="hommes">

          <div class="before-after gallery-wrapper clearfix">

            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/1.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/1.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/2.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/2.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/3.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/3.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/4.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/4.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/5.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/5.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/6.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/6.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/7.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/7.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/8.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/8.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/8.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/8.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/9.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/9.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/10.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/10.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/11.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/11.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/12.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/12.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/14.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/14.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/15.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/15.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/16.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/16.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/17.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/17.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/18.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/18.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/19.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/19.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/20.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/20.jpg') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/homme/21.jpg') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/homme/21.jpg') }}" alt="">
              </a>
            </div>



          </div><!-- gallery-wrapper End -->

        </div><!-- Tab hommes End -->
        <div role="tabpanel" class="tab-pane fade" id="chirurgiereparatrice">

          <div class="before-after gallery-wrapper clearfix">

            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/1.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/1.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/2.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/2.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/3.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/3.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/4.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/4.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/5.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/5.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/6.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/6.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/7.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/7.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/8.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/8.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/9.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/9.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/10.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/10.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/11.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/11.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/12.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/12.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/13.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/13.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-4">
              <a href="{{ asset('img/temoingnage/réparatrice/14.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/temoingnage/réparatrice/14.png') }}" alt="">
              </a>
            </div>

          </div><!-- gallery-wrapper End -->

        </div><!-- Tab chirurgie réparatrice -->

      </div>
      <div class="content testimonials">
        <h2 class="content-title">Testimonials</h2>

        <div class="testi col-md-9 col-md-offset-1">
          <div class="text row">
            <div class="col-md-5 col-xs-7 col-sm-4">
              <a href="{{ asset('img/testimonials/testimonials1.png') }}" class="picture-wrapper galerie">
                <div class="overlay"></div>
                <img src="{{ asset('img/testimonials/testimonials1.png') }}" alt="">
              </a>
            </div>
            <div class="col-md-7 col-xs-5 col-sm-8">
            <p>Dr Jamal’s clinic is very very good,the service provided by him and all his staff was truly amazing, they looked after me and my sister really well and made us feel so safe. </p>
            <p>My surgery was a huge success and I am loving my new body, all the nurses were very helpful and the clinic wasvery clean and tidy. </p>
            <p> Eya and everybody was amazing when looking after me. Great service DR Jamal you are a truly amazing person. </p>
            <p>Thank you so much XXXXXX</p>
            </div>
          </div>
          <div class="patient">
            <h4><i class="fa fa-calendar"></i> 24/12/2015</h4>
          </div>
        </div>

</div>
    </div>

    <!-- /.content -->
@endsection

@section('title','Blepharoplasty in Tunisia-Dr Djemal: Breast prostheses in Tunisia')
@section('description',' If you need a blepharoplasty or a breast protheses search for Dr Djemal one the best surgeons in Tunisia ')
