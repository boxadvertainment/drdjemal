@extends('en.innerLayout-en')

@section('class', 'page myron-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('en.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">The MYRON</span>clinic</h1>
      </div>
    </header>
@endsection

@section('innerContent')
    <div class="content">
      <p>
        The MYRON clinic is a brand new well established private clinic, founded in 2014 . Like any other health Institution in Tunisia, its healthcare policies are implemented and strictly monitored by the ministry of health, which gave Tunisia an international recognition of high quality healthcare.
    </p>

      <h2>PRESENTATION</h2>

      <p>Myron International Clinic is a brand new multidisciplinary medical-surgical clinic with a capacity accommodation of 60 rooms, 20 ICU beds, 10 day hospital beds spread on 6 floors.</p>

      <p>The Clinic is equipped with the latest technology and modern facilities enabling high levels of quality and safety, including several perfectly-equipped operating theatres and an intensive care unit.</p>

      <p>Founded in 2015, MYRON INTERNATIONAL CLINIC is located in the heart of the beautiful business and residential area of Lake II in Tunis, just a 10-minutes drive from Tunis Carthage International Airport. Nearby, the lovely village of Sidi Bou Said and the well-Known ruins of Carthage.</p>


      <p>The Myron International Clinic became the ultimate destination for major surgical procedures with a high standard service quality.</p>

      <!--
      <p>Founded in 2015 MYRON International clinic is located in the heart of the residential area of Lake II in Tunis, just a 10-minutes drive from Tunis-Carthage International Airport .Nearby, the lovely village of Sidi Bou Said and the well-known ruins of Carthage.</p>

      <p>Myron clinic is accredited by the Tunisian Ministry of Public Health.</p>

      <p>Myron Clinic is a state-of- the- art clinic equipped with the latest technology and modern facilities enabling high levels of quality and safety, including several perfectly-equipped operating theatres and an intensive care unit, for all types of  surgeries.</p>

      <p>All rooms are air-conditioned and equipped with satellite TV and WIFI

      Our motivated team of highly trained nurses and assistants is genuinely committed to providing your stay with attentive and thorough care within a safe and comfortable environment.</p>
                   

     <p> At the Clinic we have a selection of the finest and most skilled surgeons in different specialities, and we employ bilingual staff : English and French.</p>

      -->

      <h3>The clinic sets itself high standards :<br>highly qualified surgeons, doctors and specialists</h3>

      <ul>
        <li>caring and competent nurses</li>
        <li>Treatments using the most modern techniques</li>
        <li>The most sophisticated medical devices</li>
        <li>The most sophisticated medical devices</li>
        <li>Sophisticated infrastructure</li>
        <li>A nice location and easy access</li>
        <li>Luxurious , comfortable and well equipped rooms</li>
        <li>An input capacity of 59 rooms, 130 beds</li>
        <li>Single rooms with bathroom</li>
        <li>An accessible service 24 hours a day</li>
      </ul>
      </div>
      <!-- /.content -->

      <div class="btn-cta-wrapper">
        <a href="{{ url('contact') }}" class="btn-cta-content"> Get your MEDICAL questions answered by experts <i class="fa fa-arrow-circle-right"></i></a>
      </div>
      <!-- /.btn-cta-wrapper -->

      <div class="gallery-wrapper">
        <h3 class="title">Photo Gallery</h3>
        <div class="gallery">

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-02.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-02.jpg" alt="">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-03.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-03.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-04.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-04.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-05.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-05.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-06.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-06.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-07.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-07.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-08.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-08.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-09.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-09.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-10.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-10.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-11.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-11.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-12.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-12.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-13.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-13.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-49.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-49.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-07.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-07.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-39.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-39.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-50.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-50.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-33.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-33.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-41.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-41.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-27.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-27.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-29.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-29.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
@endsection

@section('title',' Clinic in aesthetic surgery in Tunisia-Dr Djemal: Aesthetic clinic in Tunisia')
@section('description','If you are looking for a surgeon who"s practicing plastic surgery, Dr Djemal is the best the best in its field he is practicing in an aesthetic clinic in Tunisia ')
