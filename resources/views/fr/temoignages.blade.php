@extends('fr.innerLayout')
@section('class', 'page cv-page')
@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-dr.jpg);">
    @include('fr.partials.header')

    <div class="container">
        <h1 class="page-title"><span class="intervantion">Résultats</span> Avant/Aprés & témoignages</h1>
    </div>
</header>
@endsection

@section('fr.innerContent')

<div class="content testimonials testimonials-clean">
    <h2 class="content-title">Témoignages</h2>

    <div class="row people">
        @foreach(Config::get('testimonials.testimonials') as $key => $testimonial)
            <div class="col-md-9 item col-md-offset-1">
                <div class="box">
                    <p class="description">{!! $testimonial['text'] !!}</p>
                </div>
                <div class="author">
                    <h4 class="name"> <i class="fa fa-user"></i> {{$testimonial['name']}} <small> <b>{{$testimonial['pays']}}</b></small></h4>
                    <p class="title">{{ $testimonial['operation'] ? 'Opération : '. $testimonial['operation'] :'' }}  {{ ($testimonial['operation'] && $testimonial['date']) ? ' – '. $testimonial['date'] : $testimonial['date']}} </p>
                </div>
            </div>
        @endforeach

    </div>



    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Voilà maintenant 2 ans que je prospectais pour 3 interventions de chirurgie esthétique. Habitant en France, j’ai consulté--}}
                {{--de nombreux sites français et pour autant je n’avais pas encore arrêté mon choix. Une amie tunisienne m’a--}}
                {{--fait part d’une clinique en Tunisie et d’un médecin : le docteur Djemal (elle venait de subir une augmentation--}}
                {{--mammaire). J’ai alors approfondie cette voie tunisienne. <br> J’ai découvert le docteur Djemal--}}
                {{--sur de nombreux sites mais quoi de plus probant que de voir le résultat sur ma copine qui avait subi une--}}
                {{--augmentation mammaire très réussie : c’est ce qui m’a décidé à arrêter mon choix vers la clinique Myron et--}}
                {{--le docteur Djemal. <br> Le contact et la prise de rendez-vous avec la clinique : tout est parfait. L’arrivée--}}
                {{--en Tunisie et ma prise en charge dès l’aéroport : encore parfait. Consultation avec le chirurgien le Dr Djemal--}}
                {{--: il vous écoute, il vous conseille, pas d’arrière-pensée financière, on se sent de suite en confiance et--}}
                {{--rassurée d’être avec un professionnel. 3 opérations en une seule fois (augmentation mammaire, liposuccion,--}}
                {{--lifting mannequin) avec un résultat qui a dépassé toutes mes espérances : BRAVO. <br> Je tenais à faire ce--}}
                {{--témoignage car vraiment je suis revenue en France RAVIE. Merci à toute l’équipe de la clinique qui sont toujours--}}
                {{--à votre écoute et qui sont tous très chaleureux, un merci particulier au docteur Djemal et un grand merci--}}
                {{--à Inès son assistante pour sa disponibilité son écoute et son réconfort. Encore Merci à tous. Pour ceux qui--}}
                {{--comme moi avaient des appréhensions, Je vous conseille cette clinique et le docteur Djemal.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>  /  / Opérations (augmentation mammaire, liposuccion, lifting mannequin)--}}
            {{--</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Bonjour Personnellement ma sœur et moi sommes très satisfaite de notre opération . Je tiens à remercier l’infirmière qui--}}
                {{--c’est occupé de moi, Inès pour son accueil, et son suivi et notre magicien le docteur Djemal . Cet intervention--}}
                {{--a changé mon quotidien dans ma vie de femme et je ne regrette rien . À bientôt--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>  / date :  </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Résultat au-delà de mes attentes. Le DR Djemal a des mains en or. Merci de m'avoir redonné goût à la mode. Je lui reconfierais--}}
                {{--mon corps les yeux fermés. Merci à Morena et à Ines pour le suivi, l'organisation et pour le soutien moral--}}
                {{--qui est plus que nécessaire lorsque nous effectuons cette opération. Cette liposuccion complété a changer--}}
                {{--ma vie d'épouse, de femme, le regard des autres sur moi. Mais le plus important mon regarde sur moi Cette--}}
                {{--lipo ma redonné confiance en moi, ma redonne l'envie de m'aimer. Sans hésiter je referai la liposuccion complété.--}}
                {{--Merci à toute l'équipe.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>  / Opération : liposuccion </h4>--}}
        {{--</div>--}}
    {{--</div>--}}


    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Le 19 décembre 2017, j ai vécu une aventure extraordinaire, j'ai fait une augmentation mammaire, cela a complètement changé--}}
                {{--ma vie, je suis tellement heureuse d'avoir franchi le pas. Bien sûr ça reste une opération mais on est vraiment--}}
                {{--entre de très bonnes mains. Il n y a rien a dire sur la prise en charge que ce soit avant l opération on--}}
                {{--est très bien conseillée, on répond à toutes nos interrogations... une fois sur place là aussi rien à dire--}}
                {{--Nabil notre chauffeur nous attend à notre arrivée à l aéroport et le jour de l opération toute l équipe est--}}
                {{--super Ines est vraiment adorable et notre Morena est une vraie maman avec nous. Pendant le séjour à la clinique--}}
                {{--toute l équipe est juste formidable toujours au petit soin avec nous.... et bien sûr le docteur Djemal le--}}
                {{--meilleur des chirurgiens, il a fait un travail remarquable... je le recommande à 100%, je ne pourrais jamais--}}
                {{--lui dire assez merci....séjour à l hôtel au top.--}}
                {{--<br/> Et même après l opération on a toujours un suivi.--}}
                {{--<br/> Alors à toutes celles qui hésitent à franchir le pas, je n ai qu un mot à vous dire " Foncez " vraiment--}}
                {{--vous ne serez pas déçue, au bout d'un mois d opération plus aucune douleur, je n ai eu aucun ecchymose, et--}}
                {{--je me sens tellement mieux dans mon corps, vraiment je revis car pour moi c'était un réel complexe.--}}
                {{--<br/> Vraiment encore un immense merci au Dr Djemal et à toute son équipe.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Laetitia /  </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Bonjour, moi et ma copine avons décidé un jour de partir en Tunisie pour subir une augmentation mammaire avec le Dr Djemal,--}}
                {{--pour ma part on m'a conseillé un lifting avec augmentation avec prothèses. Par la suite j'ai décidé d'ajouter--}}
                {{--une lipo complète a ca....--}}
                {{--<br/> J'ai eu des discussions régulières avec Morena dès que j'avais des questions et des appréhensions.--}}
                {{--La clinique est superbe, le personnel est super, je m'en suis même faites des amies ,nous avons été prises--}}
                {{--en charge dès notre arrivée par Nabil (le chauffeur) qui est très gentil et par Morena dès notre arrivée--}}
                {{--à la clinique. Nous avons toujours réussi à avoir un service en francais, tout était parfait!!! Même à mon--}}
                {{--retour au Québec le personnel de l'hôpital qui a retiré mes points ici n'en revenait pas comment c'était--}}
                {{--bien fait!!--}}
                {{--<br/> Un gros merci à tout l'équipe xx--}}
                {{--<br/> Ps : même de retour au Qc le dr Djemal a continué de répondre à mes questions lorsque j'en avais donc--}}
                {{--suivi après excellent!--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> ANICK / CANADA </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Bonjour je m'appelle Karine j'ai 35 ans Je suis allée à la clinique myron en mai 2017 pour une augmentation mammaire, et--}}
                {{--je peux vous dire que le personnel, la clinique, le chirurgien et Morena, la personne qui m'a prise en charge,--}}
                {{--sont adorables et professionnels. Ils sont à nos petits soins. Et ainsi qu'Inès qui était là à notre écoute...--}}
                {{--<br/> Je recommande vivement la clinique Myron.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>  / </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Je souhaitais vous envoyer un petit message afin de vous remercier de ce que vous avez fait pendant mon séjour, je sais vous--}}
                {{--allez me dire que c'est votre travail, que c'est normal, mais non, ce n'est pas le cas partout, on est pas--}}
                {{--toujours aussi bien traité dans d'autres cliniques, encore merci pour tout, comme dit à Inès, je vais faire--}}
                {{--bonne presse car vous le méritez, je sais que tout n'est pas facile au quotidien, qu'il n'y a pas que des--}}
                {{--clients ou patients faciles et sympathiques !;-)--}}
                {{--<br/> En tant que partient, j'aurai une requête à vous faire;-) , montrez le message suivant à toutes les--}}
                {{--filles du 5e étage qui ont travaillé durant mon court passage en clinique : (3au5 janvier 2018)--}}
                {{--<br/> J'aimerai également dire un GRAND MERCI à toutes les personnes à qui on ne dit que trop rarement des--}}
                {{--compliments je parle des femmes de ménage aux infirmières, aux assistantes, à toutes ces filles qui font--}}
                {{--un travail difficile, elles méritent qu'on leur fasse des compliments, car elles ont été très gentilles,--}}
                {{--très sympathiques et très compétentes, je n'ai que des compliments à leur faire ! MERCI pour TOUT je sais--}}
                {{--que vous faites votre job, mais je sais ô combien celui-ci est difficile !--}}
                {{--<br/> CHOKRAN !!!!!--}}
                {{--<br/> Un petit clin d'oeil également à Nabil, comme taxi, c'est la classe ! Merci pour ta gentillesse, ta--}}
                {{--disponibilité et ton sourire ! Longue vie à toi et ta famille ;-)--}}
                {{--<br/> Merci à toutes et à tous, merci aussi au Dr Djemal bien sûr pour son travail !--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> France</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}

            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>  L./ SUISSE</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}

            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> OLFA / FRANCE</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Je suis Annie de France "Charentes Maritime, J'ai prix la décision de subir une lipossuccion abdominoplastie .En septembre--}}
                {{--2016 je suis allée en Tunisie à Tunis à la clinique Myron service du docteur Djemal , une très grande satisfaction--}}
                {{--, le professionnalisme de ce docteur . Il m' a parlé, ma mise en confiance , vous pouvez compter sur le personnel--}}
                {{--de gentilles aides soignantes à votre écoute et toujours une parole gentille et réconfortante . Une superbe--}}
                {{--chambre a la clinique. Et surtout aucune douleur , Et le résultat très satisfaisant , j'ai une taille a présent--}}
                {{--que j'avais plus . Et la preuve de ma satisfaction ! j'y suis retourné en avril 2017 pour un lifting des--}}
                {{--bras et en octobre 2017 pour une lipossuccion en haut des fesses et cuisses et j'ai perdu plusieurs tailles--}}
                {{--donc très heureuse du résultat. Honnêtement j'y retournerai pour mon grand plaisir de revoir toutes ces personnes--}}
                {{--qui vous prend en charge dès l'arrivée jusqu'au départ à l'aéroport et l' hôtel superbe et sans oublier Morena,--}}
                {{--une femme fantastique à votre écoute. Le Docteur Djemal je l'admire sincèrement .--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Charentes Maritime</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--I am so happy I feel well after this Operation. I thank my Doctor Djmal so much and the whole Team. ça va super bien !!!--}}
                {{--Oui ça va me faire plaisir écrire un petit message car mon séjour à été au delà de mes attentes !!! Merci--}}
                {{--beaucoup pour votre accueil et votre support tout au long de notre séjour !!!--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Nesrine / DUBAI </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Après de longues, voire même de très longues hésitations, j'ai enfin décidé de remédier à mes formes et kilos en trop que--}}
                {{--je ne supportaient plus... Je suis donc partie en Tunisie, après avoir consulté de nombreux sites et forums.--}}
                {{--A mon arrivée à l'aéroport de Tunis, je fus accueillie par le charmant sourire de Morena, qui je dois dire--}}
                {{--m'a assistée de façon très professionnelle et chaleureuse pendant toute la préparation du dossier. Elle a--}}
                {{--répondu très honnêtement à toutes mes demandes et à su me rassurer quant à l'expérience et le savoir faire--}}
                {{--du Chirurgien. Le Dr T.Djemal est un homme d'un grand professionnalisme et d'une très grande gentillesse.--}}
                {{--Mon intervention s'est déroulée très rapidement. L'assistant du Docteur Djemal, Nourredine m'a montré toutes--}}
                {{--sortes de photos d'anciennes patientes et avec beaucoup d'humour, m'a conduite au bloc. A mon retour, 3 bonnes--}}
                {{--heures aprés, les infirmières étaient aux petits soins. J'ai revu le Chirurgien et son Assistant à plusieurs--}}
                {{--reprises lors de mon séjour. A chaque visite, ils me rassuraient et répondaient à toutes mes questions et--}}
                {{--dans la bonne humeur. J'étais déjà contente du résultat de la poitrine qui était plus petite, malgré les--}}
                {{--pansements . Pour la liposuccion , à part des douleurs comme des courbatures, je ne pouvait encore rien voir--}}
                {{--à cause de la gaine. Mais vu qu'il a enlevé 5 litres, j'étais très confiante. Maintenant, 2 mois après, je--}}
                {{--peux dire que mon intervention est une vraie réussite. J'ai une jolie silhouette et malgré encore les cicatrices,--}}
                {{--ma poitrine n'est plus tombante et présentable… …..A toutes les personnes qui souhaitent partir, n'hésitez--}}
                {{--plus et faites confiance au Chirurgien Djemal qui est vraiment exceptionnel. Vous pouvez me contacter pour--}}
                {{--avoir plus de renseignements.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Liliane /Belgique </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Oui j'avais peur de partir en Tunisie pour une chirurgie esthétique. Je suis allée sur plusieurs sites, lu et relu les témoignages,--}}
                {{--pris des contacts avec les personnes qui y sont allées et j'ai fait le grand saut la peur au ventre. Maintenant--}}
                {{--que de bons souvenirs sont restés gravés dans ma mémoire, j'ai retrouvé au bout de trois mois une silhouette--}}
                {{--qui a changé ma vie grâce au d.r. Djemal, chirurgien très expérimenté, chaleureux et plein d'humanité. Je--}}
                {{--n'oublierais pas non plus la gentillesse de Nourredine ainsi que de toute l'équipe de la clinique .. L' accompagnatrice--}}
                {{--( Morena) est très professionnelle, elle vous prend en main du début à la fin. Aucun regret d'avoir pris--}}
                {{--cette décision. Allez-y Mesdames, n'hésitez pas et si vous avez besoin tout comme moi d'être rassurées je--}}
                {{--suis là.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Chantal / France </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Après une opération malheureuse en France, au hasard d'une émission TV je me suis tournée vers la chirurgie esthétique en--}}
                {{--Tunisie : liposuccion complète et abdominoplastie J'en garde un souvenir mémorable, des gens exceptionnels,--}}
                {{--surtout le chirurgien DJemal, l'anesthésiste Nourredine. très professionnels, l'équipe médical au top, l'accueil,,--}}
                {{--la prise en charge, l'hébergement dans un hôtel sublime, le tout dans un site merveilleux, je ne parle plus--}}
                {{--d'opération mais de vacances, que de bons souvenirs....et surtout un corps plus harmonieux que j'aime mettre--}}
                {{--en valeur. N'hésitez pas à me contacter à monique.moscipan@free.fr--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Monique / France </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--J'ai subi deux chirurgies esthétiques en Tunisie, toujours avec le même chirurgien, le Dr Djemal et la même équipe. Quel--}}
                {{--bonheur d'être aussi bien assistée. Prise en charge à l'aéroport, transfert ,tout est parfait. La clinique,--}}
                {{--c'est le paradis, vous existez vraiment, tout le monde est au petit soin, que ce soit de la simple aide-soignante--}}
                {{--au Grand Chirurgien, c'est la famille. On vous écoute, vous cajole, vous donne des conseils. Mes opérations--}}
                {{--se sont supers bien passées (diminution mammaire et paupières). Les résultats sont supers. Je me sens belle--}}
                {{--et rajeunit, oh oui!!!! Quelle belle expérience!. Et surtout n'oubliez pas que la personne qui vous accompagne--}}
                {{--tout au long de votre séjour, Morena, par sa gentillesse, son savoir-faire mais surtout avec son rire vous--}}
                {{--fait oublier pourquoi vous êtes là. Ce ne sont que de merveilleuses vacances.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Gabrielle / SUISSE </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Bonjour a vous toutes et tous je me suis enfin décidée a subir 1 lifting du visage j'en rêvais depuis longtemps et je peux--}}
                {{--vous dire que ma patience m'a bien récompensée. le jour ou j'ai débarqué en Tunisie j'ai été accueillie chaleureusement--}}
                {{--par Morena que je remercie pour son accueil, son sourire, son réconfort, et sa sympathie etc... 1 femme exceptionnelle--}}
                {{--après cet accueil direction le docteur Djemal ; quel être charmant et sympathique, très a l'écoute de ses--}}
                {{--patient(e)s ensuite direction la clinique. La aussi superbe accueil et sourire a la clé ça y es j'allais--}}
                {{--passée a l'acte la peur au ventre mais j'étais déterminée toute tremblante dans ma chambre j'attendais le--}}
                {{--moment d'aller en salle d'op ! voici la visite de Nourredine (l'anesthésiste) qui vient me montrer des photos--}}
                {{--avant et après (pour me rassurer) mais voila clic clac a mon tour de jouer la star avant de m'envoyer dans--}}
                {{--les bras de Morphée, le temps de compter jusque 3 et me voila endormie ! a mon reveil j'avais Nourredine--}}
                {{--et le chirurgien Dr. Djemal a mes cotés pour voir si j'étais bien et si je n'avais besoin de rien aussi me--}}
                {{--dire que le déballage se ferai le lendemain ! J'etais impatiente mais après 1 bonne nuit j'allais enfin découvrir--}}
                {{--mon nouveau visage 1 assaut de personnel soignant défilait dans ma chambre toute la nuit aux petits soins--}}
                {{--pour moi je leur passe mon bonjour ! Quelle équipe fantastique ! Merci a vous tous (tes) le lendemain voila--}}
                {{--l'heure du verdict l'équipe de choc les inséparable dr Djemal et Nourredine les grands professionnels mieux--}}
                {{--des dieux …. wouawwwwwwwwwwww quel résultat, plus de rides j'avais le visage tendu , gonfle et colore..Mais--}}
                {{--je savais que tout allait reprendre 1 proportion normale j'ai retrouver des personnes comme moi opérées et--}}
                {{--le surlendemain visite a Carthage , sidi bou said ... quel pays fantastique et le climat .... tout est au--}}
                {{--top, les jours passent a 1 allure grand V dernière visite chez le docteur Dj avant de repartir chez moi.--}}
                {{--Morena toujours le sourire au lèvres et moi les larmes dans les yeux je regagnais l'aéroport pour me rapprochez--}}
                {{--des miens. je garderai 1 superbe souvenir de mon passage en Tunisie>merciiiiiiiiiiiiiiiiiiiii a Morena, a--}}
                {{--l'équipe médicale au personnel ,au grand Monsieur DR Djemal avec 1 grand M pour ses doigts d'or ,son sourire--}}
                {{--réconfortant et chaleureux (hé oui mesdames ils en existe encore et quand vous l'aurez vu a votre tour vous--}}
                {{--ne pourrez jamais l'oublié car vous êtes vous grace a lui ! merci également a Nourredine pour son 1,2,3 a--}}
                {{--tantôt en qq mots grand merci a vous tous bonne continuation vous êtes 1 équipe formidable--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Barbara / Belgique </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Après la perte de 20kg durant les 2 dernières années, et après y avoir longtemps pensé, le 25 octobre dernier j'ai subi une--}}
                {{--lipo et abdo complète. Quelle expérience ! ! ! Peu de temps avant j'étais allée voir un chirurgien en Belgique--}}
                {{--duquel je suis sortie moralement démolie ! Il vous traite vraiment très mal. Après cette visite j'avais laissé--}}
                {{--l'idée de côté et je me suis dit : " tant pis, ce ne sera pas pour moi ! " Ensuite le net étant une source--}}
                {{--extraordinaire d'infos, j'ai pas mal réfléchi et je me suis finalement décidée pour la Tunisie. Au départ--}}
                {{--j'étais totalement septique par rapport aux photos que publiaient les patientes sur les divers blogs, mais--}}
                {{--je peux vous dire que c'est exceptionnel et que le résultat est au dessus de mon espérance ! ! ! ! ! Plus--}}
                {{--de ventre ! ! !Je ne l'ai jamais vu aussi plat ! ! ! ! ! Et on peut dire que chez moi c'était un fameux chantier.--}}
                {{--Pour parler de ce qui vous inquiète sûrement le plus; Le chirurgien : Dr Djemal.. Super ! Il a tout de suite--}}
                {{--vu ce qu'il y avait à faire et a su faire de moi une autre femme ! La douleur : Quand je voyais les images--}}
                {{--de la cicatrice, j'ai beaucoup hésité, je me suis dit " ça doit être terrible ! " (Moi qui suit nounouche!),--}}
                {{--hé bien, très supportable et vraiment quand on sort de la clinique c'est déjà un souvenir, il ne reste que--}}
                {{--les tiraillements et les courbatures des quelques bleus. Cinq jours après l'intervention j'étais encore un--}}
                {{--peu fatiguée, mais je me promenais avec mon mari et mon petit bout de 4ans dans les souks et visite de Carthage--}}
                {{--(TB). L'accueil : Avec Mme Morena, une vraie personne sur qui compter, pas d'inquiétude de manquer de quoi--}}
                {{--que ce soit, tout est réglé comme du papier à musique ! ! ! Ce qui est super c'est que tout le monde est--}}
                {{--très compétent et que tu n'a aucun soucis, ni pour l'organisation, ni les déplacements, presque des vacances--}}
                {{--! ! ! ! ! Pas de surprise non plus pour les frais. Ce que je peux dire c'est : " si peu de douleur (car on--}}
                {{--a des antidouleurs les premiers jours) et tellement de résultat et de bonheur ! ! ! ". Je suis passée du--}}
                {{--46 au 42, je ne pensais jamais mettre un 42 un jour !et en 2 mois. Aujourd'hui, c'est une joie de me voir--}}
                {{--dans le miroir. P.S : Je repars au mois de mars pour continuer ce qui a été si bien commencé, avec liftings--}}
                {{--des cuisses et des bras.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> ISABELLE </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Beaucoup de choses m'on touchées en TUNISIE, rien que le médecin. le Dr Djemal, l'accompagnement médical, les aides soignants,--}}
                {{--toi MORENA, j'étais coucounée par vous tous, et je voudrai que tu saches que j ai un trés grand respect pour--}}
                {{--vous . Je pense beaucoup à l'équipe médicale : ils sont tous excellent dans leur travaille, ils sont humains,--}}
                {{--présents, ils me manquent tu le leur diras, et tu leur diras aussi qu'ils peuvent être fier de ce qu'ils--}}
                {{--apportent, à leurs patients. Je les remercie infiniment pour tout, toi aussi ma belle, tu as étè d'un réconfort--}}
                {{--auprès de moi, même si tu penses que c'est ton travail qui veut ça, et bien sache , que tu as une très grande--}}
                {{--générosité, et que tu donnes beaucoup, je t en remercie , je reprendrai le temps de t'écrire un peu plus--}}
                {{--tard en janvier, mes hématomes partent bien, je me sens bien. Mon médecin traitant en France m'a dit que--}}
                {{--le Chirurgien, Dr Djemal a fait du très beau travail.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Noemie Liposuccion et abdominoplastie </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Un rajeunissement spectaculaire!! je m'attendais à avoir le regard plus frais et reposé, mais c'était au delà de mes espérances.--}}
                {{--J'ai vraiment l'air plus jeune et plus fraiche, même le matin! mon maquillage est plus facile et moins long!--}}
                {{--Je prévois aussi un maquillage permanent pour l'été et profiter de la mer sans crainte de ressembler à un--}}
                {{--vieux toutou!! Cela semble exagérer, mais cette petite intervention ( je m'en rends compte aujourd'hui) à--}}
                {{--vraiment changé mon assurance! J'ai choisi la Tunisie pour le prix pour être honnête, et ensuite pour la--}}
                {{--qualité et talents du Chirurgien, le Dr Djemal et de l'équipe médicale. Tout ce professionnalisme accompagné--}}
                {{--par une charmante personne, Morena, qui sait compléter les indications du Chirurgien et nous soutenir dans--}}
                {{--les baisses de moral ( normal quand on a les yeux enflés et déformés)...à recommander sans reticences!!--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Annick blépharoplastie / 4 paupières </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Tout ce que je peux dire est merci merci et merci. Les résultats sont extraordinaires. J\'ai passé une semaine entre les--}}
                {{--mains de personnes sérieuses et professionnelles. Le DR Djemal et son équipe sont formidables. Cela n\'aurait--}}
                {{--pas pu être mieux. Merci à tout le monde--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> liposuccion / prothéses rhino / injection de graisse </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Je conseille vivement à toute personne désireuse de faire une chirurgie esthétique d'y aller en toute confiance vers ce Chirurgien,--}}
                {{--le Dr Djemal et cette equipe. Vous êtes entre de bonnes mains ( Dr Djemal, Nourredine et l'équipe médicale--}}
                {{--très,tres,tres bien) et très entourés par Morena. Allez y sans appréhension!!!--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Marie Liposuccion et 4 paupières et injection de graisse </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--AUGMENTATION MAMMAIRE j'ai fait une augmentation mammaire et le résultat 10 jours plus tard est déjà superbe! Je suis ravie!--}}
                {{--Pour cette opération, je suis venue seule et j'ai été prise en charge dès mon arrivé par Morena. Je ne me--}}
                {{--suis sentie à aucun moment seule, car l'accueil est formidable! Morena est toujours disponible pour nous,--}}
                {{--à la clinique le personnel était très attentif et gentil! Je recommande sans aucun problème le Dr Djemal--}}
                {{--ainsi que tout le personnel de la clinique! Si vous avez des questions, n'hésitez pas à me contacter!--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Nadège Brilman </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--LIPOSUCCION COMPLETE Chaque fois que j'en parlais autour de moi, on essayait de m'en dissuader...faire une liposuccion en--}}
                {{--Tunisie...tu n'y penses pas. Malgré les réticences, j'ai tenu bon. Je voulais cette lipo depuis si longtemps.--}}
                {{--J'ai eu raison de suivre mon instinct.Tout s'est déroulé mieux que je ne le pensais. L'accueil souriant et--}}
                {{--amical de Morena nous met tout de suite à l'aise et nous rassurent. Nous sommes allées directement à la Clinique.--}}
                {{--A cause d'un retard important de l'avion, le programme a changé à la dernière minute. J'étais angoissée à--}}
                {{--l'idée de ne plus être attendue, et bien au contraire, tout a été organisé comme si cela était normal. BRAVO--}}
                {{--!!! Ensuit eà la clinique, même impression; accueil parfait même à 1h00 du matin..Le matin tout s'est passé--}}
                {{--vite . Anesthésiste, douche de Bétadine, photos , explications, et surtout la rencontre avec le chirurgien,--}}
                {{--le Dr Djemal. Je ne suis jamais à l'aise lorsque je dois me dévêtir .Mais le DR Djemal était très calme et--}}
                {{--gentil. Souriant en plus. De façon très gentille, car il a remarqué ma timidité ....il a explique l'intervention,--}}
                {{--fait les dessins des parties à enlever ( j'en avais partout!!!!) et à même fait un peu d'humour. Une consultation--}}
                {{--courte mais efficace. Ensuite je ne me souviens plus de rien, jusqu'à mon retour dans la chambre...L'équipe--}}
                {{--des infirmières à été présente et Morena est revenue me voir . C'était rassurant. A vrai dire je n'avais--}}
                {{--pas vraiment de douleurs jusqu'au lendemain.. Premier lever( engourdie et vertiges) avec les infirmières--}}
                {{--et petit à petit on reprend propriété de son corps, trés courbaturé . Le reste de la semaine s'est déroulé--}}
                {{--très vite; visites et consultations, massage, repos et repos. Merci à Morena d'avoir pris soin de moi. J'avais--}}
                {{--peur de partir seule , mais elle a été suffisamment présente et pleine de conseils pour rendre ce séjour--}}
                {{--agréable. Le résultat n'est pas encore trés visible, mais assez pour me donner confiance. 3 semaines déjà--}}
                {{--et je n'ose pas encore enlevé ma gaine. Elle me rasssure et me fait une belle silhouette....on verra dans--}}
                {{--3 mois . Je suis trés optimiste . Je tiens à remercier le chirurgien , le Dr Djemal et son équipe pour ce--}}
                {{--travail si professionnel et performant. Si vous désirez m'écrire , je vous répondrai.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Patricia Patricia </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--PROTHESES/ Si vous hésitez , n'attendez pas des années. Finalement toutes ces hésitations m'ont rendues la vie encore plus--}}
                {{--dure. J'avais consulté en France, mais mon budget ne me permettait pas une telle dépense. Ensuite des émissions--}}
                {{--et des recherches m'ont conduite à DR DJEMAL. Il a fait du super travail .. En plus de toute cette prise--}}
                {{--en charge, je dois vraiment saluer le professionnalisme et le talent du Chirurgien qui a su trouver exactement--}}
                {{--ce que je voulais. Une anesthésie qui endort juste pour l'intervention , mais qui ne laisse aucune trace--}}
                {{--au réveil, une équipe médicale aux petits soins à la Clinique et un suivi incroyable après l'intervention.--}}
                {{--Sur 6 jours j'ai revu le Dr Djemal plusieurs fois et j'ai pu lui poser les questions que je voulais. J'ai--}}
                {{--rencontré une autre personne qui avait fait une liposuccion et nous avons pu visiter les alentours. Morena--}}
                {{--ne nous laissait jamais seules et nous " surveillait " comme ses " enfants ". On se sentait rassurée et heureuse.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Sonia Sonia </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Bonjour à tous, Cela fait tout juste 2 semaines que je suis revenue de Tunisie pour une augmentation mammaire. J'ai quelques--}}
                {{--toutes petites douleurs (qui sont supportables) mais cela s'estompe de jour en jour. Je suis super contente--}}
                {{--du résultat. J'ai rencontré une personne qui a fait une augmentation mammaire, il y a 2 ans je pense ; je--}}
                {{--peux vous dire que sa prothèse est naturelle au regard tout comme au toucher. Je tiens à remercier toute--}}
                {{--l'équipe qui s'est bien occupée de ma sœur (augmentation et liposuccion) et moi durant mon séjour, pendant--}}
                {{--et après l'opération. Surtout Morena qui a toujours été là comme une vraie amie (quel gentillesse . !! ),--}}
                {{--et au chirurgien . Dr Djemal, qui est très professionnel et quel beau résultat ! Bravo ... Je n'ai aucun--}}
                {{--regret de ma part d'être venu et je retournerai bientôt pour une nouvelle intervention de chirurgie .--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Marie Mimi </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--2 semaines seulement après 1 abdominoplastie et 1 lipo complète, je n'ai plus aucun "bleu" et surtout 1 résultat deja visible--}}
                {{--et mieux que ce que j'espérais. 1 grand merci à toute l'équipe médicale en particulier au medecin, le Dr--}}
                {{--Djemal. et aussi a Morena pour son accompagnement et soutient et sa toujours bonne humeur. a bientot pour--}}
                {{--1 lifting--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Antoinette Suisse </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Je suis venue pour une rhinoplastie. En fait j ai renouvelé toute ma confiance à Dr Djemal et son équipe car j étais déjà--}}
                {{--venue pour une augmentation mammaire. La encore, difficile de trouver quelque chose à redire : L accueil,--}}
                {{--la prise en charge, l écoute, le professionnalisme, l accompagnement…..tout est là ! Jamais on ne reste ni--}}
                {{--on ne se sent seule. Le chirurgien, le Dr Djemal , l anesthésiste, et toute l équipe médicale, est réellement--}}
                {{--professionnelle. On va pour la chirurgie esthétique en Tunisie pour plein de bonnes raisons : la qualité--}}
                {{--des soins, du chirurgien et personnel soignant et accompagnant, les prix bien sur, mais on ne trouve pas--}}
                {{--de la compétence au rabais !! très loin de là ! Je ne peux que vous recommander de prendre contact avec MORENA--}}
                {{--( rien que pour avoir la chance d entendre son rire inimitable !!!), et de vous rendre compte par vous-même.--}}
                {{--Si vous le souhaitez, vous pouvez m envoyer un petit mail, je réponds toujours !!!--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Florence rhinoplastie </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Après trois grossesses ma poitrine complètement vidée, j'ai décidé de prendre les choses en mains et j'ai passé le cap. Le--}}
                {{--18 mars dernier j'ai subi une augmentation mammaire avec un grand Chirurgien, le Dr Djemal et en 1 semaine--}}
                {{--des années de souffrance se sont effacées grace au Dr Djemal ainsi qu'a Morena qui a été pendant ce séjour--}}
                {{--toujours à mon écoute et disponible. "un vrai petit médecin". Elle a été là sur le moment ainsi qu'a mon--}}
                {{--retour et pour cela je l'en remercie. Je tenais à remercier aussi les infirmieres qui sont d'une gentillesse--}}
                {{--incroyable. Encore merci au Dr Djemal pour sa gentillesse et son professionnalisme.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Sandrine augmentation mammaire </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--De retour de Tunisie depuis 1 mois, pour une lipo. Accueil toujours aussi chaleureux de MORENA, le chirurgien , Dr Djemal--}}
                {{--et l'équipe médicale toujours aussi professionnels. C'est une équipe formidable, je récidive ( lifting visage--}}
                {{--et augmentation mammaire) donc, pourquoi changer ? Merci pour tout--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Monique liposuccion et injection de graisse </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Cela fait exactement 1 mois moins qq jours que je suis revenue de Tunisie, pour une augmentation mammaire et une lipo, Que--}}
                {{--dire... et bien tout simplement je suis aux anges... J'ai maintenant une vraie belle poitrine (avant : pas--}}
                {{--terrible et après 2 grossesses : encore moins), les cicatrices ne se voient presque plus et les bleus dus--}}
                {{--à la lipo ont totalement disparu !!! Je sais, prendre la décision de se faire opérer n'est pas facile et--}}
                {{--encore moins de faire faire cela dans un autre pays, je n'ai pas vraiment eu ce problème, j'ai simplement--}}
                {{--fait confiance à ma mère qui y retournait pour la 3eme fois, et franchement aucun regret... toute l'équipe--}}
                {{--qui s'occupe de nous est très sympa, tout d'abord Morena (c'est notre maman pendant 1 semaine), ensuite le--}}
                {{--chirurgien, le Dr Djemal et l' équipe médicale qui font de l'excellent travail, et le personnel de la clinique,--}}
                {{--des gens très, très gentils (et je n'exagère pas), …. Le plus de ce petit séjour et non le moins désagréable,--}}
                {{--est que l'on peut se reposer pendant 1 semaine (rien à faire, juste prendre soin de soi...) ne pas retourner--}}
                {{--dans la vie "active" avec les enfants qu'ils faut laver, porter, promener , la maison a entretenir, et/ou--}}
                {{--au boulot pour devoir se justifier si jamais bleu il y a... profiter pour visiter Tunis et ses alentours--}}
                {{--(la médina, Carthage, Sidi Bou saÏd...), même avec une augmentation mammaire et une lipo faites 4 jours plus--}}
                {{--tôt... J'encourage tout le monde a partir en Tunisie pour la chirurgie esthétique, vous ne le regretterez--}}
                {{--pas ! PS : J'en ai profité également pour faire le maquillage permanent (bcp moins cher qu'en France) et--}}
                {{--là, vous êtes séduisante dès le matin, pas besoin de passer par la case salle de bain... N'hésitez pas si--}}
                {{--vous avez des questions, et foncez !--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Aude lipo complete et prothéses mammaires </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Cela faisait 6 mois que ma décision était prise : j'ai consulte en France plusieurs chirurgiens esthétiques pour une liposuccion--}}
                {{--des cuisses et genoux. Les tarifs allaient de 3500 a 3800 euros. C'est une somme, mais quand l'envie d'être--}}
                {{--mieux dans sa peau est devenue une idée fixe, cela n'a pas de prix. Ce qui me réconfortait c'était que cela--}}
                {{--se passe en France prés de mes proches. Et puis la Tunisie, pourquoi pas. J'ai franchi le pas et ne regrette--}}
                {{--rien .Accueil chaleureux par notre Morena, le chirurgien, le dr Djemal. Consciencieux et un personnel hors--}}
                {{--pair et surtout a l'écoute. Je suis partie toute seule et j'avais beaucoup d'appréhension; mais je vous assure--}}
                {{--que cela c'est vite estompe. J'ai subie en fait une liposuccion complète, des chevilles a l'abdomen : je--}}
                {{--suis ravie. Cela fait 15 jours et déjà j'ai perdu1 taille et demi. J'ai un petit peu mal encore , mais de--}}
                {{--jours en jours je vais mieux, il faut dire qu'ils m'ont enlevé 6 litres de graisses. Je conseille vivement--}}
                {{--le Dr Djemal ainsi que Morena ( reine des douceurs) a venir en Tunisie. Sérieux, gentillesse, suivi post--}}
                {{--opératoire efficace, et le prix pas tout a fait le même quand France. Merci encore--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Fabienne/ France lipo complete </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Juste un petit mot pour remercier toute l'équipe. Ma rhinoplastie est une réussite. Mon profil est exactement ce que je cherchais.--}}
                {{--L'équipe est absolument exceptionnelle. On se sent vraiment pris en charge avec un vrai suivi. Je suis arrivée--}}
                {{--avec quelques craintes le premier jour ,mais l'équipe médicale et l'équipe d'accompagnement a su me rassurer--}}
                {{--et je recommande vivement le chirurgien Dr Djemal,qui a des doigts en or et un vrai sens de l'esthétique.--}}
                {{--Vous pouvez prendre votre billet en toute confiance, vous ne serez pas déçu. L'équipe sait combiner professionnalisme--}}
                {{--et convivialité. N'hésiter pas à me contacter, je vous communiquerais mes photos avant/après et vous expliquerais--}}
                {{--comment se passe l'opération et la convalescence.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Synia Natty / Londres rhinoplastie </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Cela fait exactement 1 mois et 10 jours que je me suis fait opérer d'une augmentation mammaire. J'ai attendu tout ce temps--}}
                {{--pour témoigner car j'ai mis du temps à réaliser que je l'ai vraiment fait. Cela fait longtemps que j'avais--}}
                {{--abandonné l'idée de me faire opérer (manque de temps et de courage), mais voilà que juste avant mes vacances--}}
                {{--j'ai visité ce site pour avoir plus de renseignements sur les opérations. Mon mari m'a encouragé à envoyer--}}
                {{--un mail, mais pour moi il était impensable qu'une telle opération puisse s'organiser en 15 jours. Je n'ai--}}
                {{--même pas eu le temps de m'inquiéter de quoi que ce soit. Tout c'est fait avec tellement de simplicité. Je--}}
                {{--n'ai eu a m'occuper que de la réservation du vol, tout le reste c'est Morena qui s'en est chargé. J'ai bien--}}
                {{--pu correspondre avec elle et elle a répondu à toutes mes questions. Ce qui est incroyable c'est que j'avais--}}
                {{--une telle confiance dans cette équipe que je suis arrivé devant l'opération sans la moindre crainte. Le chirurgien--}}
                {{--le Dr Djemal,,à fait un superbe travail, et aujourd'hui déjà on ne voit plus du tout que je me suis fait--}}
                {{--opérer. Les cicatrices ont déjà complètement disparu. Il est vrai qu'une opération n'est jamais anodine,--}}
                {{--mais si j'avais su que c'est aussi simple que cela, il y a longtemps que je l'aurai faite. J'ai 43 ans, mais--}}
                {{--il n'est jamais trop tard. Pour la première fois je me sens vraiment femme, et cela à changé mes relations--}}
                {{--avec mon mari. Je voulais encore vraiment remercier toute l'équipe pour leur gentillesse et leur bel esprit,--}}
                {{--j'ai vraiment pu ressentir que pour eux leur travail est plus une passion qu'un gagne pain.--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Martine prothèses </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--Oui j'avais peur de partir en Tunisie pour une chirurgie esthétique. Je suis allée sur plusieurs sites, lu et relu les témoignages,--}}
                {{--pris des contacts avec les personnes qui y sont allées et j'ai fait le grand saut la peur au ventre. Maintenant--}}
                {{--que de bons souvenirs sont restés gravés dans ma mémoire, j'ai retrouvé au bout de trois mois une silhouette--}}
                {{--qui a changé ma vie grâce au d.r. T.Djemal, chirurgien très expérimenté, chaleureux et plein d'humanité.--}}
                {{--Je n'oublierais pas non plus la gentillesse de Nourredine,. ainsi que de toute l'équipe de la clinique .--}}
                {{--Les accompagnateurs sont très professionnels, ils vous prennent en main du début à la fin. Aucun regret d'avoir--}}
                {{--pris cette décision. Allez-y Mesdames, n'hésitez pas et si vous avez besoin tout comme moi d'être rassurées--}}
                {{--je suis là--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Chantal lipo / abdo </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>--}}
                {{--La première fois que je suis venue en Tunisie s'était en tant qu'accompagnante pour une de mes amie qui venait faire une--}}
                {{--chirurgie d'augmentation mammaire. J'y suis allée car je n'avais pas réussi à la décourager, elle était très--}}
                {{--décidée à faire cette opération, et moi je l'avoue ,pas rassurée pour elle. (Se faire opérer dans un pays--}}
                {{--exotique, avec tous ces reportages négatifs à la Télé, quelle idée !) J'ai été agréablement surprise, dès--}}
                {{--la descente d'avion, en voyant la qualité de l'accueil et de prise en charge, avec l'adorable Morena, (une--}}
                {{--véritable amie et maman ), la modernité de la clinique, le sérieux et la gentillesse du Docteur Djemal( vous--}}
                {{--n'êtes pas un N° parmi tant d'autres, cet homme a de véritables qualité humaine, et porte la douceur sur--}}
                {{--son visage), ainsi qu' un suivi médicale poussé, avec pré-consultation, consultations, consultations post--}}
                {{--opératoires : un véritable cocooning pour le malade, de la part de l'équipe médicale, merci au Gentil Nourredine--}}
                {{--(assistant du Docteur) pour sa merveilleuse bonne humeur, merci pour le rire communicatif et le dynamisme--}}
                {{--de Morena et surtout le plus important : un résultat magnifique. Du coup, forte de cette expérience, je suis--}}
                {{--revenue à 2 reprises pour moi-même, (Lipo du ventre) et chirurgie des 4 paupières (faites par l'adorable--}}
                {{--Docteur Djemal). Pour résumer, vous êtes tellement bien encadré et suivi, que franchement si j'avais su,--}}
                {{--je l'aurais fait avant. Par ailleurs, 10 jours après la chirurgie des 4 paupières, j'avais retrouvé un visage--}}
                {{--normal, (juste un maquillage leger pour les dernières petites traces), j'ai donc repris le travail, commentaire--}}
                {{--de mes collègues et amis" les vacances t'on vraiment réussi, tu as l'air en pleine forme tu as une mine superbe--}}
                {{--" (je n'aie pas eu à révéler mon petit secret de rajeunissement à tout le monde).--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Béatrice paupières/ ventre / lifting visage </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p> Je conseille vivement à toute personne désireuse de faire une chirurgie esthétique d'y aller en toute confiance--}}
                {{--vers ce Chirurgien, le Dr Djemal et cette equipe. Vous êtes entre de bonnes mains ( Dr Djemal, Nourredine--}}
                {{--et l’équipe médicale très,tres,tres bien) et très entourés par Morena. Allez y sans appréhension!!! </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Marie</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p> j'ai fait une augmentation mammaire et le résultat 10 jours plus tard est déjà superbe! Je suis ravie! Pour cette--}}
                {{--opération, je suis venue seule et j'ai été prise en charge dès mon arrivé par Morena. Je ne me suis sentie--}}
                {{--à aucun moment seule, car l'accueil est formidable! Morena est toujours disponible pour nous, à la clinique--}}
                {{--le personnel était très attentif et gentil! Je recommande sans aucun problème le Dr Djemal ainsi que tout--}}
                {{--le personnel de la clinique! Si vous avez des questions, n'hésitez pas à me contacter! </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Nadège B. </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>2 semaines seulement après 1 abdominoplastie et 1 lipo complète, je n'ai plus aucun "bleu" et surtout 1 résultat--}}
                {{--deja visible et mieux que ce que j'espérais. 1 grand merci à toute l'équipe médicale en particulier au medecin,--}}
                {{--le Dr Djemal. et aussi a Morena pour son accompagnement et soutient et sa toujours bonne humeur. a bientot--}}
                {{--pour 1 lifting  </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Antoinette S.</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>Après une opération malheureuse en France, au hasard d'une émission TV je me suis tournée vers la chirurgie esthétique--}}
                {{--en Tunisie : liposuccion complète et abdominoplastie. <br> J'en garde un souvenir mémorable, des gens exceptionnels,--}}
                {{--<b>surtout le chirurgien--}}
                        {{--Djemal, l’anesthésiste Nourredine</b>. très professionnels, l'équipe médical au top,--}}
                {{--l'accueil,, la prise en charge, l'hébergement dans un hôtel sublime, le tout dans un site merveilleux, je--}}
                {{--ne parle plus d'opération mais de vacances, que de bons souvenirs....et surtout un corps plus harmonieux--}}
                {{--que j’aime mettre en valeur. <br> N'hésitez pas à me contacter à monique.moscipan@free.fr--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Monique - France</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>J'ai subi deux chirurgies esthétiques en Tunisie, toujours avec le même chirurgien, le Dr Djemal et la même équipe.--}}
                {{--Quel bonheur d'être aussi bien assistée. Prise en charge à l'aéroport, transfert ,tout est parfait. La clinique,--}}
                {{--c'est le paradis, vous existez vraiment, tout le monde est au petit soin, que ce soit de la simple aide-soignante--}}
                {{--au Grand Chirurgien, c'est la famille. On vous écoute, vous cajole, vous donne des conseils. Mes opérations--}}
                {{--se sont supers bien passées (diminution mammaire et paupières). Les résultats sont supers. Je me sens belle--}}
                {{--et rajeunit, oh oui!!!! Quelle belle expérience!. Et surtout n'oubliez pas que la personne qui vous accompagne--}}
                {{--tout au long de votre séjour, Morena, par sa gentillesse, son savoir-faire mais surtout avec son rire vous--}}
                {{--fait oublier pourquoi vous êtes là. Ce ne sont que de merveilleuses vacances.</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Gabrielle - Suisse </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p> Un rajeunissement spectaculaire!! je m'attendais à avoir le regard plus frais et reposé, mais c'était au delà--}}
                {{--de mes espérances. J'ai vraiment l'air plus jeune et plus fraiche, même le matin! mon maquillage est plus--}}
                {{--facile et moins long! Je prévois aussi un maquillage permanent pour l'été et profiter de la mer sans crainte--}}
                {{--de ressembler à un vieux toutou!! Cela semble exagérer, mais cette petite intervention ( je m'en rends compte--}}
                {{--aujourd'hui) à vraiment changé mon assurance! J'ai choisi la Tunisie pour le prix pour être honnête, et ensuite--}}
                {{--pour la qualité et talents du Chirurgien, le Dr Djemal et de l'équipe médicale. Tout ce professionnalisme--}}
                {{--accompagné par une charmante personne, Morena, qui sait compléter les indications du Chirurgien et nous soutenir--}}
                {{--dans les baisses de moral ( normal quand on a les yeux enflés et déformés)...à recommander sans reticences!!</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Annick </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>De retour de Tunisie depuis 1 mois, pour une lipo. Accueil toujours aussi chaleureux de MORENA, le chirurgien--}}
                {{--, Dr Djemal et l’équipe médicale toujours aussi professionnels. C'est une équipe formidable, je récidive--}}
                {{--( lifting visage et augmentation mammaire) donc, pourquoi changer ? Merci pour tout </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Monique </h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>Juste un petit mot pour remercier toute l’équipe. Ma rhinoplastie est une réussite. Mon profil est exactement--}}
                {{--ce que je cherchais. L’équipe est absolument exceptionnelle. On se sent vraiment pris en charge avec un vrai--}}
                {{--suivi. Je suis arrivée avec quelques craintes le premier jour ,mais l’équipe médicale et l’équipe d’accompagnement--}}
                {{--a su me rassurer et je recommande vivement le chirurgien Dr Djemal,qui a des doigts en or et un vrai sens--}}
                {{--de l’esthétique. Vous pouvez prendre votre billet en toute confiance, vous ne serez pas déçu. L’équipe sait--}}
                {{--combiner professionnalisme et convivialité. N’hésiter pas à me contacter, je vous communiquerais mes photos--}}
                {{--avant/après et vous expliquerais comment se passe l’opération et la convalescence</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Synia Natty </h4>--}}
        {{--</div>--}}
    {{--</div>--}}


    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
            {{--<p>Oui j'avais peur de partir en Tunisie pour une chirurgie esthétique. Je suis allée sur plusieurs sites, lu et--}}
                {{--relu les témoignages, pris des contacts avec les personnes qui y sont allées et j'ai fait le grand saut la--}}
                {{--peur au ventre. Maintenant que de bons souvenirs sont restés gravés dans ma mémoire, j'ai retrouvé au bout--}}
                {{--de trois mois une silhouette qui a changé ma vie grâce au d.r. T.Djemal, chirurgien très expérimenté, chaleureux--}}
                {{--et plein d'humanité. Je n'oublierais pas non plus la gentillesse de Nourredine,. ainsi que de toute l'équipe--}}
                {{--de la clinique . Les accompagnateurs sont très professionnels, ils vous prennent en main du début à la fin.--}}
                {{--Aucun regret d'avoir pris cette décision. Allez-y Mesdames, n'hésitez pas et si vous avez besoin tout comme--}}
                {{--moi d'être rassurées je suis là. </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Chantal - France </h4>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials2.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials2.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                {{--<ul>--}}
                    {{--<li>--}}
                        {{--<p>Chambre 604: Mme SALWA ANTAR</p>--}}
                    {{--</li>--}}
                    {{--<li>Je souhaite remercier le plus chaleureusement possible toute l&rsquo;&eacute;quipe param&eacute;dicale--}}
                        {{--: c&rsquo;est-&agrave;-dire les infirmi&egrave;res, les aides soignantes(Melle Amen, Salwa&hellip;)--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--Aussi votre &eacute;coute attentive de tout les instants et toutes les r&egrave;gles de confort de propret&eacute; et de--}}
                        {{--client&egrave;le.--}}
                        {{--<li>--}}
                            {{--Merci pour votre accueil--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--ANTAR SALWA--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--NB/ Remerciement sp&eacute;cial pour Docteur Tahar Jmel et Docteur Noureddine Azri--}}
                        {{--</li>--}}
                {{--</ul>--}}

            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Salwa Antar</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials3.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials3.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                {{--<ul>--}}
                    {{--<li>--}}
                        {{--<p>Date:13/10/2015</p>--}}
                    {{--</li>--}}
                    {{--<li>J&rsquo;ai trouv&eacute; le repas d&rsquo;une d&eacute;licatesse et d&rsquo;un raffinement, j&rsquo;ai--}}
                        {{--beaucoup aim&eacute;. Moi qui ne mange pas beaucoup de l&eacute;gumes, je suis surprise et ravie--}}
                        {{--de ce repas. F&eacute;licitations au chef . Merci beaucoup: Olle Gob--}}
                    {{--</li>--}}
                {{--</ul>--}}

            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Olle Gob</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials4.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials4.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                {{--<ul>--}}
                    {{--<li>--}}
                        {{--Je soussign&eacute;, Mr Fakhreddine Abdeli, patient hospitalis&eacute; le 05/10/2015--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--Je certifie avoir re&ccedil;u des sevices de bonne qualit&eacute; et de haute humanit&eacute;--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--Je suis &eacute;galement ravi de connaitre un staff de tr&egrave;s bonne comp&eacute;tence et de bon caract&egrave;re.--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--Merci infiniment--}}
                    {{--</li>--}}
                    {{--<li>Bien &agrave; vous</li>--}}
                {{--</ul>--}}

            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>Fakhreddine Abdeli</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials5.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials5.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8 ">--}}
                {{--<ul>--}}
                    {{--<li>--}}
                        {{--Ousmane Abou Dialo, du Mali, Chambre 508--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--Tunis, le 2 janvier 2016--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--J&rsquo;ai &eacute;t&eacute; admis le 21 d&eacute;cembre afin de subir une intervention chirurgicale .op&eacute;ration a--}}
                        {{--&eacute;t&eacute; faite le mardi 22 d&eacute;cembre avec succ&eacute;s.--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<ul>--}}
                {{--<li>--}}
                    {{--J&rsquo;ai &eacute;t&eacute; bien accueilli par le surveillant Mr Ammar. Apr&egrave;s mon admission; le Dr Kooli est pass&eacute;--}}
                    {{--me voir dans ma chambre. Nous avons &eacute;chang&eacute; l&rsquo;intervention d&rsquo;une hernie discale--}}
                    {{--dont je souffrais depuis quelques semaines.--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--Le Dr Kooli est un excellent m&eacute;decin. Il m&rsquo;a mis en confiance depuis les 1er jour de notre rencontre qui remonte--}}
                    {{--au lundi 8 d&eacute;cembre&hellip;&hellip;.--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--J&rsquo;avoue que toute l&rsquo;&eacute;quipe de la clinique, de l&rsquo;admission &agrave; l&rsquo;&eacute;tage du cinqui&egrave;me--}}
                    {{--m&rsquo;ont impressionn&eacute;s par leur professionnalisme et leur comp&eacute;tence . En aucun moment--}}
                    {{--je n&rsquo;ai senti un d&eacute;sint&eacute;ret de la part des agents soignants. Le professeur Kooli,--}}
                    {{--le Docteur Garnaoui, le surveillant&hellip;&hellip;--}}
                {{--</li>--}}
            {{--</ul>--}}


        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Ousmane Abou Dialo</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials6.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials6.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                {{--<ul>--}}
                    {{--<li>Chambre 604</li>--}}
                    {{--<li>Janine Belhadj épouse Tlili</li>--}}
                    {{--<li>Dés la réception, accueil chaleureux</li>--}}
                    {{--<li>Le service, personnel professionnel et très attentionné et à l’écoute du patient</li>--}}
                    {{--<li>Dés l’arrivée on nous a mis en confiance et de ce fait toute l’appréhension de l’opération est effacée--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<ul>--}}
                {{--<li>Anesthésiste »Nordine » au top de son savoir faire , sait communiquer sa gentillesse avec bcp d’humour.--}}
                {{--</li>--}}
                {{--<li>Dr Djemal, Chirurgien aux mains sures a su retransmettre sur mon corps tout mon désir intérieur.--}}
                {{--</li>--}}
                {{--<li>Continuez ainsi!!!Avec toute monaffection.</li>--}}

            {{--</ul>--}}

        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>Janine Belhadj épouse Tlili</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials7.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials7.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                {{--<ul>--}}
                    {{--<li>Chambre 606</li>--}}
                    {{--<li>DEBBICHE AMINA</li>--}}
                    {{--<li>Je souhaite remercier toute l’équipe paramédicale ( les infirmières, les aides soignantes)pour leur accueil--}}
                        {{--chaleureux, leur attention et leur gentillesse et surtout DR Djemal et Dr Nourrdine, je suis déjà--}}
                        {{--très satisfaite du résultat, et je ne manquerais pas à recommander votre clinique, très propre et--}}
                        {{--très bien équipée.--}}
                    {{--</li>--}}
                    {{--<li>Encore un grand merci à Dr Djemal et Dr Nourdine pour leur savoir faire,leur humour et la bonne humeur.--}}
                    {{--</li>--}}
                    {{--<li>Bonne continuation</li>--}}

                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i>DEBBICHE AMINA</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text row">--}}
            {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                {{--<a href="{{ asset('img/testimonials/testimonials8.png') }}" class="picture-wrapper galerie">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<img src="{{ asset('img/testimonials/testimonials8.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                {{--<ul>--}}
                    {{--<li>--}}
                        {{--<p>Chambre 601 </p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>Nicole Savoy</p>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<p>Je veux dire merci à toute l’équipe de jour , de nuit , aux médecins.</p>--}}
                        {{--<p> Chez nous en Suisse cela n’existe pas.</p>--}}
                        {{--<p>Je ne peux que recommander votre clinique et je veux dire mille fois mercis au médecin qui m’a opérée,--}}
                            {{--Dr Djemal et à Nordine qui sait vous envoyer dans les bras de Morphée.</p>--}}
                    {{--</li>--}}

                    {{--<li>Avec toute ma reconnaissance,Nicole</li>--}}

                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
            {{--<h4><i class="fa fa-user"></i> Nicole</h4>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>
<!-- /.content -->
@endsection

@section('title','Témoignages chirurgie esthétique - Dr Djemal')
@section('description','Découvrez les témoignages des patients ayant subi des chirurgies esthétiques realisèes par Dr Djemal.')
