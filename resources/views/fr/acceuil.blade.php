@extends('fr.layout')

@section('class', 'home')

@section('header')


@include('fr.partials.header')


<header class="header">
      <div id="banner" class="carousel slide" data-ride="carousel" data-wrap="false" data-interval="5000">
          <div class="container">
              <div class="consultation-form col-md-4 col-sm-6 col-xs-12 pull-right">
                  <form id="consultation-form-banner" class="consultation-form__form" action="{{ action('AppController@submitConsultation') }}" method="post">
                      <div class="title text-center">
                          <div class="h3">Demandez une <span class="white">consultation gratuite</span></div>
                      </div>
                      <div class="form-group">
                          <label for="name">Nom et prénom <span class="text-danger">*</span></label>
                          <input type="text" id="name" name="name" class="form-control" placeholder="Nom et prénom"  required title="Ce champ est obligatoire">
                      </div>
                      <div class="form-group">
                          <label for="email">Adresse Email <span class="text-danger">*</span></label>
                          <input type="email" id="email" name="email" class="form-control" placeholder="Email" required  title="Email incorrect">
                      </div>
                      <div class="form-group">
                          <label for="phone">Num. téléphone <span class="text-danger">*</span></label>
                          <input type="tel" id="phone" name="phone" class="form-control phone-number" placeholder="Téléphone" id="phone" required title="Téléphone incorrect">
                      </div>
                      <!-- Country Select from bootstrap Helpers -->
                      <div class="form-group">
                          <label for="country">Pays <span class="text-danger">*</span></label>
                          <div id="country" class="bfh-selectbox bfh-countries" data-country="TN" data-flags="true" data-filter="true"></div>
                      </div>
                      <!-- End Country Select from bootstrap Helpers -->
                      <div class="form-group">
                          <label for="intervention">Intervention souhaiter <span class="text-danger">*</span></label>
                          <select class="" name="intervention" title="Interventions" required data-live-search="true" id="intervention">
                              @foreach(Config::get('app.interventions') as $label => $group)
                                  <optgroup label="{{ $label }}">
                                      @foreach($group as $item => $intervention)
                                          <option value="{{ $item }}"><b>{{ $intervention }}</b></option>
                                      @endforeach
                                  </optgroup>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="message">Votre besoin <span class="text-danger">*</span></label>
                          <textarea name="message" id="message" class="form-control" rows="10" placeholder="Message" required></textarea>
                      </div>

                      <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha.sitekey')}}" style="display: flex; justify-content: center; margin-bottom: .5rem"></div>

                      <div class="form-group">
                          <button type="submit" name="submit" class="form-control submit submit-consultation"> <i class="fa fa-check"></i> Valider & envoyer</button>
                      </div>
                  </form>
              </div>
          </div>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active item1">
                <div class="container">
                    <div class="col-md-8 col-sm-6 col-xs-12 pull-left content text-left">
                        <h2 class="title"><small>la LIPOSUCCION de</small> JEAN-MICHEL MAIRE</h2>
                        <p class="description">Suivez l’expérience de Jean-Michel Maire chez le DR DJEMAL</p>
                        <a href="/#videoJeanMarie" class="btn btn-secondary">Voir la Vidéo</a> <a href="/temoignages" class="btn btn-link">Plus de témoignages</a>
                    </div>
                </div>
                <img src="img/jean-michel-mairie.jpg" alt="LA LIPOSUCCION DE JEAN-MICHEL MAIRE chez le DR DJEMAL">
            </div>
          <div class="item">
            <img src="img/bg-banner.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/bg-banner2.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/bg-banner3.jpg" alt="...">
          </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#banner" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left fa fa-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#banner" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right fa fa-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</header>

@endsection

@section('content')

    <!-- <div class="videos-section" id="videoJeanMarie">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center section-title">
                    <h1 class="title">Témoignage</h1>
                    <div class="sub-title"><span>Nos patientes racontent leurs expériences</span></div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fdr.djemal%2Fvideos%2F2473622199426940%2F&show_text=0" width="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true" class="fb-video"></iframe>
                    <h3 class="video-title">La Liposuccion de JEAN-MICHEL MAIRE</h3>
                    <p>Le journaliste et chroniqueur Jean-Michel Maire revient sur son expérience avec Dr Djemal sur le plateau de Touche Pas à Mon Poste (TPMP) </p>
                    <br>
                    <br>
                </div>
                <div class="col-md-4">
                    <div class="player" data-plyr-provider="youtube" data-plyr-embed-id="eczrTNM2ZMM" width="100%"  height="200px"></div>
                    <h4>Témoignage Rachel: Liposuccion avec Dr Djemal</h4>
                </div>
                <div class="col-md-4">
                    <div class="player" data-plyr-provider="youtube" data-plyr-embed-id="9FSHTkbexPw" width="100%"  height="200px"></div>
                    <h4>Témoignage Fabienne: Blépharoplastie avec Dr Djemal</h4>
                </div>
                <div class="col-md-4">
                    <div class="player" data-plyr-provider="youtube" data-plyr-embed-id="k5mWDGEnWT4" width="100%"  height="200px"></div>
                    <h4>Témoignage Micheline: Lifting du visage avec Dr Djemal</h4>
                </div>
                <div class="col-md-12 text-center">
                    <a href="{{ url('temoignages') }}" class="btn">Voir les autres</a>
                </div>
            </div>
        </div>
    </div> -->

    <div class="presentation">
      <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">
                  <h1 class="title">Dr Taher Djemal</h1>
                  <div class="sub-title"><span>Présentation</span></div>
              </div>
              <div class="col-md-6">
                  <p class="lead text-primary">
                      Chirurgien spécialiste en Chirurgie Plastique Reconstructrice et Esthétique.
                  </p>
                  <p>Président de l’association des chirurgiens plastiques de Tunisie et membre de la société internationale des chirurgiens plastiques : ISAPS. Il donne régulièrement des conférences dans les congrès nationaux et internationaux. </p>
                 <p> Depuis novembre, Le Dr Djemal exerce dans la toute nouvelle clinique privée multidisciplinaire MYRON située au Lac II à 15 minutes de l’aéroport Tunis Carthage.</p>
                  <a href="{{ url('cv') }}" class="btn"> la suite</a>
                  <br><br>
              </div>
              <div class="col-md-6 text-center">
                  <img src="/img/dr-djemal.png" alt="Dr Djemal - Chirurgien spécialiste en Chirurgie Plastique Reconstructrice et Esthétique">
              </div>
              {{--<iframe width="100%" height="280px" src="https://www.youtube.com/embed/9FSHTkbexPw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
          </div>

      </div>
    </div>

    <div class="operation">
      <div class="container">
      <div class="col-md-12 text-center">
          <h1 class="title">Les spécialités</h1>
          <div class="sub-title decorated"><span>les INTERVENTIONS et CHIRURGIEs</span></div>
      </div>
        <a href="{{ url('chirurgie-de-la-silhouette') }}" class="operation-item">
            <img src="img/bloc1.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">CHIRURGIE DE LA SILHOUETTE</h2>
              <span class="read-more">Lire la suite</span>
            </div>
            <div class="mask">
                <div>CHIRURGIE DE LA SILHOUETTE</div>
                <!-- <p>
                  Botox<br>
                  Acide Hyaluronique<br>
                  Lipofilling<br>
                  Laser<br>
                  Peeling
                </p> -->
            </div>
        </a>
        <a href="{{ url('medecine-esthetique') }}" class="operation-item">
            <img src="img/bloc2.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">MEDECINE ESTHETIQUE</h2>
              <span class="read-more">Lire la suite</span>
            </div>
            <div class="mask">
                <div>MEDECINE ESTHETIQUE</div>
                <!-- <p>
                  Botox<br>
                  Acide Hyaluronique<br>
                  Lipofilling<br>
                  Laser<br>
                  Peeling
                </p> -->
            </div>
        </a>
        <a href="{{ url('chirurgie-des-seins') }}" class="operation-item">
            <img src="img/bloc3.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">CHIRURGIE DES SEINS</h2>
              <span class="read-more">Lire la suite</span>
            </div>
            <div class="mask">
                <div>CHIRURGIE DES SEINS</div>
                <!-- <p>
                  Botox<br>
                  Acide Hyaluronique<br>
                  Lipofilling<br>
                  Laser<br>
                  Peeling
                </p> -->
            </div>
        </a>
        <a href="{{ url('chirurgie-du-visage') }}" class="operation-item">
            <img src="img/bloc4.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">CHIRURGIE DU VISAGE</h2>
              <span class="read-more">Lire la suite</span>
            </div>
            <div class="mask">
                <div>CHIRURGIE DU VISAGE</div>
                <!-- <p>
                  Botox<br>
                  Acide Hyaluronique<br>
                  Lipofilling<br>
                  Laser<br>
                  Peeling
                </p> -->
            </div>
        </a>
        <a href="{{ url('chirurgie-reparatrice-et-reconstructrice') }}" class="operation-item">
            <img src="img/bloc5.png" />
            <div class="control-wrap">
              <i class="arrow fa fa-caret-up fa-lg"></i>
              <h2 class="title">CHIRURGIE REPARATRICE ET RECONSTRUCTRICE</h2>
              <span class="read-more">Lire la suite</span>
            </div>
            <div class="mask">
                <div>CHIRURGIE REPARATRICE ET RECONSTRUCTRICE</div>
                <!-- <p>
                  Botox<br>
                  Acide Hyaluronique<br>
                  Lipofilling<br>
                  Laser<br>
                  Peeling
                </p> -->
            </div>
        </a>
      </div>
    </div>

    <div class="info-section">
      <div class="container">
        <a href="http://blog.dr-djemal.com/" target="_blank">
          <div class="blog-bloc bloc">

            <h2 class="title">BLOG</h2>
            <p>Consulter les articles et les nouveautés de la chirurgie plastique en Tunisie</p>
          </div>
        </a>
        <a href="{{ url('contact') }}">
          <div class="contact-bloc bloc">
            <h2 class="title">CONTACT</h2>
            <p>Tél: (+216) 97 400 029
                contact@dr-djemal.com</p>
          </div>
        </a>
      </div>
    </div>
@endsection

@section('title','Chirurgie esthétique en Tunisie  - Dr Djemal : Intervention esthétique en Tunisie')
@section('description','Vous envisagez une chirurgie esthétique en Tunisie ? Dr Djemal, chirugien esthétique de renommée réaliser votre chirurgie esthétique en Tunisie')
