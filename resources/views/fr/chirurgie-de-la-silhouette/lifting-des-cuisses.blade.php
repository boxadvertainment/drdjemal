@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span> Lifting des cuisses </h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
      <h2>DEFINITION</h2>

      <p>Lorsqu&#39;il existe un rel&acirc;chement de la peau au niveau de la face interne des cuisses, une <a href="./lipoaspiration-des-cuisses">lipoaspiration</a> isol&eacute;e ne peut suffire &agrave; corriger ce d&eacute;faut : c&rsquo;est le lifting crural ou lifting de la face interne de la cuisse.</p>

      <p>L&#39;intervention a alors pour but de r&eacute;aliser l&#39;ablation de l&#39;exc&eacute;dent de peau, de r&eacute;duire l&#39;infiltration graisseuse sous-jacente, et de bien suspendre la peau en profondeur.</p>

      <h2>INTERVENTION</h2>

      <ul>
        <li>L&rsquo;incision est situ&eacute;e, en avant, pr&egrave;s du pli de l&rsquo;aine. Elle se prolonge ensuite dans le sillon situ&eacute; entre le p&eacute;rin&eacute;e et le haut de la face interne de la cuisse, et se poursuit en arri&egrave;re jusqu&rsquo;au pli fessier o&ugrave; elle se termine. Elle correspond &agrave; la future cicatrice.</li>
        <li>L&rsquo;incision est situ&eacute;e vers l&rsquo;int&eacute;rieur des cuisses en incision verticale.</li>
        <li>Une lipoaspiration est associ&eacute;e chaque fois qu&#39;il existe une infiltration adipeuse de la r&eacute;gion.</li>
        <li>En fin d&rsquo;intervention, on r&eacute;alise un pansement &agrave; l&rsquo;aide de bandes &eacute;lastiques collantes ou bien on met en place un panty de liposuccion.</li>
      </ul>

      <p>La dur&eacute;e de l&rsquo;intervention (anesthésie générale) est, en moyenne, d&rsquo;une heure et demi, mais elle est variable en fonction du Chirurgien et de l&rsquo;ampleur des am&eacute;liorations &agrave; apporter.</p>

      <p><img src="{{ asset('img/schema_contenu/lifting-des-cuisses.jpg') }}" /></p>

      <h2>SUITES OPERATOIRES</h2>

      <p>La sortie pourra intervenir en r&egrave;gle g&eacute;n&eacute;rale le lendemain de l&rsquo;intervention.</p>

      <p>Dans les suites op&eacute;ratoires, des ecchymoses (bleus) et un &oelig;d&egrave;me (gonflement) peuvent appara&icirc;tre. Ils r&eacute;gresseront dans les 1O &agrave; 2O jours suivant l&rsquo;intervention.</p>
      <p>Les douleurs sont en r&egrave;gle g&eacute;n&eacute;rale peu importantes, limit&eacute;es &agrave; quelques ph&eacute;nom&egrave;nes de tiraillements et d&rsquo;&eacute;lancements. La p&eacute;riode de cicatrisation peut s&#39;av&eacute;rer un peu d&eacute;sagr&eacute;able du fait de la tension qui s&#39;exerce sur les berges de la suture : durant cette p&eacute;riode, il conviendra d&#39;&eacute;viter tout mouvement d&#39;&eacute;tirement brutal.</p>
      <p>Il y a lieu de pr&eacute;voir un arr&ecirc;t de travail de 1 &agrave; 3 semaines, en fonction de la nature de l&rsquo;activit&eacute; professionnelle.</p>
      <p>La pratique d&rsquo;une activit&eacute; sportive pourra &ecirc;tre reprise progressivement &agrave; partir de la 6&egrave;me semaine post-op&eacute;ratoire. La cicatrice est souvent ros&eacute;e pendant les 3 premiers mois puis elle s&rsquo;estompe en r&egrave;gle g&eacute;n&eacute;rale apr&egrave;s le 3&egrave;me mois, et ce progressivement pendant 1 &agrave; 3 ans.</p>
      <p>Elle ne doit pas &ecirc;tre expos&eacute;e au soleil avant 3 mois.</p>

      <h2>RESULTAT</h2>
      <p>Il est appr&eacute;ci&eacute; dans un d&eacute;lai de 6 &agrave; 12 mois apr&egrave;s l&#39;intervention.</p>

      <h2>Conseils pratiques</h2>
      <p>V&ecirc;tements faciles &agrave; mettre (tenir compte des bas &agrave; varices)</p>

      </div>
@endsection

@section('title','Lifting cuisses en Tunisie - Dr Djemal : Lifting Tunisie')
@section('description','Vous voulez vous offrir un Lifting de cuisses en Tunisie? Dr Djemal, chirugien esthétique réalise votre Lifting en Tunisie')
