@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Liposuccion</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
    <h2>DEFINITION</h2>

<p>La lipoaspiration permet de supprimer radicalement et d&eacute;finitivement les surcharges de graisse localis&eacute;es.</p>

<h2>PRINCIPES</h2>

        <p>Le principe de la <a href="./lipoaspiration-des-cuisses">lipoaspiration</a> est d&rsquo;introduire,&nbsp;<strong>&agrave; partir de tr&egrave;s petites incisions</strong>, des canules &agrave; bout arrondi, non tranchant, perfor&eacute;es &agrave; leur extr&eacute;mit&eacute; de plusieurs orifices.</p>

<p>C&rsquo;est ainsi que sera possible l&rsquo;aspiration harmonieuse et non traumatisante des cellules graisseuses en surnombre.</p>

        <p>En pratique, la <a href="./lipoaspiration-des-fesses">lipoaspiration</a> peut s&rsquo;appliquer &agrave; un grand nombre de r&eacute;gion du corps : &quot; la culotte de cheval &quot; bien s&ucirc;r, mais aussi les hanches, l&rsquo;abdomen, les cuisses, les genoux, les mollets, les bras. Les am&eacute;liorations techniques ont aussi permis d&rsquo;&eacute;tendre son action au niveau du visage et du cou (double menton, ovale du visage).</p>

<p>Les r&eacute;cents progr&egrave;s, notamment dans le domaine de la liposuccion superficielle, gr&acirc;ce &agrave; l&rsquo;utilisation de canules tr&egrave;s fines font que la peau sus-jacente &agrave; la zone trait&eacute;e n&rsquo;a plus &agrave; souffrir de la lipoaspiration : au contraire, l&rsquo;aspect de la peau peut &ecirc;tre am&eacute;lior&eacute; par la r&eacute;traction cutan&eacute;e que g&eacute;n&egrave;re une lipoaspiration superficielle correctement r&eacute;alis&eacute;e.</p>

<p><strong><img src="{{ asset('img/schema_contenu/liposuccion-lioaspiration.jpg') }}" /></strong></p>

<p><strong>Zones habituelles de lipoaspiration</strong></p>

<h2>INTERVENTION</h2>

<p><strong>Les incisions sont courtes (de l&rsquo;ordre de 3 ou 4 millim&egrave;tres) et discr&egrave;tes.</strong></p>

<p>La graisse est aspir&eacute;e &agrave; l&rsquo;aide de canules, </p>

<p>La quantit&eacute; de graisse extraite devra bien s&ucirc;re &ecirc;tre adapt&eacute;e &agrave; la qualit&eacute; de la peau sus-jacente qui constitue l&rsquo;un des facteurs d&eacute;terminant pour la qualit&eacute; du r&eacute;sultat.</p>

<p>On met en place une gaine de contention &nbsp;&eacute;lastique.</p>

<p>La dur&eacute;e de l&rsquo;intervention est fonction de la quantit&eacute; de graisse &agrave; extraire et du nombre de localisations &agrave; traiter. Elle peut varier de 1 heure &agrave; 3 heures (en moyenne 1 &agrave; 2 heures).</p>

<h2>SUITES OPERATOIRES</h2>

<p>Il convient de noter que le temps n&eacute;cessaire pour r&eacute;cup&eacute;rer et se remettre d&rsquo;une lipoaspiration est proportionnel &agrave; la quantit&eacute; de graisse extraite.</p>

<p>Dans les suites op&eacute;ratoires, des ecchymoses (bleus) et un &oelig;d&egrave;me (gonflement) apparaissent au niveau des r&eacute;gions trait&eacute;es.</p>

<p>Les douleurs sont variables. Une fatigue peut &ecirc;tre ressentie les premiers jours, surtout en cas d&rsquo;extraction graisseuse importante.</p>

<p>Une activit&eacute; normale pourra &ecirc;tre reprise 4 &agrave; 7 jours apr&egrave;s l&rsquo;intervention, l&agrave; aussi en fonction de l&rsquo;importance de la lipoaspiration et du type d&rsquo;activit&eacute; professionnelle.</p>

<p>Les ecchymoses se r&eacute;sorbent dans un d&eacute;lai de 10 &agrave; 20 jours apr&egrave;s l&rsquo;intervention.</p>

<p>Le port d&rsquo;un v&ecirc;tement de contention &eacute;lastique est conseill&eacute; pendant 4 semaines.</p>

<p>On peut pr&eacute;voir une reprise de l&rsquo;activit&eacute; sportive 3 semaines apr&egrave;s l&rsquo;intervention.</p>

<p>Il conviendra de ne pas exposer au soleil ou aux U.V. les r&eacute;gions op&eacute;r&eacute;es avant au moins 3 semaines.</p>

<p>Il n&rsquo;y a pas de modification nette de l&rsquo;aspect au cours des 2 &agrave; 3 premi&egrave;res semaines, dans la mesure o&ugrave; il existe au d&eacute;but un gonflement post-op&eacute;ratoire des tissus op&eacute;r&eacute;s (&oelig;d&egrave;me).</p>

<p>C&rsquo;est seulement au bout de 3 semaines, et apr&egrave;s la r&eacute;sorption de cet &oelig;d&egrave;me, que le r&eacute;sultat commencera &agrave; appara&icirc;tre. La peau mettra environ 3 &agrave; 6 mois pour se r&eacute;tracter compl&egrave;tement sur les nouveaux galbes et se r&eacute;adapter &agrave; la nouvelle silhouette.</p>

<h2>Conseils pratiques&nbsp;:</h2>

<p>V&ecirc;tements confortables (tenir compte de la gaine, longue jusqu&rsquo;aux genoux)</p>

<p>Traitement &agrave; l&rsquo;arnica</p>

      </div>
@endsection

@section('title','Liposuccion en Tunisie - Dr Djemal')
@section('description','Vous envisagez de faire une liposuccion en Tunisie? Dr Djemal, chirugien esthétique reconnu, est là pour vous donner un corps ferme ')
