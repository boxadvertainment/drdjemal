@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Chirurgie des bras</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
    <h2>A QUI S&#39;ADRESSE CETTE TECHNIQUE ?</h2>

    <p>Aux patientes qui pr&eacute;sentent un aspect inesth&eacute;tique de leurs bras : bras trop gros, bras frip&eacute;s et qui sont tr&egrave;s g&ecirc;n&eacute;es pour se d&eacute;couvrir.</p>

    <h2>PRINCIPE TECHNIQUE</h2>

    <ul>
      <li>Pour les gros bras : lorsque la peau est assez &eacute;lastique, une lipoaspiration peut suffire. On r&eacute;alise seulement deux incisions de 2 &agrave; 3 mm situ&eacute;es sur la face interne de chaque extr&eacute;mit&eacute; des bras (sup&eacute;rieure et inf&eacute;rieure)</li>
      <li>Pour les bras frip&eacute;s ou lorsque la peau n&#39;est pas &eacute;lastique : la lipoaspiration n&#39;est pas indiqu&eacute;e et il faut alors enlever de la peau et de la graisse de mani&egrave;re chirurgicale.</li>
    </ul>

    <h2>LES CICATRICES</h2>

    <p>Elles sont situ&eacute;es &agrave; la face interne des bras. Leur longueur d&eacute;pend de la quantit&eacute; de peau &agrave; enlever. Plus on enl&egrave;ve de peau, plus la cicatrice est longue.<br />
    En cas de lipoaspiration on pratique deux incisions de deux millim&egrave;tres pour chaque bras &agrave; chacune des extr&eacute;mit&eacute;s.</p>

    <h2>D&eacute;roulement de l&#39;op&eacute;ration</h2>

    <p>On r&eacute;alise une anesth&eacute;sie g&eacute;n&eacute;rale. L&#39;intervention dure environ une heure trente. En fin d&#39;intervention on place des drains et un pansement l&eacute;g&egrave;rement compressif.</p>

    <h2>LES SUITES OPERATOIRES</h2>

    <ul>
      <li><strong>Les douleurs </strong>: elles sont mod&eacute;r&eacute;es et calm&eacute;es par des antalgiques simples et durent quelques jours.</li>
      <li><strong>Les ecchymoses</strong> : elles durent quinze jours &agrave; trois semaines.</li>
      <li><strong>L&#39;oed&egrave;me</strong> : les bras sont un peu gonfl&eacute;s pendant une semaine. Le gonflement peut durer quelques semaines sans que cela vous g&ecirc;ne dans vos activit&eacute;s.</li>
    </ul>

    <p>Il faut vous reposer et ne pas porter d&#39;objets lourds pendant environ quinze jours. Les fils sont resorbables.</p>

    <h2>L&#39;arr&ecirc;t de travail</h2>

    <p>D&eacute;pend de la profession exerc&eacute;e par le patient.</p>

    <ul>
      <li>Il peut &ecirc;tre repris tr&egrave;s rapidement si vous ne devez pas porter d&#39;objets lourds.</li>
      <li>Le sport sera repris au bout d&#39;un mois et demi, mais le reste peut travailler bien avant d&eacute;s que l&#39;on se sent en forme.</li>
    </ul>

    <h2>LE RESULTAT</h2>

    <p>Le r&eacute;sultat d&rsquo;une brachioplastie est appr&eacute;ciable au bout d&#39;un mois environ et il va s&#39;am&eacute;liorer ensuite. Il faut attendre 6 mois - 1 an pour juger l&#39;aspect d&eacute;finitif des cicatrices.</p>

      </div>
@endsection

@section('title','Chirurgie des bras - Dr Djemal')
@section('description','Vous envisagez une chirurgie des bras? Dr Djemal, chirugien esthétique vous aide à dire à dieu aux bras relachés')
