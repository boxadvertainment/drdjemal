@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Chirurgie Intime</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
    <h2><strong>Chirurgie intime de la femme</strong></h2>

<h3><strong>La labiaplastie (labioplastie ) ou nymphoplastie</strong></h3>

<p><strong>Il s&rsquo;agit d&rsquo;une intervention de <a href="./interventions-chirurgie-esthetique">chirurgie plastique</a> sur les grandes l&egrave;vres et/ou les petites l&egrave;vres, ces replis externes de la peau qui entourent les structures de la vulve. Le proc&eacute;d&eacute; consiste &agrave; r&eacute;duire les l&egrave;vres &eacute;tir&eacute;es.</strong>&nbsp;</p>

<p>L&rsquo;intervention consiste en une ablation de la peau et du tissu en exc&egrave;s.</p>

<p>L&rsquo;intervention dure 30 minutes et se d&eacute;roule sous anesth&eacute;sie locale.</p>

<p>La patiente peut repartir le jour m&ecirc;me. Une g&ecirc;ne peut &ecirc;tre ressentie pendant plusieurs jours en moyenne. La patiente peut retourner travailler au bout de 3 &agrave; 4 jours si son m&eacute;tier n&rsquo;est pas excessivement physique. Le r&eacute;sultat est mesurable au bout d&rsquo;un mois.</p>

<p>Pour la r&eacute;duction des grandes l&egrave;vres, l&rsquo;intervention consiste &agrave; retirer la graisse en exc&egrave;s par lipoaspiration, souvent combin&eacute;e &agrave; un retrait de tissu de la peau. Si au contraire la patiente souhaite redonner du volume &agrave; ses grandes l&egrave;vres, on pratique un ajout de graisse.</p>

<p>L&rsquo;intervention se d&eacute;roule en anesth&eacute;sie locale et dure 30 &agrave; 45 minutes. La patiente peut repartir le jour m&ecirc;me.</p>

<h3><strong>&nbsp;La vaginoplastie</strong></h3>

<p>L&rsquo;intervention a pour but de r&eacute;duire la taille de l&rsquo;orifice vaginal due a un rel&acirc;chement musculaire suite &agrave; un accouchement, &agrave; un mauvais &eacute;tat du tissu ou au vieillissement.</p>

<p>&nbsp;La vaginoplastie tend &agrave; redonner au vagin son &eacute;tat optimal et &agrave; retrouver la sensibilit&eacute; et le plaisir sexuel qu&rsquo;il procure.&nbsp;</p>

<p>L&rsquo;intervention dure une heure et se d&eacute;roule sous anesth&eacute;sie g&eacute;n&eacute;rale. Le retour aux activit&eacute;s quotidiennes s&rsquo;effectue apr&egrave;s 4 &agrave; 5 jours. L&rsquo;activit&eacute; sexuelle peut &ecirc;tre reprise au bout de 4 semaines.</p>

<p>&nbsp;</p>

<h2><strong>Chirurgie intime de l&rsquo;homme</strong></h2>

<h3><strong>CHIRURGIE DU PENIS</strong></h3>

<p>L&rsquo;op&eacute;ration d&rsquo;agrandissement du p&eacute;nis, ou plus pr&eacute;cis&eacute;ment l&#39;augmentation de la circonf&eacute;rence, consiste en une transplantation de graisse sous la peau du p&eacute;nis.</p>

<p>L&#39;intervention se d&eacute;roule en 2 phases op&eacute;ratoires :</p>

<ul>
  <li>Extraction de la graisse au niveau de la face interne des cuisses, de l&#39;abdomen ou du pubis.</li>
  <li>Mise en place de la graisse. Cette transplantation est r&eacute;alis&eacute;e gr&acirc;ce &agrave; une instrumentation tr&egrave;s fine et particuli&egrave;rement adapt&eacute;e &agrave; cette m&eacute;thode, &agrave; l&#39;aide de fines canules introduites dans le plan sous-cutan&eacute; du p&eacute;nis.</li>
</ul>

<p>L&rsquo;intervention d&rsquo;agrandissement du p&eacute;nis correspond &agrave; une construction progressive et dure 1/2 heure.</p>

<p>Le r&eacute;sultat final est obtenu en 3 &agrave; 6 mois en moyenne, apr&egrave;s la phase d&#39;installation du greffon. Il est impossible de pr&eacute;voir l&#39;importance du gain car le r&eacute;sultat d&eacute;pend du comportement de la graisse et du m&eacute;tabolisme du patient. Dans la majorit&eacute; des cas, le gain observ&eacute; est compris entre 2 et 4 cm de circonf&eacute;rence.</p>

<p>Il n&rsquo;y a pas de douleurs particuli&egrave;res associ&eacute;es &agrave; l&rsquo;op&eacute;ration d&rsquo;agrandissement du p&eacute;nis. Les douleurs qui peuvent appara&icirc;tre concernent plut&ocirc;t la zone d&rsquo;extraction de la graisse. La reprise des activit&eacute;s quotidiennes est effective 24 heures apr&egrave;s l&rsquo;op&eacute;ration. Un repos sexuel est n&eacute;cessaire pendant 2 semaines.</p>

<p>Les complications &eacute;ventuelles concernent la greffe de tissu graisseux avec le risque de non int&eacute;gration du transplant adipeux. Des indurations graisseuses et/ou des d&eacute;formations temporaires peuvent appara&icirc;tre. Elles dispara&icirc;tront en quelques semaines, spontan&eacute;ment ou &agrave; l&#39;aide d&#39;injections localis&eacute;es et indolores de cortico&iuml;des.</p>

      </div>
@endsection

@section('title','Chirurgie intime en Tunisie - Dr Djemal')
@section('description','Vous voudrez avoir recours à une chirurgie intime en Tunisie? Dr Djemal, chirugien esthétique est un spécialiste en la matière')
