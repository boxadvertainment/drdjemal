@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Plastie abdominale</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
    <h2>D&eacute;finition</h2>

<p>Les disgr&acirc;ces qui affectent la paroi abdominale sont particuli&egrave;rement mal ressenties et mal v&eacute;cues.</p>

<p>Il convient de bien analyser les l&eacute;sions et de savoir prendre en compte plusieurs param&egrave;tres : &eacute;tat de la peau, importance de la surcharge graisseuse, tonicit&eacute; des muscles abdominaux, morphologie g&eacute;n&eacute;rale de la patiente ou du patient : on en d&eacute;duira la strat&eacute;gie la mieux adapt&eacute;e &agrave; chaque cas.</p>

<p>Sch&eacute;matiquement, en pr&eacute;sence d&#39;une demande de correction chirurgicale de la paroi abdominale, deux cas de figures peuvent &ecirc;tre observ&eacute;s :</p>

<ul>
  <li>soit une lipoaspiration abdominale sera envisag&eacute;e isol&eacute;ment</li>
  <li>soit avoir recours &agrave; une plastie abdominale ou abdominoplastie.</li>
</ul>

<h2>Les plasties abdominales&nbsp;</h2>

<p>Chaque fois qu&#39;il existe des l&eacute;sions importantes de la peau, avec une distension notable, des vergetures nombreuses, voire des cicatrices, la lipoaspiration isol&eacute;e sera insuffisante et il faudra recourir &agrave; abdominoplastie.</p>

<p>&nbsp;</p>

<h2>Objectifs et principes&nbsp;</h2>

<p>Le but d&#39;une plastie abdominale est d&#39;enlever la peau la plus ab&icirc;m&eacute;e (distendue, cicatricielle ou vergetures) et de retendre la peau saine p&eacute;riph&eacute;rique.</p>

<p>On peut y associer dans le m&ecirc;me temps le traitement d&#39;une surcharge graisseuse localis&eacute;e par lipoaspiration et le traitement de l&eacute;sions des muscles abdominaux sous-jacents (diastasis, hernie).</p>

<p>L&#39;abdominoplastie la plus habituellement r&eacute;alis&eacute;e consiste &agrave; pratiquer l&#39;ablation d&#39;un large fuseau de peau, correspondant &agrave; tout ou partie de la r&eacute;gion situ&eacute;e entre l&#39;ombilic et le pubis, selon un dessin adapt&eacute; aux l&eacute;sions. La peau sus-jacente, saine, situ&eacute;e en r&egrave;gle g&eacute;n&eacute;rale au-dessus de l&#39;ombilic, sera drap&eacute;e vers le bas, de mani&egrave;re &agrave; reconstituer une paroi abdominale avec une peau de bonne qualit&eacute;. L&#39;ombilic est conserv&eacute; et replac&eacute; en position normale, gr&acirc;ce &agrave; une incision faite dans la peau abaiss&eacute;e.</p>


<p><strong><img src="{{ asset('img/schema_contenu/abdominoplastie.jpg') }}" /></strong></p>


        <p>Une telle <a href="../interventions-chirurgie-esthetique">chirurgie esth&eacute;tique</a> laisse toujours une cicatrice plus ou moins longue et plus ou moins cach&eacute;e, selon l&#39;importance et la localisation de la peau l&eacute;s&eacute;e dont il a fallu r&eacute;aliser l&#39;ablation.</p>
<p>Le plus souvent, cette cicatrice est situ&eacute;e au bord sup&eacute;rieur des poils pubiens. Sa longueur est pr&eacute;visible avant l&rsquo;intervention : le patient devra en &ecirc;tre tr&egrave;s clairement pr&eacute;venu.</p>

<p>&nbsp;En fin d&#39;intervention, un pansement modelant est confectionn&eacute;.</p>

<p>L&rsquo;intervention se fera sous anesthésie générale et la durée varie entre 9O minutes et 2 heures, selon l&rsquo;importance du travail &agrave; accomplir.</p>

<h2>Les suites op&eacute;ratoires&nbsp;</h2>

<p>Il faut pr&eacute;voir des pansements pendant une dizaine de jours apr&egrave;s l&#39;intervention. Le port d&#39;une gaine de soutien est conseill&eacute; pendant 4 semaines, jour et nuit. Il faut pr&eacute;voir un arr&ecirc;t de travail de 2 semaines.</p>

<p>La cicatrice est souvent ros&eacute;e pendant les 2 &agrave; 3 premiers mois, puis elle s&#39;estompe, en r&egrave;gle g&eacute;n&eacute;rale apr&egrave;s le 3&egrave;me mois et ce, progressivement, pendant 1 &agrave; 3 ans. Elle ne devra pas &ecirc;tre expos&eacute;e au soleil avant 6 mois.</p>

<p>La pratique d&rsquo;une activit&eacute; sportive pourra &ecirc;tre reprise progressivement &agrave; partir de la 6&egrave;me semaine post-op&eacute;ratoire.</p>

<h2>Le r&eacute;sultat&nbsp;</h2>

<p>Il ne peut &ecirc;tre jug&eacute; qu&#39;&agrave; partir d&#39;un an apr&egrave;s l&#39;intervention. Il convient en effet d&#39;avoir la patience d&#39;attendre le d&eacute;lai n&eacute;cessaire &agrave; l&#39;att&eacute;nuation de la cicatrice et de r&eacute;aliser pendant cette p&eacute;riode une bonne surveillance.</p>

<p>En ce qui concerne la cicatrice, il faut savoir que si elle s&rsquo;estompe bien en g&eacute;n&eacute;ral avec le temps, elle ne saurait dispara&icirc;tre compl&egrave;tement. A cet &eacute;gard, il ne faut pas oublier que si c&rsquo;est le chirurgien qui r&eacute;alise les sutures, la cicatrice, elle, est le fait du (de la) patient(e).</p>

<p>Quoi qu&#39;il en soit, il s&#39;agit d&#39;une chirurgie importante et d&eacute;licate, pour laquelle la qualit&eacute; de l&#39;indication et la rigueur du geste op&eacute;ratoire ne mettent en aucune mani&egrave;re &agrave; l&#39;abri d&#39;un certain nombre d&rsquo;imperfections, voire de complications.</p>

<h2>Les imperfections de r&eacute;sultat&nbsp;</h2>

<p>Ces imperfections concernent notamment la cicatrice qui est parfois un peu trop visible, adh&eacute;rente, voire asym&eacute;trique ou ascensionn&eacute;e. Cette cicatrice peut, dans certains cas, devenir &eacute;largie, &eacute;paisse, voire ch&eacute;lo&iuml;de.</p>

<p>L&rsquo;ombilic peut &ecirc;tre imparfaitement ext&eacute;rioris&eacute; et avoir perdu un peu de son naturel.</p>

<p>Ces imperfections de r&eacute;sultat sont en r&egrave;gle g&eacute;n&eacute;rale accessibles &agrave; un traitement compl&eacute;mentaire : &quot; retouche &quot; chirurgicale r&eacute;alis&eacute;e sous anesth&eacute;sie locale ou anesth&eacute;sie locale approfondie &agrave; partir du 12&egrave;me mois post-op&eacute;ratoire.</p>
      </div>
@endsection
@section('title','Abdominoplastie en Tunisie - Dr Djemal')
@section('description','Vous voulez faireune abdominoplastie en Tunisie? Dr Djemal, chirugien esthétique intervient pour vous rendre votre ventre ventre plat en réalisant  réaliser votre abdominoplastie en Tunisie')