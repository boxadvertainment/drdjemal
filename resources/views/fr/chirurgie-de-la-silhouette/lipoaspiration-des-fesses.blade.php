@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Lipoaspiration des fesses</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">

    <h2>DEFINITION</h2>
    <p>La <a href="./lipoaspiration-des-fesses">lipoaspiration</a> permet de supprimer radicalement et d&eacute;finitivement les surcharges de graisse localis&eacute;es au niveau des fesses</p>
    <h2>PRINCIPES</h2>

    <p>Le principe de la lipoaspiration est d&rsquo;introduire, <strong>&agrave; partir de tr&egrave;s petites incisions</strong>, des canules &agrave; bout arrondi, non tranchant, perfor&eacute;es &agrave; leur extr&eacute;mit&eacute; de plusieurs orifices.</p>

    <p>C&rsquo;est ainsi que sera possible l&rsquo;aspiration harmonieuse et non traumatisante des cellules graisseuses en surnombre.</p>

    <p>En pratique, la lipoaspiration peut s&rsquo;appliquer &agrave; un grand nombre de r&eacute;gion du corps&nbsp;: <strong>les fesses, la culotte de cheval , les cuisses, les genoux, les mollets</strong>.</p>

    <h3>HOSPITALISATION</h3>

    <p>La dur&eacute;e de l&rsquo;hospitalisation est fonction de la quantit&eacute; de graisse extraite Elle sera de 1 jour sous anesth&eacute;sie g&eacute;n&eacute;rale.</p>

    <h2>SUITES OPERATOIRES</h2>

    <p>Il convient de noter que le temps n&eacute;cessaire pour r&eacute;cup&eacute;rer et se remettre d&rsquo;une lipoaspiration est proportionnel &agrave; la quantit&eacute; de graisse extraite.</p>

    <p>Dans les suites op&eacute;ratoires, des ecchymoses (bleus) et un &oelig;d&egrave;me (gonflement) apparaissent au niveau des r&eacute;gions trait&eacute;es.</p>

    <p><strong>C&rsquo;est seulement au bout de 3 semaines, et apr&egrave;s la r&eacute;sorption de cet &oelig;d&egrave;me, que le r&eacute;sultat commencera &agrave; appara&icirc;tre. La peau mettra environ 3 &agrave; 6 mois.</strong></p>

    <h2>Conseils pratiques&nbsp;:</h2>

    <p>V&ecirc;tements confortables (tenir compte de la gaine)</p>

    <p>Traitement &agrave; l&rsquo;arnica</p>

      </div>
@endsection

@section('title','Chirurgie des fesses en Tunisie - Dr Djemal : Lipoaspiration en Tunisie')
@section('description','Vous êtes tentée âr une Chirurgie des fesses en Tunisie? Dr Djemal, chirugien esthétique réalise votre lipoaspiration en Tunisie')
