<meta name="description" content="@yield('description')">

<!-- Open Graph data -->
<meta property="og:image" content="@yield('image', isset($image) ? $image : asset('img/fb-share.jpg'))"/>
<meta property="og:title" content="@yield('title')"/>
<meta property="og:url" content="@yield('url', url('/'))"/>
<meta property="og:site_name" content="{{ Config::get('app.name') }}"/>
<meta property="og:type" content="website"/>
<meta property="og:description" content="@yield('description', isset($description) ? $description : '')"/>

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@site">
<meta name="twitter:title" content="@yield('title')">
<meta name="twitter:description" content="@yield('description', isset($description) ? $description : '')">
<meta name="twitter:image" content="@yield('image', isset($image) ? $image : asset('img/fb-share.jpg')))">
