<div class="top-header">
    <div class="container">
        <ul class="list-inline pull-left social hidden-xs">
            <li>Suivez-nous sur :</li>
            <li><a href="https://www.facebook.com/Dr-Djemal-228429484155370" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/dr_djemal" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://tn.linkedin.com/in/tahar-djemal-131071112" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
        <ul class="list-inline pull-right">
            <li><a href="https://api.whatsapp.com/send?phone=+21628288636"><i class="fa fa-phone"></i> (+216) 28 288 636</a></li>
            <li><a href="{{ url('contact') }}"><i class="fa fa-envelope"></i> Contact</a></li>
            <li>
                <div class="btn-group">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe"></i> Français <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('home') }}"> English</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<!--/.top-header -->

<nav class="navbar">
    <div class="container">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/img/logo.png" alt="" class="img-responsive"></a>
            <a href="#" class="btn-cta" data-toggle="modal" data-target="#consultationModal">Consultation<br>Gratuite</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/') }}" class="{{ Request::segment(1) == 'home' | Request::segment(1) == 'accueil' ? 'active' : ''}}">Accueil</a></li>
                <li><a href="{{ url('cv') }}" class="{{ Request::segment(1) == 'cv' ? 'active' : ''}}">Le Docteur</a></li>
                {{-- <li><a href="{{ url('interventions-chirurgie-esthetique') }}" class="{{ $page == 'interventions-chirurgie-esthetique' ? 'active' : ''}}">Interventions</a></li> --}}
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle {{ Request::segment(1) == 'interventions-chirurgie-esthetique' ? 'active' : ''}}" href="{{ url('interventions-chirurgie-esthetique') }}"> Interventions </a>
                    <ul class="dropdown-menu">

                        <li class="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#">CHIRURGIE DE LA SILHOUETTE</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/lifting-des-cuisses') }}" class="treatment-btn">Lifting Des Cuisses</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/lipoaspiration-des-cuisses') }}" class="treatment-btn">Lipoaspiration Des Cuisses</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/lipoaspiration-des-fesses') }}" class="treatment-btn">Lipoaspiration Des Fesses</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/protheses-des-fesses') }}" class="treatment-btn">Prothèses Des Fesses</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/lipofilling-des-fesses') }}" class="treatment-btn">Lipofilling Des Fesses</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/implants-mollets') }}" class="treatment-btn">Implants Mollets</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/plastie-abdominale') }}" class="treatment-btn">Plastie Abdominale</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/liposuccion') }}" class="treatment-btn">Liposuccion</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/chirurgie-des-bras') }}" class="treatment-btn">Chirurgie Des Bras</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/bodylift') }}" class="treatment-btn">Bodylift</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-de-la-silhouette/chirurgie-intime') }}" class="treatment-btn">Chirurgie Intime</a></li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#">MEDECINE ESTHETIQUE</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="{{ url('medecine-esthetique/botox') }}" class="treatment-btn">Botox</a></li>
                                <li class="dropdown-item"><a href="{{ url('medecine-esthetique/acide-hyaluronique') }}" class="treatment-btn">Acide Hyaluronique</a></li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#">CHIRURGIE DES SEINS</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="{{ url('chirurgie-des-seins/lifting-des-seins') }}" class="treatment-btn">Lifting Des Seins</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-des-seins/augmentation-mammaire') }}" class="treatment-btn">Augmentation Mammaire</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-des-seins/reduction-mammaire') }}" class="treatment-btn">Réduction Mammaire</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-des-seins/gynecomastie') }}" class="treatment-btn">Gynécomastie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-des-seins/augmentation-pectoraux') }}" class="treatment-btn">Augmentation Pectoraux</a></li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#">CHIRURGIE DU VISAGE</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/blepharoplastie') }}" class="treatment-btn">Blepharoplastie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/otoplastie') }}" class="treatment-btn">Otoplastie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/rhinoplastie') }}" class="treatment-btn">Rhinoplastie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/mentoplastie') }}" class="treatment-btn">Mentoplastie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/profiloplastie') }}" class="treatment-btn">Profiloplastie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/chirurgie-de-la-calvitie') }}" class="treatment-btn">Chirurgie De La Calvitie</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/lifting-cervico-facial') }}" class="treatment-btn">Lifting Cervico-Facial</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/lifting-complet') }}" class="treatment-btn">Lifting Complet</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/lipofilling') }}" class="treatment-btn">Lipofilling</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/chirurgie-des-levres') }}" class="treatment-btn">Chirurgie Des Levres</a></li>
                                <li class="dropdown-item"><a href="{{ url('chirurgie-du-visage/liposuccion-du-cou') }}" class="treatment-btn">Liposuccion Du Cou</a></li>
                            </ul>
                        </li>

                        <li class="dropdown-item">
                            <a href="{{ url('chirurgie-reparatrice-et-reconstructrice') }}" class="treatment-btn">CHIRURGIE REPARATRICE ET RECONSTRUCTRICE</a>
                        </li>

                    </ul>
                </li>
                <li><a href="{{ url('clinique-chirurgie-esthetique-myron') }}" class="{{ $page == 'clinique-chirurgie-esthetique-myron' ? 'active' : ''}}">Clinique Myron</a></li>
                <li><a href="{{ url('avant-apres-et-temoignages') }}" class="{{ $page == 'avant-apres-et-temoignages' ? 'active' : ''}}">Avant/Aprés</a></li>
                <li><a href="{{ url('temoignages') }}" class="{{ $page == 'temoignages' ? 'active' : ''}}">Témoignages</a></li>
                <li class="visible-xs">
                    <ul class="list-inline social">
                        <li><a href="https://www.facebook.com/Dr-Djemal-228429484155370" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/dr_djemal" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://tn.linkedin.com/in/tahar-djemal-131071112" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </li>
                <li class="btn-cta-wrapper hidden-xs"><a href="#" class="btn-cta" data-toggle="modal" data-target="#consultationModal">Consultation
                        <br>Gratuite</a></li>
            </ul>
        </div><!--/.nav-collapse -->

    </div><!--/.container -->
</nav>
