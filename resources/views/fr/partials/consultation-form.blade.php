<form id="consultation-form" class="consultation-form__form"  action="{{ action('AppController@submitConsultation') }}" method="post">
    <div class="title text-center">
        <div class="h3">Demandez une <span class="white">consultation gratuite</span></div>
    </div>
    <div class="form-group">
        <label for="name2">Nom et prénom <span class="text-danger">*</span></label>
        <input type="text" id="name2" name="name" class="form-control" placeholder="Nom et prénom"  required title="Ce champ est obligatoire">
    </div>
    <div class="form-group">
        <label for="email2">Adresse Email <span class="text-danger">*</span></label>
        <input type="email" id="email2" name="email" class="form-control" placeholder="Email" required  title="Email incorrect">
    </div>
    <div class="form-group">
        <label for="phone2">Num. téléphone <span class="text-danger">*</span></label>
        <input type="tel" id="phone2" name="phone" class="form-control phone-number in-modal" placeholder="Téléphone" required title="Téléphone incorrect">
    </div>
    <!-- Country Select from bootstrap Helpers-->
        <div class="form-group">
            <label for="country">Pays <span class="text-danger">*</span></label>
            <div id="country" class="bfh-selectbox bfh-countries" data-country="TN" data-flags="true" data-filter="true"></div>
        </div>
    <!-- End Country Select from bootstrap Helpers-->

    <div class="form-group">
        <label for="intervention2">Intervention souhaiter <span class="text-danger">*</span></label>
        <select class="" name="intervention" title="Interventions" required data-live-search="true" id="intervention2">
            @foreach(Config::get('app.interventions') as $label => $group)
                <optgroup label="{{ $label }}">
                    @foreach($group as $item => $intervention)
                        <option value="{{ $item }}"><b>{{ $intervention }}</b></option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="message2">Votre besoin <span class="text-danger">*</span></label>
        <textarea name="message" id="message2" class="form-control" rows="10" placeholder="Message" required></textarea>
    </div>

    <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha.sitekey')}}" style="display: flex; justify-content: center; margin-bottom: .5rem"></div>

    <div class="form-group">
        <button type="submit" name="submit" class="form-control submit  submit-consultation"> <i class="fa fa-check"></i> Valider & envoyer</button>
    </div>
</form>
