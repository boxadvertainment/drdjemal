<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    @include('fr.partials.socialMetaTags')

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    @include('fr.partials.favicon')

    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700,200' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugins-front.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-formhelpers.css') }}">
    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Q3QEQSRD7C"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Q3QEQSRD7C');
</script>
</head>
<body class="@yield('class')">

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    @yield('header')

    <main class="main" role="main">
        @yield('content')
    </main>

    <footer class="footer">
      <div class="container">
        <div class="row no-gutter">
          <div class="col-md-5 col-sm-6">
            <div class="row no-gutter">
              <div class="footer-col col-md-6 col-xs-6 footer-links">
                <ul>
                    <li><a href="/"><i class="fa fa-caret-right"></i>Accueil</a></li>
                    <li><a href="{{ url('cv') }}"><i class="fa fa-caret-right"></i>CV</a></li>
                    <li><a href="{{ url('interventions-chirurgie-esthetique') }}"><i class="fa fa-caret-right"></i>interventions</a></li>
                    <li><a href="{{ url('clinique-chirurgie-esthetique-myron') }}"><i class="fa fa-caret-right"></i>Clinique Myron</a></li>
                </ul>
              </div><!--//foooter-col-->
              <div class="footer-col col-md-6 col-xs-6 footer-links">
                <ul>
                    <li><a href="{{ url('avant-apres-et-temoignages') }}"><i class="fa fa-caret-right"></i>Avant/aprés</a></li>
                    <li><a href="{{ url('contact') }}"><i class="fa fa-caret-right"></i>Contact</a></li>
                    <!-- <li><a href="{{-- url('cv') --}}"><i class="fa fa-caret-right"></i>Plan du site</a></li> -->
                </ul>
              </div><!--//foooter-col-->
            </div>
          </div>
          <div class="footer-col col-md-5 col-sm-6 address">
            <p>Rue de la feuille d’érable, Résidence la brise du lac<br>
              1053  Les Berges du Lac 2 Tunis, Tunisie<br>
              Tél: (+216) 97 400 029
            </p>
          </div><!--//foooter-col-->
          <div class="clearfix visible-sm-block"></div>
          <div class="footer-col col-md-2 col-sm-12 logo-footer">
            <a href="{{ url('/') }}"><img src="/img/logo-footer.png" alt=""></a>
          </div>
        </div>
      </div>
    </footer>

    @yield('body')

    <!-- Modal -->
    <div class="modal fade" id="consultationModal" tabindex="-1" role="dialog" aria-labelledby="DevisGratuit">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body consultation-form">
              <a href="#" class="pull-right close-modal" data-dismiss="modal"> <i class="fa fa-close"></i> </a>
              @include('fr.partials.consultation-form')
          </div>
        </div>
      </div>
    </div>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/plugins-front.js') }}"></script>
    <script src="{{ asset('js/bootstrap-formhelpers.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('scripts')
    <script>
      $(".galerie").colorbox({rel:'galerie',maxWidth:'90%', minWidth:'400px'});
    </script>
</body>
</html>
