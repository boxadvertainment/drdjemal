@extends('fr.innerLayout')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Chirurgie des seins : </span> RÉDUCTION MAMMAIRE</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')

    <div class="content">

        <h2>D&eacute;finition&nbsp;</h2>

<p>Les femmes aux prises avec une poitrine volumineuse ne sont pas toutes &agrave; l&#39;aise dans leur corps. Pour plusieurs d&#39;entre elles, l&#39;inconfort li&eacute; &agrave; la masse glandulaire entra&icirc;ne des probl&egrave;mes importants tels :</p>

<ul>
  <li>Des douleurs au dos et aux &eacute;paules;</li>
  <li>Des difficult&eacute;s &agrave; marcher, &agrave; courir, &agrave; dormir, &agrave; s&#39;habiller;</li>
  <li>Des marques profondes au niveau des &eacute;paules (g&eacute;n&eacute;ralement attribuables au soutien-gorge);</li>
  <li>Un inconfort g&eacute;n&eacute;ralis&eacute;, tant physique que psychologique.</li>
</ul>

<!-- <table border="0" cellpadding="0" cellspacing="0" style="width:741px">
  <tbody>
    <tr>
      <td style="width:741px">&nbsp;</td>
    </tr>
    <tr>
      <td style="width:741px">&nbsp;</td>
    </tr>
  </tbody>
</table> -->
<!-- 
<h2>Objectifs</h2>

<p>L&rsquo;intervention chirurgicale a pour but de remettre l&rsquo;ar&eacute;ole et le mamelon en bonne position, de recentrer et d&rsquo;ascensionner la glande et de retirer la peau et la glande exc&eacute;dentaire afin d&rsquo;obtenir deux seins harmonieux, joliment galb&eacute;s et ascensionn&eacute;s.</p>
 -->
<h2>Principes</h2>

<p><em>L&rsquo;op&eacute;ration consiste &agrave; remodeler le sein en agissant sur l&rsquo;enveloppe cutan&eacute;e et sur le tissu glandulaire. </em></p>

<p><em>La glande est diminu&eacute;e, concentr&eacute;e et plac&eacute;e en bonne position. </em></p>

<p><em>La m&eacute;thode traditionnelle comprend deux incisions : une autour de l&rsquo;ar&eacute;ole, la deuxi&egrave;me verticalement de l&rsquo;ar&eacute;ole jusqu&rsquo;au pli en dessous du sein </em></p>

<p><em>Une fois l&rsquo;exc&eacute;dent de peau et de glande enlev&eacute;s, le mamelon et l&rsquo;ar&eacute;ole sont remont&eacute;s. La peau qui &eacute;tait au dessus du mamelon avant la chirurgie sera tir&eacute;e vers le bas.&nbsp;<br />
<br />
Ces deux man&oelig;uvres donneront aux seins leur nouvelle forme. On fera parfois appel &agrave; la liposuccion pour am&eacute;liorer le contour sous le bras.</em></p>

<p>&nbsp;&nbsp;</p>

<p><img src="{{ asset('img/schema_contenu/reduction-mam.jpg') }}" /></p>

<p>En fin d&rsquo;intervention un soutien-gorge sera mis.</p>

<p>En fonction de l&rsquo;importance de la r&eacute;duction, l&rsquo;intervention peut durer d&rsquo;une heure trente minutes &agrave; deux heures trente minutes.</p>

<h2>Suites op&eacute;ratoires</h2>

<p>Les suites op&eacute;ratoires sont en g&eacute;n&eacute;ral peu douloureuses, ne n&eacute;cessitant que des antalgiques simples.</p>

<p>Un gonflement (&oelig;d&egrave;me) et des ecchymoses (bleus) des seins, ainsi qu&rsquo;une g&ecirc;ne &agrave; l&rsquo;&eacute;l&eacute;vation des bras sont fr&eacute;quemment observ&eacute;s.</p>

<p>Le port du soutien-gorge est conseill&eacute; pendant environ un mois, nuit et jour, au d&eacute;cours de l&rsquo;intervention.</p>

<p>Les fils de suture, sont r&eacute;sorbables.</p>

<p>Il convient d&rsquo;envisager une convalescence et un arr&ecirc;t de travail d&rsquo;une dur&eacute;e de 7 &agrave; 10 jours.</p>

<p>On conseille d&rsquo;attendre un &agrave; deux mois pour reprendre une activit&eacute; sportive.</p>

<h2>Le r&eacute;sultat</h2>

<p>Il ne peut &ecirc;tre jug&eacute; qu&rsquo;&agrave; partir d&rsquo;un an apr&egrave;s l&rsquo;intervention : la poitrine a alors le plus souvent un galbe harmonieux et naturel, sym&eacute;trique ou tr&egrave;s proche de la sym&eacute;trie. Au-del&agrave; de l&rsquo;am&eacute;lioration locale, cette intervention a en g&eacute;n&eacute;ral un retentissement favorable sur l&rsquo;&eacute;quilibre du poids, la pratique des sports, les possibilit&eacute;s vestimentaires et l&rsquo;&eacute;tat psychologique.</p>

<p>Il convient simplement d&rsquo;avoir la patience d&rsquo;attendre le d&eacute;lai n&eacute;cessaire &agrave; l&rsquo;att&eacute;nuation des cicatrices et d&rsquo;observer pendant cette p&eacute;riode une bonne surveillance.</p>

<h2>Les imperfections</h2>

<p>Il s&rsquo;agit essentiellement des cicatrices : il est fr&eacute;quent qu&rsquo;elles prennent un aspect ros&eacute; et gonfl&eacute; au cours des deuxi&egrave;me et troisi&egrave;me mois post-op&eacute;ratoires ; au-del&agrave;, elles s&rsquo;estompent en g&eacute;n&eacute;ral progressivement pour devenir, avec le temps, peu visibles. Elles peuvent toutefois demeurer &eacute;largies, blanches ou au contraire brunes.</p>


      </div>
      <!-- /.content -->
@endsection
@section('title','Reduction mammaire en Tunisie - Dr Djemal')
@section('description','Vous envisagez reduction mammaire en Tunisie? Dr Djemal, chirugien esthétique vous aide à vous sentir mieux dans votr coprs')