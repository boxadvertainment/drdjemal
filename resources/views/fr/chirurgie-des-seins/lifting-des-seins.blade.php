@extends('fr.innerLayout')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Chirurgie des seins : </span> Lifting des seins</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')

    <div class="content">
        <h2  class="content-title"  class="content-title">D&eacute;finition&nbsp;</h2>

        <p>La ptose mammaire est d&eacute;finie par un affaissement de la glande et une distension de la peau qui l&rsquo;enveloppe. Le sein est en position trop basse et de plus fr&eacute;quemment &quot; d&eacute;shabit&eacute; &quot; dans sa partie sup&eacute;rieure.</p>

        <p>La ptose peut exister d&rsquo;embl&eacute;e mais survient le plus souvent apr&egrave;s un amaigrissement important ou apr&egrave;s une grossesse avec allaitement.</p>

        <h2  class="content-title">Objectifs</h2>

        <p>L&rsquo;intervention chirurgicale a pour but de remettre l&rsquo;ar&eacute;ole et le mamelon en bonne position, de recentrer et d&rsquo;ascensionner la glande et de retirer la peau exc&eacute;dentaire afin d&rsquo;obtenir deux seins harmonieux, joliment galb&eacute;s et ascensionn&eacute;s.</p>

        <h2  class="content-title">Principes</h2>

        <p>En cas de ptose la cicatrice est form&eacute;e de 2 composantes : p&eacute;ri-ar&eacute;olaire au pourtour de l&rsquo;ar&eacute;ole, verticale entre le p&ocirc;le inf&eacute;rieur de l&rsquo;ar&eacute;ole&nbsp;.</p>

        <p>Dans certains cas de <a href="./reduction-mammaire">ptose mammaire</a> tr&egrave;s mod&eacute;r&eacute;e, il est possible d&rsquo;utiliser une technique qui permet d&rsquo;effectuer la correction de l&rsquo;affaissement uniquement avec une cicatrice autour de l&rsquo;ar&eacute;ole.</p>

        <p>Enfin, lorsque la ptose est associ&eacute;e &agrave; une insuffisance de volume, il peut &ecirc;tre souhaitable de mettre en place, dans le m&ecirc;me temps op&eacute;ratoire, une proth&egrave;se pour redonner au sein un volume satisfaisant.</p>

        <p>Dans ce cas, il est habituellement possible de retirer l&rsquo;exc&egrave;s de peau autour de l&rsquo;ar&eacute;ole et de limiter ainsi la cicatrice uniquement &agrave; un cercle p&eacute;ri-ar&eacute;olaire.</p>

        <p><img src="{{ asset('img/schema_contenu/lifting.png') }}" /></p>

        <h2  class="content-title">Suites op&eacute;ratoires</h2>

        <p>Les suites op&eacute;ratoires sont en g&eacute;n&eacute;ral peu douloureuses, ne n&eacute;cessitant que des antalgiques simples.</p>

        <p>Un gonflement (&oelig;d&egrave;me) et des ecchymoses (bleus) des seins, ainsi qu&rsquo;une g&ecirc;ne &agrave; l&rsquo;&eacute;l&eacute;vation des bras sont fr&eacute;quemment observ&eacute;s.</p>

        <p>Le premier pansement est retir&eacute; au bout de 24 heures et remplac&eacute; par un soutien-gorge assurant une bonne contention.</p>

        <p>Le port de ce soutien-gorge est conseill&eacute; pendant environ un mois, nuit et jour, au d&eacute;cours de l&rsquo;intervention.</p>

        <p>Les fils de suture, sont r&eacute;sorbables.</p>

        <p>Il convient d&rsquo;envisager une convalescence et un arr&ecirc;t de travail d&rsquo;une dur&eacute;e de 7 &agrave; 10 jours.</p>

        <p>On conseille d&rsquo;attendre un &agrave; deux mois pour reprendre une activit&eacute; sportive.</p>

        <h2  class="content-title">Le r&eacute;sultat</h2>

        <p>Il ne peut &ecirc;tre jug&eacute; qu&rsquo;&agrave; partir d&rsquo;un an apr&egrave;s l&rsquo;intervention : la poitrine a alors le plus souvent un galbe harmonieux et naturel, sym&eacute;trique ou tr&egrave;s proche de la sym&eacute;trie.</p>

        <p>Il convient simplement d&rsquo;avoir la patience d&rsquo;attendre le d&eacute;lai n&eacute;cessaire &agrave; l&rsquo;att&eacute;nuation des cicatrices</p>

        <h2  class="content-title">Les imperfections</h2>

        <p>Il s&rsquo;agit essentiellement des cicatrices: il est fr&eacute;quent qu&rsquo;elles prennent un aspect ros&eacute; et gonfl&eacute; au cours des deuxi&egrave;me et troisi&egrave;me mois post-op&eacute;ratoires ; au-del&agrave;, elles s&rsquo;estompent en g&eacute;n&eacute;ral progressivement pour devenir, avec le temps, peu visibles.</p>
       <h2  class="content-title">Conseils pratiques</h2>

        <p>V&ecirc;tements faciles &agrave; mettre (difficile de lever les bras les premiers jours)</p>



      </div>
      <!-- /.content -->
@endsection

@section('title','Chirurgie des seins en Tunisie - Dr Djemal')
@section('description','Vous voulez vous offrir une chirurgie des seins en Tunisie? Dr Djemal, chirugien esthétique intervient pour vous la réaliser')