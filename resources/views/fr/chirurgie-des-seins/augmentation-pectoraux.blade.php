@extends('fr.innerLayout')

@section('class', 'home')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Chirurgie des seins :</span> Augmentation Pectoraux</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')

    <div class="content">
      <h2>D&eacute;finition&nbsp;</h2>

      <p>Les implants pectoraux permettent &agrave; tout &acirc;ge d&rsquo;augmenter le volume pectoral en recr&eacute;ant une projection avec un aspect naturel et visiblement plus muscl&eacute;.</p>

      <!-- <p>Pour certains hommes, les exercices de musculation des pectoraux (muscles du grand pectoral et du petit pectoral) ne suffisent pas &agrave; d&eacute;velopper la musculature du thorax.</p> -->

      <h2>Principes</h2>

      <p>Les implants pectoraux sont des proth&egrave;ses en gel de silicone imitant parfaitement le muscle. Le choix de la forme et de la taille de l&rsquo;implant se fait en fonction de la morphologie et des attentes du patient.</p>

      <p>L&rsquo;implant pectoral est log&eacute; sous anesth&eacute;sie g&eacute;n&eacute;rale sous le muscle pectoral.&nbsp;</p>

      <p>&nbsp;</p>

      <!-- <h2>Avant l&#39;intervention</h2>

      <p>Un bilan pr&eacute;-op&eacute;ratoire habituel est r&eacute;alis&eacute; conform&eacute;ment aux prescriptions.</p>

      <p>Le m&eacute;decin-anesth&eacute;siste sera vu en consultation au plus tard 48 heures avant l&rsquo;intervention.</p>

      <p>Aucun m&eacute;dicament contenant de l&rsquo;aspirine ne devra &ecirc;tre pris dans les 10 jours pr&eacute;c&eacute;dant l&rsquo;intervention.</p>

      <h3>ANESTHESIE</h3>

      <p>Il s&rsquo;agit d&rsquo;une anesth&eacute;sie g&eacute;n&eacute;rale classique</p>

      <h3>HOSPITALISATION</h3>

      <p>Une hospitalisation d&rsquo;un jour est habituellement n&eacute;cessaire.</p> -->

      <h2>Intervention</h2>

      <p>La cicatrice cutan&eacute;e est cach&eacute;e sous le pli naturel de l&rsquo;aisselle (voie axillaire) et reste donc tr&egrave;s peu visible.</p>

      <p>En fin d&rsquo;intervention un pansement modelant, avec des bandes &eacute;lastiques en forme de soutien-gorge, est confectionn&eacute;.</p>

      <h2>Suites op&eacute;ratoires</h2>

      <p>Les suites op&eacute;ratoires sont en g&eacute;n&eacute;ral peu douloureuses, ne n&eacute;cessitant que des antalgiques simples.</p>

      <p>Un gonflement (&oelig;d&egrave;me) et des ecchymoses (bleus) des seins, ainsi qu&rsquo;une g&ecirc;ne &agrave; l&rsquo;&eacute;l&eacute;vation des bras sont fr&eacute;quemment observ&eacute;s.</p>

      <p>Le premier pansement est retir&eacute; au bout de 24 heures et remplac&eacute; par un soutien-gorge assurant une bonne contention.</p>

      <h2>Le r&eacute;sultat</h2>

      <p>Des gonflements et de l&eacute;g&egrave;res ecchymoses peuvent appara&icirc;tre les premi&egrave;res semaines postop&eacute;ratoires au niveau des pectoraux. Ceux-ci s&rsquo;estompent dans les premi&egrave;res semaines postop&eacute;ratoires.</p>

      <p>Une sensation de tension est ressentie les jours suivants la pose des implants pectoraux, cette sensation peu agr&eacute;able est att&eacute;nu&eacute;e et contr&ocirc;l&eacute;e gr&acirc;ce &agrave; la prise d&rsquo;un traitement adapt&eacute; prescrit par le chirurgien.</p>

      <p>L&rsquo;activit&eacute; sportive peut &ecirc;tre reprise apr&egrave;s 1 ou 2 mois postop&eacute;ratoires. Le port d&rsquo;un v&ecirc;tement de contention est prescrit pendant 1 mois jour et nuit postop&eacute;ratoire afin de limiter les chocs.</p>

      </div>
      <!-- /.content -->
@endsection

@section('title','Augmentation pectoraux en Tunisie - Dr Djemal')
@section('description','Vous voulez une augmentation pectoraux en Tunisie? Dr Djemal, chirugien esthétique vous permet de perfectionner votre corps')
