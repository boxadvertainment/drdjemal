@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>CHIRURGIE DES LEVRES</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content"><p>Une bouche soign&eacute;e et un beau sourire transmettent automatiquement de la chaleur et de l&rsquo;&eacute;nergie alors que des l&egrave;vres pinc&eacute;es donnent un air s&eacute;v&egrave;re. Une chirurgie des l&egrave;vres corrige facilement des l&egrave;vres sup&eacute;rieures ou inf&eacute;rieures trop minces.</p>

<p>L&egrave;vres trop fines, commissures tombantes, sillon naso-g&eacute;nien marqu&eacute;... Autant de probl&egrave;mes qu&#39;une chirurgie l&eacute;g&egrave;re peut r&eacute;soudre</p>

<p><strong>Des rides sur la l&egrave;vre sup&eacute;rieure</strong>:&nbsp;Une injection d&rsquo;acide hyaluronique&nbsp;dans le liser&eacute; des l&egrave;vres, puis dans chacune des rides.</p>

<p><strong>L&#39;ACIDE HYALURONIQUE&nbsp;: </strong>d&rsquo;origine non animale et hautement biocompatible, offrant des r&eacute;sultats imm&eacute;diats, durables pour quelques mois (6 &agrave; 10 mois),</p>

<p>Par une simple injection intradermique d&#39;acide hyaluronique, les imperfections et les rides peuvent d&eacute;sormais &ecirc;tre corrig&eacute;es. L&#39;acide hyaluronique est aussi un excellent volumateur.</p>

<p>Des rougeurs, un l&eacute;ger gonflement ou des h&eacute;matomes peuvent parfois appara&icirc;tre au niveau de la zone inject&eacute;e. Ces d&eacute;sagr&eacute;ments durent peu et peuvent &ecirc;tre ais&eacute;ment camoufl&eacute;s.</p>

<p><a href="http://www.marieclaire.fr/,les-commissures-de-ma-bouche-tombent,20140,21152,11" target="_blank">Des commissures tombantes</a> :&nbsp;Une injection de toxine botulique (BOTOX)&nbsp;</p>

<p><strong>BOTOX&nbsp;: Injection </strong>dans le muscle triangulaire des l&egrave;vres qui a tendance &agrave; se r&eacute;tracter avec l&rsquo;&acirc;ge et tire sur les commissures.</p>

<h2><strong>LE BOTOX</strong></h2>

<p><strong>Le<a href="../medecine-esthetique/botox">Botox</a>&nbsp; agit </strong>au niveau des liaisons neuromusculaires. Il interrompt de mani&egrave;re temporaire la transmission de l&#39;influx nerveux aux muscles et calme ainsi les mouvements musculaires, ce qui att&eacute;nue nettement les rides. L&#39;effet de lissage s&#39;installe d&egrave;s la premi&egrave;re semaine apr&egrave;s le traitement et se maintient pendant cinq &agrave; six mois. Ensuite, les liaisons neuromusculaires se r&eacute;tablissent et les muscles trait&eacute;s se r&eacute;activent.</p>

<h2>DES LEVRES TROP FINES</h2>

<p>Une injection d&rsquo;<a href="../medecine-esthetique/acide-hyaluronique">acide hyaluronique</a>&nbsp;dans le pourtour des l&egrave;vres pour un r&eacute;sultat temporaire (6 a 10 mois) ou un <a href="./lipofilling">Lipofilling</a> (injection de graisse&nbsp; pour une augmentation des l&egrave;vres d&eacute;finitive.</p>

<h3>LIPOFILLING DES LEVRES</h3>

<p>C&#39;est une intervention chirurgicale r&eacute;alis&eacute;e le plus souvent sous anesth&eacute;sie locale assist&eacute;e.</p>

<p>L&#39;intervention consiste &agrave;&nbsp;<strong>injecter la graisse du patient dans la portion de l&egrave;vre mince, afin de l&#39;&eacute;paissir.</strong>&nbsp;Cette technique est appel&eacute;e lipofilling ou injection de graisse.</p>

<p>G&eacute;n&eacute;ralement une seule intervention permet de corriger les l&egrave;vres. Cependant jusqu&#39;&agrave; 30% maximum de la graisse inject&eacute;e peut se r&eacute;sorber. Ainsi, il faut quelque fois envisager une seconde s&eacute;ance de lipofilling pour compl&eacute;ter une &eacute;ventuelle r&eacute;sorption.</p>

<p>Les cellules graisseuses sont inject&eacute;es directement dans le muscle labial (muscle orbiculaire) afin de recr&eacute;er le c&ocirc;t&eacute; pulpeux de la bouche.</p>

<p>Nous y ajoutons une tr&egrave;s fine injection dans le contour des l&egrave;vres pour en recr&eacute;er l&#39;ourlet.</p>

<p>L&#39;intervention dure entre 30 minutes (1 l&egrave;vre) et 1 heure (2 l&egrave;vres).</p>

<p>Les cicatrices sont situ&eacute;es sur la commissure des l&egrave;vres (angle de la bouche) et sont invisibles (1mm).Quel est le r&eacute;sultat de la chirurgie ?</p>

<p>Le r&eacute;sultat de l&#39;augmentation chirurgicale des l&egrave;vres est t &nbsp;imm&eacute;diat. Cependant, le r&eacute;sultat d&eacute;finitif s&#39;appr&eacute;cie &agrave;&nbsp;<strong>3 mois</strong>, apr&egrave;s &eacute;ventuelle r&eacute;sorption d&#39;une petite partie de la graisse inject&eacute;e.</p>

      </div>
@endsection

@section('title','Chirurgie des levres en Tunisie - Dr Djemal')
@section('description','Vous envisagez une chirurgie des levres en Tunisie? Dr Djemal chirugien esthétique intervient pour vous donner les meilleures lèvres')