@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>LIPOFILLING</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
    <h2>DEFINITION</h2>

    <p>Le &laquo;&nbsp;FILLING&nbsp;&raquo; ou injections de graisse permet surtout de redonner du volume (notamment aux pommettes, front, menton) et de combler les sillons les plus profonds du visage (sillons nasog&eacute;niens, plis d&#39;amertume&hellip;). Il ne traite pas les rides fines et superficielles.&nbsp;</p>

<p>Il s&#39;agit de r&eacute;injecter de la graisse pr&eacute;lev&eacute;e sous anesth&eacute;sie locale, dans une r&eacute;gion discr&egrave;te et en petite quantit&eacute; (abdomen, hanches).</p>

<p>S&#39;agissant de la graisse de la patiente elle-m&ecirc;me, il n&#39;y a de ce fait pas d&#39;allergie possible et il n&#39;est donc pas n&eacute;cessaire de faire de test pr&eacute;alable. La graisse est centrifug&eacute;e et lav&eacute;e afin d&#39;injecter uniquement des cellules graisseuses.</p>

<p>Apr&egrave;s une phase d&#39;oed&egrave;me et de rougeur qui dure deux &agrave; trois jours, le r&eacute;sultat appara&icirc;t et la ride est combl&eacute;e.</p>

<p>Les injections sont faites en petites quantit&eacute;s mais avec une hypercorrection car une partie de cette graisse va se r&eacute;sorber (entre 20 et 50%). La dur&eacute;e de cet effet est donc variable&nbsp; de quelques mois &agrave; un an mais d&#39;autres injections pourront &ecirc;tre faites ult&eacute;rieurement.&nbsp;</p>

<p>En th&eacute;orie, cette technique n&#39;aurait que des avantages :</p>

<ul>
  <li>emploi d&#39;un produit &quot; naturel &quot; toujours bien accept&eacute; par l&#39;organisme</li>
  <li>non allergisant</li>
  <li>peu co&ucirc;teux</li>
  <li>injectable directement et simple &agrave; pr&eacute;lever.</li>
</ul>

<h2>LA TECHNIQUE</h2>

<p>Le lipofilling peut-&ecirc;tre r&eacute;alis&eacute; en 90 minutes de mani&egrave;re totalement ambulatoire et ne n&eacute;cessite aucune immobilisation. Avec la technique d&#39;injections r&eacute;p&eacute;t&eacute;es, il est m&ecirc;me possible de continuer normalement ses activit&eacute;s apr&egrave;s le traitement.<br />
<br />
<strong>Le pr&eacute;l&egrave;vement de graisse:</strong><br />
Il se fait apr&egrave;s simple anesth&eacute;sie locale de la zone &quot;donneuse&quot; (ventre, hanches, fesses...).La graisse est ponctionn&eacute;e &agrave; l&#39;aide d&#39;une seringue, aucune incision n&#39;est n&eacute;cessaire. Il ne doit pas y avoir de trace visible &agrave; l&#39;endroit du pr&eacute;l&egrave;vement.&nbsp;</p>

<p><strong>L&#39;injection </strong><br />
La graisse est inject&eacute;e avec des aiguilles de 1.2 mm de diam&egrave;tre au minimum. La technique est donc bien adapt&eacute;e pour traiter des sillons, des creux et des volumes &agrave; restaurer, plut&ocirc;t que des rides. La correction des ridules superficielles est impossible, et se fera avec d&#39;autres implants ou par m&eacute;thodes d&#39;abrasion par exemple.<br />
&nbsp;</p>

<p><strong>Le traitement d&#39;un visage</strong><br />
La correction apport&eacute;e va diminuer de 30 &agrave; 60% dans les mois qui suivent. Aussi, on proc&egrave;dera &agrave; d&#39;autres injections ult&eacute;rieures qui vont se cumuler pour obtenir en final un r&eacute;sultat stable.<br />
Quelles parties peut-on traiter avec les injections de graisse&nbsp;?</p>

<p><br />
<strong>Les meilleures indications sur le visage sont, par priorit&eacute;:</strong></p>

<ul>
  <li>les volumes des pommettes</li>
  <li>les plis d&#39;amertume et la restauration de certains ovales du visage</li>
  <li>les joues trop creuses</li>
  <li>les sillons nasog&eacute;niens</li>
  <li>les tempes creuses</li>
  <li>la restauration des arcades sourcili&egrave;res</li>
  <li>l&#39;aspect bomb&eacute; du front</li>
  <li>les s&eacute;quelles de lipoaspiration en creux.&nbsp;</li>
</ul>

<p><strong>Les autres sont moins bonnes : </strong></p>

<ul>
  <li>certaines rides du lion tr&egrave;s larges</li>
  <li>le volume des l&egrave;vres de certaines bouches</li>
  <li>les cernes et avec grandes pr&eacute;cautions, les &quot; yeux creux &quot;.</li>
</ul>

<h2>Les contre-indications</h2>

        <p>Elles sont peu nombreuses, vu le caract&egrave;re personnel de l&rsquo;injection (pas d&#39;allergie, pas de probl&egrave;me immunitaire).<br />
On retiendra des pr&eacute;cautions particuli&egrave;res &agrave; prendre chez les personnes diab&eacute;tiques ou s&eacute;ropositives, plus sensibles &agrave; l&#39;infection, et on &eacute;vitera d&#39;injecter &agrave; travers des peaux pr&eacute;sentant des dermatoses ou des l&eacute;sions acn&eacute;iques.<br />
<br />
Conclusion</p>

<p>La m&eacute;thode du Lipofilling ne traite pas tous les aspects esth&eacute;tiques du visage, mais employ&eacute;e dans ses bonnes indications, elle donne de tr&egrave;s bons r&eacute;sultats.</p>


      </div>
@endsection

@section('title','Lipofilling en Tunisie - Dr Djemal : Chirurgie lipofilling ')
@section('description','Vous envisagez un lipofilling en Tunisie? Dr Djemal, chirugien esthétique vous réalise la chirurgie lipofilling en Tunisie')