@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>Lifting Cervico-facial</h1>
    </div>
  </header>
@endsection

@section('fr.innerContent')
    <div class="content">
    <h2>D&eacute;finition&nbsp;</h2>

    <p>Le lifting cervico-facial est le plus fr&eacute;quemment r&eacute;alis&eacute;; c&rsquo;est la correction chirurgicale du vieillissement au niveau du cou ainsi qu&rsquo;au niveau du visage depuis la r&eacute;gion des tempes jusqu&rsquo;&agrave; la r&eacute;gion des bajoues.</p>

    <h2>Objectifs</h2>

    <p>L&rsquo;op&eacute;ration a pour but de traiter l&rsquo;affaissement et le rel&acirc;chement de la peau et des muscles du visage (tempes et sourcils, joues, bajoues, ovale du visage) et du cou.</p>

    <p>L&rsquo;objectif d&rsquo;une telle intervention n&rsquo;est pas de changer la forme et l&rsquo;aspect du visage. Au contraire, la simple restauration des diff&eacute;rentes structures anatomiques de la face et du cou (peau, muscle, graisse) permet de retrouver l&rsquo;aspect qui &eacute;tait le sien quelques ann&eacute;es auparavant.</p>

    <h2>Principes</h2>

    <p>Les muscles sont remis en tension, de mani&egrave;re &agrave; corriger leur rel&acirc;chement. La peau est ensuite drap&eacute;e &agrave; la demande sur les nouveaux galbes, sans traction excessive. Cette double action permet d&rsquo;obtenir un r&eacute;sultat naturel (car la peau n&rsquo;est pas trop tir&eacute;e), durable (car le plan musculaire est solide), des suites op&eacute;ratoires en g&eacute;n&eacute;ral assez simples (la peau &quot; marque &quot; moins parce qu&rsquo;elle est peu traumatis&eacute;e, du fait de la tension et du d&eacute;collement limit&eacute;s). Les surcharges graisseuses &eacute;ventuelles peuvent &ecirc;tre trait&eacute;es par lipoaspiration.</p>

    <p>A l&rsquo;inverse, si le visage est &eacute;maci&eacute;, cet aspect peut &ecirc;tre corrig&eacute; dans le m&ecirc;me temps par r&eacute;injection de graisse (<a href="./lipofilling">lipofilling</a>).</p>

    <p>Ainsi le visage et le cou sont en quelque sorte &quot; reconstruits &quot;, &quot; restructur&eacute;s &quot;.</p>

    <p>Les incisions cutan&eacute;es n&eacute;cessaires sont cach&eacute;es pour l&rsquo;essentiel dans les cheveux (au niveau des tempes et de la nuque) et au pourtour de l&rsquo;oreille. La cicatrice est ainsi presque enti&egrave;rement dissimul&eacute;e.</p>

    <p>Le lifting cervico-facial peut &ecirc;tre fait d&egrave;s que les signes de vieillissement apparaissent et qu&rsquo;une demande de correction motiv&eacute;e en est formul&eacute;e, g&eacute;n&eacute;ralement &agrave; partir de 40 ou 45 ans.</p>

    <p>Cette intervention peut &ecirc;tre associ&eacute;e &agrave; un autre geste de chirurgie esth&eacute;tique faciale: <a href="./blepharoplastie">chirurgie esth&eacute;tique des paupi&egrave;res</a> des paupi&egrave;res (bl&eacute;pharoplastie). Elle peut aussi &ecirc;tre compl&eacute;t&eacute;e par des th&eacute;rapeutiques m&eacute;dico-chirurgicales (laserbrasion, dermabrasion, peeling, traitement m&eacute;dical des rides ou sillons, injections de toxine botulique).</p>

    <p><img src="{{ asset('img/schema_contenu/jou-ci-pas.jpg') }}" /></p>

    

    <h3>ANESTHESIE</h3>

    <p>Le lifting cervico-facial sera r&eacute;alis&eacute; sous anesth&eacute;sie g&eacute;n&eacute;rale</p>

    <h3>Hospitalisation</h3>

    <p>Une hospitalisation de 24 &agrave; 48 heures est habituellement n&eacute;cessaire.</p>

    <h2>Les suites op&eacute;ratoires&nbsp;</h2>

    <p>La sortie pourra intervenir soit le lendemain, soit le surlendemain de l&rsquo;intervention.</p>

    <p>Les premiers jours, il faut se reposer au maximum et &eacute;viter tout effort violent.</p>

    <p>Au cours de ces premiers jours, apparition :</p>

    <ul>
      <li>d&rsquo;un &oelig;d&egrave;me (gonflement) qui peut &ecirc;tre plus accentu&eacute; le deuxi&egrave;me jour que le premier,</li>
      <li>d&rsquo;ecchymoses (bleus) dans la r&eacute;gion du cou et du menton,</li>
      <li>d&rsquo;une sensation de tension douloureuse, surtout en arri&egrave;re des oreilles, et autour du cou.</li>
    </ul>

    <p>Ces ecchymoses et ces oed&egrave;mes disparaissent en moyenne dans les 2 semaines post-op&eacute;ratoires.</p>

    <p>Au bout du premier mois, le gonflement a en g&eacute;n&eacute;ral presque disparu. Mais il persiste une l&eacute;g&egrave;re induration des zones d&eacute;coll&eacute;es, plus palpable que visible. Les oreilles ne retrouvent leur sensibilit&eacute; normale qu&rsquo;un ou deux mois plus tard.</p>

    <p>Les cicatrices sont cach&eacute;es en avant et en arri&egrave;re par les cheveux. La seule zone l&eacute;g&egrave;rement visible, en avant de l&rsquo;oreille, peut &ecirc;tre temporairement masqu&eacute;e par la coiffure ou le maquillage.</p>

    <p><strong>En r&eacute;sum&eacute; on est :</strong></p>

    <ul>
      <li>au septi&egrave;me jour, pr&eacute;sentable pour les intimes,</li>
      <li>vers le douzi&egrave;me jour, pr&eacute;sentable pour ses amis,</li>
      <li>mais pour para&icirc;tre devant les personnes dont on veut qu&rsquo;elles ignorent l&rsquo;op&eacute;ration, il est n&eacute;cessaire de pr&eacute;voir trois semaines.</li>
    </ul>

    <h2>Le r&eacute;sultat&nbsp;</h2>

    <p>Au bout de deux &agrave; trois mois, on peut avoir une bonne id&eacute;e du r&eacute;sultat d&eacute;finitif du lifting cervico-facial. Mais les cicatrices sont encore un peu ros&eacute;es et indur&eacute;es et ne s&rsquo;att&eacute;nuent que vers le sixi&egrave;me mois.</p>

    <p>Gr&acirc;ce aux progr&egrave;s accomplis et &agrave; une grande minutie technique, on obtient le plus souvent un effet de rajeunissement appr&eacute;ciable, qui reste toutefois tr&egrave;s naturel : le visage n&rsquo;a pas un aspect &quot; chirurgical &quot; et a retrouv&eacute; approximativement les traits qui &eacute;taient les siens huit ou douze ans auparavant, ce qui donne globalement un aspect repos&eacute;, d&eacute;tendu et rafra&icirc;chi.</p>

    <h2>Les imperfections de r&eacute;sultat&nbsp;</h2>

    <p>Il peut s&rsquo;agir pour l&rsquo;essentiel :</p>

    <ul>
      <li>d&rsquo;un &oelig;d&egrave;me (gonflement) persistant au niveau de certaines zones au-del&agrave; du troisi&egrave;me mois et qui peut n&eacute;cessiter des massages,</li>
      <li>d&rsquo;une reproduction partielle du rel&acirc;chement des tissus (ptose), notamment de la partie ant&eacute;rieure du cou, lorsque la ptose &eacute;tait importante avant l&rsquo;intervention,</li>
      <li>de cicatrices trop visibles ou de chute de cheveux localis&eacute;e dans la r&eacute;gion des tempes (alop&eacute;cie) qui peuvent n&eacute;cessiter une retouche chirurgicale &agrave; distance (six mois &agrave; un an).</li>
    </ul>

      </div>
@endsection

@section('title','Lifting cervico facial en Tunisie - Dr Djemal : lifting cervical en Tunisie ')
@section('description','Vous envisagez Lifting cervico facial en Tunisie? Dr Djemal, chirugien esthétique vous réalise le lifting cervical en Tunisie')