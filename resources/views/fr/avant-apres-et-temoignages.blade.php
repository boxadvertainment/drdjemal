@extends('fr.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-dr.jpg);">

    @include('fr.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">Résultats</span> Avant/Aprés & témoignages</h1>
    </div>
</header>
@endsection

@section('fr.innerContent')
    <div class="content">
      <h2 class="content-title">avant/aprés</h2>
    </div>

    <div class="tabs-wrapper">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#visage" aria-controls="visage" role="tab" data-toggle="tab">visage</a></li>
        <li role="presentation"><a href="#silhouette" aria-controls="silhouette" role="tab" data-toggle="tab">silhouette</a></li>
        <li role="presentation"><a href="#seins" aria-controls="seins" role="tab" data-toggle="tab">seins</a></li>
        <li role="presentation"><a href="#hommes" aria-controls="hommes" role="tab" data-toggle="tab">hommes</a></li>
        <li role="presentation"><a href="#chirurgiereparatrice" aria-controls="chirurgiereparatrice" role="tab" data-toggle="tab">chirurgie réparatrice</a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="visage">

            <div class="before-after gallery-wrapper clearfix">

                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/1.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/1.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/2.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/2.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/3.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/3.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/5.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/5.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/6.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/6.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/7.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/7.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/8.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/8.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/08.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/08.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/9.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/9.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/09.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/09.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/10.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/10.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/11.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/11.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/12.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/12.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/13.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/13.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/14.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/14.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/16.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/16.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/17.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/17.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/18.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/18.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/19.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/19.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/20.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/20.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/21.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/21.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/22.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/22.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/23.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/23.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/24.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/24.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/25.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/25.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/26.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/26.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/27.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/27.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/28.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/28.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/29.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/29.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/30.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/30.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/31.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/31.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/37.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/37.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/38.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/38.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/39.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/39.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/1.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/1.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/2.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/2.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/3.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/3.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/5.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/5.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/7.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/7.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/9.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/9.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/11.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/11.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/16.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/16.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/18.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/18.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/19.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/19.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/20.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/20.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/22.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/22.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/24.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/24.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/32.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/32.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/34.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/34.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/35.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/35.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/visage/36.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/visage/36.png') }}" alt="">
                    </a>
                </div>





            </div><!-- gallery-wrapper End -->

        </div><!-- Tab visage -->

        <div role="tabpanel" class="tab-pane fade" id="silhouette">

            <div class="before-after gallery-wrapper clearfix">

                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/1.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/1.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/01.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/01.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/2.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/2.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/02.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/02.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/3.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/3.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/03.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/03.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/4.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/4.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/5.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/5.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/6.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/6.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/7.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/7.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/8.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/8.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/9.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/9.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/10.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/10.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/11.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/11.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/12.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/12.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/13.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/13.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/14.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/14.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/15.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/15.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/16.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/16.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/17.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/17.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/18.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/18.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/silhouette/19.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/silhouette/19.jpg') }}" alt="">
                    </a>
                </div>



            </div><!-- gallery-wrapper End -->

        </div><!-- Tab silhouette End -->
        <div role="tabpanel" class="tab-pane fade" id="seins">

            <div class="before-after gallery-wrapper clearfix">

                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/1.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/1.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/2.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/2.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/3.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/3.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/4.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/4.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/5.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/5.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/6.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/6.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/7.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/7.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/8.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/8.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/9.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/9.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/10.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/10.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/11.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/11.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/12.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/12.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/13.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/13.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/14.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/14.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/15.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/15.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/16.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/16.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/seins/17.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/seins/17.jpg') }}" alt="">
                    </a>
                </div>


            </div><!-- gallery-wrapper End -->

        </div><!-- Tab seins End -->

        <div role="tabpanel" class="tab-pane fade" id="hommes">

            <div class="before-after gallery-wrapper clearfix">

                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/1.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/1.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/2.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/2.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/3.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/3.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/4.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/4.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/5.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/5.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/6.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/6.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/7.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/7.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/8.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/8.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/8.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/8.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/9.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/9.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/10.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/10.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/11.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/11.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/12.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/12.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/14.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/14.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/15.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/15.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/16.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/16.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/17.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/17.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/18.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/18.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/19.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/19.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/20.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/20.jpg') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/homme/21.jpg') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/homme/21.jpg') }}" alt="">
                    </a>
                </div>



            </div><!-- gallery-wrapper End -->

        </div><!-- Tab hommes End -->
        <div role="tabpanel" class="tab-pane fade" id="chirurgiereparatrice">

            <div class="before-after gallery-wrapper clearfix">

                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/1.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/1.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/2.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/2.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/3.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/3.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/4.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/4.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/5.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/5.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/6.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/6.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/7.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/7.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/8.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/8.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/9.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/9.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/10.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/10.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/11.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/11.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/12.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/12.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/13.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/13.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <a href="{{ asset('img/temoingnage/réparatrice/14.png') }}" class="picture-wrapper galerie">
                        <div class="overlay"></div>
                        <img src="{{ asset('img/temoingnage/réparatrice/14.png') }}" alt="">
                    </a>
                </div>

            </div><!-- gallery-wrapper End -->

        </div><!-- Tab chirurgie réparatrice -->

      </div>

    </div>

    {{--<div class="content testimonials">--}}
      {{--<h2 class="content-title">Témoignages</h2>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p> Je conseille vivement à toute personne désireuse de faire une chirurgie esthétique d'y aller en toute confiance vers ce Chirurgien, le Dr Djemal et cette equipe. Vous êtes entre de bonnes mains ( Dr Djemal, Nourredine et l’équipe médicale très,tres,tres  bien) et très entourés par Morena. Allez y sans appréhension!!! </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Marie</h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p> j'ai fait une augmentation mammaire et le résultat 10 jours plus tard est déjà superbe! Je suis ravie! Pour cette opération, je suis venue seule et j'ai été prise en charge dès mon arrivé par Morena. Je ne me suis sentie à aucun moment seule, car l'accueil est formidable! Morena est toujours disponible pour nous, à la clinique le personnel était très attentif et gentil! Je recommande sans aucun problème le Dr Djemal  ainsi que tout le personnel de la clinique! Si vous avez des questions, n'hésitez pas à me contacter! </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Nadège B. </h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p>2 semaines seulement après 1 abdominoplastie et 1 lipo complète, je n'ai plus aucun "bleu" et surtout 1 résultat deja visible et mieux que ce que j'espérais. 1 grand merci à toute l'équipe médicale en particulier au medecin, le Dr Djemal. et aussi a Morena pour son accompagnement et soutient et sa toujours bonne humeur. a bientot pour 1 lifting  </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Antoinette S.</h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p>Après une opération malheureuse en France, au hasard d'une émission TV je me suis tournée  vers la chirurgie esthétique en Tunisie : liposuccion complète et abdominoplastie. <br>--}}
{{--J'en garde un souvenir mémorable, des gens exceptionnels, <b>surtout le chirurgien Djemal, l’anesthésiste Nourredine</b>. très professionnels, l'équipe médical au top, l'accueil,, la prise en charge, l'hébergement dans un hôtel sublime, le tout dans un site merveilleux, je ne parle plus d'opération mais de vacances, que de bons souvenirs....et surtout un corps plus harmonieux que j’aime mettre en valeur. <br>--}}
{{--N'hésitez pas à me contacter à monique.moscipan@free.fr--}}
{{--</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Monique - France</h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p>J'ai subi deux chirurgies esthétiques en Tunisie, toujours avec le même chirurgien, le Dr Djemal et la même équipe. Quel bonheur d'être aussi bien assistée. Prise en charge à l'aéroport, transfert ,tout est parfait. La clinique, c'est le paradis, vous existez vraiment, tout le monde est au petit soin, que ce soit de la simple aide-soignante au Grand Chirurgien, c'est la famille. On vous écoute, vous cajole, vous donne des conseils. Mes opérations se sont supers bien passées (diminution mammaire et paupières). Les résultats sont supers. Je me sens belle et rajeunit, oh oui!!!! Quelle belle expérience!. Et surtout n'oubliez pas que la personne qui vous accompagne tout au long de votre séjour, Morena, par sa gentillesse, son savoir-faire mais surtout avec son rire vous fait oublier pourquoi vous êtes là. Ce ne sont que de merveilleuses vacances.</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Gabrielle - Suisse </h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p> Un rajeunissement spectaculaire!! je m'attendais à avoir le regard plus frais et reposé, mais c'était au delà de mes espérances. J'ai vraiment l'air plus jeune et plus fraiche, même le matin! mon maquillage est plus facile et moins long! Je prévois aussi un maquillage permanent pour l'été et profiter de la mer sans crainte de ressembler à un vieux toutou!! Cela semble exagérer, mais cette petite intervention ( je m'en rends compte aujourd'hui) à vraiment changé mon assurance! J'ai choisi la Tunisie pour le prix pour être honnête, et ensuite pour la qualité et talents du Chirurgien, le Dr Djemal et de l'équipe médicale. Tout ce professionnalisme accompagné par une charmante personne, Morena, qui sait compléter les indications du Chirurgien et nous soutenir dans les baisses de moral ( normal quand on a les yeux enflés et déformés)...à recommander sans reticences!!</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Annick </h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p>De retour de Tunisie depuis 1 mois, pour une lipo. Accueil toujours aussi chaleureux de MORENA, le chirurgien , Dr Djemal et l’équipe médicale toujours aussi professionnels. C'est une équipe formidable, je récidive ( lifting visage et augmentation mammaire) donc, pourquoi changer ? Merci pour tout </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Monique  </h4>--}}
        {{--</div>--}}
      {{--</div>--}}

      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p>Juste un petit mot pour remercier toute l’équipe. Ma rhinoplastie est une réussite. Mon profil est exactement ce que je cherchais. L’équipe est absolument exceptionnelle. On se sent vraiment pris en charge avec un vrai suivi. Je suis arrivée avec quelques craintes le premier jour ,mais l’équipe médicale et l’équipe d’accompagnement a su me rassurer et je recommande vivement le chirurgien Dr Djemal,qui a des doigts en or et un vrai sens de l’esthétique. Vous pouvez prendre votre billet en toute confiance, vous ne serez pas déçu. L’équipe sait combiner professionnalisme et convivialité. N’hésiter pas à me contacter, je vous communiquerais mes photos avant/après et vous expliquerais comment se passe l’opération et la convalescence</p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Synia Natty  </h4>--}}
        {{--</div>--}}
      {{--</div>--}}


      {{--<div class="testi col-md-9 col-md-offset-1">--}}
        {{--<div class="text">--}}
          {{--<p>Oui j'avais peur de partir en Tunisie pour une chirurgie esthétique. Je suis allée sur plusieurs sites, lu et relu les témoignages, pris des contacts avec les personnes qui y sont allées et j'ai fait le grand saut la peur au ventre. Maintenant que de bons souvenirs sont restés gravés dans ma mémoire, j'ai retrouvé au bout de trois mois une silhouette qui a changé ma vie grâce au d.r. T.Djemal, chirurgien très expérimenté, chaleureux et plein d'humanité. Je n'oublierais pas non plus la gentillesse de Nourredine,. ainsi que de toute l'équipe de la clinique . Les accompagnateurs sont très professionnels, ils vous prennent en main du début à la fin. Aucun regret d'avoir pris cette décision. Allez-y Mesdames, n'hésitez pas et si vous avez besoin tout comme moi d'être rassurées je suis là. </p>--}}
        {{--</div>--}}
        {{--<div class="patient">--}}
          {{--<h4><i class="fa fa-user"></i> Chantal - France </h4>--}}
        {{--</div>--}}
      {{--</div>--}}
            {{--<div class="testi col-md-9 col-md-offset-1">--}}
                {{--<div class="text row">--}}
                    {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                        {{--<a href="{{ asset('img/testimonials/testimonials2.png') }}" class="picture-wrapper galerie">--}}
                            {{--<div class="overlay"></div>--}}
                            {{--<img src="{{ asset('img/testimonials/testimonials2.png') }}" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                        {{--<ul>--}}
                            {{--<li>--}}
                                {{--<p>Chambre 604: Mme SALWA ANTAR</p>--}}
                            {{--</li>--}}
                            {{--<li>Je souhaite remercier le plus chaleureusement possible toute l&rsquo;&eacute;quipe param&eacute;dicale : c&rsquo;est-&agrave;-dire les infirmi&egrave;res, les aides soignantes(Melle Amen, Salwa&hellip;)--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--Aussi votre &eacute;coute attentive de tout les instants et toutes les r&egrave;gles de confort de propret&eacute; et de client&egrave;le.--}}
                            {{--<li>--}}
                                {{--Merci pour votre accueil--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--ANTAR SALWA--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--NB/ Remerciement sp&eacute;cial pour Docteur Tahar Jmel et Docteur Noureddine Azri--}}
                            {{--</li>--}}
                        {{--</ul>--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="patient">--}}
                    {{--<h4><i class="fa fa-user"></i> Salwa Antar</h4>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--<div class="testi col-md-9 col-md-offset-1">--}}
            {{--<div class="text row">--}}
                {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                    {{--<a href="{{ asset('img/testimonials/testimonials3.png') }}" class="picture-wrapper galerie">--}}
                        {{--<div class="overlay"></div>--}}
                        {{--<img src="{{ asset('img/testimonials/testimonials3.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                    {{--<ul>--}}
                        {{--<li>--}}
                            {{--<p>Date:13/10/2015</p>--}}
                        {{--</li>--}}
                        {{--<li>J&rsquo;ai trouv&eacute; le repas d&rsquo;une d&eacute;licatesse et d&rsquo;un raffinement, j&rsquo;ai beaucoup aim&eacute;. Moi qui ne mange pas beaucoup de l&eacute;gumes, je suis surprise et ravie de ce repas. F&eacute;licitations au chef .--}}
                            {{--Merci beaucoup: Olle Gob</li>--}}
                    {{--</ul>--}}

                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="patient">--}}
                {{--<h4><i class="fa fa-user"></i> Olle Gob</h4>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="testi col-md-9 col-md-offset-1">--}}
            {{--<div class="text row">--}}
                {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                    {{--<a href="{{ asset('img/testimonials/testimonials4.png') }}" class="picture-wrapper galerie">--}}
                        {{--<div class="overlay"></div>--}}
                        {{--<img src="{{ asset('img/testimonials/testimonials4.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                    {{--<ul>--}}
                        {{--<li>--}}
                            {{--Je soussign&eacute;, Mr Fakhreddine Abdeli, patient hospitalis&eacute; le 05/10/2015--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--Je certifie avoir re&ccedil;u des sevices de bonne qualit&eacute; et de haute humanit&eacute;--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--Je suis &eacute;galement ravi de connaitre un staff de tr&egrave;s bonne comp&eacute;tence et de bon caract&egrave;re.--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--Merci infiniment--}}
                        {{--</li>--}}
                        {{--<li>Bien &agrave; vous</li>--}}
                    {{--</ul>--}}

                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="patient">--}}
                {{--<h4><i class="fa fa-user"></i>Fakhreddine Abdeli</h4>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="testi col-md-9 col-md-offset-1">--}}
            {{--<div class="text row">--}}
                {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                    {{--<a href="{{ asset('img/testimonials/testimonials5.png') }}" class="picture-wrapper galerie">--}}
                        {{--<div class="overlay"></div>--}}
                        {{--<img src="{{ asset('img/testimonials/testimonials5.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-5 col-sm-8 ">--}}
                    {{--<ul>--}}
                        {{--<li>--}}
                            {{--Ousmane Abou Dialo, du Mali, Chambre 508--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--Tunis, le 2 janvier 2016--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--J&rsquo;ai &eacute;t&eacute; admis le 21 d&eacute;cembre afin de subir une intervention chirurgicale .op&eacute;ration a &eacute;t&eacute; faite le mardi 22 d&eacute;cembre avec succ&eacute;s.--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<ul>--}}
                        {{--<li>--}}
                            {{--J&rsquo;ai &eacute;t&eacute; bien accueilli par le surveillant Mr Ammar. Apr&egrave;s mon admission; le Dr Kooli est pass&eacute; me voir dans ma chambre. Nous avons &eacute;chang&eacute; l&rsquo;intervention d&rsquo;une hernie discale dont je souffrais depuis quelques semaines.--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--Le Dr Kooli est un excellent m&eacute;decin. Il m&rsquo;a mis en confiance depuis les 1er jour de notre rencontre qui remonte au lundi 8 d&eacute;cembre&hellip;&hellip;.--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--J&rsquo;avoue que toute l&rsquo;&eacute;quipe de la clinique, de l&rsquo;admission &agrave; l&rsquo;&eacute;tage du cinqui&egrave;me m&rsquo;ont impressionn&eacute;s par leur professionnalisme et leur comp&eacute;tence . En aucun moment je n&rsquo;ai senti un d&eacute;sint&eacute;ret de la part des agents soignants. Le professeur Kooli, le Docteur Garnaoui, le surveillant&hellip;&hellip;--}}
                        {{--</li>--}}
                    {{--</ul>--}}


            {{--</div>--}}
            {{--<div class="patient">--}}
                {{--<h4><i class="fa fa-user"></i> Ousmane Abou Dialo</h4>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="testi col-md-9 col-md-offset-1">--}}
            {{--<div class="text row">--}}
                {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                    {{--<a href="{{ asset('img/testimonials/testimonials6.png') }}" class="picture-wrapper galerie">--}}
                        {{--<div class="overlay"></div>--}}
                        {{--<img src="{{ asset('img/testimonials/testimonials6.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                    {{--<ul>--}}
                        {{--<li>Chambre 604 </li>--}}
                        {{--<li>Janine Belhadj épouse Tlili</li>--}}
                        {{--<li>Dés la réception, accueil chaleureux </li>--}}
                        {{--<li>Le service, personnel professionnel et très attentionné et à l’écoute du patient</li>--}}
                        {{--<li>Dés l’arrivée on nous a mis en confiance et de ce fait toute l’appréhension de l’opération est effacée</li>--}}
                    {{--</ul></div>--}}
                {{--<ul>--}}
                        {{--<li>Anesthésiste »Nordine » au top de son savoir faire , sait communiquer sa gentillesse avec bcp d’humour.</li>--}}
                        {{--<li>Dr Djemal, Chirurgien aux mains sures a su retransmettre sur mon corps tout mon désir intérieur.</li>--}}
                        {{--<li>Continuez ainsi!!!Avec toute monaffection.</li>--}}

                    {{--</ul>--}}

            {{--</div>--}}
            {{--<div class="patient">--}}
                {{--<h4><i class="fa fa-user"></i>Janine Belhadj épouse Tlili</h4>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="testi col-md-9 col-md-offset-1">--}}
            {{--<div class="text row">--}}
                {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                    {{--<a href="{{ asset('img/testimonials/testimonials7.png') }}" class="picture-wrapper galerie">--}}
                        {{--<div class="overlay"></div>--}}
                        {{--<img src="{{ asset('img/testimonials/testimonials7.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                    {{--<ul>--}}
                        {{--<li>Chambre 606 </li>--}}
                        {{--<li>DEBBICHE AMINA</li>--}}
                        {{--<li>Je souhaite remercier toute l’équipe paramédicale ( les infirmières, les aides soignantes)pour leur accueil chaleureux, leur attention et leur gentillesse et surtout DR Djemal et Dr Nourrdine, je suis déjà très satisfaite du résultat, et je ne manquerais pas à recommander votre clinique, très propre et très bien équipée. </li>--}}
                        {{--<li>Encore un grand merci à Dr Djemal et Dr Nourdine pour leur savoir faire,leur humour et la bonne humeur.</li>--}}
                        {{--<li>Bonne continuation</li>--}}

                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="patient">--}}
                {{--<h4><i class="fa fa-user"></i>DEBBICHE AMINA</h4>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="testi col-md-9 col-md-offset-1">--}}
            {{--<div class="text row">--}}
                {{--<div class="col-md-5 col-xs-7 col-sm-4">--}}
                    {{--<a href="{{ asset('img/testimonials/testimonials8.png') }}" class="picture-wrapper galerie">--}}
                        {{--<div class="overlay"></div>--}}
                        {{--<img src="{{ asset('img/testimonials/testimonials8.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-5 col-sm-8">--}}
                    {{--<ul>--}}
                        {{--<li><p>Chambre 601 </p></li>--}}
                           {{--<li><p>Nicole Savoy</p></li>--}}

                       {{--<li><p>Je veux dire merci à toute l’équipe de jour , de nuit , aux médecins.</p>--}}
                          {{--<p> Chez nous en Suisse cela n’existe pas.</p>--}}
                           {{--<p>Je ne peux que recommander votre clinique et je veux dire mille fois mercis au médecin qui m’a opérée, Dr Djemal et à Nordine qui sait vous--}}
                            {{--envoyer dans les bras de Morphée.</p> </li>--}}

                       {{--<li>Avec toute ma reconnaissance,Nicole</li>--}}

                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="patient">--}}
                {{--<h4><i class="fa fa-user"></i> Nicole</h4>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
    <!-- /.content -->
@endsection

@section('title','Témoignages chirurgie esthétique - Dr Djemal : Avant Apres chirurgie esthétique')
@section('description','Découvrez les témoignages des patients ayant subi des chirurgies esthétiques realisèes par Dr Djemal? Découvrez en photos les resultats des chirugies esthétiques en Tunisie')
