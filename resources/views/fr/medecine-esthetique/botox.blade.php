@extends('fr.innerLayout')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Médecine esthétique : </span> Le botox</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')

    <div class="content">
        <h2 class="content-title">D&eacute;finition&nbsp;</h2>
        <p>Les rides apparaissent notamment en raison de la contraction r&eacute;guli&egrave;re de certains muscles du visage, par exemple lorsque on rit ou que l&#39;on parle. Avec le vieillissement, la peau perd de son &eacute;lasticit&eacute; et son hydratation diminue. Avec le temps, les lignes et ridules li&eacute;es &agrave; la mimique se transforment ainsi en rides permanentes. Celles-ci sont souvent associ&eacute;es aux soucis, aux contrari&eacute;t&eacute;s, &agrave; l&#39;anxi&eacute;t&eacute; et &agrave; la fatigue, et sont donc ressenties comme g&ecirc;nantes.</p>

<p>La toxine botulique est une substance active (prot&eacute;ine naturelle obtenue &agrave; partir d&#39;une bact&eacute;rie) qui permet d&#39;att&eacute;nuer nettement les rides sans intervention chirurgicale. Gr&acirc;ce &agrave; son effet th&eacute;rapeutique, cette prot&eacute;ine est utilis&eacute;e en m&eacute;decine depuis de nombreuses ann&eacute;es sous le nom de Botox</p>

<p><strong>Le Botox&nbsp; agit </strong>au niveau des liaisons neuromusculaires. Il interrompt de mani&egrave;re temporaire la transmission de l&#39;influx nerveux aux muscles et calme ainsi les mouvements musculaires, ce qui att&eacute;nue nettement les rides. L&#39;effet de lissage s&#39;installe d&egrave;s la premi&egrave;re semaine apr&egrave;s le traitement et se maintient pendant cinq &agrave; six mois. Ensuite, les liaisons neuromusculaires se r&eacute;tablissent et les muscles trait&eacute;s se r&eacute;activent.</p>

<p><strong>Le Botox</strong>&nbsp; est inject&eacute; avec pr&eacute;cision par le m&eacute;decin, au moyen d&#39;une aiguille fine, dans les zones musculaires impliqu&eacute;es. Imm&eacute;diatement apr&egrave;s, le (la) patient(e) peut reprendre ses activit&eacute;s habituelles. Le traitement peut &ecirc;tre r&eacute;p&eacute;t&eacute; lorsque l&#39;effet s&#39;att&eacute;nue. Les injections ne devraient cependant pas &ecirc;tre r&eacute;p&eacute;t&eacute;es plus souvent que tous les 6 mois.</p>

<h2 class="content-title">Effets secondaires et contre-indications</h2>

<p>La toxine botulique est investigu&eacute;e &agrave; des fins m&eacute;dicales depuis plus de 20 ans dans le monde entier. Le Botox a, par cons&eacute;quent, fait la preuve de son efficacit&eacute; et de sa tol&eacute;rance.</p>

      </div>
      <!-- /.content -->
@endsection

@section('title','Botox Tunisie - Dr Djemal')
@section('description','Vous envisagez faire du Botox en Tunisie? Dr Djemal, chirugien esthétique vous aide à garder un visage jeune')
