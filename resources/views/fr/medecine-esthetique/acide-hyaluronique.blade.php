@extends('fr.innerLayout')

@section('class', 'page lifting-des-seins-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Médecine esthétique : </span> Acide Hyaluronique</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')

    <div class="content">
        <h2  class="content-title">D&eacute;finition&nbsp;</h2>

<p><strong>Par une simple injection intradermique d&#39;acide hyaluronique,</strong> les imperfections et les rides peuvent d&eacute;sormais &ecirc;tre corrig&eacute;es.</p>

<p>L&#39;acide hyaluronique est aussi un excellent volumateur pour remodeler les joues et sculpter l&#39;ovale du visage, qui retrouve ainsi toute son &eacute;nergie et sa vitalit&eacute;.</p>

<p>L&#39;acide hyaluronique est une substance naturellement pr&eacute;sente dans le derme. Il est un composant essentiel de la structure de la peau. Il permet d&#39;hydrater les tissus.</p>

<p>Au fur et &agrave; mesure du vieillissement, la qualit&eacute; de cet acide hyaluronique se d&eacute;grade, sa quantit&eacute; diminue entra&icirc;nant une alt&eacute;ration du derme. La peau s&#39;ass&egrave;che, les ridules puis les rides apparaissent. Les joues, les pommettes et les l&egrave;vres perdent de leur substance.</p>

<p>Gr&acirc;ce &agrave; son effet volumateur et hydratant, l&#39;acide hyaluronique vient combler les rides et restaure ainsi les volumes du visage. Il permet ainsi d&#39;effacer certaines imperfections ou asym&eacute;tries, de remodeler les joues et d&#39;harmoniser les contours du visage.</p>

<h2 class="content-title">L&#39;acide hyaluronique comporte de nombreux avantages:</h2>

<ul>
  <li>Il n&#39;est pas d&#39;origine animale,</li>
  <li>Il n&#39;est pas reconnu comme un corps &eacute;tranger,</li>
  <li>Il ne n&eacute;cessite pas de test cutan&eacute;</li>
  <li>Il est enti&egrave;rement r&eacute;sorbable</li>
  <li>Les <a href="../avant-apres-et-temoignages">r&eacute;sultats esth&eacute;tiques</a> sont imm&eacute;diats et tr&egrave;s naturels et ne n&eacute;cessitent aucune interruption de la vie sociale.</li>
  <li>Les injections sont peu douloureuses. (le m&eacute;decin proposera une anesth&eacute;sie locale &agrave; base de cr&egrave;me anesth&eacute;siante pour suivre le traitement confortablement).</li>
</ul>

<h2 class="content-title">Injections d&#39;acide hyaluronique chez l&#39;homme</h2>

<p>Parce que la peau des hommes est plus &eacute;paisse d&#39;environ 15 &agrave; 20% que celle des femmes et que leurs rides sont plus profondes, il existe des produits plus concentr&eacute;s, adapt&eacute;s sp&eacute;cialement &agrave; la physiologie masculine.</p>

<h2 class="content-title">Dur&eacute;e d&#39;action</h2>

<p>La tenue moyenne des injections d&#39;acide hyaluronique est de 6 &agrave; 12 mois. Elle varie en fonction de l&#39;&acirc;ge, de la qualit&eacute; de la peau, de l&#39;hygi&egrave;ne de vie (fum&eacute;e et exposition au soleil par ex.), du choix du produit et de la zone d&#39;injection.</p>

      </div>
      <!-- /.content -->
@endsection

@section('title','Acide hyaluronique Tunisie - Dr Djemal')
@section('description',"Vous envisagez faire de l'acide hyaluronique en Tunisie? Consultez, lechirurgien esthétique de renommée, Dr Djemal")