@extends('fr.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')
    <div class="content">
              <h2 class="content-title">MEDECINE ESTHETIQUE</h2>
<p>Les soins esthétiques permettent de remédier de manière douce et précoce, lorsque surviennent les premiers signes de vieillissement. Ils interviennent sur les rides en les comblant au moyen de produits de comblement, ou en agissant directement sur les muscles qui en sont à l'origine. La technique et les produits choisis dépendent de la forme, de la profondeur et de la localisation des rides. On peut également associer différentes techniques pour un meilleur résultat. </p>
    </div>
@endsection
@section('title','Medecine Esthetique - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','Les soins esthétiques permettent de remédier de manière douce et précoce, lorsque surviennent les premiers signes de vieillissement. Ils interviennent sur les rides en les comblant au moyen de produits...')