@extends('fr.innerLayout')

@section('class', 'page myron-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Clinique</span>  Internationale Myron</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')
    <div class="content">
        <h2 class="content-title">Myron</h2>
        <p>Le nom de MYRON désigne l'un des plus célèbres sculpteurs grecs. Myron, né à Éleuthères en Attique, dans la première moitié du Vᵉ siècle av. J.-C. Il est l'auteur de nombreuses statues d'athlètes, la plus connue étant le Discobole</p>
        <p>Située dans le beau quartier d’affaires et résidentiel  du LAC II, la toute nouvelle clinique privée(2015) multidisciplinaire  MYRON est à 15 minutes du centre de Tunis et à 10 minutes de l’aéroport de Tunis Carthage.</p>
        <ul>
          <li>Etablissement privé conforme aux normes européennes dotée d’un équipement à la pointe de la technologie médicale mondiale.</li>
           <li>Des équipes médicales et chirurgicales hautement qualifiées, spécialisées et expérimentées.</li>
           <li>Un personnel attentif, disponible, agréable et efficace et chaleureux.</li>
         </ul>
        <p>La <b>Clinique Internationale MYRON</b>, est un établissement de santé privé à vocation médico-chirurgicale d’une capacité d’hébergement de xx lits répartis sur 6 étages</p>
        <p>Géographiquement, elle se situe au quartier des Berges du Lac II, qui a attiré un nombre de multinationales, ambassades et hommes d’affaire. </p>
        <p>La <b>Clinique Internationale MYRON</b> construite en 2015 est implantée a  10 min de la banlieue nord de Tunis (Sidi Bou Said, La Marsa, Carthage, Gammarth), à 15 min du centre ville de Tunis et a 10 minutes de l’aéroport International de Carthage.</p>

      </div>
      <!-- /.content -->

      <div class="btn-cta-wrapper">
        <a href="{{ url('contact') }}" class="btn-cta-content"> poster vos questions, un spécialiste vous répondra <i class="fa fa-arrow-circle-right"></i></a>
      </div>
      <!-- /.btn-cta-wrapper -->

      <div class="gallery-wrapper">
        <h3 class="title">Galerie photos</h3>
        <div class="gallery">

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-02.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-02.jpg" alt="">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-03.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-03.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-04.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-04.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-05.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-05.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-06.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-06.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-07.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-07.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-08.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-08.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-09.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-09.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-10.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-10.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-11.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-11.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-12.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-12.jpg" alt="">
            </a>
          </div>

          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-13.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-13.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-49.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-49.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-07.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-07.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-39.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-39.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-50.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-50.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-33.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-33.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-41.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-41.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-27.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-27.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-6 col-sm-4">
            <a href="img/album/Myron-29.jpg" class="picture-wrapper galerie">
              <div class="overlay"></div>
              <img src="img/album/Myron-29.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
@endsection

@section('title',' Clinique chirurgie esthétique Myron - Dr Djemal: Clinique esthétique en Tunisie')
@section('description','Rendez-vous à la clinique de chirurgie esthétique Myron, où Dr Djemal, chirugien esthétique réalise votre intervention')
