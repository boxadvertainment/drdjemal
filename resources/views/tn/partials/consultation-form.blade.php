<form id="consultation-form" class="consultation-form__form"  action="{{ action('AppController@submitConsultation') }}" method="post">
    <div class="title text-center">
        <div class="h3">Demandez une <span class="white">consultation gratuite</span></div>
    </div>
    <div class="form-group">
        <label for="name">Nom et prénom <span class="text-danger">*</span></label>
        <input type="text" id="name" name="name" class="form-control" placeholder="Nom et prénom"  required title="Ce champ est obligatoire">
    </div>
    <div class="form-group">
        <label for="email">Adresse Email <span class="text-danger">*</span></label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Email" required  title="Email incorrect">
    </div>
    <div class="form-group">
        <label for="phone">Num. téléphone <span class="text-danger">*</span></label>
        <input type="tel" id="phone" name="phone" class="form-control phone-number" placeholder="Téléphone" id="phone" required title="Téléphone incorrect">
    </div>

    <div class="form-group">
        <label for="intervention">Intervention souhaiter <span class="text-danger">*</span></label>
        <select class="" name="intervention" title="Interventions" required data-live-search="true" id="intervention">
            @foreach(Config::get('app.interventions') as $label => $group)
                <optgroup label="{{ $label }}">
                    @foreach($group as $item => $intervention)
                        <option value="{{ $item }}"><b>{{ $intervention }}</b></option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="message">Votre besoin <span class="text-danger">*</span></label>
        <textarea name="message" id="message" class="form-control" rows="10" placeholder="Message" required></textarea>
    </div>

    <div class="g-recaptcha" data-sitekey="{{config('app.recaptcha.sitekey')}}" style="display: flex; justify-content: center; margin-bottom: .5rem"></div>
    
    <div class="form-group">
        <button type="submit" name="submit" class="form-control submit  submit-consultation"> <i class="fa fa-check"></i> Valider & envoyer</button>
    </div>
</form>
