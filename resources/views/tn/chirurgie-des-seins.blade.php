@extends('tn.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('tn.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('tn.innerContent')
    <div class="content">
              <h2 class="content-title">CHIRURGIE DES SEINS</h2>
<p>La chirurgie des seins reste une solution pour réaliser rapidement une poitrine ferme, épanouie et vaincre ses complexes. Pour augmenter le volume, l’augmentation mammaire est l’une des interventions la plus spectaculaire. Les plasties mammaires, quant à elles, s’appliquent aux seins beaucoup plus relâchés. Il est alors nécessaire, en plus du galbe apporté par les prothèses, de retendre la peau pour rehausser l’aréole et le mamelon. Dans certains cas, une réduction mammaire est nécessaire. L’intervention chirurgicale a pour but  de retirer la peau et la glande excédentaire afin d’obtenir deux seins harmonieux, joliment galbés et ascensionnés</p>
    </div>
@endsection
@section('title','Chirurgie Des Seins - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','La chirurgie des seins reste une solution pour réaliser rapidement une poitrine ferme, épanouie et vaincre ses complexes. Pour augmenter le volume, l’augmentation mammaire...')
