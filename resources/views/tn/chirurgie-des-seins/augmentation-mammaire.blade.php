@extends('tn.innerLayout')

@section('class', 'augmentation-mammaire')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('tn.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Chirurgie des seins :</span> Augmentation mammaire</h1>
      </div>
    </header>
@endsection

@section('tn.innerContent')

    <div class="content">
        <h2 class="content-title">D&eacute;finition</h2>
        <p>L&rsquo;hypoplasie mammaire est d&eacute;finie par un volume de seins insuffisamment d&eacute;velopp&eacute;s par rapport &agrave; la morphologie de la patiente.</p>
        <h2 class="content-title">Objectifs</h2>
        <p>Une augmentation mammaire consiste à corriger le volume jugé insuffisant des seins par la mise en place d’implants (prothèses) mammaires. </p>
        <h2 class="content-title">Principes</h2>
        <p>Tous les implants mammaires actuellement utilisés sont composés d’une enveloppe, et d’un produit de remplissage. L’enveloppe est toujours constituée de silicone élastique (élastomère de silicone). Elle peut être lisse ou rugueuse (texturée).</p>
        <p>En ce qui concerne le produit de remplissage : le gel de silicone. L’implant mammaire est dit pré-rempli lorsque le produit de remplissage a été incorporé en usine (gel de silicone). La gamme des différents volumes est donc fixée par le fabricant.</p>
        <h2 class="content-title">Avant L’intervention</h2>
        <p>En ce qui concerne le produit de remplissage : le gel de silicone . L’implant est dit pré-rempli lorsque le produit de remplissage a été incorporé en usine (gel de silicone). La gamme des différents volumes est donc fixée par le fabricant.</p>
        <h3>Anesthesie</h3>
        <p>Une hospitalisation d’une journée est habituellement suffisante. Il faut rester à jeun depuis la veille à minuit.</p>
        <h3>Hospitalisation</h3>
        <p>Une hospitalisation d’une journée est habituellement suffisante.</p>
        <h3>Incision Cutanee</h3>
        <p>L’implant mammaire est introduit par une courte incision située :</p>
        <ul>
          <li>Soit sur l’aréole</li>
          <li>Soit dans la région de l’aisselle.</li>
          <li>Soit dans le pli sous mammaire.</li>
        </ul>

        <h3>Position De L’implant</h3>
        <p>La loge qui est aménagée par décollement et dans laquelle la prothèse mammaire est implantée est située: </p>
        <ul>
          <li>Soit derrière la glande mammaire et devant le muscle grand pectoral</li>
          <li>Soit derrière la glande et derrière le muscle grand pectoral</li>
        </ul>
<!--
        <h3>En Cas De Ptose Mammaire</h3>
        <p>Seins tombants, aréole basse, Il est souhaitable d’associer un geste de réduction de l’enveloppe cutanée ce qui implique une rançon cicatricielle plus importante (péri aréolaire, verticale).
        En fin d’intervention, un pansement modelant, avec des bandes élastiques en forme de soutien-gorge, est confectionné. <br>
        En fonction du chirurgien et de la nécessité éventuelle d’un geste complémentaire associé, l’intervention peut durer de 1 à 2 heures.</p> -->

        <h2>Suites Operatoires</h2>
        <p>Les suites op&eacute;ratoires peuvent &ecirc;tre douloureuses les premiers jours, notamment lorsque l&rsquo;implant mammaire est plac&eacute; derri&egrave;re le muscle grand pectoral.</p>

        <p>On a alors recours &agrave; un traitement antalgique pendant quelques jours. Dans le meilleur des cas, la patiente ressentira une forte sensation de tension.</p>

        <p>&OElig;d&egrave;me (gonflement) et ecchymoses (bleus) des seins, g&ecirc;ne &agrave; l&rsquo;&eacute;l&eacute;vation des bras sont fr&eacute;quents au d&eacute;but. Le premier pansement est retir&eacute; au bout de 24 heures et remplac&eacute; par un soutien-gorge assurant une bonne contention.</p>

        <p>Le port de ce soutien-gorge est conseill&eacute; pendant environ un mois, nuit et jour. Les fils de suture sont r&eacute;sorbables et se dissolvent entre le huiti&egrave;me et le quinzi&egrave;me jour post-op&eacute;ratoire.</p>

        <p>Il convient d&rsquo;envisager une convalescence et un arr&ecirc;t de travail d&rsquo;une dur&eacute;e de 8 &agrave; 10 jours.</p>

        <p>On conseille d&rsquo;attendre un &agrave; deux mois pour reprendre une activit&eacute; sportive.</p>

        <h2>Resultat</h2>
        <p>Il peut être apprécié à partir du troisième mois, délai nécessaire à l’assouplissement des seins et à la stabilisation des prothèses mammaires. Au-delà de l’amélioration esthétique, le retentissement psychologique est le plus souvent bénéfique.</p>

        <!-- <h2>Imperfections Possibles</h2>
        <p>Il est possible que la cicatrice ait une évolution anormale, sous forme d’épaississement ou de rétraction. Des douleurs des seins, des troubles de la sensibilité mamelonnaire sont également possibles. D’autre part, une insatisfaction du résultat esthétique peut motiver une retouche après avis du chirurgien.

        <h2>Duree De L’implant</h2>
        <p>Une prothèse mammaire remplie de gel de silicone a une durée de vie incertaine que l’on ne peut estimer précisément a priori puisqu’elle dépend de l’éventuelle survenue de complication. Ainsi, la durée de vie de l’implant ne peut être garantie.</p>
        <p>Une femme porteuse d’implants mammaires est exposée au risque d’avoir recours à une intervention complémentaire de remplacement pour que l’effet bénéfique soit maintenu. Cependant, il faut savoir qu’a priori un implant de qualité n’a pas une durée de vie théoriquement limitée : il n’y a pas d’échéance au-delà de laquelle le changement d’implant est obligatoire. Ainsi, en l’absence d’usure ou de complication, l’implant mammaire peut être conservé aussi longtemps que la patiente le désire.</p>

        <h2>Complications Envisageables</h2>
        <p>Les suites opératoires sont en général simples au décours d’une augmentation mammaire par prothèses. Toutefois, des complications peuvent survenir, certaines inhérentes à l’ensemble des interventions de chirurgie mammaire, d’autres liées à la mise en place d’un corps étranger dans le sein : les risques spécifiques aux implants mammaires. </p>

        <h3>1 - Les complications inhérentes à l'ensemble des interventions de chirurgie mammaire:</h3>
        <p> la survenue d’une infection nécessite un traitement antibiotique et parfois un drainage chirurgical. <br>
        - un hématome peut nécessiter un geste d’évacuation.<br>
        - des altérations de la sensibilité, notamment mamelonnaire, peuvent être observées, mais la sensibilité normale réapparaît le plus souvent dans un délai de 6 à 18 mois.<br>
        - surtout l’évolution des cicatrices peut être défavorable avec la survenue de cicatrices hypertrophiques voire chéloïdes, d’apparition et d’évolution imprévisibles, qui peuvent compromettre l’aspect esthétique du résultat et requièrent des traitements locaux spécifiques souvent longs.<br>
        </p>
        <h3>2 - Les risques spécifiques aux implants mammaires</h3>

        <h4>Formation de plis ou aspect de "vagues" :</h4>
        <p>L’implant, pour rester souple, n’est jamais rempli sous tension. De ce fait, les plis de l’enveloppe de la prothèse peuvent être visibles sous la peau , donnant alors un aspect de " vagues " notamment dans les parties supérieure, externe et inférieure du sein. Cet aspect est limité dans la partie supérieure en cas de mise en place en position rétro-musculaire.</p>
        <h4>Contracture capsulaire et coque fibreuse </h4>
        <p>La formation d’une capsule fibreuse autour d’un implant mammaire est obligatoire. C’est une réaction normale de l’organisme qui forme une sorte de membrane fibreuse autour de tout corps étranger afin de l’isoler et de se protéger (" membrane ou capsule d’exclusion "). Dans certains cas, cette membrane est le siège d’une évolution défavorable comparable aux chéloïdes des cicatrices cutanées : elle s’épaissit, se rétracte et forme une véritable coque fibreuse autour de l’implant. Il s’agit de la contracture capsulaire. La coque n’augmente pas le risque de rupture mais expose à une complication d’ordre esthétique. Une intervention chirurgicale peut corriger cette contracture par section de la capsule (capsulotomie).<br>
        Différents auteurs ont proposé des solutions techniques pour limiter l’apparition de cette contracture : <br>
        • la position de l’implant mammaire derrière le muscle pectoral, <br>
        la fabrication de parois rugueuses au niveau de la face externe de l’implant (prothèses texturées),</p>

        <h2>FAQ</h2>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Est-il possible d'allaiter? 
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                La mise en place d’implants mammaires derrière la glande mammaire ne semble pas avoir de retentissement sur l’allaitement.
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading2">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapseOne">
                  Les prothèses mammaires favorisent-elles l'apparition du cancer du sein?
                </a>
              </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
              <div class="panel-body">
                La relation entre cancer du sein et implant a été recherchée mais aucun lien entre les deux n’a été mis en évidence et l’implantation d’une prothèse mammaire n’augmente en rien le risque de survenue d’un cancer du sein. Les chirurgiens des centres anti-cancéreux utilisent régulièrement les prothèses mammaires pour la chirurgie reconstructrice.

              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading3">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapseOne">
                  La surveillance du sein est-elle possible? 

                </a>
              </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="panel-body">
                La prothèse étant derrière la glande mammaire, la surveillance clinique est simple. La présence d’un implant peut modifier la capacité des rayons X à dépister le cancer du sein. Les patientes porteuses d’un implant mammaire doivent le préciser au radiologue qui pourra utiliser des méthodes spécifiques et adaptées (échographie, mammographie numérisée).

              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading4">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapseOne">
                  Y a t’il une surveillance après la mise en place d'un implant mammaire? 
                </a>
              </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
              <div class="panel-body">
                Ultérieurement, la présence d’un implant mammaire ne nécessite pas de faire réaliser des examens en plus de la surveillance médicale habituelle mais il est indispensable de préciser au médecin que vous êtes porteuse d’un implant mammaire. Il est impératif en cas de modification d’un sein (durcissement ou au contraire ramollissement) de consulter un médecin (médecin de famille, gynécologue, chirurgien) qui saura juger s’il est nécessaire d’avoir recours à un examen radiographique ou échographique. <br>
                Il ne faut pas surévaluer les risques, mais simplement prendre conscience qu’une intervention chirurgicale, même apparemment simple, comporte toujours une petite part d’aléas.
              </div>
            </div>
          </div>
        </div>

        <h2>Conseils pratiques</h2>
        <p>Vêtements faciles à mettre (difficile de lever les bras les premiers jours)</p>



 -->

      </div>
      <!-- /.content -->
@endsection

@section('title','Augmentation mammaire en Tunisie - Dr Djemal')
@section('description','Vous envisagez une augmentation mammaire en Tunisie? Dr Djemal, chirugien esthétique intervient pour que vous réalisiez votre rêve')
