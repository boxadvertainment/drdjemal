@extends('tn.innerLayout')

@section('class', 'home')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

      @include('tn.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Chirurgie des seins :</span> Gynécomastie</h1>
      </div>
    </header>
@endsection

@section('tn.innerContent')

    <div class="content">
      <h2>DEFINITION</h2>

      <p>La gyn&eacute;comastie d&eacute;signe une glande mammaire hypertrophi&eacute;e chez l&rsquo;homme, d&rsquo;un seul c&ocirc;t&eacute; ou des deux c&ocirc;t&eacute;s.</p>

      <p>Lorsque la gyn&eacute;comastie est due &agrave; un exc&egrave;s de graisse sans atteinte de la glande mammaire, on parle d&#39;adipomastie plut&ocirc;t que de gyn&eacute;comastie graisseuse.<br />
      <br />
      Dans la plupart des cas, l&#39;hypertrophie mammaire chez un homme est une combinaison de graisse et de d&eacute;r&egrave;glement hormonal. On effectue alors une gyn&eacute;comastie combin&eacute;e, associant une liposuccion &agrave; l&#39;ablation de la glande mammaire.</p>

      <h2>INTERVENTION</h2>

      <p>Lorsque la gyn&eacute;comastie est due &agrave; un exc&egrave;s de graisse, on pratique une <a href="../chirurgie-de-la-silhouette/liposuccion">liposuccion</a>.<br />
      Il introduit une fine canule par de petites incisions et aspire la graisse. Les incisions sont ensuite sutur&eacute;es.<br />
      Quand une gyn&eacute;comastie tr&egrave;s importante est associ&eacute;e &agrave; un exc&egrave;s cutan&eacute;, on enl&egrave;ve un segment de peau pour redraper le thorax. Dans ce cas, le chirurgien &eacute;tend l&rsquo;incision au-del&agrave; de l&rsquo;ar&eacute;ole et la cicatrice sera plus longue et plus visible.</p>

      <p>L&#39;op&eacute;ration de gyn&eacute;comastie est l&eacute;g&egrave;re et peu douloureuse.</p>

      <h2>SUITES POST OPERATOIRES</h2>

      <p>Le patient porte pendant 15 jours un pansement compressif pour appliquer une pression uniforme sur la poitrine, de fa&ccedil;on &agrave; r&eacute;duire l&#39;enflure qui persiste pendant 3 mois.<br />
      <br />
      Entre le 7e et le 8e jour, le chirurgien retire les fils en cas de gyn&eacute;comastie glandulaire.<br />
      L&rsquo;&oelig;d&egrave;me et les ecchymoses se r&eacute;sorbent g&eacute;n&eacute;ralement en 15 jours.<br />
      <br />
      D&egrave;s le 3e jour, le patient peut reprendre les activit&eacute;s qui ne n&eacute;cessitent pas d&rsquo;effort physique. On conseille d&#39;attendre la 6e semaine pour recommencer les activit&eacute;s sportives.<br />
      <br />
          Le <a href="../avant-apres-et-temoignages">r&eacute;sultat chirurgical</a> est souvent tr&egrave;s satisfaisant. Le r&eacute;sultat est d&eacute;finitif pour une gyn&eacute;comastie glandulaire et durable en cas d&rsquo;adipomastie &agrave; condition que le patient fasse attention &agrave; son alimentation.</p>

    </div>
      <!-- /.content -->
@endsection
@section('title','Gynécomastie en Tunisie - Dr Djemal')
@section('description','Vous envisagez une Gynécomastie en Tunisie? Dr Djemal, chirugien esthétique intervient pour réaliser votre Gynécomastie en Tunisie')
