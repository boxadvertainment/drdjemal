@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>chirurgie de la calvitie</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">

    <h2>DEFINITION</h2>

    <p>L&rsquo;apparition des microgreffes est une technique extr&ecirc;mement pr&eacute;cise et fiable qui gomme les effets du temps sur la chevelure.<br />
    Il s&#39;agit du transfert de vos propres cheveux, dans la r&eacute;gion de la couronne qui pourront cro&icirc;tre comme s&#39;ils avaient toujours &eacute;t&eacute; en place.<br />
    <br />
    La technique des implants capillaires est &eacute;l&eacute;gante, fine et exige un travail m&eacute;ticuleux.<br />
    C&#39;est une intervention b&eacute;nigne, sous anesth&eacute;sie locale.</p>

    <h2><strong>INTERVENTION</strong></h2>
    <p>Le principe est simple. Il s&rsquo;agit de transf&eacute;rer les cheveux de la partie post&eacute;rieure du cuir chevelu vers la partie ant&eacute;rieure o&ugrave; les cheveux sont g&eacute;n&eacute;tiquement diff&eacute;rents.<br />
    L&#39;intervention se d&eacute;roule en quelques heures, sous une anesth&eacute;sie locale pure et locale assist&eacute;e.<br />
    Aucun pansement n&#39;est requis, aussi la vie sociale peut reprendre d&egrave;s les jours suivants.<br />
    Tous ces avantages font des microgreffes une technique de pointe.</p>

    <h2><strong>La bandelette</strong></h2>

    <p>Le pr&eacute;l&egrave;vement de la bandelette est r&eacute;alis&eacute; sur la partie haute de la nuque, sous la forme d&#39;une bande de cheveux qui sera ensuite minutieusement d&eacute;coup&eacute;e.<br />
    La cicatrice de 1 mm de largeur, est totalement invisible, car noy&eacute;e dans la masse chevelue.<br />
    Lorsque plusieurs s&eacute;ances de greffes sont r&eacute;alis&eacute;es, la nouvelle bande est extraite de fa&ccedil;on &agrave; reprendre l&rsquo;ancienne cicatrice et n&rsquo;en conserver qu&rsquo;une seule.</p>
    <h3><strong>Les incisions</strong></h3>

    <p>Les greffes sont implant&eacute;es dans de petites incisions r&eacute;alis&eacute;es &agrave; la surface du cuir chevelu &agrave; l&#39;aide de micro bistouris sp&eacute;cifiques.<br />
    Les incisions sont effectu&eacute;es selon une topographie et un angle particuliers pour donner &agrave;<br />
    l&#39;implantation un aspect discret et naturel. La largeur des incisions est adapt&eacute;e &agrave; la taille des greffes.</p>

    <p>Chaque greffe est implant&eacute;e manuellement.<br />
    Il est tr&egrave;s important de manipuler les greffes avec douceur afin de ne pas les ab&icirc;mer.<br />
    Ainsi, les greffes retrouvent le milieu dans lequel elles vont pouvoir a nouveau se d&eacute;velopper.<br />
    Les greffes vont tr&egrave;s vite s&rsquo;ancrer dans la peau et d&egrave;s le lendemain, on peut les toucher sans avoir le souci de les extraire</p>

    <h2>SUITES OPERATOIRES</h2>

    <p>Il faut pr&eacute;voir un arr&ecirc;t de travail de 4 &agrave; 7 jours.</p>

    <p>Pour les micro greffes, il se forme une petite cro&ucirc;te sur chaque greffon qui tombe entre 8 &agrave; 10 jours. Les cheveux greff&eacute;s tombent avec la cro&ucirc;te et repoussent ensuite entre le deuxi&egrave;me et le troisi&egrave;me mois. Ils grandissent de 1 cm par mois environ.</p>

    <p>Les shampoings sont en g&eacute;n&eacute;ral autoris&eacute;s 48 heures apr&egrave;s l&rsquo;op&eacute;ration. La pratique d&rsquo;une activit&eacute; sportive pourra &ecirc;tre reprise progressivement &agrave; partir de la 4&egrave;me semaine post-op&eacute;ratoire. Les douleurs &agrave; type de c&eacute;phal&eacute;es peuvent persister quelques jours, elles seront calm&eacute;es par la &nbsp;prescription d&rsquo;antalgiques.</p>

    <p>Les suites op&eacute;ratoires de cette chirurgie esth&eacute;tique sont essentiellement marqu&eacute;es par l&rsquo;apparition d&rsquo;un &oelig;d&egrave;me (gonflement) et d&rsquo;ecchymoses (bleus) dont l&rsquo;importance et la dur&eacute;e sont tr&egrave;s variables d&rsquo;un individu &agrave; l&rsquo;autre.</p>

    <h2>RESULTAT</h2>

    <p>G&eacute;n&eacute;ralement, les greffes tombent &agrave; partir du dixi&egrave;me jour.<br />
    Les cheveux qui tombent sont entiers, y compris le bulbe ce qui ne manque pas d&#39;inqui&eacute;ter le patient. Cependant les cellules souches, qui produiront le nouveau cheveu, sont toujours pr&eacute;sentes dans le cuir chevelu et assureront une repousse au bout de quelques mois.</p>

    <p>Un d&eacute;lai de 3 &agrave; 6 mois est n&eacute;cessaire pour appr&eacute;cier le r&eacute;sultat des micro greffes et dans tous les cas deux interventions au moins seront n&eacute;cessaires pour obtenir une densit&eacute; de cheveux suffisante.</p>

      </div>
@endsection

@section('title','Chirurgie de la calvitie en Tunisie - Dr Djemal')
@section('description','Vous envisagez une chirurgie de calvitie en Tunisie? Dr Djemal, chirugien esthétique est le meilleur pour vous la réaliser')
