@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>Mentoplastie</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">

   <h2>D&eacute;finition&nbsp;</h2>
   <p>La G&eacute;nioplastie est une intervention de chirurgie esth&eacute;tique du menton, dont le but est de modifier la position du menton, pour recr&eacute;er un visage harmonieux de face et de profil.</p>

   <p>Nous distinguons sch&eacute;matiquement 2 types de menton :</p>

   <ul>
    <li><strong>menton fuyant</strong>&nbsp;: le menton est insuffisamment d&eacute;velopp&eacute;. Cet aspect augmente souvent l&#39;aspect allong&eacute; du nez.</li>
    <li><strong>menton trop saillant en galoch</strong>e : le menton est trop fort, trop avanc&eacute;.</li>
   </ul>



   <h2>L&rsquo;INTERVENTION&nbsp;:</h2>

   <p>Lors d&rsquo;une chirurgie du menton, le <a href="../cv">chirurgien esth&eacute;tique</a> effectue une incision sous le menton ou dans la bouche. Ensuite, l&rsquo;implant mentonnier est plac&eacute; sous le p&eacute;rioste de la m&acirc;choire et sous les muscles du menton. L&rsquo;incision est sutur&eacute;e. Cette incision r&eacute;alis&eacute;e dans la bouche ne laisse aucune cicatrice apparente. Vous pouvez compter une heure pour une chirurgie du menton et elle se d&eacute;roule sous anesth&eacute;sie g&eacute;n&eacute;rale.</p>

   <h3>L&rsquo;IMPLANT MENTONNIER</h3>

   <p>Il existe diverses tailles et formes d&rsquo;implants mentonniers que le chirurgien peut vous proposer. Un implant pour le menton est une proth&egrave;se en silicone.</p>

   <h3>SOINS POST-OP&Eacute;RATOIRES&nbsp;:</h3>

   <p>Une compresse sera appliqu&eacute;e &agrave; l&rsquo;ext&eacute;rieur durant la premi&egrave;re semaine. Cela permet &agrave; l&rsquo;implant mentonnier de na pas bouger. Apr&egrave;s cette semaine, vous pouvez reprendre le travail. Un &oelig;d&egrave;me et des ecchymoses apparaissent et disparaissent g&eacute;n&eacute;ralement apr&egrave;s deux semaines</p>

   <p>LA GENIOPLASTIE AVEC GREFFE OSSEUSE</p>

   <ul>
    <li>Cela correspond essentiellement &agrave; la profiloplastie, avec avancement mineur du menton.</li>
    <li>Cette technique permet un petit avancement du menton, de 3 &agrave; 5 mm.</li>
    <li>Le r&eacute;sultat est imm&eacute;diat.</li>
   </ul>

   <p>GENIOPLASTIE AVEC ACIDE HYALURONIQUE</p>

   <p>Il existe un acide hyaluronique tr&egrave;s coh&eacute;sif qui permet de recr&eacute;er les reliefs insuffisants.</p>

   <p>Cet acide hyaluronique peut &ecirc;tre utilis&eacute; au niveau du menton pour un r&eacute;sultat s&eacute;duisant et imm&eacute;diat.</p>

   <p>La contrainte est la n&eacute;cessit&eacute; de renouveler les injections tous les 8 mois &agrave; 1 an.</p>

   <h3>TRAITEMENT DU DOUBLE MENTON</h3>

   <p>Une peau rel&acirc;ch&eacute;e et rid&eacute;e au niveau du cou et du menton, cr&eacute;ent un double menton. La solution est le <a href="./lifting-cervico-facial">lifting cervico facial</a>.</p>

   <p>Dans le cas d&rsquo;un double menton sans rel&acirc;chement cutan&eacute;, une petite liposuccion permettra de restructure le menton.</p>

   <p> <strong>Lors d&#39;une liposuccion sous-mentonni&egrave;re ou du menton</strong>, de petites incisions sont pratiqu&eacute;es sous le menton et un tube creux appel&eacute;&nbsp; canule est ins&eacute;r&eacute; dans le tissu graisseux et &eacute;limine par aspiration l&#39;exc&eacute;dent de graisse du menton et du cou.<br />
   <br />
   Chez les jeunes patients ayant une peau plus &eacute;lastique, cette intervention peut parfois remplacer un <a href="./lifting-cervico-facial">lifting du visage</a> .</p>

      </div>
@endsection
@section('title','Mentoplastie en Tunisie - Dr Djemal : Chirurgie mentoplastie Tunisie ')
@section('description','Vous allez avoir une mentoplastie en Tunisie? Dr Djemal, chirugien esthétiqueréalise votre chirurgie mentoplastie en Tunisie')
