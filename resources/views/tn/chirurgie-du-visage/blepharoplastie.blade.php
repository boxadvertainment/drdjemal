@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>bl&eacute;pharoplastie</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">
    <h2>DEFINITION</h2>

    <p>Les &quot;bl&eacute;pharoplasties&quot; d&eacute;signent les interventions de chirurgie esth&eacute;tique des paupi&egrave;res. Elles peuvent concerner uniquement les deux paupi&egrave;res sup&eacute;rieures ou inf&eacute;rieures, ou encore les quatre paupi&egrave;res &agrave; la fois.</p>

    <p>Une bl&eacute;pharoplastie peut &ecirc;tre r&eacute;alis&eacute;e isol&eacute;ment ou &ecirc;tre associ&eacute;e &agrave; une autre intervention de <a href="./lifting-cervico-facial">chirurgie esth&eacute;tique du visage</a> (lifting cervico-facial).</p>

    <h2>OBJECTIFS</h2>

    <p>Une bl&eacute;pharoplastie se propose de corriger les signes de vieillissement pr&eacute;sents au niveau des paupi&egrave;res et de remplacer l&rsquo;aspect &quot;fatigu&eacute;&quot; du regard par une apparence plus repos&eacute;e et d&eacute;tendue.</p>

    <ul>
      <li>Paupi&egrave;res sup&eacute;rieures lourdes et tombantes, avec exc&egrave;s de peau formant un repli plus ou moins marqu&eacute;,</li>
      <li>Paupi&egrave;res inf&eacute;rieures affaiss&eacute;es et fl&eacute;tries, avec petites rides horizontales cons&eacute;cutives &agrave; la distension cutan&eacute;e,</li>
      <li>Hernies de graisse, responsables de &quot;poches sous les yeux&quot; au niveau des paupi&egrave;res inf&eacute;rieures ou de paupi&egrave;res sup&eacute;rieures &quot;bouffies&quot;.</li>
    </ul>

    <h2>INTERVENTION</h2>

    <p>L&#39;intervention de bl&eacute;pharoplastie vise &agrave; corriger de fa&ccedil;on durable, en supprimant chirurgicalement les exc&egrave;s cutan&eacute;s et musculaires ainsi que les protrusions graisseuses, et ce, bien s&ucirc;r, sans alt&eacute;rer les fonctions essentielles des paupi&egrave;res.</p>

    <p>L&#39;intervention, pratiqu&eacute;e aussi bien chez la femme que chez l&#39;homme, est couramment effectu&eacute;e d&egrave;s la quarantaine. Toutefois, elle est parfois r&eacute;alis&eacute;e beaucoup plus pr&eacute;cocement,&nbsp; comme certaines &quot;poches graisseuses&quot;.</p>

    <h2>AVANT L&rsquo;INTERVENTION</h2>

    <h3>ANESTHESIE</h3>

    <p>Deux proc&eacute;d&eacute;s sont envisageables :</p>

    <ul>
      <li>Anesth&eacute;sie locale approfondie par des tranquillisants administr&eacute;s par voie intra-veineuse (anesth&eacute;sie &quot; vigile &quot;).</li>
      <li>Anesth&eacute;sie g&eacute;n&eacute;rale classique, durant laquelle vous dormez compl&egrave;tement.</li>
    </ul>

    <h3>HOSPITALISATION</h3>

    <p>La chirurgie des paupi&egrave;res peut se pratiquer avec une courte hospitalisation. L&#39;entr&eacute;e s&#39;effectue alors le matin et la sortie est autoris&eacute;e d&egrave;s le lendemain.</p>

    <h3>INCISIONS CUTANEES</h3>

    <ul>
      <li>Paupi&egrave;res sup&eacute;rieures: elles sont dissimul&eacute;es dans le sillon situ&eacute; &agrave; mi-hauteur de la paupi&egrave;re, entre la partie mobile et la partie fixe de la paupi&egrave;re.</li>
      <li>Paupi&egrave;res inf&eacute;rieures: elles sont plac&eacute;es 1 &agrave; 2 mm sous les cils, et peuvent se prolonger un peu en dehors.</li>
    </ul>

    <p>Le trac&eacute; de ces incisions correspond bien s&ucirc;r &agrave; l&#39;emplacement des futures cicatrices, qui seront donc dissimul&eacute;es dans des plis naturels.</p>

    <p>Remarque: Pour les paupi&egrave;res inf&eacute;rieures, en cas de &quot;poches&quot; isol&eacute;es (sans exc&egrave;s de peau &agrave; enlever), on pourra r&eacute;aliser une bl&eacute;pharoplastie par voie trans-conjonctivale, c&#39;est &agrave; dire utilisant des incisions plac&eacute;es &agrave; l&#39;int&eacute;rieur des paupi&egrave;res et ne laissant donc aucune cicatrice visible sur la peau.</p>

    <p><img src="{{ asset('img/schema_contenu/blepharoplastie3.jpg') }}" /></p>

    <p>A partir de ces incisions, les hernies graisseuses inesth&eacute;tiques sont retir&eacute;es et l&#39;exc&eacute;dent de muscle et de peau rel&acirc;ch&eacute;s est supprim&eacute;. A ce stade, de nombreux raffinements techniques peuvent &ecirc;tre apport&eacute;s, pour s&#39;adapter &agrave; chaque cas et en fonction des habitudes du chirurgien.</p>

    <h3>SUTURES</h3>

    <p>Elles sont r&eacute;alis&eacute;es avec des fils tr&egrave;s fins, habituellement non r&eacute;sorbables (&agrave; retirer apr&egrave;s quelques jours).</p>

    <h2>SUITES OPERATOIRES&nbsp;</h2>

    <p>Il n&#39;y a pas de v&eacute;ritables douleurs, mais &eacute;ventuellement un certain inconfort avec une sensation de tension des paupi&egrave;res, une l&eacute;g&egrave;re irritation des yeux ou quelques troubles visuels.</p>

    <p>Les premiers jours il faut se reposer au maximum et &eacute;viter tout effort violent.</p>

    <p>Les suites op&eacute;ratoires de la bl&eacute;pharoplastie sont essentiellement marqu&eacute;es par l&#39;apparition d&#39;un &oelig;d&egrave;me (gonflement) et d&#39;ecchymoses (bleus) dont l&#39;importance et la dur&eacute;e sont tr&egrave;s variables d&#39;un individu &agrave; l&#39;autre.</p>

    <p>On observe parfois durant les premiers jours une impossibilit&eacute; de fermer totalement les paupi&egrave;res ou un l&eacute;ger d&eacute;collement de l&#39;angle externe de l&#39;&oelig;il qui ne s&#39;applique plus parfaitement sur le globe. Il ne faudra pas s&#39;inqui&eacute;ter de ces signes qui sont en r&egrave;gle rapidement r&eacute;versibles.</p>

    <p>Les fils sont retir&eacute;s le 6&egrave;me jour apr&egrave;s l&rsquo;intervention.</p>

    <p>Les stigmates de la chirurgie des paupi&egrave;res vont s&#39;att&eacute;nuer progressivement, permettant le retour &agrave; une vie socio-professionnelle normale apr&egrave;s quelques jours (6 &agrave; 20 jours selon l&#39;ampleur des suites).</p>

    <p>Les cicatrices peuvent rester un peu ros&eacute;es durant les premi&egrave;res semaines, mais leur maquillage est rapidement autoris&eacute; (habituellement d&egrave;s le 7&egrave;me jour).</p>

    <p>Une l&eacute;g&egrave;re induration des zones d&eacute;coll&eacute;es peut persister quelques mois, mais n&#39;est pas perceptible par l&#39;entourage.</p>

    <h2>RESULTAT</h2>

    <p>Un d&eacute;lai de 3 &agrave; 6 mois est n&eacute;cessaire pour appr&eacute;cier le r&eacute;sultat. C&#39;est le temps n&eacute;cessaire pour que les tissus aient retrouv&eacute; toute leur souplesse et que les cicatrices se soient estomp&eacute;es au mieux.</p>

    <p>Les r&eacute;sultats d&#39;une bl&eacute;pharoplastie sont, en r&egrave;gle g&eacute;n&eacute;rale, parmi les plus durables de la <a href="../interventions-chirurgie-esthetique">chirurgie esth&eacute;tique</a>. L&rsquo;ablation des &quot;poches&quot; est pratiquement d&eacute;finitive, et ces derni&egrave;res ne r&eacute;cidivent habituellement jamais.</p>

    <h2>CONSEILS PRATIQUES</h2>

    <p>Pr&eacute;voir &laquo;&nbsp;un masque bleu&nbsp;&raquo; pour soulager les oed&egrave;mes</p>

    <p>V&ecirc;tements faciles &agrave; enfiler&rsquo; (pas par la t&ecirc;te les premiers jours)</p>

    <p>Lunettes de soleil</p>

    <p>Cr&egrave;me anti cernes</p>

    <p>Traitement &agrave; l&rsquo;Arnica</p>

      </div>
@endsection
@section('title','Blépharoplastie en Tunisie - Dr Djemal : Chirurgie Blépharoplastie Tunisie')
@section('description','Vous envisagez une Blépharoplastie en Tunisie? Dr Djemal, chirugien esthétique intervient pour vous réaliser la Chirurgie Blépharoplastie en Tunisie')
