@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>Rhinoplastie</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">

   <h2>D&eacute;finition&nbsp;</h2>

   <p><strong>Rhinoplastie</strong>&nbsp;: d&eacute;signe la chirurgie plastique et esth&eacute;tique du nez.</p>

   <h2>Objectifs&nbsp;</h2>

   <p>L&#39;intervention vise &agrave; remodeler le nez pour l&#39;embellir. Il s&#39;agit de corriger sp&eacute;cifiquement les disgr&acirc;ces pr&eacute;sentes et d’éventuels problèmes de respiration nasale.</p>

   <p>Le but est d&#39;obtenir un nez d&#39;aspect naturel, s&#39;harmonisant avec les autres traits du visage, convenant &agrave; la psychologie et &agrave; la personnalit&eacute; du patient, et r&eacute;pondant aux demandes de ce dernier.</p>

   <h2>Principes &nbsp;</h2>

   <p>Le principe est, &agrave; partir d&#39;incisions dissimul&eacute;es dans les narines, de remodeler l&#39;os et le cartilage qui constituent l&#39;infrastructure solide du nez et lui conf&egrave;rent sa forme particuli&egrave;re. Une rhinoplastie ne laisse pas habituellement de cicatrice visible sur la peau.</p>

   <p>Lorsqu&#39;une obstruction nasale g&ecirc;nant la respiration existe, elle sera trait&eacute;e dans le m&ecirc;me temps op&eacute;ratoire, qu&#39;elle soit due &agrave; une d&eacute;viation de la cloison ou &agrave; une hypertrophie des cornets</p>

   <h3>Incisions&nbsp;</h3>

   <p>Elles sont dissimul&eacute;es, le plus souvent &agrave; l&#39;int&eacute;rieur des narines ou quelquefois sous la l&egrave;vre sup&eacute;rieure, et il n&#39;en r&eacute;sulte donc aucune cicatrice visible &agrave; l&#39;ext&eacute;rieur.</p>

   <p>Parfois pourtant, des incisions externes peuvent &ecirc;tre requises</p>

   <p>&nbsp;</p>

   <table align="center" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td style="width:307px">
        <p><img src="{{ asset('img/schema_contenu/resection-de-la-bosse.jpg') }}" style="height:204px; width:199px" /></p>
        </td>
        <td style="width:23px">
        <p>&nbsp;</p>
        </td>
      </tr>
      <tr>
        <td style="width:307px">
        <p>Types d&#39;incisions</p>
        </td>
        <td style="width:23px">
        <p>&nbsp;</p>
        </td>
      </tr>
    </tbody>
   </table>

   <h3>Rectifications&nbsp;</h3>

   <p>On pourra ainsi r&eacute;tr&eacute;cir un nez trop large, r&eacute;aliser l&#39;ablation d&#39;une bosse, corriger une d&eacute;viation, affiner une pointe, raccourcir un nez trop long, redresser une cloison ou r&eacute;duire des cornets g&ecirc;nants&hellip; Parfois, des greffons cartilagineux ou osseux seront utilis&eacute;s pour combler une d&eacute;pression, soutenir une portion du nez ou am&eacute;liorer la forme de la pointe.</p>

   <h3>Sutures&nbsp;</h3>

   <p>Les incisions sont referm&eacute;es avec de petits fils, le plus souvent r&eacute;sorbables.</p>

   <h3>Pansements et attelles&nbsp;</h3>

   <p>Les fosses nasales peuvent &ecirc;tre m&eacute;ch&eacute;es avec diff&eacute;rents mat&eacute;riaux absorbants. Un pansement modelant est souvent r&eacute;alis&eacute; &agrave; la surface du nez &agrave; l&#39;aide de petites bandelettes adh&eacute;sives. Enfin, une attelle de maintien et de protection, en pl&acirc;tre est moul&eacute;e et fix&eacute;e sur le nez, pouvant parfois remonter sur le front.</p>

   <p>En fonction du chirurgien, de l&#39;ampleur des am&eacute;liorations &agrave; apporter, et de la n&eacute;cessit&eacute; &eacute;ventuelle de gestes compl&eacute;mentaires, l&#39;intervention peut durer de 45 minutes &agrave; deux heures.</p>

   <h2>Les suites op&eacute;ratoires&nbsp;</h2>

   <p>Les suites de la rhinoplastie sont rarement douloureuses et c&#39;est plut&ocirc;t l&#39;impossibilit&eacute; de respirer par le nez (du fait de la pr&eacute;sence des m&egrave;ches) qui constitue le principal d&eacute;sagr&eacute;ment des premiers jours.</p>

   <p>On observe, surtout au niveau des paupi&egrave;res, l&#39;apparition d&#39;un &oelig;d&egrave;me (gonflement) et parfois d&#39;ecchymoses (bleus) dont l&#39;importance et la dur&eacute;e sont tr&egrave;s variables d&#39;un individu &agrave; l&#39;autre.</p>

   <p>Il est recommand&eacute; de se reposer et de ne faire aucun effort les jours suivant l&rsquo;intervention.</p>

   <p>Les m&egrave;ches sont &ocirc;t&eacute;es le lendemain de l&rsquo;intervention. L&#39;attelle est retir&eacute;e le 6&egrave;me jour.</p>

   <p>&nbsp;Le nez appara&icirc;tra alors encore assez massif du fait de l&#39;&oelig;d&egrave;me et une g&ecirc;ne respiratoire sera encore pr&eacute;sente, due au gonflement de la muqueuse et &agrave; la formation possible de cro&ucirc;tes dans les fosses nasales.</p>

   <p>Les stigmates de l&#39;intervention vont s&#39;att&eacute;nuer progressivement, permettant le retour &agrave; une vie socio-professionnelle normale apr&egrave;s quelques jours (10 &agrave; 20 jours selon l&#39;ampleur des suites).</p>

   <p>Les sports et activit&eacute;s violentes sont &agrave; &eacute;viter les 3 premiers mois.</p>

   <h2>Le r&eacute;sultat&nbsp;</h2>

   <p>Un d&eacute;lai de deux &agrave; trois mois est n&eacute;cessaire pour avoir un bon aper&ccedil;u du r&eacute;sultat, en sachant que l&#39;aspect d&eacute;finitif ne sera obtenu qu&#39;apr&egrave;s six mois &agrave; un an de lente et subtile &eacute;volution.</p>
   <h2>Conseils pratiques</h2>

   <p>V&ecirc;tements faciles &agrave; mettre.</p>

   <p>Masque bleu contre les &oelig;d&egrave;mes</p>

   <p>Casquette (lunettes difficiles &agrave; mettre)</p>

   <p>Cr&egrave;me anti cerne</p>

   <p>Traitement &agrave; l&rsquo;arnica</p>

      </div>
@endsection

@section('title','Rhinoplastie en Tunisie - Dr Djemal : Chirurgie Rhinoplastie Tunisie ')
@section('description','Vous envisagez une Rhinoplastie en Tunisie? Dr Djemal, chirugien esthétique intervient pour vous réaliser votre chirurgie Rhinoplastie en Tunisie')
