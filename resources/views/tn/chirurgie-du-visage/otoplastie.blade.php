@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>OTOPLASTIE</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">

    <h2>D&eacute;finition</h2>

<p>La correction d&#39;oreilles d&eacute;coll&eacute;es n&eacute;cessite une intervention chirurgicale, appel&eacute;e &ldquo;otoplastie&rdquo;, visant &agrave; remodeler les pavillons jug&eacute;s excessivement visibles. L&#39;op&eacute;ration est habituellement r&eacute;alis&eacute;e sur les deux oreilles, mais peut parfois &ecirc;tre unilat&eacute;rale.</p>

<h2>Principes&nbsp;</h2>

<p>L&#39;otoplastie vise &agrave; corriger d&eacute;finitivement des anomalies en remodelant le cartilage, de fa&ccedil;on &agrave; obtenir des oreilles &ldquo;recoll&eacute;es&rdquo;, sym&eacute;triques, de taille et d&#39;aspect naturels.</p>

<p>Une otoplastie peut &ecirc;tre r&eacute;alis&eacute;e chez l&#39;adulte ou l&#39;adolescent, mais la plupart du temps la correction est envisag&eacute;e d&egrave;s l&#39;enfance o&ugrave; elle peut &ecirc;tre pratiqu&eacute;e &agrave; partir de l&#39;&acirc;ge de 7 ans&nbsp;</p>

<h2>Avant l&#39;intervention&nbsp;</h2>

<p>Un examen des oreilles aura &eacute;t&eacute; r&eacute;alis&eacute; par le chirurgien afin d&#39;analyser les modifications &agrave; apporter.</p>

<p>Pour les gar&ccedil;ons, une coupe de cheveux bien courte est souhaitable. (Pour les filles, une queue de cheval sera la bienvenue)</p>

<p>La t&ecirc;te et les cheveux seront soigneusement lav&eacute;s la veille de l&#39;op&eacute;ration.</p>

<h3>HOSPITALISATION</h3>


<p>Une courte hospitalisation peut &ecirc;tre pr&eacute;f&eacute;r&eacute;e. L&#39;entr&eacute;e s&#39;effectue alors le matin et la sortie est habituellement autoris&eacute;e d&egrave;s le lendemain.</p>

<h2>L&#39;intervention&nbsp;</h2>

<p>Incisions cutan&eacute;es : Habituellement, elles sont situ&eacute;es uniquement dans le sillon r&eacute;tro-auriculaire, c&rsquo;est-&agrave;-dire dans le pli naturel situ&eacute; derri&egrave;re l&#39;oreille. Dans certains cas, de petites incisions compl&eacute;mentaires seront pratiqu&eacute;es &agrave; la face ant&eacute;rieure du pavillon, mais elles seront alors dissimul&eacute;es dans des replis naturels.</p>

<p>Pansement : Il est r&eacute;alis&eacute; gr&acirc;ce &agrave; des bandes &eacute;lastiques autour de la t&ecirc;te afin de maintenir les oreilles en bonne position.</p>

<p>En fonction du chirurgien et de l&#39;importance des malformations &agrave; corriger, une otoplastie bilat&eacute;rale peut durer d&rsquo;une demi-heure &agrave; une heure et demie.</p>


<p><img src="{{ asset('img/schema_contenu/otoplastie.jpg') }}" /></p>

<p><strong>Incisions</strong></p>

<h2>Les suites op&eacute;ratoires&nbsp;</h2>

<p>Les douleurs sont habituellement mod&eacute;r&eacute;es et, si n&eacute;cessaire, combattues par un traitement antalgique et anti-inflammatoire.</p>

<p>Le premier gros pansement sera &ocirc;t&eacute; le lendemain. Il sera remplac&eacute; par un autre bandage plus l&eacute;ger .Les oreilles pourront alors appara&icirc;tre gonfl&eacute;es, avec des reliefs masqu&eacute;s par l&rsquo;&oelig;d&egrave;me (gonflement). Des bleus plus ou moins importants sont parfois pr&eacute;sents. Cet aspect &eacute;ventuel ne doit pas inqui&eacute;ter.</p>

<p>Un bandeau de contention et de protection (type &ldquo;bandeau de tennis&rdquo;) devra &ecirc;tre port&eacute; nuit et jour pendant une quinzaine de jours, puis uniquement la nuit pendant encore quelques semaines. Durant cette p&eacute;riode, les activit&eacute;s physiques ou sportives avec risque de contact devront &ecirc;tre &eacute;vit&eacute;es.</p>

<p>L&#39;exposition au grand froid est d&eacute;conseill&eacute;e pendant au moins deux mois compte tenu du risque de gelures du fait de la diminution transitoire de la sensibilit&eacute; des oreilles.</p>

<h2>RESULTAT</h2>

<p>Un d&eacute;lai d&rsquo;un &agrave; deux mois est n&eacute;cessaire pour appr&eacute;cier le r&eacute;sultat final. C&#39;est le temps n&eacute;cessaire pour que les tissus se soient assouplis et que la totalit&eacute; de l&#39;&oelig;d&egrave;me se soit r&eacute;sorb&eacute;, laissant appara&icirc;tre nettement les reliefs de l&#39;oreille. Pass&eacute; ce d&eacute;lai, seules les cicatrices seront encore un peu ros&eacute;es et indur&eacute;es avant de s&#39;estomper.</p>

      </div>
@endsection
@section('title','Otoplastie en Tunisie - Dr Djemal :  Chirurgie otoplastie Tunisie ')
@section('description','Vous envisagez une Otoplastie en Tunisie? Dr Djemal, chirugien esthétique intervient pour vous réaliser votre chirurgie otoplastie en Tunisie')
