@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>profiloplastie</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">
<p>Intervention&nbsp;chirurgicale esth&eacute;tique&nbsp; indiqu&eacute;e pour tous les patients (femmes et hommes) qui souhaitent obtenir un meilleur &eacute;quilibre de leur&nbsp;visage.</p>

<h2>DEFINITION</h2>

<p>La&nbsp;profiloplastie&nbsp;est la&nbsp;chirurgie plastique du profil du visage&nbsp;dans sa globalit&eacute;.</p>

<p>Le but est d&rsquo;harmoniser l&rsquo;ensemble des parties du visage en proc&eacute;dant &agrave; des corrections sur certaines zones&nbsp;:</p>

<p>Ainsi une&nbsp;<a href="./rhinoplastie">rhinoplastie</a> ou chirurgie du nez&nbsp;retirant une bosse, peut se r&eacute;v&eacute;ler insuffisante si un menton trop fuyant n&#39;est pas corrig&eacute;.</p>

<p>De m&ecirc;me, un nez peut para&icirc;tre trop petit dans le cas d&rsquo;un menton trop saillant ou trop projet&eacute;.</p>

<p>La correction du menton par une g&eacute;nioplastie ( <a href="./mentoplastie">mentoplastie</a> ) &nbsp;peut &eacute;galement &ecirc;tre indiqu&eacute;e dans le cas d&#39;une pro g&eacute;nie (menton en avant) ou d&#39;une r&eacute;tro g&eacute;nie (menton fuyant).&nbsp;</p>

<p>Le relief des pommettes est &eacute;galement &eacute;tudi&eacute; pour harmoniser tout le visage.</p>

<p>Lors de la preconsultation pour une&nbsp;chirurgie plastique du nez (rhinoplastie), votre&nbsp;<a href="./cv">chirurgien maxillo-facial</a>&nbsp;pourra donc &ecirc;tre amen&eacute; &agrave; vous proposer une profiloplastie&nbsp;: une rhinoplastie associ&eacute;e a une&nbsp;chirurgie plastique du menton (g&eacute;nioplastie).</p>

<h2>SOINS POST OPERATOIRES</h2>

<p>La&nbsp;profiloplastie&nbsp;est g&eacute;n&eacute;ralement peu douloureuse.</p>

<p>Des compresses glac&eacute;es, permettront de limiter le d&eacute;veloppement de l&#39;&oelig;d&egrave;me des paupi&egrave;res. Les compresses sont renouvel&eacute;es aussi souvent que possible&nbsp; pendant les 24 premi&egrave;res heures.</p>

<h2>SUITES POSTOPERATOIRES</h2>

<p>Un &oelig;d&egrave;me facial appara&icirc;t dans les 48 heures qui suivent l&#39;op&eacute;ration et durera environ 8 jours. Il s&#39;att&eacute;nue progressivement sous l&#39;effet d&#39;un traitement adapt&eacute; &agrave; l&rsquo;arnica.</p>

<p>L&rsquo;activit&eacute; sportive peut &ecirc;tre reprise apr&egrave;s un mois.</p>
      </div>
@endsection
@section('title','Profiloplastie en Tunisie - Dr Djemal : Chirurgie profiloplastie tunisie ')
@section('description','Il vous faut une profiloplastie en Tunisie? Dr Djemal, chirugien esthétique vous réalise la chirurgie profiloplastie en tunisie')
