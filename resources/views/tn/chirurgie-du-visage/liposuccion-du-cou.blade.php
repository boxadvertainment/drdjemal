@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DU VISAGE</span>Liposuccion du cou</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">

    <h2>DEFINITION :</h2>

    <p>Supprimer localement par aspiration la graisse au niveau du cou. Avec une canule reli&eacute;e &agrave; un syst&egrave;me de pompe, le chirurgien va aspirer l&rsquo;exc&eacute;dent graisseux situ&eacute; sous le menton et les parties lat&eacute;rales du cou le long de la m&acirc;choire inf&eacute;rieure. Gr&acirc;ce &agrave; la lipoaspiration, on peut &eacute;galement traiter le double menton.</p>

    <p>Cette intervention peut s&#39;effectuer isol&eacute;ment ou lors de la r&eacute;alisation d&#39;un lifting.</p>

    <h2>INTERVENTION</h2>

    <p>En g&eacute;n&eacute;ral, on pratique 3 petites incisions : une m&eacute;diane (juste sous le menton) et deux lat&eacute;rales (sous l&rsquo;angle des m&acirc;choires). C&rsquo;est par ces ouvertures que l&#39;on introduit la canule d&rsquo;aspiration.</p>

    <p>- la graisse enlev&eacute;e ne r&eacute;appara&icirc;t pas car les adipocytes n&rsquo;ont qu&rsquo;un tr&egrave;s faible pouvoir de r&eacute;g&eacute;n&eacute;ration,</p>

    <p>- cette graisse peut &eacute;ventuellement &ecirc;tre r&eacute;inject&eacute;e &agrave; un autre endroit pour combler un creux ou une d&eacute;pression (joue, sillons naso g&eacute;niens, cernes&hellip;),</p>

    <p>Dans de nombreux cas la lipoaspiration du cou est souvent associ&eacute;e a un lifting cervico facial.</p>

    <h2>SUITES POST OPERATOIRES</h2>

    <p>- &oelig;d&egrave;me et h&eacute;matomes peuvent persister durant 15 jours,</p>

    <p>- il est n&eacute;cessaire de porter un bandage de contention pendant 15 jours &agrave; 3 semaines.</p>

    <h2>RISQUES</h2>

    <p>- infection locale, &oelig;d&egrave;me prolong&eacute;,</p>

    <p>- irr&eacute;gularit&eacute; de la peau, insuffisance de r&eacute;sultat.</p>

      </div>
@endsection


@section('title','Liposuccion en Tunisie - Dr Djemal : Liposuccion du cou en Tunisie')
@section('description','Vous envisagez Liposuccion en Tunisie? Dr Djemal, chirugien esthétique réalise votre liposuccion du cou en Tunisie')
