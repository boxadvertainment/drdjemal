<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">

    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>

    <style type="text/css">

        body{
            width: 100%;
            background-color: #d5dce6;
            margin:0;
            padding:0;
            -webkit-font-smoothing: antialiased;
        }

        p,h1,h2,h3,h4{
            margin-top:0;
            margin-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }

        span.preheader{display: none; font-size: 1px;}

        html{
            width: 100%;
        }

        table{
            font-size: 14px;
            border: 0;
            border-collapse:collapse;
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }

        /* ----------- responsivity ----------- */
         @media only screen and (max-width: 640px){
            /*------ top header ------ */
            .top-bg{width: 440px !important;}
            .main-header{line-height: 28px !important; font-size: 20px !important;}
            .main-subheader{line-height: 28px !important;}

            /*----- main image -------*/
            .main-image img{width: 420px !important; height: auto !important;}

            /*-------- container --------*/
            .container600{width: 440px !important;}
            .container560{width: 420px !important;}

            /*-------- divider --------*/
            .divider img{width: 440px !important; height: 1px !important;}


            /*-------- footer ------------*/
            .vertical-spacing{width: 420px !important;}
            .bottom-shadow{width: 440px !important;}
        }

        @media only screen and (max-width: 479px){

            /*------ top header ------ */
            .top-bg{width: 280px !important;}
            .logo{width: 260px !important;}
            .nav{width: 260px !important;}
            .main-header{line-height: 28px !important; font-size: 18px !important;}
            .main-subheader{line-height: 28px !important;}

            /*----- main image -------*/
            .main-image img{width: 260px !important; height: auto !important;}

            /*-------- container --------*/
            .container600{width: 280px !important;}
            .container560{width: 260px !important;}

            /*-------- divider --------*/
            .divider img{width: 280px !important; height: 1px !important;}


            /*-------- footer ------------*/
            .vertical-spacing{width: 260px !important;}
            .bottom-shadow{width: 280px !important;}
        }

    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d5dce6">
        <tbody>
        <tr>
            <td height="47"></td>
        </tr>
        <tr>
            <td align="center">
                <table width="600" cellpadding="0" align="center" cellspacing="0" border="0" class="container600">
                    <tbody>
                        <tr>
                            <td style="line-height: 25px;">
                                <img src="{{ asset('img/contact-email/top-header-bg.png') }}" style="display: block; width: 600px; height: 25px;" width="600" height="25" border="0" alt="" class="top-bg">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td align="center">
                <table width="600" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="f8f8f8" class="container600">
                    <tbody>
                    <tr><td height="30"></td></tr>
                    <tr>
                        <td align="center">
                            <table width="540" cellpadding="0" align="center" cellspacing="0" border="0" class="container600" >
                                <tbody>
                                    <tr>
                                        <td align="left">
                                            <table cellpadding="0" align="left" cellspacing="0" border="0" class="container560">
                                                <tbody>
                                                    <tr>
                                                        <td style="color: #9099a6; font-size: 14px; font-family: &#39;Noto Sans&#39;, Arial, sans-serif;">
                                                            Nom : <span style="color: #6c7480;">{{ $name }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr><td height="5"></td></tr>
                                                    <tr>
                                                        <td style="color: #9099a6; font-size: 14px; font-family: &#39;Noto Sans&#39;, Arial, sans-serif;">
                                                            E-mail : <a href="mailto:{{ $email }}" style="color: #4882ce; text-decoration: none;">{{ $email }}</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <a href="{{ url('/') }}" style="display: block; border-style: none !important; border: 0 !important;">
                                                <img border="0" height="60px" style="display: block;" src="{{ asset('img/logo-mail.png') }}" alt="logo">
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr><td height="30"></td></tr>
                    <tr>
                        <td>
                            <table width="600" cellpadding="0" align="center" cellspacing="0" border="0" class="container600">
                                <tbody><tr>
                                    <td style="line-height: 2px;" class="divider">
                                        <img editable="false" src="{{ asset('img/contact-email/divider.png') }}" style="display: block; width: 600px; height: 2px;" width="600" height="2" border="0" alt="divider">
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                    <tr><td height="35"></td></tr>
                    <tr>
                        <td align="center">
                            <table width="600" cellpadding="0" align="center" cellspacing="0" border="0" class="container600">
                                <tbody><tr>
                                    <td align="center">
                                        <table border="0" width="520" align="center" cellpadding="0" cellspacing="0" class="container560">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="color: #6c7480; font-size: 25px; font-family: &#39;Noto Sans&#39;, Arial, sans-serif;" class="main-header">{{ $subject }}</td>
                                                </tr>
                                                <tr><td height="15"></td></tr>
                                                <tr>
                                                    <td align="center" style="color: #b2b8bf; font-size: 14px; font-family: &#39;PT Sans&#39;, Arial, sans-serif; line-height: 36px;" class="main-subheader">{{ $msg }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                    <tr><td height="45"></td></tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td align="center">

                <table width="600" cellpadding="0" align="center" cellspacing="0" border="0" class="container600">
                    <tbody><tr>
                        <td align="center" style="line-height: 35px;">
                            <img src="{{ asset('img/contact-email/footer-shadow.png') }}" style="display: block; width: 600px; height: 35px;" width="600" height="35" border="0" class="bottom-shadow">
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>

        <tr><td height="20"></td></tr>
        <tr>
            <td align="center" style="color: #9da5b0; font-size: 14px; font-family: 'PT Sans', Arial, sans-serif;">Copyright &copy; {{ date("Y") }} {{ Config::get('app.name') }}. All rights reserved.</td>
        </tr>
        <tr><td height="20"></td></tr>
    </tbody></table>

</body></html>
