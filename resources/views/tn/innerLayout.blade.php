@extends('tn.layout')


@section('content')
    <div class="container inside-page">
        <div class="row">
          <div class="col-md-9 col-sm-8 col-md-push-3 col-sm-push-4 content-wrapper">

              @yield('tn.innerContent')

          </div>
          <!-- /.content-wrapper -->

          <!-- SIDEBAT -->
          <aside class="col-md-3 col-sm-4 col-xs-8 col-xs-offset-2 col-md-pull-9 col-sm-pull-8 col-sm-offset-0" id="sidebar">
            <ul class="list-unstyled branchs-list">
              <li class="{{ Request::segment(1) == 'chirurgie-de-la-silhouette' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">CHIRURGIE DE LA SILHOUETTE</h4>
                  <span class="read-more">Lire la suite</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('chirurgie-de-la-silhouette/lifting-des-cuisses') }}" class="treatment-btn">Lifting Des Cuisses</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/lipoaspiration-des-cuisses') }}" class="treatment-btn">Lipoaspiration Des Cuisses</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/lipoaspiration-des-fesses') }}" class="treatment-btn">Lipoaspiration Des Fesses</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/protheses-des-fesses') }}" class="treatment-btn">Prothèses Des Fesses</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/lipofilling-des-fesses') }}" class="treatment-btn">Lipofilling Des Fesses</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/implants-mollets') }}" class="treatment-btn">Implants Mollets</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/plastie-abdominale') }}" class="treatment-btn">Plastie Abdominale</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/liposuccion') }}" class="treatment-btn">Liposuccion</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/chirurgie-des-bras') }}" class="treatment-btn">Chirurgie Des Bras</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/bodylift') }}" class="treatment-btn">Bodylift</a></li>
                  <li><a href="{{ url('chirurgie-de-la-silhouette/chirurgie-intime') }}" class="treatment-btn">Chirurgie Intime</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'medecine-esthetique' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">MEDECINE ESTHETIQUE</h4>
                  <span class="read-more">Lire la suite</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('medecine-esthetique/botox') }}" class="treatment-btn">Botox</a></li>
                  <li><a href="{{ url('medecine-esthetique/acide-hyaluronique') }}" class="treatment-btn">Acide Hyaluronique</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'chirurgie-des-seins' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">CHIRURGIE DES SEINS</h4>
                  <span class="read-more">Lire la suite</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('chirurgie-des-seins/lifting-des-seins') }}" class="treatment-btn">Lifting Des Seins</a></li>
                  <li><a href="{{ url('chirurgie-des-seins/augmentation-mammaire') }}" class="treatment-btn">Augmentation Mammaire</a></li>
                  <li><a href="{{ url('chirurgie-des-seins/reduction-mammaire') }}" class="treatment-btn">Réduction Mammaire</a></li>
                  <li><a href="{{ url('chirurgie-des-seins/gynecomastie') }}" class="treatment-btn">Gynécomastie</a></li>
                  <li><a href="{{ url('chirurgie-des-seins/augmentation-pectoraux') }}" class="treatment-btn">Augmentation Pectoraux</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'chirurgie-du-visage' ? 'active' : '' }}">
                <a href="#" class="branch-btn">
                  <h4 class="title">CHIRURGIE DU VISAGE</h4>
                  <span class="read-more">Lire la suite</span>
                </a>

                <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('chirurgie-du-visage/blepharoplastie') }}" class="treatment-btn">Blepharoplastie</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/otoplastie') }}" class="treatment-btn">Otoplastie</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/rhinoplastie') }}" class="treatment-btn">Rhinoplastie</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/mentoplastie') }}" class="treatment-btn">Mentoplastie</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/profiloplastie') }}" class="treatment-btn">Profiloplastie</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/chirurgie-de-la-calvitie') }}" class="treatment-btn">Chirurgie De La Calvitie</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/lifting-cervico-facial') }}" class="treatment-btn">Lifting Cervico-Facial</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/lifting-complet') }}" class="treatment-btn">Lifting Complet</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/lipofilling') }}" class="treatment-btn">Lipofilling</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/chirurgie-des-levres') }}" class="treatment-btn">Chirurgie Des Levres</a></li>
                  <li><a href="{{ url('chirurgie-du-visage/liposuccion-du-cou') }}" class="treatment-btn">Liposuccion Du Cou</a></li>
                </ul>

              </li>
              <li class="{{ Request::segment(1) == 'chirurgie-reparatrice-et-reconstructrice' ? 'active' : '' }}">
                <a href="{{ url('chirurgie-reparatrice-et-reconstructrice') }}" class="branch-btn">
                  <h4 class="title">CHIRURGIE REPARATRICE ET RECONSTRUCTRICE</h4>
                  <span class="read-more">Lire la suite</span>
                </a>

                <!-- <ul class="list-unstyled treatments-list">
                  <li><a href="{{ url('chirurgie-reparatrice-et-reconstructrice') }}" class="treatment-btn">Maladies Destructrices</a></li>
                  <li><a href="{{ url('chirurgie-reparatrice-et-reconstructrice/') }}" class="treatment-btn">Malformations Congénitales</a></li>
                  <li><a href="{{ url('chirurgie-reparatrice-et-reconstructrice/') }}" class="treatment-btn">Traumatismes Accidentels</a></li>
                  <li><a href="{{ url('chirurgie-reparatrice-et-reconstructrice/') }}" class="treatment-btn">Cancer Du Sein</a></li>
                </ul> -->

              </li>
            </ul>

            <div class="contact-box">
              <h4>Contact</h4>
              <p>
                <big>Tél: (+216) 97 400 029</big> <br>
                Rue de la feuille d’érable,
                 Résidence la brise du lac
                1053  Les Berges du Lac 2 Tunis, <br>
                Tunisie</p>
            </div>

            <div class="myron-box">
              <img src="/img/myron-logo-sidebar.png" alt="Myron Clinic">
              <p>Située dans le beau quartier d’affaires et résidentiel  du LAC II, la toute nouvelle clinique privée(2015) multidisciplinaire  MYRON est à 15 minutes du centre de Tunis et à 10 minutes de l’aéroport de Tunis Carthage.</p>
              <a href="http://myronclinic.com/" target="_blank"> Plus d'info</a>
            </div>

          </aside>
          <!-- /#sidebar md-3 -->

        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->

@endsection
