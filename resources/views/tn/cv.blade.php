@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-dr.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">Chirurgien plastique</span> DR Taher Djemal</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">
      <h2 class="content-title">présentation</h2>
      <div class="picutre-wrapper pull-left">
        <img src="img/dr-image.png" alt="">
      </div>
      <div class="description">
        <p>Chirurgien spécialiste en <a href="./chirurgie-reparatrice-et-reconstructrice">chirurgie plastique reconstructrice</a> et esthétique. </p>
        <p>Président de l’association des chirurgiens plastiques de Tunisie. <br>
        Il donne régulièrement des conférences dans les congrès nationaux et internationaux.</p>
        <p>Dr Djemal est inscrit à <b>l'ordre des médecins</b> <a href="http://www.ordre-medecins.org.tn" target="_blank">www.ordre-medecins.org.tn</a>, sous <b> le numéro :  5985</b>
        </p>
        <p>Depuis novembre, Le Dr Djemal exerce dans la toute nouvelle clinique privée multidisciplinaire MYRON située au Lac II à 15 minutes de l’aéroport Tunis Carthage.</p>
      </div>
    </div>
    <!-- /.content -->

      <div class="tabs-wrapper">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#parcours" aria-controls="parcours" role="tab" data-toggle="tab">Parcours</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Diplomes</a></li>
          <li role="presentation"><a href="#medias" aria-controls="medias" role="tab" data-toggle="tab">Magazines</a></li>
          <li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Vidéos</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="parcours">
            <h3>Qualifications</h3>
            <ul>
              <li><b>1983   : </b>Diplômé en Médecine Générale (Tunis)</li>
              <li><b>1988   : </b>Obtention du Diplôme de Spécialiste en Chirurgie Plastique et Esthétique et Maxillo Faciale</li>
              <li><b>1990   : </b>Diplômé du Collège Français de Chirurgie Plastique Paris</li>
              <li><b>1990   : </b>Diplômé en Chirurgie Plastique et Maxillo Faciale à l’Université de Médecine de Nancy</li>
            </ul>

            <h3>Formation pratique à l’étranger</h3>
            <ul>
              <li><b>De 1990 à 1992 : </b>Exercice de la chirurgie plastique à l’Hôpital Foch (Paris et Hôpital de Nancy.</li>
              <li><b>1994   : </b>Pendant trois mois, exercice de la chirurgie plastique en clinique privée à Baltimore (USA).</li>
            </ul>

            <h3>Expérience professionnelle</h3>
            <ul>
              <li><b>1992 : </b>Ouverture d’un cabinet privé à Tunis.
                <p>Les compétences et la grande expérience du Dr DJEMEL couvrent tous les domaines de la chirurgie plastique (liposuccion, plastie mammaire, lifting, rhinoplastie, implants capillaires…).</p>

                <p>Parallèlement aux opérations réalisées sur les patients tunisiens et étrangers de son cabinet, le Dr DJEMEL consacre une journée par
                semaine à des opérations de reconstruction extrêmement complexes dans les hôpitaux de Tunis.</p>

                <p>Grâce à ses compétences et à sa générosité, des centaines d’enfants et d’adultes ayant eu une malformation congénitale, un accident
                ou une maladie retrouvent un visage ou un corps qui leur permet de vivre normalement.</p>
              </li>
            </ul>

            <h3>Associations professionnelles</h3>
            <ul>
              <li><b>De 1998 à 2004 : </b>Secrétaire Général de l’Association des Chirurgiens Plastiques de Tunisie.</li>
              <li><b>Depuis 2004   : </b>Président de l’Association des Chirurgiens Plastiques de Tunisie.
              Membre de la Société Méditerranéenne de Chirurgie Plastique.
              Animation de cours et de conférences et réalisation de démonstrations opératoires en Allemagne, Italie, Espagne, Portugal, Martinique
              et dans les pays du Golfe.</li>
            </ul>

          </div>
          <div role="tabpanel" class="tab-pane fade" id="profile">
            <div class="col-md-6"><a href="{{ asset('img/about/certif1.jpg') }}" class="galerie"><img src="{{ asset('img/about/certif1.jpg') }}" width="100%"></a></div>
            <div class="col-md-6"><a href="{{ asset('img/about/certif2.jpg') }}" class="galerie"><img src="{{ asset('img/about/certif2.jpg') }}" width="100%"></a></div>
            <div class="col-md-6"><a href="{{ asset('img/about/certif3.jpg') }}" class="galerie"><img src="{{ asset('img/about/certif3.jpg') }}" width="100%"></a></div>

          </div>
          <div role="tabpanel" class="tab-pane fade" id="medias">
            <div class="row">
              <div class="col-md-4"><a href="{{ asset('img/about/mag1.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag1.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag2.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag2.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag3.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag3.jpg') }}" width="100%"></a></div>
             </div>
            <div class="row">
              <div class="col-md-4"><a href="{{ asset('img/about/mag4.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag4.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag5.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag5.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag6.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag6.jpg') }}" width="100%"></a></div>
            </div>
            <div class="row">
              <div class="col-md-4"><a href="{{ asset('img/about/mag7.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag7.jpg') }}" width="100%"></a></div>
              <div class="col-md-4"><a href="{{ asset('img/about/mag8.jpg') }}" class="galerie"><img src="{{ asset('img/about/mag8.jpg') }}" width="100%"></a></div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="videos">
            <div class="row">
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/5G9WvcAc3A8?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/3Vr8WrqA65U?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/vjiB78k8_D0?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/rsGHUQLXQs?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
              <iframe class="col-md-6" height="215" src="https://www.youtube.com/embed/HRi8BzILxk4?rel=0&amp;showinfo=0" style="margin-bottom: 10px;" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('title','Chirurgien esthétique Tunisie  - Dr Djemal : Chirugien Plasticien')
@section('description','Vous cherchez un chirurgien esthétique en Tunisie? Dr Djemal, chirugien esthétique parmi les meilleurs chirugiens plasticiens en Tunisie  ')
