@extends('fr.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('fr.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('fr.innerContent')
    <div class="content">
              <h2 class="content-title">CHIRURGIE DU VISAGE</h2>
<p>Plusieurs interventions permettent de rafraichir et de rajeunir le visage : le lifting cervico-facial ou complet retend la peau relâchée. La rhinoplastie donne un nez plus harmonieux et la blépharoplastie rajeunit  le regard (2 ou 4 paupières). L’otoplastie remet  les oreilles dans l’axe de la tête et la génioplastie harmonise la plastie du menton. L’apparition des micro greffes est une technique élégante, précise et fiable qui gomme les effets du temps sur la chevelure.</p>
    </div>
@endsection
@section('title','Chirurgie Du Visage - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','Plusieurs interventions permettent de rafraichir et de rajeunir le visage : le lifting cervico-facial ou complet retend la peau relâchée. La rhinoplastie donne un nez plus harmonieux et la blépharoplastie...')