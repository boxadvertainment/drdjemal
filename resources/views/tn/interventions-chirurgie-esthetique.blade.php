@extends('tn.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('tn.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('tn.innerContent')
    <div class="content">
              <h2 class="content-title">Interventions</h2>
              <h3>LES SEINS</h3>

              <ul>
                <li>Proth&egrave;ses mammaires pour augmenter le volume des seins&nbsp;</li>
                <li>R&eacute;duction lorsqu&#39;il y a hypertrophie mammaire&nbsp;</li>
                <li>Plastie pour remonter des seins tombants&nbsp;</li>
                <li>Faire ressortir des mamelons ombiliqu&eacute;s&nbsp;</li>
                <li>Gyn&eacute;comastie pour r&eacute;duire la poitrine des hommes</li>
              </ul>

              <h3>LE VISAGE &nbsp; &nbsp;</h3>

              <ul>
                <li><a href="./chirurgie-du-visage/rhinoplastie">Rhinoplastie</a> pour refaire un nez plus harmonieux&nbsp;</li>
                <li><a href="./chirurgie-du-visage/blepharoplastie">Bl&eacute;pharoplastie</a> pour rajeunir le regard (2 ou 4 paupi&egrave;res)&nbsp;</li>
                <li>Otoplastie pour remettre les oreilles dans l&#39;axe de la t&ecirc;te&nbsp;</li>
                <li>G&eacute;nioplastie pour refaire la plastie du menton&nbsp;</li>
                <li>Profiloplastie pour harmoniser le menton et le nez&nbsp;</li>
                <li><a href="./chirurgie-du-visage/lifting-cervico-facial">Lifting cervico facial</a> pour retendre la peau rel&acirc;ch&eacute;e et redonner l&rsquo;ovale au visage.</li>
                <li>Lifting complet (lifting cervico facial et bl&eacute;pharoplastie)</li>
                <li>Chirurgie des l&egrave;vres</li>
              </ul>

              <h3>LE VENTRE ET HANCHES</h3>

              <ul>
                <li>La liposuccion qui aspire la graisse&nbsp;</li>
                <li>Plastie abdominale (abdominoplastie) pour retirer l&rsquo;exc&eacute;dent de peau.</li>
              </ul>

              <h3>LES JAMBES</h3>

              <ul>
                <li>La <a href="./chirurgie-du-visage/liposuccion-du-cou">liposuccion</a> aspire la graisse sur les cuisses, les chevilles ou les mollets.</li>
                <li>Lifting des cuisses pour retendre la peau rel&acirc;ch&eacute;e</li>
              </ul>

              <h3>LES FESSES</h3>

              <ul>
                <li>Liposuccion</li>
                <li>Mise en place de proth&egrave;ses&nbsp;</li>
                <li>Lifting des fesses</li>
              </ul>

              <h3>LES BRAS</h3>

              <ul>
                <li>Lifting des bras pour retendre la peau rel&acirc;ch&eacute;e (notamment apr&egrave;s une liposuccion)&nbsp;</li>
                <li>Liposuccion des bras</li>
              </ul>

              <h3>LA CHIRURGIE INTIME</h3>

              <ul>
                <li>Augmenter&nbsp; un p&eacute;nis trop petit&nbsp;</li>
                <li>Remodeler les l&egrave;vres du vagin</li>
              </ul>

              <h3>LA CALVITIE</h3>

              <ul>
                <li>microgreffes est une technique extr&ecirc;mement pr&eacute;cise et fiable qui gomme les effets du temps sur la chevelure.<br />
                Il s&#39;agit du transfert de vos propres cheveux, dans la r&eacute;gion de la couronne qui pourront cro&icirc;tre comme s&#39;ils avaient toujours &eacute;t&eacute; en place.</li>
              </ul>

            </div>
@endsection
@section('title','Intervention chirurgie esthétique - Dr Djemal : Chirurgie esthétique Tunisie ')
@section('description','Vous envisagez recourir à une chirurgie esthétique? Dr Djemal, chirugien esthétique très reconnu réalise votre chirurgie esthétique en Tunisie')
