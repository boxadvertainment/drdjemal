@extends('tn.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('tn.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">chirurgie</span>réparatrice et reconstructrice</h1>
      </div>
    </header>
@endsection

@section('tn.innerContent')
    <div class="content">
              <h2>D&eacute;finition&nbsp;</h2>
              <p>La chirurgie r&eacute;paratrice est une des sp&eacute;cialisations de la chirurgie plastique.</p>

<p>Elle a pour but de corriger ou reconstruire une zone du corps abim&eacute;e ou traumatis&eacute;e.</p>

<p>La chirurgie reconstructrice est une chirurgie de r&eacute;paration du corps &agrave; la suite de malformations cong&eacute;nitales, de traumatismes accidentels ou de maladies destructrices.</p>

<p>L&#39;acte de chirurgie reconstructrice englobe la dimension esth&eacute;tique.</p>

<p>Dans le cas de la reconstruction d&#39;un sein apr&egrave;s son ablation &agrave; la suite d&#39;un cancer, le geste r&eacute;parateur permettra de reconstruire le sein de mani&egrave;re a obtenir une jolie plastique voire une belle esth&eacute;tique.</p>

<p>On peut reconstruire presque toutes les parties du corps.</p>

<p>Les cancers de la peau et du sein repr&eacute;sentent aujourd&#39;hui, chez l&#39;adulte, les premi&egrave;res causes de reconstruction cons&eacute;cutive au traitement initial de la tumeur.</p>

<p>D&#39;autres pathologies peuvent aussi faire l&#39;objet de reconstructions complexes : malformations cong&eacute;nitales, br&ucirc;lures profondes et &eacute;tendues, traumatismes des membres et leurs s&eacute;quelles, infections cutan&eacute;es &eacute;tendues.&nbsp;</p>

<h2><strong>Elle regroupe des interventions du type&nbsp;:</strong></h2>

<ul>
  <li>Tumeurs de la peau et des tissus mous</li>
  <li>Infections</li>
  <li>Reprise de cicatrices</li>
  <li>Reconstruction et changement d&rsquo;aspect</li>
  <li>Soins aux brul&eacute;s</li>
  <li>Chirurgie cranio-faciale</li>
</ul>

<p><br />
La chirurgie r&eacute;paratrice permettra par exemple de corriger une malformation ou d&rsquo;y avoir recours pour une r&eacute;paration faciale apr&egrave;s un traumatisme et ainsi am&eacute;liorer ou redonner un aspect normal a une partie du corps abim&eacute;e.<br />
<br />
Dans ce type de chirurgie on inclut aussi la&nbsp;reconstruction d&rsquo;un sein apr&egrave;s mastectomie&nbsp;(dans le cadre du traitement d&rsquo;un cancer du sein), la correction d&rsquo;une hypertrophie mammaire mais aussi d&rsquo;une&nbsp;une ptose mammaire.<br />
&nbsp;</p>

            </div>
@endsection

@section('title','Chirurgie réparatrice en Tunisie - Dr Djemal : Chirurgie reconstructrice en Tunisie ')
@section('description',"Vous avez besoin d'une chirurgie réparatrice en Tunisie? Dr Djemal, chirugien esthétique réalise votre chirurgie reconstructrice en Tunisie")
