@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Lipoaspiration des cuisses</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content"><h2>DEFINITION</h2>

<p>La lipoaspiration des cuisses permet de supprimer radicalement et d&eacute;finitivement les surcharges de graisse localis&eacute;es.</p>

<p>Cependant, lorsqu&rsquo;il existe un rel&acirc;chement de la peau au niveau de la face interne des cuisses, une lipoaspiration seule ne suffit pas &agrave; corriger ce d&eacute;faut , il faut y associer un lifting de la face interne de la cuisse.</p>

<p>L&#39;intervention a alors pour but de r&eacute;aliser l&#39;ablation de l&#39;exc&eacute;dent de peau, de r&eacute;duire l&#39;infiltration graisseuse sous-jacente, et de bien suspendre la peau en profondeur.</p>

<h2>INTERVENTION</h2>

<ul>
  <li>Le principe de la lipoaspiration est d&rsquo;introduire, &agrave; partir de tr&egrave;s petites incisions, des canules mousses, dans les zones a aspirer.</li>
  <li>Un <a href="./lifting-des-cuisses">lifting</a> est associ&eacute; chaque fois qu&#39;il existe un exc&eacute;dent de peau au niveau de la face interne de la cuisse&nbsp;</li>
  <li>La peau en exc&egrave;s est retir&eacute;e &agrave; la demande et une fixation .La cicatrice se trouve ainsi cach&eacute;e dans un pli naturel et sera donc assez discr&egrave;te.</li>
  <li>En fin d&rsquo;intervention on met en place une gaine de liposuccion.</li>
</ul>

<p>La dur&eacute;e de l&rsquo;intervention est, en moyenne, d&rsquo;une heure et se pratique sous anesthésie générale.</p>

<h2>SUITES OPERATOIRES</h2>

<p>La sortie pourra intervenir en r&egrave;gle g&eacute;n&eacute;rale le lendemain de l&rsquo;intervention.</p>

<p>Dans les suites op&eacute;ratoires, des ecchymoses (bleus) et un &oelig;d&egrave;me (gonflement) peuvent appara&icirc;tre.</p>

<p>Les douleurs sont en r&egrave;gle g&eacute;n&eacute;rale peu importantes, limit&eacute;es &agrave; quelques ph&eacute;nom&egrave;nes de tiraillements et d&rsquo;&eacute;lancements.</p>

<p>La pratique d&rsquo;une activit&eacute; sportive pourra &ecirc;tre reprise progressivement &agrave; partir de la 2</p>

<p>&egrave;me semaine post-op&eacute;ratoire.</p>

<h2>RESULTAT</h2>

<p>Il est appr&eacute;ci&eacute; dans un d&eacute;lai de 1 &agrave; 3 mois apr&egrave;s l&#39;intervention. On observe, le plus souvent, une bonne correction de l&#39;infiltration graisseuse.</p>

      </div>
@endsection

@section('title','Lipoaspiration en Tunisie - Dr Djemal : Lipoaspiration cuisses')
@section('description','Vous comptez faire une Lipoaspiration en Tunisie? Dr Djemal, chirugien esthétique intervient pour votre Lipoaspiration des cuisses en Tunisie')
