@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>Bodylift</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">
    <h2>DEFINITION</h2>

<p>Le body lift est une intervention qui donne des r&eacute;sultats tr&egrave;s spectaculaires.</p>

<p>Le terme body lift veut dire qu&rsquo;on enl&egrave;ve de l&rsquo;exc&egrave;s de peau tout autour du corps.</p>

<p>C&rsquo;est l&rsquo;intervention ideale pour&nbsp;remonter les fesses&nbsp;et&nbsp;l&rsquo;ext&eacute;rieur des cuisses mais aussi le ventre, le pubis et le devant des cuisses.<br />
Si on ne fait que la partie post&eacute;rieure de l&rsquo;op&eacute;ration : c&rsquo;est un lifting des fesses. Si on ne fait que le devant : l&rsquo;intervention est la&nbsp;<a href="./plastie-abdominale">plastie abdominale</a>.</p>

<p>Au final, on aboutit &agrave; une cicatrice circulaire, au niveau de la ceinture, facile &agrave; cacher par une culotte.</p>

<p>Le body lift est une technique qui permet de retirer, sur la totalit&eacute; de la circonf&eacute;rence de la taille, un exc&egrave;s de peau. Cet exc&egrave;s peut &ecirc;tre ou non associ&eacute; &agrave; des surcharges graisseuses. Un body lift agit sur le ventre, la face externe des cuisses et les fesses. Le chirurgien va enlever une bande de peau qui peut aller de 15 &agrave; 40 cm de hauteur tout autour du corps.</p>

<p>Un exc&eacute;dent de peau au&nbsp; niveau de la silhouette r&eacute;sulte de nombreux facteurs&nbsp;:</p>

<ul>
  <li>le vieillissement</li>
  <li>les amaigrissements importants ou non</li>
  <li>la liposuccion &nbsp;</li>
  <li>la peau est molle et les fesses tombent (cong&eacute;nital).</li>
</ul>

<h2>INTERVENTION</h2>

<p>La dur&eacute;e de l&rsquo;intervention est exceptionnellement longue pour une intervention de <a href="../chirurgie-reparatrice-et-reconstructrice">chirurgie esth&eacute;tique</a>, entre 4 et 5 heures, avec tous les risques li&eacute;s aux interventions de longue dur&eacute;e.</p>

<p>L&rsquo;hospitalisation &agrave; la suite d&rsquo;un body lift est une hospitalisation variable de 3 &agrave; 4 jours</p>

<h2>LES SUITES POST&nbsp;OPERATOIRES</h2>

<p><br />
- la fatigue : op&eacute;ration un peu longue<br />
- la cicatrisation : elle peut &ecirc;tre ralentie &agrave; certains endroits, mais cela finit toujours par se refermer.</p>

<p>Pansements et bas de contention anti-phl&eacute;bite seront mis d&egrave;s la fin de l&rsquo;intervention. Les bas seront conserv&eacute;s huit jours apr&egrave;s la sortie, la gaine de contention sera conserv&eacute;e 1 mois&nbsp; nuit et jour .</p>

<p>Les suites op&eacute;ratoires apr&egrave;s un body lift : gonflement (&oelig;d&egrave;me), bleus (ecchymoses), diminution ou disparition de la sensibilit&eacute; des zones d&eacute;coll&eacute;es, des douleurs plus ou moins importantes calm&eacute;es par des antalgiques.</p>

<p>Dans ce type d&rsquo;intervention il y a aussi, pendant toute la dur&eacute;e de la cicatrisation, des sensations de tension. Eviter tout mouvement d&rsquo;&eacute;tirement brutal.</p>

<h2>RESULTATS</h2>

<p>Il faut compter un d&eacute;lai de 3 mois pour laisser le temps de cicatrisation et de r&eacute;cup&eacute;ration de la souplesse des tissus.</p>

<p>Un d&eacute;lai de minimum 8 mois pour une totale r&eacute;cup&eacute;ration.</p>

<p>Dans tous les cas une l&eacute;g&egrave;re asym&eacute;trie entre les deux c&ocirc;t&eacute;s persistera. Une cicatrice de body lift pr&eacute;sente parfois certaines caract&eacute;ristiques : trop visible, distendue ou adh&eacute;rente.</p>

<h2>La cicatrisation est&nbsp;impr&eacute;visible&nbsp;</h2>

<p>Le chirurgien r&eacute;alise les sutures mais la cicatrisation d&eacute;pend enti&egrave;rement de chaque patient(e). Les cicatrices feront l&rsquo;objet d&rsquo;une surveillance pouvant entra&icirc;ner une retouche chirurgicale ou un traitement propre. Il faut compter un .La protection contre le soleil est indispensable pour &eacute;viter le risque de pigmentation d&eacute;finitive. L&rsquo;aspect d&eacute;finitif des cicatrices sera jug&eacute; au bout de 12 mois.</p>

      </div>
@endsection

@section('title','Lifting Tunisie - Dr Djemal : lifting du corps tunisie ')
@section('description','Vous êtes tentée par un lifting en Tunisie? Dr Djemal, chirugien esthétique intervient pour votre lifting du corps en tunisie')
