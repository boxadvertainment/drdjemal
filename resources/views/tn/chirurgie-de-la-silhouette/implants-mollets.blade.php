@extends('tn.innerLayout')

@section('class', 'page cv-page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(/img/banner-innerpages.jpg);">

    @include('tn.partials.header')

    <div class="container">
      <h1 class="page-title"><span class="intervantion">CHIRURGIE DE LA SILHOUETTE</span>implants mollets</h1>
    </div>
  </header>
@endsection

@section('tn.innerContent')
    <div class="content">
    <h2>DEFINITION</h2>

        <p>La correction des mollets trop petits peut intervenir soit dans un contexte de <a href="./../chirurgie-reparatrice-et-reconstructrice">chirurgie r&eacute;paratrice</a> soit dans un contexte de chirurgie esth&eacute;tique</p>

<p>3 m&eacute;thodes sont utilisables :</p>

<ul>
  <li>Lipofilling (transferts graisseux) ;</li>
  <li>La mise en place d&#39;implants de mollets ;</li>
  <li>Les injections d&#39;acide hyaluronique</li>
</ul>

<p>L&#39;implant&nbsp;est le plus souvent&nbsp;plac&eacute;&nbsp;en position interne. Plus rarement, un implant externe est indiqu&eacute;. Enfin, 2 implants peuvent &ecirc;tre mis en place dans le m&ecirc;me temps, lorsque l&#39;augmentation de mollet doit &ecirc;tre plus globale.</p>

<h2>LA PROTH&Egrave;SE DE MOLLETS</h2>

<p>Les&nbsp;implants&nbsp;utilis&eacute;s sont&nbsp;sp&eacute;cialement &eacute;tudi&eacute;s&nbsp;pour l&rsquo;augmentation des mollets. Ils sont pr&eacute;-remplis d&rsquo;un gel de silicone tr&egrave;s coh&eacute;sif (tr&egrave;s ferme) et l&#39;enveloppe est tr&egrave;s r&eacute;sistante. Le risque de rupture est tr&egrave;s faible en l&#39;absence de traumatisme violent (accident de la voie publique&hellip;). En cas de rupture, l&#39;implant rompu doit &ecirc;tre chang&eacute;</p>

<p>L&rsquo;aspect des implants est tr&egrave;s naturel.</p>

<!-- <h2>AVANT L&rsquo;INTERVENTION</h2>

<p>Un bilan sanguin est r&eacute;alis&eacute; conform&eacute;ment aux prescriptions. Le m&eacute;decin anesth&eacute;siste sera vu en consultation avant l&rsquo;intervention. Aucun m&eacute;dicament contenant de l&rsquo;aspirine ne devra &ecirc;tre pris dans les 10 jours pr&eacute;c&eacute;dant l&rsquo;intervention.</p>

<h2>HOSPITALISATION</h2>

<p>La dur&eacute;e de l&rsquo;hospitalisation sera de 1 jour sous anesth&eacute;sie g&eacute;n&eacute;rale.</p> -->

<h2>L&rsquo;INTERVENTION</h2>

<p>L&#39;incision est&nbsp;cach&eacute;e dans un pli de flexion&nbsp;du genou et mesure entre 3 et 5 cm.<br />
L&rsquo;implant est positionn&eacute; en profondeur, contre les muscles du mollet.<br />
L&#39;intervention se d&eacute;roule sous&nbsp;anesth&eacute;sie g&eacute;n&eacute;rale&nbsp;et dure en moyenne&nbsp;1 heure. Le patient est positionn&eacute; sur le ventre.&nbsp;Aucun drain&nbsp;n&#39;est mis en place.&nbsp;</p>

<h2>SOINS POST-OP&Eacute;RATOIRES</h2>

<p>La&nbsp;douleur, tr&egrave;s mod&eacute;r&eacute;e et bien calm&eacute;e par les antalgiques adapt&eacute;s, ne dure que les premiers jours mais un inconfort peut &ecirc;tre ressenti pendant plusieurs semaines.<br />
La&nbsp;douche&nbsp;est&nbsp;autoris&eacute;e&nbsp;d&egrave;s le lendemain de l&rsquo;intervention.<br />
La&nbsp;cicatrisation&nbsp;est obtenue en&nbsp;2 &agrave; 3 semaines.<br />
La reprise d&#39;une&nbsp;activit&eacute; sportive intensive&nbsp;sollicitant les membres inf&eacute;rieurs&nbsp;peut se faire&nbsp;3 mois&nbsp;apr&egrave;s l&#39;intervention</p>

<h2>INJECTION DE GRAISSE&nbsp;: LIPOFILLING DES MOLLETS</h2>

<p>Le &laquo;&nbsp;FILLING&nbsp;&raquo; ou injections de graisse permet surtout de redonner du volume aux mollets trop fins ou arques.</p>

<p>Il s&#39;agit de r&eacute;injecter de la graisse pr&eacute;lev&eacute;e sous anesth&eacute;sie locale, dans une r&eacute;gion discr&egrave;te et en petite quantit&eacute; (abdomen, hanches).</p>

<p>S&#39;agissant de la graisse de la patiente elle - m&ecirc;me, il n&#39;y a de ce fait pas d&#39;allergie possible et il n&#39;est donc pas n&eacute;cessaire de faire de test pr&eacute;alable. La graisse est centrifug&eacute;e et lav&eacute;e afin d&#39;injecter uniquement des cellules graisseuses.</p>

<p>Apr&egrave;s une phase d&#39;&oelig;d&egrave;me et de rougeur qui dure deux &agrave; trois jours, le r&eacute;sultat appara&icirc;t .Les injections sont faites en petites quantit&eacute;s mais avec une hypercorrection car une partie de cette graisse va se r&eacute;sorber (entre 20 et 50%). La dur&eacute;e de cet effet est donc variable&nbsp; de quelques mois &agrave; un an mais d&#39;autres injections pourront &ecirc;tre faites ult&eacute;rieurement.&nbsp;</p>

<p>En th&eacute;orie, cette technique n&#39;aurait que des avantages :</p>

<ul>
  <li>emploi d&#39;un produit &quot; naturel &quot; toujours bien accept&eacute; par l&#39;organisme</li>
  <li>non allergisant</li>
  <li>peu co&ucirc;teux</li>
  <li>injectable directement et simple &agrave; pr&eacute;lever.</li>
</ul>

<p><strong>Les meilleures indications&nbsp;;</strong></p>

<ul>
  <li>les volumes des mollets</li>
  <li>les mollets trop creux</li>
  <li>les s&eacute;quelles de lipoaspiration en creux.&nbsp;</li>
</ul>

<p><strong>Les contre-indications</strong></p>

<p>Elles sont peu nombreuses, vu le caract&egrave;re personnel de l&rsquo;injection (pas d&#39;allergie, pas de probl&egrave;me immunitaire).<br />
On retiendra des pr&eacute;cautions particuli&egrave;res &agrave; prendre chez les personnes diab&eacute;tiques ou s&eacute;ropositives, plus sensibles &agrave; l&#39;infection, et on &eacute;vitera d&#39;injecter &agrave; travers des peaux pr&eacute;sentant des dermatoses ou des l&eacute;sions acn&eacute;iques.<br />
</p>
<h2>Conclusion</h2>
<p>La m&eacute;thode du Lipofilling donne de tr&egrave;s bons r&eacute;sultats.</p>


      </div>
@endsection

@section('title','Implant mollets Tunisie - Dr Djemal')
@section('description','Vous allez avoir un implant mollets Tunisie? Dr Djemal, chirugien esthétique intervient pour vous donner les mollets galbés dont vous rêvez.')
