@extends('tn.innerLayout')

@section('class', 'page')

@section('header')
<header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url(img/banner-innerpages.jpg);">

      @include('tn.partials.header')

      <div class="container">
        <h1 class="page-title"><span class="intervantion">Les</span> Interventions</h1>
      </div>
    </header>
@endsection

@section('tn.innerContent')
    <div class="content">
              <h2 class="content-title">CHIRURGIE DE LA SILHOUETTE</h2>
<p>Les interventions de chirurgie de la silhouette permettent de redessiner et  remodeler certaines parties du corps telles que le ventre, l’abdomen, les hanches, les fesses, le dos, les cuisses, les genoux et les bras. On notera : la liposuccion ; l’abdominoplastie ; le body lift ; le lifting des cuisses ,bras, fesses et les implants des mollets et des fesses.</p>
    </div>
@endsection
@section('title','Chirurgie De La Silhouette - Dr Djemal : Chirurgie esthétique Tunisie')
@section('description','Les interventions de chirurgie de la silhouette permettent de redessiner et  remodeler certaines parties du corps telles que le ventre, l’abdomen, les hanches, les fesses, le dos...')
