@extends('admin/layout')
@section('content')

    <div class="page-header">
        <div class="left">
            Consultations ({{ $consultations->total() }})
            {{ request()->get('month') | request()->get('year') ? '- '.request()->get('month').' '.request()->get('year') : '' }}
        </div>
        <div class="right">
            @if (Auth::user()->email == 'fouis@box.agency')
                <button type="button" class="btn btn-danger" id="btn-delete" style="display: none;"><i class="fa fa-trash"></i> Supprimer</button>
                <div class="dropdown" style="display: inline-block;">
                    <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Consulations MyronClinic
                        <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="?from=myronclinic">Tout</a></li>
                    </div>
                </div>
                <div class="dropdown" style="display: inline-block;">
                    <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Filtrer par mois
                        <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-buttons" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="?all">Tout</a></li>
                        @foreach($archivesMonth as $key => $archiveMonth)
                            <li>
                                <a class="dropdown-item" href="?month={{$archiveMonth->month}}&year={{ $archiveMonth->year }}">{{ $archiveMonth->month .' - '. $archiveMonth->year }}</a>
                            </li>
                        @endforeach
                    </div>
                </div>
                <div class="dropdown" style="display: inline-block;">
                    <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Filtrer par année
                        <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-buttons" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="?all">Tout</a></li>
                        @foreach($archivesYear as $key => $archiveYear)
                            <li>
                                <a class="dropdown-item" href="?year={{ $archiveYear->year }}">{{ $archiveYear->year }}</a>
                            </li>
                        @endforeach
                    </div>
                </div>
            @endif
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#consultationModal"><i class="fa fa-plus"></i> Ajouter une consultation</button>
        </div>

    </div>

    <div>
        <table class="table table-striped table-bordered">
            <thead>
            <th>
                @if (Auth::user()->email == 'fouis@box.agency')
                    <label class="btn btn-danger" for="select-all">
                        <i class="fa fa-trash fa-2x"></i>
                        <input type="checkbox" name="select-all" id="select-all" class="hidden">
                    </label>
                @else
                    #
                @endif
            </th>
            <th>Date demande</th>
            <th>Nom et prénom</th>
            <th>Téléphone</th>
            <th>Intervention</th>
            <th>Message</th>
            <th>Statut</th>
            <th>Pays</th>
            @if (Auth::user()->email == 'fouis@box.agency')
                <th>Qualification</th>
                <th></th>
            @elseif(Auth::user()->email == 'contact@dr-djemal.com')
                <th>Qualification</th>
            @endif
            <th width="170px"></th>
            </thead>
            <tbody>
            @foreach($consultations as $key => $consultation)
                <tr>
                    <td>
                        @if (Auth::user()->email == 'fouis@box.agency')
                            <div class="input-group">
                                <label class="btn btn-danger" for="checkbox{{ $consultation->id }}">

                                    <input type="checkbox" name="checkboxDelete" class="" value="{{ $consultation->id }}"
                                        id="checkbox{{ $consultation->id }}" onClick="checkIfChecked();">
                                </label>
                            </div>
                        @endif
                    </td>
                    <td style="width: 1px;"><small>{{ $consultation->created_at->format('Y/m/d') }}</small></td>
                    <td style="width: 1px;">
                        <h5 style="margin-top: 0" class="text-info">{{ $consultation->name}}</h5>
                         <small><a class="btn btn-default" href="mailto:{{ $consultation->email }}" target="_blank" style="font-size: 11px; padding: 3px 10px">{{ $consultation->email }}</a></small>
                    </td>
                    <td>{{ $consultation->phone }}</td>
                    <td>{{ $consultation->intervention }}</td>
                    <td>
                        <div data-container="body" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ $consultation->message }}">
                            {{ str_limit($consultation->message, 30) }}
                        </div>
                    </td>
                    <td data-id="{{ $consultation->id }}">
                        @if(Auth::user()->role == 'user')
                            <span class="label label-{{ $status[$consultation->status][1] }}">{{ $status[$consultation->status][0] }}</span>
                        @else
                            <div class="btn-group">
                                <button class="btn btn-{{ $status[$consultation->status][1] }} btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ $status[$consultation->status][0] }} <small style="font-size: 10px">{{ @$consultation->date_status[$consultation->status] }}</small><span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach($status as $key => $stat)
                                        @if($key !==  $consultation->status)
                                            <li><a href="#" data-val="{{ $key }}" class="bg-{{ $stat[1] }} dropdown-change-status-item">{{ $stat[0] }} <small style="font-size: 10px">{{ @$consultation->date_status[$key] }}</small></a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </td>
                    <td>
                        @if ( $consultation->country != '' )
                            <span class="bfh-countries" data-country="{{ $consultation->country }}" data-flags="true"></span>
                        @else
                            <span>Non Renseigné</span>
                        @endif
                    </td>
                    @if (Auth::user()->email == 'fouis@box.agency')
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ $consultation->qualification != '' ? $consultation->qualification : 'Selectionnez...' }}<span class="caret"></span>
                                </button>
                                <?php
                                    $qualifications = Config::get('app.qualifications');
                                ?>
                                <ul class="dropdown-menu">
                                    @foreach($qualifications as $key => $value)
                                        @if($value !==  $consultation->qualification)
                                            <li><a href="#" data-id="{{ $consultation->id }}" data-value="{{ $value }}" class="dropdown-item dropdown-item-qualification">{{ $value }}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($consultation->hide_f_m ==  0)
                                        Selectionnez...
                                    @elseif($consultation->hide_f_m ==  1)
                                        Affiché à Morena
                                    @elseif($consultation->hide_f_m ==  2)
                                        Affiché à Nda
                                    @endif
                                </button>
                                <ul class="dropdown-menu">
                                    @if($consultation->hide_f_m !=  0)
                                        <li><a href="#" class="pass-morena" data-id="{{ $consultation->id }}" data-value="0">Hide From All</a></li>
                                    @endif
                                    @if($consultation->hide_f_m !=  1)
                                        <li><a href="#" class="pass-morena" data-id="{{ $consultation->id }}" data-value="1">Show Morena</a></li>
                                    @endif
                                    @if($consultation->hide_f_m !=  2)
                                        <li><a href="#" class="pass-morena" data-id="{{ $consultation->id }}" data-value="2">Show Nda</a></li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    @else
                        <td>{{ $consultation->qualification != '' ? $consultation->qualification : 'Non Renseigné' }}</td>
                    @endif
                    <td>
                        @if($consultation->passion_id)
                            <div class="btn-group">
                                <a href="{{ action('Admin\AdminController@consultationEdit', ['id' => $consultation->id]) }}" class="btn btn-info btn-sm">Ouvrir le dossier</a>
                                <button type="button" class="btn btn-info btn-sm"  data-toggle="popover" title="Identifiants de connexion" data-content="<b>Email:</b> {{ $consultation->email }}<br><b>Mot de passe:</b> {{ $consultation->passion->_password }}" data-placement="left" data-container="body" data-html="true"><i class="fa fa-key"></i></button>
                            </div>
                        @else
                            <button class="btn btn-success btn-sm create-patient" data-id="{{ $consultation->id }}">Créer un nouveau patient</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {{ $consultations->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
    </div>

    <script>
        function checkAny() {
            var chk_arr = document.getElementsByName("checkboxDelete");
            for( k=0; k < chk_arr.length; k++ ) {
                if(chk_arr[k].checked==true){
                    return true;
                }
            }
            return false;
        }
        function checkIfChecked() {
            var delete_button=document.getElementById('btn-delete');
            checkAny() ? delete_button.style.display="inline-block" : delete_button.style.display="none" ;
        }
    </script>
@endsection
