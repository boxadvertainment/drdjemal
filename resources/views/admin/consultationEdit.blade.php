@extends('admin/layout')
@section('content')
    <h1 class="page-header">
        Détail consultation
    </h1>
    <form action="" method="post" class="consultation-form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#demande" class="color-1" aria-controls="demande" role="tab" data-toggle="tab">Votre
                    demande</a></li>
            <li role="presentation"><a href="#antecedents" class="color-1"aria-controls="antecedents" role="tab" data-toggle="tab">Vos
                    antécédents médicaux</a></li>
            <li role="presentation"><a href="#photos" class="color-1" aria-controls="photos" role="tab" data-toggle="tab">Vos
                    Photos</a></li>
            <li role="presentation"><a href="#voyage" class="color-1" aria-controls="voyage" role="tab" data-toggle="tab">Votre
                    voyage</a></li>
            <li role="presentation"><a href="#diagnostic" class="color-2" aria-controls="diagnostic" role="tab" data-toggle="tab">Diagnostic</a></li>
            <li role="presentation"><a href="#devis" class="color-2" aria-controls="devis" role="tab" data-toggle="tab">Votre devis</a></li>
            <li role="presentation"><a href="#planning" class="color-2" aria-controls="devis" role="tab" data-toggle="tab">Planning</a></li>
            <li role="presentation"><a href="#post-operatoire" class="color-2" aria-controls="photos" role="tab" data-toggle="tab">Post opératoire</a></li>
            <li role="presentation"><a href="#message" class="color-2" aria-controls="photos" role="tab" data-toggle="tab">Message / Infos</a></li>
        </ul>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="demande">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Nom et prénom:</label>
                                        <input type="text" id="name" name="name" class="form-control" value="{{ $consultation->name }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Age:</label>
                                        <input type="number" id="age" name="age" class="form-control" value="{{ @$dossier['age'] }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nationality">Nationalité:</label>
                                        <input type="text" id="nationality" class="form-control" name="nationality" value="{{ @$dossier['nationality'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="country">Pays:</label>
                                        <input type="text" id="country" class="form-control" name="country" value="{{ @$dossier['country'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Tél. Mobile:</label>
                                        <input type="text" id="phone" class="form-control" name="phone" value="{{ $consultation->phone }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="email" id="email" class="form-control" name="email" value="{{ $consultation->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="viber">Viber</label>
                                        <input type="text" id="viber" class="form-control" name="viber" value="{{ @$dossier['viber'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="whatsapp">WhatsApp</label>
                                        <input type="text" id="whatsapp" class="form-control" name="whatsapp" value="{{ @$dossier['whatsapp'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="skype">Skype</label>
                                        <input type="text" id="skype" class="form-control" name="skype" value="{{ @$dossier['skype'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="reason">Raisons de la demande d'interventions:</label>
                                      <textarea name="reason" id="reason" cols="30" rows="4"
                                                class="form-control">{{ $consultation->message }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="timeOperation">Depuis combien de temps avez vous décidé de vous faire
                                    opérer ?</label>
                                <input type="text" id="timeOperation" class="form-control" name="timeOperation" value="{{ @$dossier['timeOperation'] }}">
                            </div>
                            <div class="form-group">
                                <label for="consulteChirurgien">Avez vous déjà consulté un chirurgien esthétique
                                    ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="consulteChirurgien" value="Oui" {{ (@$dossier['consulteChirurgien'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="consulteChirurgien" value="Non" {{ (@$dossier['consulteChirurgien'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="proposeIntervention">Si oui, que vous a-t-il proposé comme intervention ?</label>
                                <textarea name="proposeIntervention" id="proposeIntervention" cols="30" rows="4" class="form-control">{{ @$dossier['proposeIntervention'] }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="choixTraitement1">Premier choix de traitement:</label>
                                        <select name="intervention" class="form-control">
                                            <option value="">Choisissez un traitement</option>
                                            @foreach(Config::get('app.interventions') as $label => $group)
                                                <optgroup label="{{ $label }}">
                                                    @foreach($group as $item => $intervention)
                                                        <option value="{{ $item }}" @if($item == $consultation->intervention) selected @endif>{{ $intervention }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="choixTraitement2">Deuxième choix de traitement:</label>
                                        <select name="choixTraitement2" class="form-control">
                                            <option value="" selected>Choisissez un traitement</option>
                                            @foreach(Config::get('app.interventions') as $label => $group)
                                                <optgroup label="{{ $label }}">
                                                    @foreach($group as $item => $intervention)
                                                        <option value="{{ $item }}" @if($item == $dossier['choixTraitement2']) selected @endif>{{ $intervention }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="autreTraitement">Autre traitement:</label>
                                        <input type="text" id="autreTraitement" class="form-control" name="autreTraitement" value="{{ @$dossier['autreTraitement'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">Remarques:</label>
                                      <textarea name="remarques" id="remarques" cols="30" rows="4"
                                                class="form-control">{{ $dossier['remarques'] }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="antecedents">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="poids">Quel est votre poids actuel ?</label>
                                        <input type="text" id="poids" class="form-control" name="poids" value="{{ @$dossier['poids'] }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="poidsMax">Quel est le poids maximum que vous avez pesé ?</label>
                                        <input type="text" id="poidsMax" class="form-control" name="poidsMax" value="{{ @$dossier['poidsMax'] }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="taille">Quelle est votre taille ?</label>
                                        <input type="text" id="taille" class="form-control" name="taille" value="{{ @$dossier['taille'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tailleVetHaut">Quelle est la taille de vêtements en haut ?</label>
                                        <input type="text" id="tailleVetHaut" class="form-control" name="tailleVetHaut" value="{{ @$dossier['tailleVetHaut'] }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tailleVetBas">Quelle est la taille de vêtements en bas ?</label>
                                        <input type="text" id="tailleVetBas" class="form-control" name="tailleVetBas" value="{{ @$dossier['tailleVetBas'] }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tailleSoutien">Quelle est la taille de soutien gorge (Pour intervention
                                            sur les seins) ?</label>
                                        <input type="text" id="tailleSoutien" class="form-control" name="tailleSoutien" value="{{ @$dossier['tailleSoutien'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="consulteChirurgien">Consommez-vous ou avez vous consommé du tabac
                                    ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationTabac" value="Oui" {{ (@$dossier['consommationTabac'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationTabac" value="Non" {{ (@$dossier['consommationTabac'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="depuisAge">et depuis quel âge?</label>
                                <input type="text" id="depuisAge" class="form-control" name="depuisAge" value="{{ @$dossier['depuisAge'] }}">
                            </div>
                            <div class="form-group">
                                <label>Avez vous arrêté de fumer ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="arreterFumer" value="Oui" {{ (@$dossier['arreterFumer'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="arreterFumer" value="Non" {{ (@$dossier['arreterFumer'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Consommez vous de l'alcool ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationAlcool" value="Oui" {{ (@$dossier['consommationAlcool'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationAlcool" value="Non" {{ (@$dossier['consommationAlcool'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Prenez vous des médicaments ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="prisMedicament" value="Oui" {{ (@$dossier['prisMedicament'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="prisMedicament" value="Non" {{ (@$dossier['prisMedicament'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbGrossesses">Combien de grossesses avez vous eues ?</label>
                                        <select name="nbGrossesses" class="form-control">
                                            @for($i=0; $i<6; $i++)
                                                <option value="{{ $i }}" @if( @$dossier['nbGrossesses'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbEnfants">Combien d'enfants avez vous eus ?</label>
                                        <select name="nbEnfants" id="nbEnfants" class="form-control">
                                            @for($i=0; $i<6; $i++)
                                                <option value="{{ $i }}" @if( @$dossier['nbEnfants'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbCesarienne">Combien de césarienne(s) avez vous eu ?</label>
                                        <select name="nbCesarienne" id="nbCesarienne" class="form-control">
                                            @for($i=0; $i<6; $i++)
                                                <option value="{{ $i }}" @if( @$dossier['nbCesarienne'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Désirez vous encore de nouvelles grossesses ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="nouvelleGrossesses" value="Oui" {{ (@$dossier['nouvelleGrossesses'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="nouvelleGrossesses" value="Non" {{ (@$dossier['nouvelleGrossesses'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="nouvelleGrossesses" value="unknown" {{ (@$dossier['nouvelleGrossesses'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà présenté un cancer du sein ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="cancerSein" value="Oui" {{ (@$dossier['cancerSein'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cancerSein" value="Non" {{ (@$dossier['cancerSein'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cancerSein" value="unknown" {{ (@$dossier['cancerSein'] == 'unknown') ? 'checked': '' }}> Ne
                                        sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous des antécédents familiaux de cancer du sein chez votre mère, vos
                                    sours, votre grand-mère maternelle ou vos tantes maternelles ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="antecedentsFamiliaux" value="Oui" {{ (@$dossier['antecedentsFamiliaux'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="antecedentsFamiliaux" value="Non" {{ (@$dossier['antecedentsFamiliaux'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="antecedentsFamiliaux" value="unknown" {{ (@$dossier['antecedentsFamiliaux'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà pratiqué une mammographie ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="pratiqueMammographie" value="Oui" {{ (@$dossier['pratiqueMammographie'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="pratiqueMammographie" value="Non" {{ (@$dossier['pratiqueMammographie'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Le résultat est-il normal ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="resultatNormal" value="Oui" {{ (@$dossier['resultatNormal'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="resultatNormal" value="Non" {{ (@$dossier['resultatNormal'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Etes vous allergique ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="allergique" value="Oui" {{ (@$dossier['allergique'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergique" value="Non" {{ (@$dossier['allergique'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergique" value="unknown" {{ (@$dossier['allergique'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>si oui, êtes vous allergique à un médicament ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueMedicament" value="Oui" {{ (@$dossier['allergiqueMedicament'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueMedicament" value="Non" {{ (@$dossier['allergiqueMedicament'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">si oui, lequel(s) ?</label>
                                      <textarea name="medicamentAllergique" id="medicamentAllergique" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['medicamentAllergique'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Etes vous allergique au latex ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueLatex" value="Oui" {{ (@$dossier['allergiqueLatex'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueLatex" value="Non" {{ (@$dossier['allergiqueLatex'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueLatex" value="unknown" {{ (@$dossier['allergiqueLatex'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">Autres allergies (merci de préciser)</label>
                                      <textarea name="autresAllergies" id="autresAllergies" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['autresAllergies'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Souffrez vous d 'hypertension artérielle ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertensionArterielle" value="Oui" {{ (@$dossier['hypertensionArterielle'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertensionArterielle" value="Non" {{ (@$dossier['hypertensionArterielle'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertensionArterielle" value="unknown" {{ (@$dossier['hypertensionArterielle'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Souffrez vous de diabète ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="diabete" value="Oui" {{ (@$dossier['diabete'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="diabete" value="Non" {{ (@$dossier['diabete'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="diabete" value="unknown" {{ (@$dossier['diabete'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Souffrez vous du cholestérol ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="cholesterol" value="Oui" {{ (@$dossier['cholesterol'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cholesterol" value="Non" {{ (@$dossier['cholesterol'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cholesterol" value="unknown" {{ (@$dossier['cholesterol'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà présenté un caillot dans une veine de la jambe (phlébite)
                                    ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="phlebite" value="Oui" {{ (@$dossier['phlebite'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="phlebite" value="Non" {{ (@$dossier['phlebite'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="phlebite" value="unknown" {{ (@$dossier['phlebite'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà fait une dépression ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="depression" value="Oui" {{ (@$dossier['depression'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="depression" value="Non" {{ (@$dossier['depression'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>si oui, êtes vous actuellement sous antidépresseur ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="antidepresseur" value="Oui" {{ (@$dossier['antidepresseur'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="antidepresseur" value="Non" {{ (@$dossier['antidepresseur'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Etes vous atteint(e) de maladies virales (HIV, Hépatite) ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="maladiesVirales" value="Oui" {{ (@$dossier['maladiesVirales'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="maladiesVirales" value="Non" {{ (@$dossier['maladiesVirales'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="maladiesVirales" value="unknown" {{ (@$dossier['maladiesVirales'] == 'unknown') ? 'checked': '' }}> Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">si oui, lesquelles ?</label>
                                      <textarea name="maladiesViralesRemarques" id="maladiesViralesRemarques" cols="30"
                                                rows="4"
                                                class="form-control">{{ @$dossier['maladiesViralesRemarques'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="autresAntecedents">Autres antécédents médicaux (Merci de préciser)</label>
                                      <textarea name="autresAntecedents" id="autresAntecedents" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['autresAntecedents'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà eu des interventions chirurgicales</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsChirurgicales" value="Oui" {{ (@$dossier['interventionsChirurgicales'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsChirurgicales" value="Non" {{ (@$dossier['interventionsChirurgicales'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="interventionsChirurgicalesRemarques">si oui, lesquelles ?</label>
                                      <textarea name="interventionsChirurgicalesRemarques"
                                                id="interventionsChirurgicalesRemarques" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['interventionsChirurgicalesRemarques'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà eu des interventions de chirurgie esthétique ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsEsthetique" value="Oui" {{ (@$dossier['interventionsEsthetique'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsEsthetique" value="Non" {{ (@$dossier['interventionsEsthetique'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="interventionsEsthetiqueRemarques">si oui, lesquelles ?</label>
                                      <textarea name="interventionsEsthetiqueRemarques"
                                                id="interventionsEsthetiqueRemarques" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['interventionsEsthetiqueRemarques'] }}</textarea>
                            </div>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="photos">
                        <div class="panel-body">
                            @for($i=0; $i<3; $i++)
                                <div class="row">
                                    @for($j=1; $j<4; $j++)
                                        <div class="col-md-4">
                                            <p><img class="img-thumbnail" style="height: 150px;" src="{{  @$dossier['image' . ( $i * 3 + $j)]  ? route('getPhoto', ['filename' => $dossier['image' . ( $i * 3 + $j)]]) : "data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%20viewBox%3D'0%200%201400%20933'%3E%3Cdefs%3E%3Csymbol%20id%3D'a'%20viewBox%3D'0%200%2090%2066'%20opacity%3D'0.3'%3E%3Cpath%20d%3D'M85%205v56H5V5h80m5-5H0v66h90V0z'%2F%3E%3Ccircle%20cx%3D'18'%20cy%3D'20'%20r%3D'6'%2F%3E%3Cpath%20d%3D'M56%2014L37%2039l-8-6-17%2023h67z'%2F%3E%3C%2Fsymbol%3E%3C%2Fdefs%3E%3Cuse%20xlink%3Ahref%3D'%23a'%20width%3D'20%25'%20x%3D'40%25'%2F%3E%3C%2Fsvg%3E" }}"></p>
                                            <div class="form-group">
                                                <input type="file" name="image{{ ( $i * 3) + $j }}">
                                                <input type="hidden" name="oldImage{{ ( $i * 3) + $j }}" value="{{  @$dossier['image' . ( $i * 3 + $j)] }}">
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            @endfor
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="voyage">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="formuleVoyage">Hôtel:</label>
                                <select name="formuleVoyage" id="formuleVoyage" class="form-control">
                                    <option value="Hotel Myron" @if(@$dossier['formuleVoyage'] == 'Hotel Myron') selected @endif>Hotel Myron</option>
                                    <option value="Hotel El Mouradi" @if(@$dossier['formuleVoyage'] == 'Hotel El Mouradi') selected @endif>Hotel El Mouradi</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Avez vous un passeport en cours de validité ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="passeport" value="Oui" @if(@$dossier['passeport'] == 'Oui') checked @endif> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="passeport" value="Non" @if(@$dossier['passeport'] == 'Non') checked @endif> Non
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="arrangement">Arrangement</label>
                                        <select name="arrangement" id="arrangement" class="form-control">
                                            <option value="pc" @if(@$dossier['arrangement'] == 'pc') selected @endif>Pension Compléte</option>
                                            <option value="dp" @if(@$dossier['arrangement'] == 'dp') selected @endif>Demi pension</option>
                                            <option value="lpd" @if(@$dossier['arrangement'] == 'lpd') selected @endif>Lit et petit déjeuner</option>
                                        </select>
                                        <p class="help-block">(la pension n’inclut pas les dépenses personnelles telles que
                                            téléphone,
                                            boissons...)</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="typeChambre">Type de chambre</label>
                                        <select name="typeChambre" id="typeChambre" class="form-control">
                                            <option value="single" @if(@$dossier['typeChambre'] == 'single') selected @endif>Single</option>
                                            <option value="double" @if(@$dossier['typeChambre'] == 'double') selected @endif>Double</option>
                                            <option value="triple" @if(@$dossier['typeChambre'] == 'triple') selected @endif>Triple</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date d'arrivée :</label>
                                        <input type="date" class="form-control" name="dateArrivee" value="{{ @$dossier['dateArrivee'] }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date de départ :</label>
                                        <input type="date" class="form-control" name="dateDepart" value="{{ @$dossier['dateDepart'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbAdultes">Nombre d'adultes :</label>
                                        <select name="nbAdultes" class="form-control">
                                            @for($i=0; $i<11; $i++)
                                                <option value="{{ $i }}" @if( @$dossier['nbAdultes'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbBebes">Nombre de Bébés de 0 à 2 ans :</label>
                                        <select name="nbBebes" class="form-control">
                                            @for($i=0; $i<11; $i++)
                                                <option value="{{ $i }}" @if( @$dossier['nbBebes'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbEnfants">Nombre d'enfants de 2 à 12 ans :</label>
                                        <select name="nbEnfants" class="form-control">
                                            @for($i=0; $i<11; $i++)
                                                <option value="{{ $i }}" @if( @$dossier['nbEnfants'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><strong>Assistance :</strong></p>
                                    <p>Nous vous prenons en charge depuis votre arrivée à l'aéroport et jusqu'à votre départ:</p>
                                    <ul>
                                        <li>Accueil</li>
                                        <li>Transferts Hôtel/Clinique et Hôtel/Aéroport</li>
                                        <li>Rendez-vous médicaux</li>
                                    </ul>
                                    <br>
                                </div>
                            </div>

                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Vos vols</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <table border="0" cellpadding="2" cellspacing="1" class="table table-bordered">
                                                <tbody><tr valign="top">
                                                    <td colspan="4" class="info"><b>Vol aller</b></td>
                                                </tr>
                                                <tr align="center" valign="top" class="fdl2">
                                                    <td>Date</td>
                                                    <td>Aéroport</td>
                                                    <td>Numéro de Vol</td>
                                                    <td>Heure d'Arrivée&nbsp;</td>
                                                </tr>
                                                <tr valign="center" class="fdc">
                                                    <td>{{ @$dossier['dateConseilleSejour1'] }}</td>
                                                    <td>														<input name="aeroport_depart" type="text" size="20" maxlength="100" value="{{ @$dossier['aeroport_depart'] }}">
                                                    </td>
                                                    <td>														<input name="num_vol_depart" type="text" size="20" maxlength="100" value="{{ @$dossier['num_vol_depart'] }}">
                                                    </td>
                                                    <td align="center">														<input name="heure_depart" type="text" size="6" maxlength="10" value="{{ @$dossier['heure_depart'] }}">
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                            <br>
                                            <table border="0" cellpadding="2" cellspacing="1" class="table table-bordered">
                                                <tbody><tr valign="top" class="info">
                                                    <td colspan="4" class="fdl1"><b>Vol retour</b></td>
                                                </tr>
                                                <tr align="center" valign="top" class="fdl2">
                                                    <td>Date</td>
                                                    <td>Aéroport</td>
                                                    <td>Numéro de Vol</td>
                                                    <td>Heure du Départ</td>
                                                </tr>
                                                <tr valign="center" class="fdc">
                                                    <td>{{ @$dossier['dateConseilleSejour2'] }}</td>
                                                    <td>														<input name="aeroport_arrivee" type="text" size="20" maxlength="100" value="{{ @$dossier['aeroport_arrivee'] }}">
                                                    </td>
                                                    <td>														<input name="num_vol_arrivee" type="text" size="20" maxlength="100" value="{{ @$dossier['num_vol_arrivee'] }}">
                                                    </td>
                                                    <td align="center">														<input name="heure_arrivee" type="text" size="6" maxlength="10" value="{{ @$dossier['heure_arrivee'] }}">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-5"></div>
                                    </div>

                                    <p><b>Formalités à la frontière Tunisienne</b><br>
                                        Veuillez nous consulter pour vous indiquer si vous pouvez entrer en Tunisie avec votre carte d’identité ou votre passeport et si vous avez besoin ou non d’un visa.<br>
                                        A partir du 1er octobre 2014, les étrangers non-résidents en Tunisie (touristes en particulier) doivent s’acquitter d’une taxe de 30 dinars (environ 13,10 Euros) lors de leur sortie du territoire tunisien.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="diagnostic">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="chirurgienTraitant">Chirurgien traitant:</label>
                                        <input type="text" id="chirurgienTraitant" name="chirurgienTraitant" class="form-control" value="{{ @$dossier['chirurgienTraitant'] }}" @if(Auth::user()->role == 'user') readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="interventionProposes">Intervention proposée:</label>
                                        <textarea class="form-control" rows="4" name="interventionProposes" @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['interventionProposes'] }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="typeAnesthesie">Type d'anesthésie:</label>
                                        <select name="typeAnesthesie" class="form-control">
                                            <option value="Générale" @if(@$dossier['typeAnesthesie'] == 'Générale') selected @endif>Générale</option>
                                            <option value="Locale" @if(@$dossier['typeAnesthesie'] == 'Locale') selected @endif>Locale</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dureeIntervention">Durée d'Intervention:</label>
                                        <input type="text" id="dureeIntervention" class="form-control" name="dureeIntervention" value="{{ @$dossier['dureeIntervention'] }}" @if(Auth::user()->role == 'user') readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dureeHospitalisation">Durée d'Hospitalisation:</label>
                                        <input type="text" id="dureeHospitalisation" class="form-control" name="dureeHospitalisation" value="{{ @$dossier['dureeHospitalisation'] }}" @if(Auth::user()->role == 'user') readonly @endif>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dureeSejour">Durée de séjour post opératoire en Tunisie:</label>
                                        <input type="text" id="dureeSejour" class="form-control" name="dureeSejour" value="{{ @$dossier['dureeSejour'] }}" @if(Auth::user()->role == 'user') readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dureeArretTravail">Durée d'arrêt de travail (depuis l'intervention):</label>
                                        <input type="text" id="dureeArretTravail" class="form-control" name="dureeArretTravail" value="{{ @$dossier['dureeArretTravail'] }}" @if(Auth::user()->role == 'user') readonly @endif>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dateProposeeIntervention">Date proposée pour l'intervention:</label>
                                        <input type="date" id="dateProposeeIntervention" class="form-control" name="dateProposeeIntervention" value="{{ @$dossier['dateProposeeIntervention'] }}" @if(Auth::user()->role == 'user') readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="diagnosticChirurgien">Diagnostic du chirurgien:</label>
                                        <textarea id="diagnosticChirurgien" class="form-control" rows="40" name="diagnosticChirurgien" @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['diagnosticChirurgien'] }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">CV {{ @$dossier['chirurgienTraitant'] }}</h3>
                            </div>
                            <div class="panel-body">
                                <div @if(Auth::user()->role != 'user') class="summernote" @endif>
                                    @if(@$dossier['cvContent'] == '')
                                        <img src="/img/dr-image.png" alt="" class="img-thumbnail img-circle pull-left" style="margin-right: 20px;
">
                                        <div class="description">
                                            <p>Chirurgien spécialiste en chirurgie plastique reconstructrice et esthétique.&nbsp;</p>
                                            <p>Président de l’association des chirurgiens plastiques de Tunisie et membre de la société internationale des chirurgiens plastiques&nbsp;: ISAPS. <br>
                                                Il donne régulièrement des conférences dans les congrès nationaux et internationaux.</p>
                                            <p>Dr Djemal est inscrit à <b>l'ordre des médecins</b> <a href="http://www.ordre-medecins.org.tn" target="_blank">www.ordre-medecins.org.tn</a>, sous <b> le numéro :  5985</b>
                                            </p>
                                            <p>Depuis novembre, Le Dr Djemal exerce dans la toute nouvelle clinique privée multidisciplinaire MYRON située au Lac II à 15 minutes de l’aéroport Tunis Carthage.</p>
                                        </div>
                                    @else
                                        {!! @$dossier['cvContent'] !!}
                                    @endif
                                </div>
                                <input type="hidden" name="cvContent" value="{{ @$dossier['cvContent'] }}">
                            </div>
                        </div>
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">Photos de références par le chirurgien</h3>
                            </div>
                            <div class="panel-body">
                                @for($i=0; $i<3; $i++)
                                    <div class="row">
                                        @for($j=1; $j<4; $j++)
                                            <div class="col-md-4">
                                                <p><img class="img-thumbnail" style="height: 150px;" src="{{  @$dossier['imageRef' . ( $i * 3 + $j)]  ? route('getPhoto', ['filename' => $dossier['imageRef' . ( $i * 3 + $j)]]) : "data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%20viewBox%3D'0%200%201400%20933'%3E%3Cdefs%3E%3Csymbol%20id%3D'a'%20viewBox%3D'0%200%2090%2066'%20opacity%3D'0.3'%3E%3Cpath%20d%3D'M85%205v56H5V5h80m5-5H0v66h90V0z'%2F%3E%3Ccircle%20cx%3D'18'%20cy%3D'20'%20r%3D'6'%2F%3E%3Cpath%20d%3D'M56%2014L37%2039l-8-6-17%2023h67z'%2F%3E%3C%2Fsymbol%3E%3C%2Fdefs%3E%3Cuse%20xlink%3Ahref%3D'%23a'%20width%3D'20%25'%20x%3D'40%25'%2F%3E%3C%2Fsvg%3E" }}"></p>
                                                <div class="form-group">
                                                    @if(Auth::user()->role != 'user')
                                                    <input type="file" name="imageRef{{ ( $i * 3) + $j }}">
                                                    @endif
                                                    <input type="hidden" name="oldImageRef{{ ( $i * 3) + $j }}" value="{{  @$dossier['imageRef' . ( $i * 3 + $j)] }}">
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">Examens médicaux avant de venir en Tunisie</h3>
                            </div>
                            <div class="panel-body">
                                <div @if(Auth::user()->role != 'user') class="summernote" @endif>
                                    @if(@$dossier['examensMedicaux'] == '')
                                        <p>Un bilan préopératoire complet doit être effectué afin de s'assurer de la faisabilité de l'intervention. Il comprend :</p>
                                        <p><strong>Pour tout type d’intervention :</strong></p>
                                        <ul>
                                            <li>Bilan biologique (groupe sanguin, NFS, Plaquettes, TP, TCA),</li>
                                            <li>Bilan virologique HIV, Hépatite B et C.</li>
                                            <li>Bilan cardiologique comprenant un examen cardio-vasculaire avec un électrocardiogramme</li>
                                            <li>Radiographie pulmonaire (de moins de deux ans) si vous avez plus de 50 ans ou si vous êtes fumeur.</li>
                                            <li>Bilan récent de maladie en cas de maladie chronique (exemple diabète, cholestérol, etc)  </li>
                                        </ul>

                                        <p><strong>Pour une chirurgie mammaire :</strong></p>
                                        <p>Mammographie (de moins de 2 ans) en plus des examens cités ci-dessus. </p>
                                        <p>Le coût de ce bilan n’est pas inclus dans votre devis.</p>

                                        <p align="center"><strong>N’oubliez surtout pas</strong></p>
                                        <ul>
                                            <li>d’apporter avec vous l’original des rapports demandés,</li>
                                            <li>de ne pas prendre de médicament contenant de l’aspirine pendant les 15 jours qui précèdent votre intervention (risque de saignement pendant et après l'intervention), </li>
                                            <li>de rester à jeun au minimum 8 heures avant votre intervention (ne rien manger, boire ou fumer).</li>
                                        </ul>
                                    @else
                                        {!! @$dossier['examensMedicaux'] !!}
                                    @endif
                                </div>
                                <input type="hidden" name="examensMedicaux" value="{{ @$dossier['examensMedicaux'] }}">
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="post-operatoire">
                        <div class="panel-body">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Photos avant intervention</h3>
                                </div>
                                <div class="panel-body">
                                    @for($i=0; $i<3; $i++)
                                        <div class="row">
                                            @for($j=1; $j<4; $j++)
                                                <div class="col-md-4">
                                                    <p><img class="img-thumbnail" style="height: 150px;" src="{{  @$dossier['imagePO' . ( $i * 3 + $j)]  ? route('getPhoto', ['filename' => $dossier['imagePO' . ( $i * 3 + $j)]]) : "data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%20viewBox%3D'0%200%201400%20933'%3E%3Cdefs%3E%3Csymbol%20id%3D'a'%20viewBox%3D'0%200%2090%2066'%20opacity%3D'0.3'%3E%3Cpath%20d%3D'M85%205v56H5V5h80m5-5H0v66h90V0z'%2F%3E%3Ccircle%20cx%3D'18'%20cy%3D'20'%20r%3D'6'%2F%3E%3Cpath%20d%3D'M56%2014L37%2039l-8-6-17%2023h67z'%2F%3E%3C%2Fsymbol%3E%3C%2Fdefs%3E%3Cuse%20xlink%3Ahref%3D'%23a'%20width%3D'20%25'%20x%3D'40%25'%2F%3E%3C%2Fsvg%3E" }}"></p>
                                                    <div class="form-group">
                                                        @if(Auth::user()->role != 'user')
                                                        <input type="file" name="imagePO{{ ( $i * 3) + $j }}">
                                                        @endif
                                                        <input type="hidden" name="oldImagePO{{ ( $i * 3) + $j }}" value="{{  @$dossier['imagePO' . ( $i * 3 + $j)] }}">
                                                    </div>
                                                </div>
                                            @endfor
                                        </div>
                                    @endfor
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="compteRendu">Compte rendu:</label>
                                            <textarea name="compteRendu" id="compteRendu" cols="30" rows="6"
                                                class="form-control" @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['compteRendu'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="conseil">Conseil:</label>
                                            <textarea name="conseil" id="conseil" cols="30" rows="6" class="form-control" @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['conseil'] }}</textarea>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="devis">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">Votre devis</h3>
                            </div>
                            <div class="panel-body">
                                <table class="devis-table table table-bordered">
                                    <tbody>
                                    <tr class="fdc form-inline">
                                        <td>Montant Intervention
                                            <input type="text" name="interventionDesc" class="form-control" @if(Auth::user()->role == 'user') readonly @endif value="{{ @$dossier['interventionDesc'] }}" placeholder="Intitulé" size="40">
                                        </td>
                                        <td align="right">
                                            @if(Auth::user()->role == 'user')
                                                <span class="money">{{ @$dossier['montantVols'] }}</span>
                                                <input type="hidden" name="montantVols" value="{{ @$dossier['montantVols'] }}">
                                            @else
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantVols" id="montantVols" value="{{ @$dossier['montantVols'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @if(@$dossier['montantInterv2'] != '' || Auth::user()->role != 'user')
                                    <tr class="fdc form-inline">
                                        <td>Montant Intervention
                                            <input type="text" name="interventionDesc2" class="form-control" @if(Auth::user()->role == 'user') readonly @endif value="{{ @$dossier['interventionDesc2'] }}" placeholder="Intitulé" size="40">
                                        </td>
                                        <td align="right" class="">
                                            @if(Auth::user()->role == 'user')
                                                <span class="money">{{ @$dossier['montantInterv2'] }}</span>
                                                <input type="hidden" name="montantInterv2" value="{{ @$dossier['montantInterv2'] }}">
                                            @else
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantInterv2" id="montantInterv2" value="{{ @$dossier['montantInterv2'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if(@$dossier['montantInterv2'] != '' || Auth::user()->role != 'user')
                                    <tr class="fdc form-inline">
                                        <td>Montant Intervention
                                            <input type="text" name="interventionDesc3" class="form-control" @if(Auth::user()->role == 'user') readonly @endif value="{{ @$dossier['interventionDesc3'] }}" placeholder="Intitulé" size="40">
                                        </td>
                                        <td align="right" class="">
                                            @if(Auth::user()->role == 'user')
                                                <span class="money">{{ @$dossier['montantInterv3'] }}</span>
                                                <input type="hidden" name="montantInterv3" value="{{ @$dossier['montantInterv3'] }}">
                                            @else
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantInterv3" id="montantInterv3" value="{{ @$dossier['montantInterv3'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="fdc">
                                        <td>Tarifs du séjour HOTEL en PC (nombre de nuits x montant de la nuitée)</td>
                                        <td align="right" class="form-inline">
                                            @if(Auth::user()->role == 'user')
                                                <span class="nuit">{{ @$dossier['nombreNuits'] }}</span>
                                                <input type="hidden" name="nombreNuits" value="{{ @$dossier['nombreNuits'] }}">
                                                x
                                                <span class="money">{{ @$dossier['montantHorsVols'] }}</span>
                                                <input type="hidden" name="montantHorsVols" value="{{ @$dossier['montantHorsVols'] }}">
                                            @else
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="nombreNuits" id="nombreNuits" value="{{ @$dossier['nombreNuits'] }}">
                                                    <div class="input-group-addon">nuit(s)</div>
                                                </div>
                                                x
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantHorsVols" id="montantHorsVols" value="{{ @$dossier['montantHorsVols'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class="fdc">
                                        <td>Tarif séjour accompagnement HOTEL en PC (nombre de nuits x montant de la nuitée)</td>
                                        <td align="right" class="form-inline">
                                            @if(Auth::user()->role == 'user')
                                                <span class="nuit">{{ @$dossier['nombreNuitsAC'] }}</span>
                                                <input type="hidden" name="nombreNuitsAC" value="{{ @$dossier['nombreNuitsAC'] }}">
                                                x
                                                <span class="money">{{ @$dossier['montantHorsVolsAC'] }}</span>
                                                <input type="hidden" name="montantHorsVolsAC" value="{{ @$dossier['montantHorsVolsAC'] }}">
                                            @else
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="nombreNuitsAC" id="nombreNuitsAC" value="{{ @$dossier['nombreNuitsAC'] }}">
                                                    <div class="input-group-addon">nuit(s)</div>
                                                </div>
                                                x
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantHorsVolsAC" id="montantHorsVolsAC" value="{{ @$dossier['montantHorsVolsAC'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class="fdc">
                                        <td><b>Total</b></td>
                                        <td align="right" id="totalDevis"><b></b>
                                            <input type="hidden" name="totalDevis" value="{{ @$dossier['totalDevis'] }}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">Modalités de paiement</h3>
                            </div>
                            <div class="panel-body">
                                <div @if(Auth::user()->role != 'user') class="summernote" @endif>
                                    @if(@$dossier['modalitePaiement'] == '')
                                        <p><b>Est inclus dans votre devis :</b></p>
                                        <p><ul>
                                            <li>L’intervention chirurgicale, l’anesthésie, l’utilisation du bloc opératoire, les frais hospitaliers et médicaments.</li>
                                            <li>Les honoraires du chirurgien et de l’anesthésiste</li>
                                            <li>Les consultations pré et post opératoires</li>
                                            <li>L’hospitalisation de 1 nuit à la clinique</li>
                                            <li>Les prothèses en Gel Silicone et le soutien gorge spécial pour les interventions sur la poitrine</li>
                                            <li>La gaine de contention pour la lipoaspiration et la plastie abdominale</li>
                                            <li>Les injections post opératoires si besoin</li>
                                            <li>Les transferts médicaux</li>
                                            <li>Le séjour en pension complète dans un <b>hôtel</b></li>
                                            <li>Réduction pour deux ou plusieurs interventions.</li>
                                        </ul></p>
                                        <p><b>N’est pas inclus dans votre devis:</b></p>
                                        <p><ul>
                                            <li>Le billet d’avion et l’assurance voyage</li>
                                            <li>Les frais personnels (boissons, visites, extras...) engagés sur place.</li>
                                        </ul></p>
                                        <p><b>PAIEMENT  de votre intervention et séjour</b></p>
                                        <p>Le paiement de l’intervention se fera le lendemain de votre arrivée après la consultation avec le Chirurgien.<br>
                                            Paiement en espèces (euros) ou en traveller’s chèques. Si vous optez pour les traveller’s chèques, n’oubliez pas le bordereau de change délivré par la banque.</p>
                                        <p><b>Montant des billets d’avion</b></p>
                                        <p>Il devra être réglé dans votre pays directement à l’agence de voyages ou site web de votre choix.</p>
                                    @else
                                        {!! @$dossier['modalitePaiement'] !!}
                                    @endif
                                </div>
                                <input type="hidden" name="modalitePaiement" value="{{ @$dossier['modalitePaiement'] }}">
                                <p><b>Validité de l’offre</b></p>
                                <p>Six mois à compter du <input type="date" name="datePaiement" @if(Auth::user()->role == 'user') readonly @endif value="{{ @$dossier['datePaiement'] }}"></p>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="planning">
                        <div class="panel-body">
                            <div class="row">
                                @if(Auth::user()->role != 'user')
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="file" name="planning">
                                            <input type="hidden" name="oldPlanning" value="{{  @$dossier['planning'] }}">
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="oldPlanning" value="{{  @$dossier['planning'] }}">
                                @endif
                                <div class="{{ (Auth::user()->role == 'user') ? 'col-md-12' : 'col-md-8'  }}">
                                    @if(@$dossier['planning'] != '')
                                        <iframe src="{{ route('getPhoto', ['filename' => $dossier['planning']]) }}" width="100%" height="600" align="middle"></iframe>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="message">
                        <div class="panel-body">
                            <div class="form-group">
                                <textarea name="messageInfo" id="messageInfo" rows="6" class="form-control" @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['messageInfo'] }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Enregistrer</button>
                <a href="{{ action('Admin\AdminController@index') }}" class="btn btn-default">Annuler</a>
            </div>
        </div>
    </form>

@endsection
