(function ($) {
    "use strict";

    if (APP_ENV == 'production') {
        console.log = function () {
        };
    }

    $('.loader').addClass('hide');

    function showLoader() {
        $('.loader').removeClass('hide');
        $('body').css('overflow', 'hidden');
    }

    function hideLoader() {
        $('.loader').addClass('hide');
        $('body').css('overflow', 'visible');
    }

    function showPage(selector) {
        $(selector).removeClass('hide').siblings().addClass('hide');
        facebookUtils.resizeCanvas();
    }

    var input = document.querySelector("#phone");
    var iti1;
    var iti2;
    if (input) {
        iti1 = intlTelInput(input, {
            initialCountry: "auto",
            separateDialCode: true
        });
    }

    $(document).on('show.bs.modal', '.modal', function () {
        console.log('entered');
        iti1.destroy();
        var input = document.querySelector("#phone2");
        iti2 = intlTelInput(input, {
            initialCountry: "auto",
            separateDialCode: true,
        });
    });

    
    $('select').selectpicker();
    autosize($('textarea'));


    $(document).scroll(function () {
        var scroll = $(this).scrollTop();
        var topDist = $(".main").position();
        if (scroll > topDist.top) {
            $('nav.navbar').addClass('fixed');
        } else {
            $('nav.navbar').removeClass('fixed');
        }
    });


    const players = Array.from(document.querySelectorAll('.player'));
    players.map(player => new Plyr(player));

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function (jqXHR) {
            hideLoader();

            if (/<html>/i.test(jqXHR.responseText))
                sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');
            else
                sweetAlert({
                    title: "Oups!",
                    text: jqXHR.responseText,
                    type: "error",
                    html: true
                });
        }
    });

    function hasHtml5Validation() {
        return typeof document.createElement('input').checkValidity === 'function';
    }

    

    if (hasHtml5Validation()) {
        // $('#consultation-form, #consultation-form-banner').submit(function (e) {
        //     e.preventDefault();
        //     if (this.checkValidity()) {

        //         if (typeof iti1 !== 'undefined') {
        //             $(this).find('input.phone-number').val(iti1.getNumber());
        //         }

        //         if (typeof iti2 !== 'undefined') {
        //             $(this).find('input.in-modal').val(iti2.getNumber());
        //         }
                
        //         if ($(this).hasClass('disabled')) return false;
        //         $(this).addClass('disabled');
        //         showLoader();
        //         //disable button

        //         var $this = $(this);

        //         $.ajax({
        //             url: $this.attr('action'),
        //             method: 'POST',
        //             data: $this.serialize()
        //         }).done(function (response) {
        //             $this.removeClass('disabled');
        //             hideLoader();
        //             if (response.success) {
        //                 $this[0].reset();
        //                 $('#consultationModal').modal('hide');
        //                 window.dataLayer = window.dataLayer || [];
        //                 window.dataLayer.push({ 'event': 'ConsultationFormSubmitted' });
        //                 return swal("Merci!", "On vous contactera dés que possible", "success");
        //             }
        //             return sweetAlert({ title: 'Oups!', text: response.message, type: 'error', html: true });
        //         });
        //     }
        // });
    }

    $('.branch-btn').hover(function (event) {
        event.preventDefault();

        $('.branch-btn').parent('li').removeClass('active');
        $(this).parent('li').addClass('active');

        $('.treatments-list').stop().slideUp(100);
        $(this).parent().find('.treatments-list').stop().delay(100).slideDown(300);
    });

})(jQuery);
