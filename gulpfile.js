var elixir = require('laravel-elixir');
require('./elixir-extensions');
require('./gulpfile-admin');

elixir(function(mix) {

    mix
        .sass('main.scss')
        .babel([
            'main.js'
        ], 'public/js/main.js')

        /*******************     scripts front End start here          ******************/
        .scripts([
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        ], 'public/js/vendor-front.js', 'bower_components')
        .scripts([
            // bower:js
            'bower_components/sweetalert/dist/sweetalert.min.js',
            'bower_components/jquery-colorbox/jquery.colorbox.js',
            // endbower
            'bower_components/plyr/dist/plyr.js',
            'bower_components/intl-tel-input/build/js/intlTelInput.js',
            'bower_components/intl-tel-input/build/js/utils.js',
            'bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
            'bower_components/autosize/dist/autosize.min.js'
        ], 'public/js/plugins-front.js', 'bower_components')
        /*******************     styles front End start here          ******************/
        .styles([
            // bower:css
            'bower_components/sweetalert/dist/sweetalert.css',
            // endbower
            'bower_components/plyr/dist/plyr.css',
            'bower_components/intl-tel-input/build/css/intlTelInput.css',
            'bower_components/bootstrap-select/dist/css/bootstrap-select.css'
        ], 'public/css/plugins-front.css', 'bower_components')
        .copy('bower_components/gentelella/vendors/bootstrap/fonts/', 'public/fonts')
        .copy('bower_components/gentelella/vendors/font-awesome/fonts/', 'public/fonts')
        .copy('bower_components/modernizer/modernizr.js', 'public/js')
        .images()
        .wiredep()
        .minifyCss()
        .compress()
        .browserSync({
            proxy: process.env.APP_URL
        });

    //.pluginsJS(['fastclick','validator','icheck','chartjs','parsley','sweetalert2','scrollbar'],'plugins-admin')
    //.pluginsJS(['bootstrap','gentelella'],'vendor-front')
    //.pluginsJS(['gentelella','datatable'],'vendor-admin')
    //.pluginsCSS(['sweetalert2'],'plugins-admin')
    //.pluginsCSS(['bootstrap','scrollbar','gentelella','fontawesome','datatable'],'vendor-admin')



});
