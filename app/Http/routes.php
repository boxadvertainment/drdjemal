<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'auth'
], function()
{
    Route::get('/', 'AdminController@index');
    Route::get('info', 'AdminController@info');
    Route::get('consultation', ['uses' => 'AdminController@index', 'as' => 'admin.dashboard']);
    Route::get('consultation/{id}', 'AdminController@consultationEdit');
    Route::post('consultation/{id}', 'AdminController@submitConsultationEdit');
    Route::get('consultation/changeStatus/{id}', 'AdminController@consultationChangeStatus');
    Route::post('createPatient', 'AdminController@consultationCreatePatient');
    Route::post('consultation/addQualification', 'AdminController@addQualification');
    Route::get('consultation/pass-morena/{id}/{value}', 'AdminController@passToMorena');
    Route::get('getPhoto/{filename}', ['uses' => 'AdminController@getPhoto', 'as' => 'getPhoto']);
    Route::post('submitConsultation', 'AdminController@submitConsultation');
    Route::post('deleteConsultation', 'AdminController@deleteConsultation');
});

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');



Route::get('/', 'AppController@home');
Route::post('submitConsultation', ['middleware' => 'cors' , 'uses'=> 'AppController@submitConsultation']);
Route::post('/contact', 'AppController@contact');
Route::get('/{page}', 'AppController@page');
Route::get('/{category}/{page}', 'AppController@subPage');
