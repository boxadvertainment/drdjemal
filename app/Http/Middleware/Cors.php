<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $response = $next($request);
        
        $headers = [
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, OPTIONS, DELETE',
            'Access-Control-Max-Age' => 3600,
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'X-Requested-With, Origin, X-CSRF-TOKEN, Content-Type, Accept',
        ];
        return $response->withHeaders($headers);
        
    }
}
