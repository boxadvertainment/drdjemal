<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Consultation;
use Carbon\Carbon;

class AdminController extends Controller{

    public function index(Request $request)
    {

        $status = [
            'pending' => ['EN ATTENTE', 'default'],
            'devis' => ['ENVOYÉ DEVIS', 'info'],
            'confirmed' => ['CONFIRME', 'success'],
            'called' => ['TÉLÉPHONÉ', 'warning'],
            'contacted' => ['CONTACTE', 'primary'],
            'relance1' => ['RELANCE 1', 'danger'],
            'relance2' => ['RELANCE 2', 'danger'],
        ];

        if (Auth::user()->role == 'user') {
            $consultations = Consultation::where('passion_id', Auth::user()->id);
        } else {
            $consultations = Consultation::where('status', '<>', '');
        }

        if (Auth::user()->email == 'contact@dr-djemal.com') {
            $consultations = Consultation::where('hide_f_m', 1);
        }

        if (Auth::user()->email == 'info@dr-djemal.com') {
            $consultations = Consultation::where('hide_f_m', 2);
        }

        if ($month = $request['month']) {
            $consultations->whereMonth('created_at', '=', date_parse($month)['month']);
        }

        if ($year = $request['year']) {
            $consultations->whereYear('created_at', '=', $year);
        }

        if ($from = $request['from']) {
            $consultations->where('from', 1);
        }

        $consultations = $consultations->orderBy('created_at', 'DESC')->paginate(50);

        $archivesMonth = Consultation::selectRaw('monthname(created_at) month, year(created_at) year')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get();

        $archivesYear = Consultation::selectRaw('year(created_at) year')
            ->groupBy('year')
            ->orderByRaw('min(created_at) desc')
            ->get();


        return view('admin.consultationList')->with([
            'status' => $status,
            'archivesYear' => $archivesYear,
            'archivesMonth' => $archivesMonth,
            'consultations' => $consultations 
        ]);
    }

    public function consultationChangeStatus(Request $request, $id)
    {
        if (Auth::user()->role == 'user') {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $consultation = Consultation::find($id);
        $consultation->status = $request->input('status');

        $date_status = $consultation->date_status;
        $date_status[$consultation->status] = date("Y/m/d");
        $consultation->date_status = $date_status;


        if ( $consultation->save() ) {
            return response()->json(['success' => true ]);
        }
        return response()->json(['success' => false ]);
    }

    public function addQualification(Request $request)
    {
        if (Auth::user()->role == 'user' || Auth::user()->email != 'fouis@box.agency') {
            return response()->json(['success' => false ]);
        }
        
        $consultation = Consultation::find($request->input('id'));
        $consultation->qualification = $request->input('value');

        if ( $consultation->save() ) {
            return response()->json(['success' => true ]);
        }
        return response()->json(['success' => false ]);
    }

    public function passToMorena($id, $value)
    {
        if (Auth::user()->role == 'user' || Auth::user()->email != 'fouis@box.agency') {
            response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
        }

        $consultation = Consultation::find($id);
        $consultation->hide_f_m = (int)$value;
        if ($consultation->save()) {
            return response()->json(['success' => true]);
        }

        response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
    }

    public function consultationCreatePatient(Request $request)
    {
        if (Auth::user()->role == 'user') {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $consultation = Consultation::find($request->id);
        $passion = User::where('email', $consultation->email)->orWhere('name', $consultation->name)->first();
        if (!$passion) {
            $passion = new User;
            $passion->email = $consultation->email;
            $passion->name  = $consultation->name;
            $passion->role = 'user';
            $passion->_password = str_random(6);
            $passion->password = bcrypt($passion->_password);
            $passion->remember_token = str_random(10);

            if (!$passion->save() ) {
                return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
            }
        }

        $consultation->passion()->associate($passion);
        if ($consultation->save()) {
            return response()->json(['success' => true, 'email' => $passion->email, 'password' => $passion->_password]);
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function consultationEdit($id)
    {
        $consultation = Consultation::find($id);
        if (Auth::user()->role == 'user' && $consultation->passion_id !=  Auth::user()->id) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $dossier = unserialize($consultation->dossier);

        return view('admin.consultationEdit', compact('consultation', 'dossier'));
    }

    public function info()
    {
        return view('admin.info');
    }

    public function submitConsultationEdit(Request $request, $id)
    {
        $consultation = Consultation::find($id);
        if (Auth::user()->role == 'user' && $consultation->passion_id !=  Auth::user()->id) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $except[] = '_token';
        for($i=1; $i<10; $i++) {
            $except[] = 'image' . $i;
            $except[] = 'imagePO' . $i;
            $except[] = 'imageRef' . $i;
            $rules['image' . $i] = 'max:8000|image';
            $rules['imagePO' . $i] = 'max:8000|image';
            $rules['imageRef' . $i] = 'max:8000|image';
        }
        $data = $request->except($except);

        $validator = \Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }


        $consultation->name = $request->input('name');
        $consultation->email = $request->input('email');
        $consultation->phone = $request->input('phone');
        $consultation->intervention = $request->input('intervention');
        $consultation->message = $request->input('reason');

        $directory = 'photos';
        //$path      = storage_path('app/' . $directory);
        for($i=1; $i<10; $i++) {
            if ($request->hasFile('image' . $i))
            {
                $file = $request->file('image' . $i);
                // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
                $filename  = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
                $fileContents = \File::get($file);

                \Storage::put($directory . '/' . $filename,  $fileContents);
                $data['image'. $i] = $filename;
            } else {
                $data['image'. $i] = $request->input('oldImage' . $i);
            }

            if ($request->hasFile('imagePO' . $i))
            {
                $file = $request->file('imagePO' . $i);
                // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
                $filename  = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
                $fileContents = \File::get($file);

                \Storage::put($directory . '/' . $filename,  $fileContents);
                $data['imagePO'. $i] = $filename;
            } else {
                $data['imagePO'. $i] = $request->input('oldImagePO' . $i);
            }

            if ($request->hasFile('imageRef' . $i))
            {
                $file = $request->file('imageRef' . $i);
                // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
                $filename  = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
                $fileContents = \File::get($file);

                \Storage::put($directory . '/' . $filename,  $fileContents);
                $data['imageRef'. $i] = $filename;
            } else {
                $data['imageRef'. $i] = $request->input('oldImageRef' . $i);
            }
        }

        if ($request->hasFile('planning'))
        {
            $file = $request->file('planning');
            // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
            $filename  = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
            $fileContents = \File::get($file);

            \Storage::put($directory . '/' . $filename,  $fileContents);
            $data['planning'] = $filename;
        } else {
            $data['planning'] = $request->input('oldPlanning');
        }

        $consultation->dossier = serialize($data);

        if ($consultation->save()) {
            if (Auth::user()->role == 'user') {
                \Mail::send(['raw' => 'Le patient "'. $consultation->name .'" a apporté des modifications à son dossier'], [], function($message)
                {
                    $message->from(\Config::get('app.email'), \Config::get('app.name'));
                    $message->to(\Config::get('app.email'), \Config::get('app.name'))->subject(\Config::get('app.name') . ' - Mise à jour du dossier d\'un patient');
                });
            }
            $dossier = unserialize($consultation->dossier);
            return view('admin.consultationEdit', compact('consultation', 'dossier'));
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function getPhoto(Request $request, $filename)
    {
        $path = 'photos/' . ($request->input('size') ? $request->input('size') . '/' : '') . $filename;
        if (! \Storage::exists($path)) {
            abort(404);
        }

        return response(\Storage::get($path))->header('Content-Type', \Storage::mimeType($path));
    }

    public function submitConsultation(Request $request)
    {
        
        $validator = \Validator::make( $request->all(), [
            'name' => 'required',
            'email' => 'email',
            'message' => 'required'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom et prénom',
            'email' => 'Email',
            'phone' => 'Téléphone',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all()) ]);
        }

        $consultation = new Consultation;

        $consultation->name         = $request->input('name');
        $consultation->email        = $request->input('email');
        $consultation->phone        = $request->input('phone');
        $consultation->intervention = $request->input('intervention');
        $consultation->message      = $request->input('message');
        $consultation->ip           = $request->ip();

        if ( $consultation->save() ) {
            \Mail::send(['raw' => 'Une nouvelle demande de consultation a été créé par "' . $consultation->name . '"'],[], function($message)
            {
                $message->from(\Config::get('app.email'), \Config::get('app.name'));
                $message->to(\Config::get('app.email'), \Config::get('app.name'))->subject(\Config::get('app.name') . ' - Consultation Form');
            });
            return response()->json(['success' => true ]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function deleteConsultation(Request $request)
    {
        if (!$request->ids) {
            return response()->json(['success' => false, 'message' => 'Veuillez verifier votre liste svp.']);
        }

        try {
            foreach ($request->ids as $key => $id) {
                $consultation = Consultation::find($id);
                $consultation->delete();
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
        } finally {
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
    }

}
