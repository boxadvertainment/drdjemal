<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Consultation;


class AppController extends Controller {

    public function home()
    {
        //return view('fr.acceuil', ['page' => 'accueil']);
        
        $location = \GeoIP::getLocation( \Request::ip() );

        if(config('app.env') === 'local')
            return view('fr.acceuil', ['page' => 'accueil']);

        if( $location['iso_code'] !== "TN")
            return view('fr.acceuil', ['page' => 'accueil']);
        else
            return view('tn.accueil', ['page' => 'accueil']);
        
    }

    public function contact(Request $request)
    {
        $rules = array(
            'name'        => 'required',
            'email'     => 'required|email',
            'subject'        => 'required',
            'message'     => 'required',
        );

        $validator = \Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return redirect()->url('contact')->withInput()->withErrors($validator);
        }

        \Mail::send('emails.contact', [
            'msg'     => $request->input('message'),
            'name'    => $request->input('name'),
            'email'   => $request->input('email'),
            'subject' => $request->input('subject')
        ], function($message) use($request)
        {
            $message->from($request->input('email'), $request->input('name'));
            $message->to(\Config::get('app.email'), \Config::get('app.name'))->subject(\Config::get('app.name') . ' - Contact Form');
        });

        return redirect('contact?success=true')->with('success', true);
    }

    public function submitConsultation(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'email' => 'email',
            'name' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ], [
            'email' => 'Email Invalide.',
            'required' => 'Le champ :attribute est obligatoire.',
            'g-recaptcha-response.recaptcha' => 'La vérification du captcha a échoué',
            'g-recaptcha-response.required' => 'Veuillez compléter le captcha'
        ]);

        $validator->setAttributeNames([
            'email' => 'Email',
            'phone' => 'Téléphone',
            'name' => 'Nom et prénom',
            'g-recaptcha-response' => 'Google Captcha',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all()) ]);
        }

        $consultation = new Consultation;
        $location = \GeoIP::getLocation( $request->ip() );

        $consultation->name         = $request->input('name');
        $consultation->email        = $request->input('email');
        $consultation->phone        = $request->input('phone');
        $consultation->country      = $location['iso_code'] ? $location['iso_code'] : 'TN';
        $consultation->intervention = $request->input('intervention');
        $consultation->message      = $request->input('message');
        $consultation->ip           = $request->ip();
        $consultation->hide_f_m     = false;
        
        if ($request->input('from')) {
            $consultation->from     = (bool)$request->input('from');
        }

        if ( $consultation->save() ) {
            \Mail::send(['raw' => 'Une nouvelle demande de consultation a été créé par "' . $consultation->name . '"'],[], function($message)
            {
                $message->from(\Config::get('app.email'), \Config::get('app.name'));
                $message->to(\Config::get('app.email'), \Config::get('app.name'))->subject(\Config::get('app.name') . ' - Consultation Form');
            });
            return response()->json(['success' => true, 'message' => 'Merci pour votre demande. Vous serez contacté dans les plus brefs délais.' ]);
        }
        return response(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.' ]);
    }

    public function page($page)
    {

        $location = \GeoIP::getLocation( \Request::ip() );

        if( $location['iso_code'] !== "TN"){
            if (view()->exists('fr.'.$page)) {
                return view('fr.'.$page, ['page' => 'fr.'.$page]);
            }
            if (view()->exists('en.'.$page)) {
                return view('en.'.$page, ['page' => 'en.'.$page]);
            }
        } else {
            if (view()->exists('tn.'.$page)) {
                return view('tn.'.$page, ['page' => 'tn.'.$page]);
            }
        }

        abort(404);
    }

    public function subPage( $category, $page )
    {

        $location = \GeoIP::getLocation( \Request::ip() );
        if( $location['iso_code'] !== "TN") {
            if (view()->exists('fr.' . $category . '.' . $page)) {
                return view('fr.' . $category . '.' . $page, ['page' => 'en.' . $category . '.' . $page]);
            }
            if (view()->exists('en.' . $category . '.' . $page)) {
                return view('en.' . $category . '.' . $page, ['page' => 'en.' . $category . '.' . $page]);
            }
        }else{
            if (view()->exists('tn.'.$category.'.'.$page)) {
                return view('tn.'.$category.'.'.$page, ['page' => 'tn.'.$category.'.'.$page]);
            }
        }
        abort(404);
    }

}
